/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2DEFAULTDRAWPOINT_H
#define TH2DEFAULTDRAWPOINT_H

#include "graphics/th2drawpoint.h"

#include <QBrush>
#include <QPen>

class TH2DefaultDrawPoint : public TH2DrawPoint
{
protected:
    qreal m_width = 4;
    qreal m_height = 4;

public:
    TH2DefaultDrawPoint(const QPointF& aPoint);

    // TH2DrawPoint interface
    QGraphicsItem *drawPoint() override;
};

#endif // TH2DEFAULTDRAWPOINT_H
