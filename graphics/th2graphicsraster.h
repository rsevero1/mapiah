/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2GRAPHICSRASTER_H
#define TH2GRAPHICSRASTER_H

#include "th2graphicsimage.h"

#include <QImage>

class TH2GraphicsRaster : public TH2GraphicsImage
{
protected:
    static constexpr qreal DEFAULT_GAMMA = 1;

    QImage *m_image = nullptr;

    qreal m_gamma;

    void applyGamma();
    qreal apply2_2Gamma(qreal c);

public:
    TH2GraphicsRaster(
        const QString &aFilename,
        qreal aX,
        qreal aY,
        TH2Element *aImageElement,
        QGraphicsItem *parent = nullptr);

    // TH2GraphicsImage interface
    void dataChanged() override;
    void clearItems() override;

    qreal gamma() const;
    void setGamma(qreal newGamma);
};

#endif // TH2GRAPHICSRASTER_H
