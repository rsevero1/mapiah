/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2GRAPHICSIMAGE_H
#define TH2GRAPHICSIMAGE_H

#include "../data/th2/th2element.h"

#include <QGraphicsItemGroup>
#include <QString>

class TH2GraphicsBuilder;

class TH2GraphicsImage : public QGraphicsItemGroup
{
protected:
    QString m_filename;
    qreal m_x;
    qreal m_y;
    TH2Element *m_imageElement;

public:
    TH2GraphicsImage(
        const QString &aFilename,
        qreal aX,
        qreal aY,
        TH2Element *aImageElement,
        QGraphicsItem *parent = nullptr);

    // Always make base class destructor as virtual in C++
    // https://codeyarns.com/tech/2016-12-28-always-make-base-class-destructor-as-virtual-in-c.html
    virtual ~TH2GraphicsImage();

    virtual void dataChanged() = 0;
    virtual void clearItems() = 0;

    const QString &filename() const;
    void setFilename(const QString &newFilename);
    qreal x() const;
    void setX(qreal newX);
    qreal y() const;
    void setY(qreal newY);
    TH2Element *imageElement() const;
};

#endif // TH2GRAPHICSIMAGE_H
