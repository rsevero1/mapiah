/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2graphicsprimitives.h"

namespace TH2GraphicsPrimitives
{
    QGraphicsEllipseItem *circle(
            const qreal x,
            const qreal y,
            const qreal width,
            const qreal height,
            const QPen &pen,
            const QBrush &brush
            )
    {
        QGraphicsEllipseItem *newCircle = new QGraphicsEllipseItem(
                    (x - (width / 2)), (y - (height / 2)),
                    width, height
                    );

        newCircle->setPen(pen);
        newCircle->setBrush(brush);

        return newCircle;
    }

    QGraphicsEllipseItem *circle(
            const QPointF &xy,
            const qreal width,
            const qreal height,
            const QPen &pen,
            const QBrush &brush
            )
    {
        return circle(
                    xy.x(),
                    xy.y(),
                    width,
                    height,
                    pen,
                    brush
                    );
    }

    QGraphicsPolygonItem *triangle(
            const qreal x_center,
            const qreal y_center,
            const qreal length,
            const QPen& pen,
            const QBrush& brush
            )
    {
        QList<QPointF> pointList;
        qreal x_corner, y_corner, height;

        /**
         * Triangule height formula: h = √3 / 2 * a
         * Taken from https://rechneronline.de/pi/equilateral-triangle.php
         *
         * √3 / 2 = 0.8660254
         */
        height = 0.8660254 * length;

        // Lower left corner
        x_corner = x_center - (length / 2);
        y_corner = y_center - (height / 2);
        pointList.append(QPointF(x_corner, y_corner));

        // Lower right corner
        x_corner = x_center + (length / 2);
        pointList.append(QPointF(x_corner, y_corner));

        // Top corner
        x_corner = x_center;
        y_corner = y_center + (height / 2);
        pointList.append(QPointF(x_corner, y_corner));

        QGraphicsPolygonItem *newTriangle = new QGraphicsPolygonItem(QPolygonF(pointList));

        newTriangle->setPen(pen);
        newTriangle->setBrush(brush);

        return newTriangle;
    }

    QGraphicsPolygonItem *triangle(
            const QPointF &xy,
            const qreal length,
            const QPen& pen,
            const QBrush& brush
            )
    {
        return triangle(
                    xy.x(),
                    xy.y(),
                    length,
                    pen,
                    brush
                    );
    }

    QGraphicsLineItem *straightLine(
            const qreal x_from,
            const qreal y_from,
            const qreal x_to,
            const qreal y_to,
            const QPen& pen
            )
    {
        QGraphicsLineItem *newLine = new QGraphicsLineItem(
                    x_from, y_from,
                    x_to, y_to
                    );

        newLine->setPen(pen);

        return newLine;
    }

    QGraphicsLineItem *straightLine(
            const QPointF& from,
            const QPointF& to,
            const QPen& pen
            )
    {
        return straightLine(
                    from.x(),
                    from.y(),
                    to.x(),
                    to.y(),
                    pen
                    );
    }

    QGraphicsItemGroup *multiStraightLine(
            QList<QPointF> *pointList,
            const QPen& pen
            )
    {
        QGraphicsItemGroup *newMultiLine = new QGraphicsItemGroup();

        if (pointList->count() < 2)
        {
            return newMultiLine;
        }

        qint16 pointCount = pointList->count();
        QGraphicsLineItem *newLine;

        for (qint16 i = 1; i < pointCount; ++i)
        {
            newLine = straightLine(pointList->at(i - 1), pointList->at(i), pen);
            newMultiLine->addToGroup(newLine);
        }

        return newMultiLine;
    }
};
