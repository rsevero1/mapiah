/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2GRAPHICSLINE_H
#define TH2GRAPHICSLINE_H

#include "../data/th2/th2line.h"

#include <QGraphicsItem>
#include <QGraphicsItemGroup>
#include <QRectF>

class TH2GraphicsLine : public QGraphicsItemGroup
{
    TH2Line *m_th2line;

    QGraphicsItemGroup *m_pointsGroup, *m_linesGroup;

    QPointF m_lastXY;

public:
    TH2GraphicsLine(TH2Line *aTH2Line, QGraphicsItem *parent = nullptr);
    ~TH2GraphicsLine();

    TH2Line *th2line() const;

    void dataChanged();
    void clearItems();
};

#endif // TH2GRAPHICSLINE_H
