/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2bezierdrawline.h"

TH2BezierDrawLine::TH2BezierDrawLine(const QPointF& from, TH2LinePointBezier* aLinePointBezier)
    : TH2DrawLine(from), m_linePointBezier(aLinePointBezier)
{

}

QGraphicsItem *TH2BezierDrawLine::drawLine()
{
    QPainterPath newPath = QPainterPath(m_from);
    newPath.cubicTo(
                m_linePointBezier->c1(),
                m_linePointBezier->c2(),
                m_linePointBezier->xy()
                );

    QGraphicsPathItem *newLine = new QGraphicsPathItem(newPath);

    newLine->setPen(m_pen);

    return newLine;
}
