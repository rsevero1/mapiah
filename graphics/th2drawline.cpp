/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2drawline.h"

#include "../data/th2/th2linepointbezier.h"
#include "../data/th2/th2linepointstraight.h"

void TH2DrawLine::setPen(const QPen &newPen)
{
    m_pen = newPen;
}

void TH2DrawLine::setFrom(QPointF newFrom)
{
    m_from = newFrom;
}

TH2DrawLine::TH2DrawLine(const QPointF& from)
    : m_from(from)
{
    QPen linePen = QPen("orange");
    linePen.setWidth(2);
    linePen.setCapStyle(Qt::RoundCap);
    m_pen = linePen;
}
