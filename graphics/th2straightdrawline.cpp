/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2straightdrawline.h"

void TH2StraightDrawLine::setLinePointStraight(const TH2LinePointStraight *newLinePointStraight)
{
    m_linePointStraight = newLinePointStraight;
}

TH2StraightDrawLine::TH2StraightDrawLine(const QPointF& from, const TH2LinePointStraight* aLinePointStraight)
    : TH2DrawLine(from), m_linePointStraight(aLinePointStraight)
{

}

QGraphicsItem *TH2StraightDrawLine::drawLine()
{
    QGraphicsLineItem *newLine = new QGraphicsLineItem(
                m_from.x(), m_from.y(),
                m_linePointStraight->xy().x(), m_linePointStraight->xy().y()
                );

    newLine->setPen(m_pen);

    return newLine;
}
