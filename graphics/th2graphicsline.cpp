/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2graphicsline.h"

#include "th2defaultdrawpoint.h"
#include "th2defaultlinepointdrawpoint.h"
#include "th2drawline.h"

#include "../data/th2/th2linepoint.h"
#include "../data/th2/th2linepointbezier.h"
#include "../data/th2/th2linepointstraight.h"

#include "../factories/th2factory.h"

#include <QBrush>
#include <QColor>
#include <QGraphicsLineItem>
#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QPen>

TH2GraphicsLine::TH2GraphicsLine(TH2Line *aTH2Line, QGraphicsItem *parent): QGraphicsItemGroup(parent),
    m_th2line(aTH2Line)
{
    m_pointsGroup = new QGraphicsItemGroup();
    addToGroup(m_pointsGroup);

    m_linesGroup = new QGraphicsItemGroup();
    addToGroup(m_linesGroup);

    dataChanged();
}

TH2GraphicsLine::~TH2GraphicsLine()
{
    clearItems();

    delete m_pointsGroup;
    delete m_linesGroup;
}

TH2Line *TH2GraphicsLine::th2line() const
{
    return m_th2line;
}

void TH2GraphicsLine::dataChanged()
{
    clearItems();

    QList<TH2LinePoint *> linePointsList = m_th2line->linePoints();

    TH2DefaultLinePointDrawPoint *aDrawPoint = dynamic_cast<TH2DefaultLinePointDrawPoint*>(TH2Factory::createDrawPoint(
                QPointF(0, 0), "linepoint"
                ));

    bool firstPoint = true;
    foreach(TH2LinePoint *aLinePoint, linePointsList)
    {
        aDrawPoint->setXY(aLinePoint->xy());
        m_pointsGroup->addToGroup(aDrawPoint->drawPoint());
        if (firstPoint)
        {
            firstPoint = false;
        }
        else
        {
            m_linesGroup->addToGroup(TH2Factory::createDrawLine(m_lastXY, aLinePoint)->drawLine());
        }
        m_lastXY = aLinePoint->xy();
    }
}

void TH2GraphicsLine::clearItems()
{
    QList<QGraphicsItem *> groupChildren = m_pointsGroup->childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();

    groupChildren = m_linesGroup->childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();
}
