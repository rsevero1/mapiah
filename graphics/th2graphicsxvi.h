/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2GRAPHICSXVI_H
#define TH2GRAPHICSXVI_H

#include "th2graphicsimage.h"

#include "../data/xvi/xvi.h"

#include <QBrush>
#include <QPen>

class TH2GraphicsXVI : public TH2GraphicsImage
{
    constexpr static qreal M_SHOT_PEN_WIDTH = 2;
    constexpr static qreal M_SKETCHLINE_PEN_WIDTH = 4;
    constexpr static qreal M_FINALGRID_PEN_WIDTH = 1;
    constexpr static qreal M_STATION_TRIANGLE_LENGTH = 11;

    XVI::File *m_xvifile;

    QBrush m_stationBrush;

    QPen m_shotPen;
    QPen m_sketchLinePen;
    QPen m_finalGridPen;

    QGraphicsItemGroup *m_initialGridGroup;
    QGraphicsItemGroup *m_stationsGroup;
    QGraphicsItemGroup *m_imagesGroup;
    QGraphicsItemGroup *m_shotsGroup;
    QGraphicsItemGroup *m_sketchlinesGroup;
    QGraphicsItemGroup *m_finalGridGroup;

    void drawStation(const XVI::Station &aXVIStation);
    void drawShot(const XVI::Shot &aXVIShot);
    void drawSketchLine(const XVI::SketchLine &aXVISketchLine);
    void drawFinalGrid();

    bool parseFile();

public:
    TH2GraphicsXVI(
        const QString &aFilename,
        qreal aX,
        qreal aY,
        TH2Element *aImageElement,
        QGraphicsItem *parent = nullptr);
    ~TH2GraphicsXVI() override;

    XVI::File *xvifile() const;

    void clearItems() override;
    void dataChanged() override;
};

#endif // TH2GRAPHICSXVI_H
