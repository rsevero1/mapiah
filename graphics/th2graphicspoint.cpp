/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2graphicspoint.h"

#include "factories/th2factory.h"

#include <QBrush>
#include <QGraphicsEllipseItem>
#include <QPen>

TH2Point *TH2GraphicsPoint::th2point() const
{
    return m_th2point;
}

void TH2GraphicsPoint::clearItems()
{
    QList<QGraphicsItem *> groupChildren = m_pointsGroup->childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();
}

TH2GraphicsPoint::TH2GraphicsPoint(TH2Point *aTH2Point, QGraphicsItem *parent):
    QGraphicsItemGroup(parent), m_th2point(aTH2Point)
{
    m_pointsGroup = new QGraphicsItemGroup();
    addToGroup(m_pointsGroup);

    dataChanged();
}

TH2GraphicsPoint::~TH2GraphicsPoint()
{
    clearItems();

    delete m_pointsGroup;
}

void TH2GraphicsPoint::dataChanged()
{
    clearItems();

    m_pointsGroup->addToGroup(TH2Factory::createDrawPoint(m_th2point->xy(), m_th2point->type())->drawPoint());
}
