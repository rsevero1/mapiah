/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2GRAPHICSPOINT_H
#define TH2GRAPHICSPOINT_H

#include "th2drawpoint.h"
#include "../data/th2/th2point.h"

#include <QGraphicsItemGroup>

class TH2GraphicsPoint : public QGraphicsItemGroup
{
    TH2Point *m_th2point;

    QGraphicsItemGroup *m_pointsGroup;

public:
    TH2GraphicsPoint(TH2Point *aTH2Point, QGraphicsItem *parent = nullptr);
    ~TH2GraphicsPoint();

    TH2Point *th2point() const;

    void clearItems();
    void dataChanged();

};

#endif // TH2GRAPHICSPOINT_H
