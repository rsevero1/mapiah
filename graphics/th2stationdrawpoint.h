/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2STATIONDRAWPOINT_H
#define TH2STATIONDRAWPOINT_H

#include "th2drawpoint.h"

class TH2StationDrawPoint : public TH2DrawPoint
{
    const qreal M_DEFAULT_LENGTH = 11;

    qreal m_length;

public:
    TH2StationDrawPoint(const QPointF& aPoint);

    // TH2DrawPoint interface
    QGraphicsItem *drawPoint() override;

    qreal length() const;
    void setLength(qreal newLength);
};

#endif // TH2STATIONDRAWPOINT_H
