/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2DRAWLINE_H
#define TH2DRAWLINE_H

#include "../data/th2/th2linepoint.h"

#include <QBrush>
#include <QGraphicsItemGroup>
#include <QPen>
#include <QPointF>

class TH2DrawLine
{
protected:
    QPen m_pen;

    QPointF m_from;

public:
    TH2DrawLine(const QPointF& from);

    virtual QGraphicsItem *drawLine() = 0;
    void setPen(const QPen &newPen);
    void setFrom(QPointF newFrom);
};

#endif // TH2DRAWLINE_H
