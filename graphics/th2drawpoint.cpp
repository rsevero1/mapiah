﻿/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2drawpoint.h"

#include <QGraphicsEllipseItem>

QPointF TH2DrawPoint::xy() const
{
    return m_xy;
}

void TH2DrawPoint::setXY(QPointF newXY)
{
    m_xy = newXY;
}

const QPen &TH2DrawPoint::pen() const
{
    return m_pen;
}

void TH2DrawPoint::setPen(const QPen &newPen)
{
    m_pen = newPen;
}

const QBrush &TH2DrawPoint::brush() const
{
    return m_brush;
}

void TH2DrawPoint::setBrush(const QBrush &newBrush)
{
    m_brush = newBrush;
}

TH2DrawPoint::TH2DrawPoint(const QPointF &aPoint): m_xy(aPoint)
{

}
