/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2STRAIGHTDRAWLINE_H
#define TH2STRAIGHTDRAWLINE_H

#include "th2drawline.h"
#include "../data/th2/th2linepointstraight.h"

class TH2StraightDrawLine : public TH2DrawLine
{
    const TH2LinePointStraight* m_linePointStraight;

public:
    TH2StraightDrawLine(const QPointF& from, const TH2LinePointStraight* aLinePointStraight);

    // TH2DrawLine interface
    QGraphicsItem *drawLine() override;

    void setLinePointStraight(const TH2LinePointStraight *newLinePointStraight);
};

#endif // TH2STRAIGHTDRAWLINE_H
