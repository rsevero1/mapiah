/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2stationdrawpoint.h"

#include "graphics/th2graphicsprimitives.h"

#include <QPolygonF>

qreal TH2StationDrawPoint::length() const
{
    return m_length;
}

void TH2StationDrawPoint::setLength(qreal newLength)
{
    m_length = newLength;
}

TH2StationDrawPoint::TH2StationDrawPoint(const QPointF& aPoint)
    : TH2DrawPoint(aPoint), m_length(M_DEFAULT_LENGTH)
{
    m_pen = QPen(Qt::NoPen);
    m_brush = QBrush(QColor("lime"));
}

QGraphicsItem *TH2StationDrawPoint::drawPoint()
{
    return TH2GraphicsPrimitives::triangle(
                m_xy,
                m_length,
                m_pen,
                m_brush
                );
}
