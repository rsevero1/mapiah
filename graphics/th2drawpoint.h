/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2DRAWPOINT_H
#define TH2DRAWPOINT_H

#include <QBrush>
#include <QGraphicsItem>
#include <QPen>

class TH2DrawPoint
{
protected:
    QPen m_pen;
    QBrush m_brush;

    QPointF m_xy;

public:
    TH2DrawPoint(const QPointF &aPoint);

    virtual QGraphicsItem *drawPoint() = 0;

    QPointF xy() const;
    void setXY(QPointF newXY);
    const QPen &pen() const;
    void setPen(const QPen &newPen);
    const QBrush &brush() const;
    void setBrush(const QBrush &newBrush);
};

#endif // TH2DRAWPOINT_H
