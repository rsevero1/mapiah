/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2graphicsraster.h"

#include "../data/th2/th2file.h"

#include <QDir>
#include <QFileInfo>
#include <QPixmap>

TH2GraphicsRaster::TH2GraphicsRaster(
    const QString &aFilename,
    qreal aX,
    qreal aY,
    TH2Element *aImageElement,
    QGraphicsItem *parent)
    : TH2GraphicsImage(aFilename, aX, aY, aImageElement, parent)
    , m_gamma(DEFAULT_GAMMA)
{

}

void TH2GraphicsRaster::dataChanged()
{
    clearItems();

    /**
     *  Making sure the image filename starts with an absolute path, either it's own (if it
     *  is a absolute path) or with the TH2 file path.
     */
    QDir imageFilename(m_filename);
    if (!imageFilename.isAbsolute())
    {
        QDir th2Filename = QFileInfo(m_imageElement->th2File()->filename()).canonicalPath();
        imageFilename.setPath(th2Filename.canonicalPath() + "/" + imageFilename.path());
    }

    m_image = new QImage(imageFilename.path());
    if (m_image->format() == QImage::Format_Invalid)
    {
        QString message = "Can't load image '%1'. Tried to load '%2'";
        qInfo() << message.arg(m_filename, imageFilename.path());
        return;
    }

    applyGamma();

    QPixmap newPixmap = QPixmap::fromImage(*m_image);
    QGraphicsPixmapItem *newPixmapItem = new QGraphicsPixmapItem(newPixmap);

    newPixmapItem->setPos(m_x, m_y);

    // Fixing image orientation as positive Y is up for Therion but down for QT.
    QTransform aTransform = newPixmapItem->transform();
    aTransform.setMatrix(
        aTransform.m11(),
        aTransform.m12(),
        aTransform.m13(),
        aTransform.m21(),
        -aTransform.m22(),
        aTransform.m23(),
        aTransform.m31(),
        aTransform.m32(),
        aTransform.m33());
    newPixmapItem->setTransform(aTransform);

    addToGroup(newPixmapItem);
}

void TH2GraphicsRaster::clearItems()
{
    if (m_image != nullptr)
    {
        delete(m_image);
    }

    QList<QGraphicsItem *> groupChildren = childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();
}

qreal TH2GraphicsRaster::gamma() const
{
    return m_gamma;
}

void TH2GraphicsRaster::setGamma(qreal newGamma)
{
    m_gamma = newGamma;
}

void TH2GraphicsRaster::applyGamma()
{
    /**
     *  When "qFussyComparing", add 1 to both sides as qFussyCompare has troubles with
     *  any zero value.
     */
    // Is gamma == 1.0?
    if (qFuzzyCompare(m_gamma + 1, 2.0))
    {
        return;
    }
    // Is gamma == 2.2?
    else if (qFuzzyCompare(m_gamma + 1, 3.2))
    {
        for (int x = 0; x < m_image->width(); ++x) {
            for (int y = 0 ; y < m_image->height(); ++y) {
                const QRgb rgb = m_image->pixel(x, y);
                const double r = qRed(rgb) / 255.0;
                const double g = qGreen(rgb) / 255.0;
                const double b = qBlue(rgb) / 255.0;
                m_image->setPixelColor(x, y,
                   QColor(
                       255.0 * apply2_2Gamma(r),
                       255.0 * apply2_2Gamma(g),
                       255.0 * apply2_2Gamma(b)));
            }
        }
    }
    /**
     *  For all other cases (which are probably abberations as per
     *  https://youtu.be/_zQ_uBAHA4A
     *  Guy Davidson - Everything you know about colour is wrong - Meeting C++ online
     */
    else
    {
        for (int x = 0; x < m_image->width(); ++x) {
            for (int y = 0 ; y < m_image->height(); ++y) {
                const QRgb rgb = m_image->pixel(x, y);
                const double r = qRed(rgb) / 255.0;
                const double g = qGreen(rgb) / 255.0;
                const double b = qBlue(rgb) / 255.0;
                m_image->setPixelColor(x, y,
                   QColor(
                       255.0 * std::pow(r, m_gamma),
                       255.0 * std::pow(g, m_gamma),
                       255.0 * std::pow(b, m_gamma)));
            }
        }
    }
}

qreal TH2GraphicsRaster::apply2_2Gamma(qreal c)
{
    // Taken from https://youtu.be/_zQ_uBAHA4A?t=1281
    // Guy Davidson - Everything you know about colour is wrong - Meeting C++ online
    qreal gammifiedColor = (0.012522878 * c) +
        (0.682171111 * c * c) +
        (0.305306011 * c * c * c);

    return gammifiedColor;
}
