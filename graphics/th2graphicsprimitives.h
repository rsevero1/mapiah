/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2GRAPHICSPRIMITIVES_H
#define TH2GRAPHICSPRIMITIVES_H

#include <QGraphicsEllipseItem>

namespace TH2GraphicsPrimitives
{
    QGraphicsEllipseItem *circle(
            const qreal x,
            const qreal y,
            const qreal width,
            const qreal height,
            const QPen &pen,
            const QBrush &brush
            );

    QGraphicsEllipseItem *circle(
            const QPointF &xy,
            const qreal width,
            const qreal height,
            const QPen &pen,
            const QBrush &brush
            );

    QGraphicsPolygonItem *triangle(
            const qreal x_center,
            const qreal y_center,
            const qreal length,
            const QPen& pen,
            const QBrush& brush
            );

    QGraphicsPolygonItem *triangle(
            const QPointF &xy,
            const qreal length,
            const QPen& pen,
            const QBrush& brush
            );

    QGraphicsLineItem *straightLine(
            const qreal x_from,
            const qreal y_from,
            const qreal x_to,
            const qreal y_to,
            const QPen& pen
            );

    QGraphicsLineItem *straightLine(
            const QPointF& from,
            const QPointF& to,
            const QPen& pen
            );

    QGraphicsItemGroup *multiStraightLine(
            QList<QPointF> *pointList,
            const QPen& pen
            );
};

#endif // TH2GRAPHICSPRIMITIVES_H
