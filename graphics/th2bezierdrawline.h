/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2BEZIERDRAWLINE_H
#define TH2BEZIERDRAWLINE_H

#include "th2drawline.h"
#include "../data/th2/th2linepointbezier.h"

class TH2BezierDrawLine : public TH2DrawLine
{
    TH2LinePointBezier* m_linePointBezier;

public:
    TH2BezierDrawLine(const QPointF& from, TH2LinePointBezier* aLinePointBezier);

    // TH2DrawLine interface
    QGraphicsItem *drawLine() override;
};

#endif // TH2BEZIERDRAWLINE_H
