/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2GRAPHICSAREA_H
#define TH2GRAPHICSAREA_H

#include "../data/th2/th2area.h"

#include <QGraphicsEllipseItem>
#include <QGraphicsItemGroup>

class TH2GraphicsArea : public QGraphicsItemGroup
{
    TH2Area *m_th2area;

public:
    TH2GraphicsArea(TH2Area *aTH2Area, QGraphicsItem *parent = nullptr);

    TH2Area *th2area() const;
};

#endif // TH2GRAPHICSAREA_H
