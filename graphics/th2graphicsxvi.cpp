/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2graphicsxvi.h"

#include "th2graphicsprimitives.h"
#include "th2stationdrawpoint.h"
#include "th2straightdrawline.h"

#include "../data/th2/th2file.h"

#include "../factories/th2factory.h"

#include "../parsers/xviparser.h"

#include <QDir>
#include <QFileInfo>

XVI::File *TH2GraphicsXVI::xvifile() const
{
    return m_xvifile;
}

void TH2GraphicsXVI::clearItems()
{
    QList<QGraphicsItem *> groupChildren = m_initialGridGroup->childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();

    groupChildren = m_stationsGroup->childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();

    groupChildren = m_imagesGroup->childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();

    groupChildren = m_shotsGroup->childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();

    groupChildren = m_sketchlinesGroup->childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();

    groupChildren = m_finalGridGroup->childItems();
    qDeleteAll(groupChildren);
    groupChildren.clear();

    setPos(0, 0);
}

void TH2GraphicsXVI::drawStation(const XVI::Station &aXVIStation)
{
    QGraphicsPolygonItem *newStation = TH2GraphicsPrimitives::triangle(
        aXVIStation.coordinates.x,
        aXVIStation.coordinates.y,
        M_STATION_TRIANGLE_LENGTH,
        QPen(Qt::NoPen),
        m_stationBrush
        );

    m_stationsGroup->addToGroup(newStation);
}

void TH2GraphicsXVI::drawShot(const XVI::Shot &aXVIShot)
{
    QGraphicsLineItem *newLine = TH2GraphicsPrimitives::straightLine(
        aXVIShot.from.x, aXVIShot.from.y,
        aXVIShot.to.x, aXVIShot.to.y,
        m_shotPen
        );

    m_shotsGroup->addToGroup(newLine);
}

void TH2GraphicsXVI::drawSketchLine(const XVI::SketchLine &aXVISketchLine)
{

    QList<QPointF> *pointList = XVI::toQListOfQPointfs(&(aXVISketchLine.points));

    QPen pen;
    if (QColor::isValidColor(aXVISketchLine.color.c_str()))
    {
        pen = QPen(QColor(aXVISketchLine.color.c_str()));
        pen.setWidth(M_SKETCHLINE_PEN_WIDTH);
        pen.setCapStyle(Qt::RoundCap);
    }
    else
    {
        pen = m_sketchLinePen;
    }

    QGraphicsItemGroup *newSketchLine =
        TH2GraphicsPrimitives::multiStraightLine(pointList, pen);
    m_sketchlinesGroup->addToGroup(newSketchLine);
}

void TH2GraphicsXVI::drawFinalGrid()
{
    QGraphicsLineItem *newGridLine;

    qreal x_from;
    qreal x_to;
    qreal y_from;
    qreal y_to;

    qint32 qt_x = m_xvifile->finalGrid.qt_x;
    qint32 qt_y = m_xvifile->finalGrid.qt_y;

    qreal grid_width = m_xvifile->finalGrid.x_dist_1 * qt_x;
    qreal grid_height = m_xvifile->finalGrid.y_dist_2 * qt_y;

    x_from = x_to = m_xvifile->finalGrid.bottom.x;
    y_from = m_xvifile->finalGrid.bottom.y;
    y_to = y_from + grid_height;
    for(decltype (qt_x) ix = 0; ix <= qt_x; ++ix)
    {
       newGridLine = TH2GraphicsPrimitives::straightLine(
           x_from, y_from,
           x_to, y_to,
           m_finalGridPen);
       m_finalGridGroup->addToGroup(newGridLine);
       x_from += m_xvifile->finalGrid.x_dist_1;
       x_to = x_from;
    }

    x_from = m_xvifile->finalGrid.bottom.x;
    x_to = x_from + grid_width;
    y_from = y_to = m_xvifile->finalGrid.bottom.y;
    for(decltype (qt_y) iy = 0; iy <= qt_y; ++iy)
    {
        newGridLine = TH2GraphicsPrimitives::straightLine(
            x_from, y_from,
            x_to, y_to,
            m_finalGridPen);
        m_finalGridGroup->addToGroup(newGridLine);
        y_from += m_xvifile->finalGrid.y_dist_2;
        y_to = y_from;
    }
}

bool TH2GraphicsXVI::parseFile()
{
    /**
     *  Making sure the image filename starts with an absolute path, either it's own (if it
     *  is a absolute path) or with the TH2 file path.
     */
    QDir imageFilename(m_filename);
    if (!imageFilename.isAbsolute())
    {
        QDir xviPath = QFileInfo(m_imageElement->th2File()->filename()).canonicalPath();
        imageFilename.setPath(xviPath.canonicalPath() + "/" + imageFilename.path());
    }

    QString xviFileName = imageFilename.path();

    if (!QFileInfo::exists(xviFileName) || !QFileInfo(xviFileName).isFile())
    {
        return false;
    }

    XVIParser aParser(xviFileName);
    bool parseOk = aParser.parse();

    qInfo() << "XVI file read result: " << parseOk;

    if (parseOk)
    {
        m_xvifile = aParser.parsedFile();
    }

    return parseOk;
}

void TH2GraphicsXVI::dataChanged()
{
    bool parseOk = parseFile();

    if (!parseOk)
    {
        return;
    }

    clearItems();

    // Is there anything to be drawn for initialGrid?

    // Drawing stations
    std::for_each(
        m_xvifile->stations.cbegin(),
        m_xvifile->stations.cend(),
        [this] (const XVI::Station& aXVIStation) { drawStation(aXVIStation); }
    );

    // Drawing shots
    std::for_each(
        m_xvifile->shots.cbegin(),
        m_xvifile->shots.cend(),
        [this] (const XVI::Shot& aXVIShot) { drawShot(aXVIShot); }
    );

    // Drawing sketchlines
    std::for_each(
        m_xvifile->sketchlines.cbegin(),
        m_xvifile->sketchlines.cend(),
        [this] (const XVI::SketchLine& aXVISketchLine) { drawSketchLine(aXVISketchLine); }
    );

    // Drawing images: need a file example with image

    // Drawing finalGrid
    drawFinalGrid();

    setPos(m_x, m_y);
}

TH2GraphicsXVI::TH2GraphicsXVI(
    const QString &aFilename,
    qreal aX,
    qreal aY,
    TH2Element *aImageElement,
    QGraphicsItem *parent)
    : TH2GraphicsImage(aFilename, aX, aY, aImageElement, parent)
{
    /**
     * The order the layers are added define the order they are drawn.
     * The first one added is the bottom-most layer.
     */
    m_imagesGroup = new QGraphicsItemGroup();
    addToGroup(m_imagesGroup);

    m_initialGridGroup = new QGraphicsItemGroup();
    addToGroup(m_initialGridGroup);

    m_finalGridGroup = new QGraphicsItemGroup();
    addToGroup(m_finalGridGroup);

    m_shotsGroup = new QGraphicsItemGroup();
    addToGroup(m_shotsGroup);

    m_sketchlinesGroup = new QGraphicsItemGroup();
    addToGroup(m_sketchlinesGroup);

    m_stationsGroup = new QGraphicsItemGroup();
    addToGroup(m_stationsGroup);

    m_stationBrush = QBrush(QColor("yellowgreen"));

    QPen pen = QPen(QColor("goldenrod"));
    pen.setWidth(M_SHOT_PEN_WIDTH);
    pen.setCapStyle(Qt::RoundCap);
    m_shotPen = pen;

    pen = QPen(QColor("mediumorchid"));
    pen.setWidth(M_SKETCHLINE_PEN_WIDTH);
    pen.setCapStyle(Qt::RoundCap);
    m_sketchLinePen = pen;

    pen = QPen(QColor("aquamarine"));
    pen.setWidth(M_FINALGRID_PEN_WIDTH);
    pen.setCapStyle(Qt::FlatCap);
    m_finalGridPen = pen;
}

TH2GraphicsXVI::~TH2GraphicsXVI()
{
    delete m_initialGridGroup;
    delete m_stationsGroup;
    delete m_imagesGroup;
    delete m_shotsGroup;
    delete m_sketchlinesGroup;
    delete m_finalGridGroup;
}
