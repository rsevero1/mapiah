/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2defaultdrawpoint.h"

#include "graphics/th2graphicsprimitives.h"

#include <QGraphicsEllipseItem>

TH2DefaultDrawPoint::TH2DefaultDrawPoint(const QPointF& aPoint) : TH2DrawPoint(aPoint)
{
    m_pen = QPen(Qt::NoPen);
    m_brush = QBrush(Qt::red);
}

QGraphicsItem *TH2DefaultDrawPoint::drawPoint()
{
    QGraphicsEllipseItem *newPoint = TH2GraphicsPrimitives::circle(
                m_xy,
                m_width,
                m_height,
                m_pen,
                m_brush
                );

    return newPoint;
}
