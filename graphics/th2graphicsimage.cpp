/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2graphicsimage.h"

#include "../data/th2/th2file.h"

#include <QDir>
#include <QFileInfo>
#include <QPixmap>

const QString &TH2GraphicsImage::filename() const
{
    return m_filename;
}

void TH2GraphicsImage::setFilename(const QString &newFilename)
{
    m_filename = newFilename;
}

qreal TH2GraphicsImage::x() const
{
    return m_x;
}

void TH2GraphicsImage::setX(qreal newX)
{
    m_x = newX;
}

qreal TH2GraphicsImage::y() const
{
    return m_y;
}

void TH2GraphicsImage::setY(qreal newY)
{
    m_y = newY;
}

TH2Element *TH2GraphicsImage::imageElement() const
{
    return m_imageElement;
}

TH2GraphicsImage::TH2GraphicsImage(
    const QString &aFilename,
    qreal aX,
    qreal aY,
    TH2Element *aImageElement,
    QGraphicsItem *parent)
    : QGraphicsItemGroup(parent)
    , m_filename(aFilename)
    , m_x(aX)
    , m_y(aY)
    , m_imageElement(aImageElement)
{

}

TH2GraphicsImage::~TH2GraphicsImage()
{

}
