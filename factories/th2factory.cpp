/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2factory.h"

#include "../data/th/thinput.h"

#include "../data/th2/th2area.h"
#include "../data/th2/th2borderlineid.h"
#include "../data/th2/th2comment.h"
#include "../data/th2/th2line.h"
#include "../data/th2/th2linepoint.h"
#include "../data/th2/th2linepointbezier.h"
#include "../data/th2/th2linepointstraight.h"
#include "../data/th2/th2multilinecomment.h"
#include "../data/th2/th2point.h"
#include "../data/th2/th2scrap.h"
#include "../data/th2/th2unrecognized.h"
#include "../data/th2/th2xtherionimagesetting.h"
#include "../data/th2/th2xtherionunknownsetting.h"

#include "../data/th2/option/th2altitudeoption.h"
#include "../data/th2/option/th2authoroption.h"
#include "../data/th2/option/th2contextoption.h"
#include "../data/th2/option/th2copyrightoption.h"
#include "../data/th2/option/th2doubleoption.h"
#include "../data/th2/option/th2extendoption.h"
#include "../data/th2/option/th2freetextoption.h"
#include "../data/th2/option/th2lengthoption.h"
#include "../data/th2/option/th2markoption.h"
#include "../data/th2/option/th2orientationoption.h"
#include "../data/th2/option/th2pointscaleoption.h"
#include "../data/th2/option/th2projectionoption.h"
#include "../data/th2/option/th2scrapscaleoption.h"
#include "../data/th2/option/th2singlesetoption.h"
#include "../data/th2/option/th2sketchoption.h"
#include "../data/th2/option/th2stationnamesoption.h"
#include "../data/th2/option/th2subtypeoption.h"
#include "../data/th2/option/th2valueoption.h"

#include "../data/th2/option/th2doubleoptiondef.h"
#include "../data/th2/option/th2lengthoptiondef.h"

#include "../graphics/th2bezierdrawline.h"
#include "../graphics/th2defaultdrawpoint.h"
#include "../graphics/th2defaultlinepointdrawpoint.h"
#include "../graphics/th2graphicsraster.h"
#include "../graphics/th2graphicsxvi.h"
#include "../graphics/th2stationdrawpoint.h"
#include "../graphics/th2straightdrawline.h"

#include "../helpers/mapiahenums.h"

#include <QFileInfo>

namespace TH2Factory
{
    constexpr createElementPtrType createTHInput = &createElement<THInput>;

    constexpr createElementPtrType createTH2AltitudeOption = &createElement<TH2AltitudeOption>;
    constexpr createElementPtrType createTH2AuthorOption = &createElement<TH2AuthorOption>;
    constexpr createElementPtrType createTH2ContextOption = &createElement<TH2ContextOption>;
    constexpr createElementPtrType createTH2CopyrightOption = &createElement<TH2CopyrightOption>;
    constexpr createElementPtrType createTH2ExtendOption = &createElement<TH2ExtendOption>;
    constexpr createElementPtrType createTH2MarkOption = &createElement<TH2MarkOption>;
    constexpr createElementPtrType createTH2OrientationOption = &createElement<TH2OrientationOption>;
    constexpr createElementPtrType createTH2PointScaleOption = &createElement<TH2PointScaleOption>;
    constexpr createElementPtrType createTH2ProjectionOption = &createElement<TH2ProjectionOption>;
    constexpr createElementPtrType createTH2ScrapScaleOption = &createElement<TH2ScrapScaleOption>;
    constexpr createElementPtrType createTH2SketchOption = &createElement<TH2SketchOption>;
    constexpr createElementPtrType createTH2StationNamesOption = &createElement<TH2StationNamesOption>;
    constexpr createElementPtrType createTH2ValueOption = &createElement<TH2ValueOption>;

    constexpr createQStringTypeElementPtrType createTH2Area = &createTypedElement<TH2Area, QString>;
    constexpr createQStringTypeElementPtrType createTH2BorderLineID = &createTypedElement<TH2BorderLineID, QString>;
    constexpr createQStringTypeElementPtrType createTH2FreeTextOption = &createTypedElement<TH2FreeTextOption, QString>;
    constexpr createQStringTypeElementPtrType createTH2LengthOption = &createTypedElement<TH2LengthOption, QString>;
    constexpr createQStringTypeElementPtrType createTH2Line = &createTypedElement<TH2Line, QString>;
    constexpr createQStringTypeElementPtrType createTH2MultiLineComment = &createTypedElement<TH2MultiLineComment, QString>;
    constexpr createQStringTypeElementPtrType createTH2Scrap = &createTypedElement<TH2Scrap, QString>;
    constexpr createQStringTypeElementPtrType createTH2SubtypeOption = &createTypedElement<TH2SubtypeOption, QString>;
    constexpr createQStringTypeElementPtrType createTH2XTherionImageSetting = &createTypedElement<TH2XTherionImageSetting, QString>;

    constexpr createSingleSetValueDefTypeElementPtrType createTH2SingleSetOption = &createTypedElement<TH2SingleSetOption, TH2SingleSetValueDef>;

    constexpr createDoubleOptionDefTypeElementPtrType createTH2DoubleOption = &createTypedElement<TH2DoubleOption, TH2DoubleOptionDef>;

    TH2DrawPoint* createDrawPoint(const QPointF &xy, const QString &pointType)
    {
        if (pointType == STATION_OPTION_NAME)
        {
            return new TH2StationDrawPoint(xy);
        }

        if (pointType == LINEPOINT_ELEMENT_TYPE)
        {
            return new TH2DefaultLinePointDrawPoint(xy);
        }

        return new TH2DefaultDrawPoint(xy);
    }

    TH2DrawLine *createDrawLine(const QPointF &from, TH2LinePoint *linePoint)
    {
        if (linePoint->elementType() == "linepointbezier")
        {
            TH2LinePointBezier* bezierLinePoint = dynamic_cast<TH2LinePointBezier*>(linePoint);
            if (bezierLinePoint == nullptr)
            {
                return nullptr;
            }
            return new TH2BezierDrawLine(from, bezierLinePoint);
        }

        if (linePoint->elementType() == "linepointstraight")
        {
            TH2LinePointStraight* straightLinePoint = dynamic_cast<TH2LinePointStraight*>(linePoint);
            if (straightLinePoint == nullptr)
            {
                return nullptr;
            }
            return new TH2StraightDrawLine(from, straightLinePoint);
        }

        return nullptr;
    }

    th2element_up createUnrecognizableLine(
        QString lineContent,
        QString errorReason,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber)
    {
        TH2Unrecognized *newUnrecognizedLine_raw =
            new TH2Unrecognized(parent, th2file);

        th2element_up newUnrecognizedLine {newUnrecognizedLine_raw};

        if(newUnrecognizedLine_raw == nullptr)
        {
            return newUnrecognizedLine;
        }

        newUnrecognizedLine_raw->m_content = lineContent;
        newUnrecognizedLine_raw->m_reason = errorReason;

        if (lineNumber > 0)
        {
            newUnrecognizedLine_raw->m_lineNumber = lineNumber;
        }

        return newUnrecognizedLine;
    }

    th2element_up createLinePointBezier(
        QPointF c1,
        QPointF c2,
        QPointF coordinates,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber)
    {
        TH2LinePointBezier *newLinePoint_raw =
            new TH2LinePointBezier(parent, th2file);

        th2element_up newLinePoint {newLinePoint_raw};

        if (newLinePoint_raw == nullptr)
        {
            return newLinePoint;
        }

        newLinePoint_raw->setC1(c1);
        newLinePoint_raw->setC2(c2);
        newLinePoint_raw->setXY(coordinates);

        if (lineNumber > 0)
        {
            newLinePoint_raw->m_lineNumber = lineNumber;
        }

        return newLinePoint;
    }

    th2element_up createLinePointStraight(
        QPointF coordinates,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber)
    {
        TH2LinePointStraight *newLinePoint_raw =
            new TH2LinePointStraight(parent, th2file);

        th2element_up newLinePoint {newLinePoint_raw};

        if (newLinePoint_raw == nullptr)
        {
            return newLinePoint;
        }

        newLinePoint_raw->setXY(coordinates);

        if (lineNumber > 0)
        {
            newLinePoint_raw->m_lineNumber = lineNumber;
        }

        return newLinePoint;
    }

    th2element_up createComment(
        QString content,
        bool isPreviousLineEndComment,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber)
    {
        TH2Comment *newComment_raw =
            new TH2Comment(
                content,
                isPreviousLineEndComment,
                parent,
                th2file);

        th2element_up newComment {newComment_raw};

        if(newComment_raw == nullptr)
        {
            return newComment;
        }

        if (lineNumber > 0)
        {
            newComment_raw->m_lineNumber = lineNumber;
        }

        return newComment;
    }

    th2element_up createXTherionUnknownSetting(
        QString aSetting,
        QString aParameters,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber)
    {
        TH2XTherionUnknownSetting *newXTherionSetting_raw =
            new TH2XTherionUnknownSetting(aParameters, parent, th2file);

        th2element_up newXTherionSetting {newXTherionSetting_raw};

        if (newXTherionSetting_raw == nullptr)
        {
            return newXTherionSetting;
        }

        newXTherionSetting_raw->m_setting = aSetting;

        if (lineNumber > 0)
        {
            newXTherionSetting_raw->m_lineNumber = lineNumber;
        }

        return newXTherionSetting;
    }

    th2element_up createPoint(
        QString aType,
        QPointF coordinates,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber)
    {
        // Removing subtype info from the type to be apllied to the new point.
        if (aType.contains(TH2SubtypeOption::SUBTYPE_SEPARATOR))
        {
            aType.chop(aType.length() - aType.indexOf(TH2SubtypeOption::SUBTYPE_SEPARATOR));
        }

        TH2Point *newPoint_raw = new TH2Point(aType, parent, th2file);

        th2element_up newPoint {newPoint_raw};

        if (newPoint_raw == nullptr)
        {
            return newPoint;
        }

        newPoint_raw->setXY(coordinates);

        if (lineNumber > 0)
        {
            newPoint_raw->m_lineNumber = lineNumber;
        }

        return newPoint;
    }

    th2element_up createOption(
        const QString &aElementType,
        const QString &aSymbolType,
        const QString &aOptionName,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber)
    {
        // Is it a double option?
        if ((TH2_DOUBLE_VALUE_DEFS.count(aElementType) != 0)
                 && (TH2_DOUBLE_VALUE_DEFS.at(aElementType).count(aOptionName) != 0))
        {
            return createTH2DoubleOption(
                TH2_DOUBLE_VALUE_DEFS.at(aElementType).at(aOptionName),
                parent,
                th2file,
                lineNumber);
        }
        // Or is it a length option?
        else if ((TH2_LENGTH_OPTIONS.count(aElementType) != 0)
                 && (TH2_LENGTH_OPTIONS.at(aElementType).count(aOptionName) != 0))
        {
            return createTH2LengthOption(
                aOptionName,
                parent,
                th2file,
                lineNumber);
        }
        // A SingleSetOption?
        else if ((TH2_SINGLE_SET_VALUE_DEFS.count(aElementType) != 0)
            && (TH2_SINGLE_SET_VALUE_DEFS.at(aElementType).count(aOptionName) != 0))
        {
            const TH2SingleSetValueDef &aSingleSetValueDef =
                TH2_SINGLE_SET_VALUE_DEFS.at(aElementType).at(aOptionName);

            if (aOptionName == CONTEXT_OPTION_NAME)
            {
                if (parent->isPoint() || parent->isLine() || parent->isArea())
                {
                    return createTH2ContextOption(
                        parent,
                        th2file,
                        lineNumber);
                }
            }
            else if (aOptionName == POINT_SCALE_OPTION_NAME)
            {
                if (parent->isPoint())
                {
                    return createTH2PointScaleOption(
                        parent,
                        th2file,
                        lineNumber);
                }
            }
            else
            {
                return createTH2SingleSetOption(
                    aSingleSetValueDef,
                    parent,
                    th2file,
                    lineNumber);
            }
        }
        else if (aOptionName == ALTITUDE_OPTION_NAME)
        {
            return createTH2AltitudeOption(parent, th2file, lineNumber);
        }
        else if (aOptionName == AUTHOR_OPTION_NAME)
        {
            return createTH2AuthorOption(parent, th2file, lineNumber);
        }
        else if (aOptionName == COPYRIGHT_OPTION_NAME)
        {
            return createTH2CopyrightOption(parent, th2file, lineNumber);
        }
        else if (aOptionName == EXTEND_OPTION_NAME)
        {
            return createTH2ExtendOption(parent, th2file, lineNumber);
        }
        else if (aOptionName == MARK_OPTION_NAME)
        {
            return createTH2MarkOption(parent, th2file, lineNumber);
        }
        else if ((aOptionName == ORIENTATION_OPTION_NAME)
                 || (aOptionName == ORIENTATION_ALTERNATIVE_OPTION_NAME))
        {
            return createTH2OrientationOption(parent, th2file, lineNumber);
        }
        else if (aOptionName == PROJECTION_OPTION_NAME)
        {
            return createTH2ProjectionOption(parent, th2file, lineNumber);
        }
        else if (aOptionName == SCRAP_SCALE_OPTION_NAME)
        {
            return createTH2ScrapScaleOption(parent, th2file, lineNumber);
        }
        else if (aOptionName == SKETCH_OPTION_NAME)
        {
            return createTH2SketchOption(parent, th2file, lineNumber);
        }
        else if (aOptionName == STATION_NAMES_OPTION_NAME)
        {
            return createTH2StationNamesOption(parent, th2file, lineNumber);
        }
        else if (aOptionName == SUBTYPE_OPTION_NAME)
        {
            return createTH2SubtypeOption(aSymbolType, parent, th2file, lineNumber);
        }
        else if (aOptionName == VALUE_OPTION_NAME)
        {
            return createTH2ValueOption(parent, th2file, lineNumber);
        }

        return createTH2FreeTextOption(aOptionName, parent, th2file, lineNumber);
    }

    TH2GraphicsImage *createGraphicsImage(
        const QString &aFilename,
        qreal aX,
        qreal aY,
        TH2Element *aImageElement,
        QGraphicsItem *parent)
    {
        QFileInfo fileInfo(aFilename);

        QString fileExtension = fileInfo.completeSuffix().toLower();

        TH2GraphicsImage *graphicImage;

        if (fileExtension == XVI_FILE_EXTENSION)
        {
            graphicImage =
                new TH2GraphicsXVI(aFilename, aX, aY, aImageElement, parent);
        }
        else
        {
            graphicImage =
                new TH2GraphicsRaster(aFilename, aX, aY, aImageElement, parent);
        }

        return graphicImage;
    }

}
