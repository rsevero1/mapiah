/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2FACTORY_H
#define TH2FACTORY_H

#include "../data/th2/th2typedefs.h"

#include "../helpers/mapiahenums.h"

#include <QGraphicsScene>
#include <QPointF>
#include <QString>

/**
 * Inspired by https://stackoverflow.com/questions/9321/how-do-you-create-a-static-class-in-c/112451#112451
 */

class THInput;

class TH2Area;
class TH2File;
class TH2LinePoint;
class TH2Scale;

class TH2OrientationOption;
class TH2ProjectionOption;
class TH2SingleSetOption;
class TH2SubtypeOption;
class TH2ValueOption;

class TH2DrawLine;
class TH2DrawPoint;

class TH2GraphicsImage;

namespace TH2Factory
{
    TH2DrawPoint *createDrawPoint(const QPointF &xy, const QString &pointType = "default");
    TH2DrawLine *createDrawLine(const QPointF &from, TH2LinePoint *linePoint);

    TH2GraphicsImage *createGraphicsImage(
        const QString &aFilename,
        qreal aX,
        qreal aY,
        TH2Element *aImageElement,
        QGraphicsItem *parent = nullptr);

    template<typename RawTH2ElementType>
    th2element_up createElement(
            TH2Element *parent,
            TH2File *th2file,
            qint32 lineNumber = -1)
    {
        RawTH2ElementType *newElement_raw = new RawTH2ElementType(parent, th2file);

        th2element_up newElement {newElement_raw};

        if (newElement_raw == nullptr)
        {
            return newElement;
        }

        if (lineNumber > 0)
        {
            newElement_raw->m_lineNumber = lineNumber;
        }

        return newElement;
    }

    using createElementPtrType = th2element_up(*)(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);
    extern const createElementPtrType createTHInput;

    extern const createElementPtrType createTH2AltitudeOption;
    extern const createElementPtrType createTH2AuthorOption;
    extern const createElementPtrType createTH2ContextOption;
    extern const createElementPtrType createTH2CopyrightOption;
    extern const createElementPtrType createTH2ExtendOption;
    extern const createElementPtrType createTH2MarkOption;
    extern const createElementPtrType createTH2OrientationOption;
    extern const createElementPtrType createTH2PointScaleOption;
    extern const createElementPtrType createTH2ProjectionOption;
    extern const createElementPtrType createTH2ScrapScaleOption;
    extern const createElementPtrType createTH2SketchOption;
    extern const createElementPtrType createTH2StationNamesOption;
    extern const createElementPtrType createTH2ValueOption;

    template<typename RawTH2ElementType, typename TypeType>
    th2element_up createTypedElement(
        const TypeType &aType,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber = -1)
    {
        RawTH2ElementType *newElement_raw =
            new RawTH2ElementType(aType, parent, th2file);

        th2element_up newElement {newElement_raw};

        if (newElement_raw == nullptr)
        {
            return newElement;
        }

        if (lineNumber > 0)
        {
            newElement_raw->m_lineNumber = lineNumber;
        }

        return newElement;
    }

    using createQStringTypeElementPtrType = th2element_up(*)(
        const QString &aType,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);
    extern const createQStringTypeElementPtrType createTH2Area;
    extern const createQStringTypeElementPtrType createTH2BorderLineID;
    extern const createQStringTypeElementPtrType createTH2FreeTextOption;
    extern const createQStringTypeElementPtrType createTH2LengthOption;
    extern const createQStringTypeElementPtrType createTH2Line;
    extern const createQStringTypeElementPtrType createTH2MultiLineComment;
    extern const createQStringTypeElementPtrType createTH2Scrap;
    extern const createQStringTypeElementPtrType createTH2SubtypeOption;
    extern const createQStringTypeElementPtrType createTH2XTherionImageSetting;

    using createSingleSetValueDefTypeElementPtrType = th2element_up(*)(
        const TH2SingleSetValueDef &aType,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);
    extern const createSingleSetValueDefTypeElementPtrType createTH2SingleSetOption;

    using createDoubleOptionDefTypeElementPtrType = th2element_up(*)(
        const TH2DoubleOptionDef &aType,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);
    extern const createDoubleOptionDefTypeElementPtrType createTH2DoubleOption;

    th2element_up createComment(
        QString content,
        bool isPreviousLineEndComment,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr,
        qint32 lineNumber = -1);
    th2element_up createLinePointBezier(QPointF c1,
        QPointF c2,
        QPointF coordinates,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr,
        qint32 lineNumber = -1);
    th2element_up createLinePointStraight(QPointF coordinates,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr,
        qint32 lineNumber = -1);
    th2element_up createOption(
        const QString &aElementType,
        const QString &aSymbolType,
        const QString &aOptionName,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr,
        qint32 lineNumber = -1);
    th2element_up createPoint(
        QString aType,
        QPointF coordinates,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr,
        qint32 lineNumber = -1);
    th2element_up createUnrecognizableLine(
        QString lineContent,
        QString errorReason,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr,
        qint32 lineNumber = -1);
    th2element_up createXTherionUnknownSetting(
        QString aSetting,
        QString aParameters,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr,
        qint32 lineNumber = -1);
};

#endif // TH2FACTORY_H
