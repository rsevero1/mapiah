/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef MAPIAH_H
#define MAPIAH_H

#include "functional/th2actionbutton.h"
#include "functional/th2scene.h"
#include "functional/th2view.h"

#include "data/th2/th2file.h"
#include "data/th2/th2typedefs.h"

#include <QButtonGroup>
#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGroupBox>
#include <QLabel>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Mapiah; }
QT_END_NAMESPACE

class Mapiah : public QMainWindow
{
    Q_OBJECT

    TH2View *m_view;
    TH2Scene *m_scene;
    QLabel *m_scaleLabel;
    th2file_up m_th2file;

    TH2ActionButton *zoomToFit;
    TH2ActionButton *zoomTo100;
    TH2ActionButton *openTH2File;
    TH2ActionButton *saveAsTH2File;

    QString m_saveAsPath;
    QString m_openPath;

    QString pathOnly(const QString &aPath);

public:
    Mapiah(QWidget *parent = nullptr);
    ~Mapiah();

    TH2Scene *getScene();
    TH2View *getView();

    QString &saveAsPath();
    void setSaveAsPath(const QString &newSaveAsPath);

    QString openPath();
    void setOpenPath(const QString &newOpenPath);

private slots:
    void on_actionZoom_to_fit_triggered();

    void on_action100_zoom_triggered();

    void on_actionOpenTH2File_triggered();

    void on_actionInsert_Image_triggered();

    void on_actionSave_TH2_File_As_triggered();

public slots:
    void on_zoomUpdated(qreal newScale);

private:
    Ui::Mapiah *ui;
    QWidget * QMainWindow;
};
#endif // MAPIAH_H
