/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2VIEW_H
#define TH2VIEW_H

#include <QGraphicsView>
#include <QObject>
#include <QPoint>
#include <QWheelEvent>

class TH2View : public QGraphicsView
{
    Q_OBJECT

    qint16 m_wheelZoomStepSize = 15;
    qreal m_wheelZoomFactor = 1.1;
    qreal m_zoomToFitFactor = 0.1;

public:
    TH2View();
    TH2View(QWidget *parent = nullptr);

    void zoomWithDegrees(const QPoint aZoom);
    void zoomWithPixels(const QPoint aZoom);

    void scaleZoom(const qreal aScaleFactor);
    void setZoom(const qreal aZoom);

    void zoomToFit();

    // QWidget interface
protected:
    void wheelEvent(QWheelEvent *event) override;

signals:
    void zoomChanged(const qreal factor);
};

#endif // TH2VIEW_H
