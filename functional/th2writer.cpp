/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2writer.h"

#include "../data/th/thinput.h"

#include "../data/th2/th2area.h"
#include "../data/th2/th2comment.h"
#include "../data/th2/th2file.h"
#include "../data/th2/th2line.h"
#include "../data/th2/th2linepointbezier.h"
#include "../data/th2/th2linepointstraight.h"
#include "../data/th2/th2multilinecomment.h"
#include "../data/th2/th2point.h"
#include "../data/th2/th2scrap.h"
#include "../data/th2/th2unrecognized.h"
#include "../data/th2/th2xtherionimagesetting.h"
#include "../data/th2/th2xtherionsetting.h"
#include "../data/th2/th2xtherionunknownsetting.h"

#include "../data/th2/option/th2optionlist.h"
#include "../data/th2/option/th2subtypeoption.h"

#include "../helpers/mapiahenums.h"
#include "../helpers/mapiahsupport.h"

#include <QFile>
#include <QBuffer>

bool TH2Writer::fileWithPrefix() const
{
    return m_fileWithPrefix;
}

void TH2Writer::setFileWithPrefix(bool newFileWithPrefix)
{
    m_fileWithPrefix = newFileWithPrefix;
}

const QByteArray &TH2Writer::output() const
{
    return m_output;
}

void TH2Writer::writeEncoding()
{
    if (!m_encodingWritten)
    {
        m_outputLine = QString("encoding %1").arg(m_th2file->encoding());
        writeOutput();
        m_encodingWritten = true;
    }
}

void TH2Writer::writeInput()
{
    THInput *aInput = dynamic_cast<THInput *>(m_currentElement);

    if (aInput == nullptr)
    {
        qInfo() << "dynamic_cast failure to THInput for m_currentElement in TH2Writer::writeInput(). Skipping...";
        return;
    }

    const QString outputPattern =
        aInput->filenameValue().contains(SPACE_STRING) ?
                R"RX(%1 "%2")RX" :
                "%1 %2";

    m_outputLine = outputPattern.arg(INPUT_ELEMENT_TYPE, aInput->filenameValue());

    writeOutput();
}

void TH2Writer::writeXTherionSetting()
{
    TH2XTherionSetting *aXTherionSetting = dynamic_cast<TH2XTherionSetting *>(m_currentElement);

    if (aXTherionSetting == nullptr)
    {
        qInfo() << "dynamic_cast failure to TH2XTherionSetting for m_currentElement in TH2Writer::writeXTherionSetting(). Skipping...";
        return;
    }

    QString settingType = aXTherionSetting->setting();
    if (settingType == QStringLiteral("xth_me_image_insert"))
    {
        writeXTherionImageSetting();
    }
    else
    {
        writeXTherionUnknownSetting();
    }
}

void TH2Writer::writeComment()
{
    TH2Comment *aComment = dynamic_cast<TH2Comment *>(m_currentElement);
    if (aComment == nullptr)
    {
        qInfo() << "dynamic_cast failure to TH2Comment for m_currentElement in TH2Writer::writeComment(). Skipping...";
        return;
    }

    m_outputLine = aComment->content();
    writeOutput();
}

void TH2Writer::writeMultiLineComment()
{
    TH2MultiLineComment *aMultiLineComment =
            dynamic_cast<TH2MultiLineComment *>(m_currentElement);
    if (aMultiLineComment == nullptr)
    {
        qInfo() << "dynamic_cast failure to TH2MultiLineComment for m_currentElement in TH2Writer::writeMultiLineComment(). Skipping...";
        return;
    }

    m_outputLine = QStringLiteral("comment");
    writeOutput(false);
    // Joining the content to the 'endcomment' keyword to avoid including an
    // extra line ending at the end of the content.
    m_outputLine = aMultiLineComment->content() + QStringLiteral("endcomment");
    writeOutput(false);
}

void TH2Writer::writePoint()
{
    TH2Point *aPoint = dynamic_cast<TH2Point *>(m_currentElement);
    if (aPoint == nullptr)
    {
        qInfo() << "dynamic_cast failure to TH2Point for m_currentElement in TH2Writer::writePoint(). Skipping...";
        return;
    }

    QPointF aCoordinates = aPoint->xy();

    QString aType = aPoint->type();
    if (aPoint->existsOptionByType("subtype"))
    {
        TH2SubtypeOption *aSubtypeOption = dynamic_cast<TH2SubtypeOption *>(
            aPoint->getOptionByType("subtype"));

        if (aSubtypeOption == nullptr)
        {
            qInfo() << "dynamic_cast failure for TH2SubtypeOption in TH2Writer::writePoint(). Skipping...";
        }
        else if (aSubtypeOption->isSubtypeInline())
        {
            aType.append(QString(":%1").arg(aSubtypeOption->valueForFile()));
        }
    }

    m_outputLine = QString("point %1 %2").
        arg(outputCoordinates(aCoordinates), aType);

    QString options = optionListOptions(aPoint);
    m_outputLine.append(options);

    writeOutput();
}

void TH2Writer::writeLine()
{
    TH2Line *aLine = dynamic_cast<TH2Line *>(m_currentElement);
    if (aLine == nullptr)
    {
        qInfo() << "dynamic_cast failure to TH2Line for m_currentElement in TH2Writer::writePoint(). Skipping...";
        return;
    }

    m_outputLine = QString("line %1").arg(aLine->type());

    QString options = optionListOptions(aLine);
    m_outputLine.append(options);
    writeOutput();

    m_outputLinePrefix.append("\t");
    for(auto &aChildCod : aLine->childCods())
    {
        m_currentElement = m_th2file->element(aChildCod);
        QString elementType = m_currentElement->elementType();
        if (elementType == QStringLiteral("linepointbezier"))
        {
            writeLinePointBezier();
        }
        else if (elementType == QStringLiteral("linepointstraight"))
        {
            writeLinePointStraight();
        }
        else if (elementType == QStringLiteral("comment"))
        {
            writeComment();
        }
        else if (elementType == QStringLiteral("multilinecomment"))
        {
            writeMultiLineComment();
        }
        else
        {
            qInfo() << QString("Unknown point type '%1'").arg(elementType);
        }
    }
    m_outputLinePrefix.chop(1);

    m_outputLine = QStringLiteral("endline");
    writeOutput();
}

void TH2Writer::writeArea()
{
    TH2Area *aArea = dynamic_cast<TH2Area *>(m_currentElement);
    if (aArea == nullptr)
    {
        qInfo() << "dynamic_cast failure to TH2Area for m_currentElement in TH2Writer::writeArea(). Skipping...";
        return;
    }

    m_outputLine = QString("area %1").arg(aArea->type());

    QString options = optionListOptions(aArea);
    m_outputLine.append(options);
    writeOutput();

    m_outputLinePrefix.append("\t");
    foreach(QString aBorderLineID, aArea->borderLineIDs())
    {
        m_outputLine = aBorderLineID;
        writeOutput();
    }
    m_outputLinePrefix.chop(1);

    m_outputLine = QStringLiteral("endarea");
    writeOutput();
}

QString TH2Writer::optionListOptions(TH2OptionList *aOptionList)
{
    QString options;

    for(auto &aOptionCod : aOptionList->optionCods())
    {
        TH2Option *aOption = m_th2file->option(aOptionCod);
        if (aOption == nullptr)
        {
           qInfo() << QString("Can't get TH2Option for cod '%1' at TH2Writer::optionListOptions(). Skipping...").arg(aOptionCod);
           continue;
        }

        // Skipping inline subtype options as they are written together with
        // the point type.
        QString aOptionType = aOption->name();
        if (aOptionType == SUBTYPE_OPTION_NAME)
        {
            TH2SubtypeOption *aSubtypeOption = dynamic_cast<TH2SubtypeOption *>(aOption);

            if (aSubtypeOption == nullptr)
            {
                qInfo() << QString("Failure casting aOption to <TH2SubtypeOption *> TH2Writer::optionListOptions(). Skipping...");
                continue;
            }

            if (aSubtypeOption->isSubtypeInline())
            {
                continue;
            }
        }

        QString optionValue = aOption->valueForFile();
        QString optionOutput = optionValue.isEmpty() ?
            QString(" -%1").arg(aOption->name()) :
            QString(" -%1 %2").arg(aOption->name(), optionValue);
        options.append(optionOutput);
    }

    return options;
}

void TH2Writer::writeScrap()
{
    TH2Scrap *aScrap = dynamic_cast<TH2Scrap *>(m_currentElement);
    if (aScrap == nullptr)
    {
        qInfo() << "Failure casting m_currentElement to TH2Scrap in TH2Writer::writeScrap(). Skipping...";
        return;
    }

    m_outputLine = QString("scrap %1").arg(aScrap->id());
    QString options = optionListOptions(aScrap);
    m_outputLine.append(options);
    writeOutput();

    m_outputLinePrefix.append("\t");
    writeList(aScrap->childCods());
    m_outputLinePrefix.chop(1);

    m_outputLine = QStringLiteral("endscrap");
    writeOutput();
}

void TH2Writer::writeUnrecognized()
{
    TH2Unrecognized *aUnrecognized = dynamic_cast<TH2Unrecognized *>(m_currentElement);
    if (aUnrecognized == nullptr)
    {
        qInfo() << QString("dynamic_cast failure to TH2Unrecognized for m_currentElement in TH2Writer::writeUnrecognized(). It is in fact a '%1'. Skipping...")
            .arg(m_currentElement->elementType());
        return;
    }

    m_outputLine = aUnrecognized->content();
    writeOutput(false);
}

void TH2Writer::writeOutput(bool withPrefix)
{
    QByteArray encodedLine = codec->fromUnicode(m_outputLine);

    if (m_fileWithPrefix && withPrefix)
    {
        m_outputFile->write(m_outputLinePrefix);
    }
    m_outputFile->write(encodedLine);
    m_outputFile->write(lineEnding);
    m_outputLine = EMPTY_STRING;
}

void TH2Writer::writeLinePointBezier()
{
    TH2LinePointBezier *aLinePointBezier = dynamic_cast<TH2LinePointBezier *>(m_currentElement);
    if (aLinePointBezier == nullptr)
    {
        qInfo() << "Can't dynamic_cast m_currentElement to TH2LinePointBezier at TH2Writer::writeLinePointBezier(). Skipping...";
        return;
    }

    m_outputLinePrefix.append("\t");
    writeLinePointOptions();
    m_outputLinePrefix.chop(1);

    QPointF controlPoint1 = aLinePointBezier->c1();
    QPointF controlPoint2 = aLinePointBezier->c2();
    QPointF aCoordinates = aLinePointBezier->xy();
    m_outputLine = QString("%1 %2 %3").arg(
                outputCoordinates(controlPoint1),
                outputCoordinates(controlPoint2),
                outputCoordinates(aCoordinates));
    writeOutput();
}

void TH2Writer::writeLinePointStraight()
{
    TH2LinePointStraight *aLinePointStraight = dynamic_cast<TH2LinePointStraight *>(m_currentElement);
    if (aLinePointStraight == nullptr)
    {
        qInfo() << "Can't dynamic_cast m_currentElement to TH2LinePointStraight at TH2Writer::writeLinePointStraight(). Skipping...";
        return;
    }

    m_outputLinePrefix.append("\t");
    writeLinePointOptions();
    m_outputLinePrefix.chop(1);

    QPointF aCoordinates = aLinePointStraight->xy();
    m_outputLine = outputCoordinates(aCoordinates);
    writeOutput();
}

void TH2Writer::writeLinePointOptions()
{
    TH2LinePoint *aLinePoint = dynamic_cast<TH2LinePoint *>(m_currentElement);
    for(auto &aOptionCod : aLinePoint->optionCods())
    {
        TH2Option *aOption = m_th2file->option(aOptionCod);
        if (aOption == nullptr)
        {
            qInfo() << QString("Can't get TH2Option for cod '%1' at TH2Writer::writeLinePointOptions(). Skipping...").arg(aOptionCod);
            continue;
        }

        m_outputLine = QString("%1 %2").arg(aOption->name(), aOption->valueForFile());
        writeOutput();
    }
}

/**
 * @brief TH2Writer::formatedFloat
 * @param n
 * @param maxDecimalPlaces
 * @return
 *
 * Returns as many decimal places as seem significant (as defined by qFuzzyIsNull and qFussyCompare).
 */
QString TH2Writer::formatedFloat(const qreal n) const
{
    return MapiahSupport::formatedFloatForWriting(n);
}

void TH2Writer::writeXTherionImageSetting()
{
    TH2XTherionImageSetting *aXTherionImageSetting = dynamic_cast<TH2XTherionImageSetting *>(m_currentElement);
    if (aXTherionImageSetting == nullptr)
    {
        qInfo() << "dynamic_cast failure to TH2XTherionImageSetting for m_currentElement in TH2Writer::writeXTherionImageSetting(). Skipping...";
        return;
    }

    m_outputLine = QString("%1 %2").
            arg(m_xtherionSettingPrefix, aXTherionImageSetting->setting());

    // xx
    m_outputLine.append(QString(" {%1 %2 %3}").
                        arg(formatedFloat(aXTherionImageSetting->xx())).
                        arg(aXTherionImageSetting->vsb()).
                        arg(aXTherionImageSetting->igamma(), 1, 'f', 1)
                        );

    // yy
    QString aXVIroot = (aXTherionImageSetting->XVIroot().length() > 0) ?
             aXTherionImageSetting->XVIroot() : "{}";
    m_outputLine.append(QString(" {%1 %2}").
                        arg(formatedFloat(aXTherionImageSetting->yy())).
                        arg(aXVIroot)
                        );

    // fname
    m_outputLine.append(' ' + aXTherionImageSetting->fname());

    // iidx
    m_outputLine.append(QString(" %1").arg(aXTherionImageSetting->iidx()));

    // imgx
    if (aXTherionImageSetting->ximage() == 0)
    {
        m_outputLine.append(" {}");
    }
    else
    {
        m_outputLine.append(QString(" {%1 %2}").
                            arg(
                                aXTherionImageSetting->imgx(),
                                aXTherionImageSetting->xdata())
                            );
    }

    writeOutput();
}

void TH2Writer::writeXTherionUnknownSetting()
{
    TH2XTherionUnknownSetting *aXTherionUnknownSetting = dynamic_cast<TH2XTherionUnknownSetting *>(m_currentElement);
    if (aXTherionUnknownSetting == nullptr)
    {
        qInfo() << "dynamic_cast failure to TH2XTherionUnknownSetting for m_currentElement in TH2Writer::writeXTherionUnknownSetting(). Skipping...";
        return;
    }

    m_outputLine = QString("%1 %2 %3").
            arg(m_xtherionSettingPrefix,
                aXTherionUnknownSetting->setting(),
                aXTherionUnknownSetting->xtherionParameters()
                );

    writeOutput();
}

QString TH2Writer::outputCoordinates(QPointF aCoordinates) const
{
    return QString("%1 %2").
            arg(formatedFloat(aCoordinates.x()),
                formatedFloat(aCoordinates.y()));
}

void TH2Writer::openFile()
{
    if (m_filename == QStringLiteral(""))
    {
        m_outputFile = std::make_shared<QBuffer>(&m_output);
    }
    else
    {
        m_outputFile = std::make_shared<QFile>(m_filename);
    }
    if (!m_outputFile->open(QIODevice::WriteOnly)) {
        qInfo() << "Failure opening " << m_filename << "for writing.";
    }
}

void TH2Writer::closeFile()
{
    m_outputFile->close();
}

bool TH2Writer::writeList(QStringList aCodList)
{
    QString aElementType;

    writeEncoding();

    for(auto &aCod : aCodList)
    {
        m_currentElement = m_th2file->element(aCod);
        if (m_currentElement == nullptr)
        {
            qInfo() << QString("Failure getting TH2Element at TH2Writer::writeList() for cod '%1'. Skipping...").arg(aCod);
            continue;
        }

        aElementType = m_currentElement->elementType();

        if (aElementType == AREA_ELEMENT_TYPE)
        {
            writeArea();
            continue;
        }
        else if (aElementType == COMMENT_ELEMENT_TYPE)
        {
            writeComment();
            continue;
        }
        else if (aElementType == ENCODING_ELEMENT_TYPE)
        {
            continue;
        }
        else if (aElementType == INPUT_ELEMENT_TYPE)
        {
            writeInput();
            continue;
        }
        else if (aElementType == LINE_ELEMENT_TYPE)
        {
            writeLine();
            continue;
        }
        else if (aElementType == MULTILINE_COMMENT_ELEMENT_TYPE)
        {
            writeMultiLineComment();
            continue;
        }
        else if (aElementType == POINT_ELEMENT_TYPE)
        {
            writePoint();
            continue;
        }
        else if (aElementType == SCRAP_ELEMENT_TYPE)
        {
            writeScrap();
            continue;
        }
        else if (aElementType.startsWith(XTHERION_SETTING_ID))
        {
            writeXTherionSetting();
            continue;
        }
        else
        {
            writeUnrecognized();
            continue;
        }
    }

    return true;
}

void TH2Writer::setLineEnding()
{
    MapiahEnums::LineEndingType aType = m_th2file->lineEndingType();

    switch (aType) {
    case MapiahEnums::LineEndingType::CR:
        lineEnding = "\r";
        break;
    case MapiahEnums::LineEndingType::CRLF:
        lineEnding = "\r\n";
        break;
    case MapiahEnums::LineEndingType::LFCR:
        lineEnding = "\n\r";
        break;
    default:
        lineEnding = "\n";
    }
}

TH2Writer::TH2Writer(TH2File *aTH2File, QString outputFilename)
    : m_th2file(aTH2File)
    , m_filename(outputFilename)
    , m_fileWithPrefix(true)
{
    setLineEnding();

    codec = QTextCodec::codecForName(m_th2file->encoding().toLatin1());
}

bool TH2Writer::write()
{
    m_encodingWritten = false;
    openFile();
    bool result = writeList(m_th2file->elementCods());
    closeFile();

    return result;
}
