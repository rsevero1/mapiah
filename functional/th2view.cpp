/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2view.h"

#include <QGuiApplication>
#include <QScreen>

TH2View::TH2View()
{

}

void TH2View::wheelEvent(QWheelEvent *event)
{
    QPoint numPixels = event->pixelDelta();
    QPoint numDegrees = event->angleDelta() / 8;

//    qInfo() << "numPixels: " << numPixels;
//    qInfo() << "numDegrees: " << numDegrees;

    if (!numPixels.isNull()) {
        zoomWithPixels(numPixels);
    } else if (!numDegrees.isNull()) {
        zoomWithDegrees(numDegrees);
    }

    event->accept();
}

TH2View::TH2View(QWidget *parent) : QGraphicsView(parent)
{

}

/**
 * @brief TH2View::zoomWithDegrees
 * @param aZoom
 *
 * Used for X11 and Windows.
 */
void TH2View::zoomWithDegrees(const QPoint aZoom)
{
    qreal steps = aZoom.y() / m_wheelZoomStepSize;
    qreal factor = m_wheelZoomFactor * steps;

    if (factor < 0)
    {
        factor = 1 / abs(factor);
    }

    if (factor != 0)
    {
        scaleZoom(factor);
    }
}

/**
 * @brief TH2View::zoomWithPixels
 * @param aZoom
 *
 * Used for Mac?
 */
void TH2View::zoomWithPixels(const QPoint aZoom)
{

}

void TH2View::scaleZoom(const qreal aScaleFactor)
{
    if (aScaleFactor != 1)
    {
        scale(aScaleFactor, aScaleFactor);
        QTransform cT = transform();
//        qInfo() << "New zoom: " << cT.m11();
        emit zoomChanged(cT.m11());
    }
}

void TH2View::setZoom(const qreal aZoom)
{
    QTransform cT = transform();

//    qInfo() << "Current zoom: " << cT.m11();
//    qInfo() << "New zoom: " << aZoom;

    if (aZoom == cT.m11())
    {
//        qInfo() << "New zoom is the same as current zoom: doing nothing.";
        return;
    }

    /**
     * The second aZoom below (the Y scale factor) is inverted because positive Y is up for Therion
     * and for QT is down.
     */
    cT.setMatrix(aZoom, cT.m12(), cT.m13(), cT.m21(), -aZoom, cT.m23(), cT.m31(), cT.m32(), cT.m33());
    setTransform(cT);
    emit zoomChanged(aZoom);
}

void TH2View::zoomToFit()
{
    QSizeF screenSize = QGuiApplication::primaryScreen()->size();
    qreal viewWidth = width();
    qreal viewHeight = height();
    qreal sceneWidth = scene()->width() + (screenSize.width() * m_zoomToFitFactor);
    qreal sceneHeight = scene()->height() + (screenSize.height() * m_zoomToFitFactor);
    qreal widthRatio = viewWidth / sceneWidth;
    qreal heightRatio = viewHeight / sceneHeight;
    qreal newZoom = std::min(widthRatio, heightRatio);

//    qInfo() << "screenSize: " << screenSize;
//    qInfo() << "viewWidth: " << viewWidth;
//    qInfo() << "viewHeight: " << viewHeight;
//    qInfo() << "sceneWidth: " << sceneWidth;
//    qInfo() << "sceneHeight: " << sceneHeight;
//    qInfo() << "widthRatio: " << widthRatio;
//    qInfo() << "heightRatio: " << heightRatio;
//    qInfo() << "newZoom: " << newZoom;

    setZoom(newZoom);
}
