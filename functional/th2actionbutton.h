/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2ACTIONBUTTON_H
#define TH2ACTIONBUTTON_H

#include <QAction>
#include <QPushButton>

/**
 * An extension of a QPushButton that supports QAction.
 * This class represents a QPushButton extension that can be
 * connected to an action and that configures itself depending
 * on the status of the action.
 * When the action changes its state, the button reflects
 * such changes, and when the button is clicked the action
 * is triggered.
 */

class TH2ActionButton : public QPushButton
{
    Q_OBJECT

    // The action associated to this button.
    QAction *actionOwner = nullptr;

public:
    // Default constructor. Parent the widget parent of this button
    explicit TH2ActionButton(QWidget *parent = nullptr);

    /*!
     * Set the action owner of this button, that is the action
     * associated to the button. The button is configured immediately
     * depending on the action status and the button and the action
     * are connected together so that when the action is changed the button
     * is updated and when the button is clicked the action is triggered.
     * action the action to associate to this button
     */
    void setAction(QAction *action);

public slots:
    /*!
     * Update the button status depending on a change
     * on the action status. This slot is invoked each time the action
     * "changed" signal is emitted.
     */
    void updateButtonStatusFromAction();
};

#endif // TH2ACTIONBUTTON_H
