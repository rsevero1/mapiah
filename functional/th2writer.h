/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2WRITER_H
#define TH2WRITER_H

#include <QIODevice>
#include <QString>
#include <QTextCodec>

#include <memory>

class TH2Element;
class TH2File;
class TH2OptionList;
class TH2Symbol;

class TH2Writer
{
    TH2File *m_th2file;
    QString m_filename;
    std::shared_ptr<QIODevice> m_outputFile;
    TH2Element *m_currentElement;
    QString m_currentParent;
    QString m_outputLine;
    QByteArray m_outputLinePrefix;
    QByteArray lineEnding;
    bool m_fileWithPrefix;
    bool m_encodingWritten;
    QByteArray m_output;

    const QString m_xtherionSettingPrefix = QStringLiteral("##XTHERION##");

    // Encoding support
    QTextCodec *codec;

    void writeArea();
    void writeComment();
    void writeEncoding();
    void writeInput();
    void writeLine();
    void writeLinePointBezier();
    void writeLinePointOptions();
    void writeLinePointStraight();
    void writeMultiLineComment();
    void writePoint();
    void writeScrap();
    void writeUnrecognized();
    void writeXTherionImageSetting();
    void writeXTherionSetting();
    void writeXTherionUnknownSetting();

    void writeOutput(bool withPrefix = true);

    QString optionListOptions(TH2OptionList *aOptionList);
    QString outputCoordinates(QPointF aCoordinates) const;
    QString formatedFloat(const qreal n) const;

    void openFile();
    void closeFile();

    bool writeList(QStringList aCodList);

    void setLineEnding();

public:
    TH2Writer(TH2File *aTH2File, QString outputFilename);

    bool write();
    bool fileWithPrefix() const;
    void setFileWithPrefix(bool newFileWithPrefix);
    const QByteArray &output() const;
};

#endif // TH2WRITER_H
