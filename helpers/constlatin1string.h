/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef CONSTLATIN1STRING_H
#define CONSTLATIN1STRING_H

#include <QLatin1String>

// From https://stackoverflow.com/a/56209811/11754455
struct ConstLatin1String : public QLatin1String
{
    constexpr ConstLatin1String(const char* const s) :
        QLatin1String(s, static_cast<int>(std::char_traits<char>::length(s))) {}

};

#endif // CONSTLATIN1STRING_H
