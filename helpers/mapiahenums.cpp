/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "mapiahenums.h"

const QString CR_LF_STRING = QStringLiteral("\r\n");
const QString CR_STRING = QStringLiteral("\r");
const QString LF_CR_STRING = QStringLiteral("\n\r");
const QString LF_STRING = QStringLiteral("\n");

const QString BRACKETS_STRING = QStringLiteral("[]");
const QString CLOSE_BRACKET_STRING = QStringLiteral("]");
const QString EMPTY_STRING = QStringLiteral("");
const QString OPEN_BRACKET_STRING = QStringLiteral("[");
const QString PLUS_STRING = QStringLiteral("+");
const QString QUOTE_IN_QUOTE_STRING = QStringLiteral(R"RX("")RX");
const QString QUOTE_STRING = QStringLiteral(R"RX(")RX");
const QString SPACE_STRING = QStringLiteral(" ");
const QString ZERO_STRING = QStringLiteral("0");

const QString AREA_ELEMENT_TYPE = QStringLiteral("area");
const QString COMMENT_ELEMENT_TYPE = QStringLiteral("comment");
const QString ENCODING_ELEMENT_TYPE = QStringLiteral("encoding");
const QString LINEPOINT_ELEMENT_TYPE = QStringLiteral("linepoint");
const QString LINE_ELEMENT_TYPE = QStringLiteral("line");
const QString MULTILINE_COMMENT_ELEMENT_TYPE = QStringLiteral("multilinecomment");
const QString POINT_ELEMENT_TYPE = QStringLiteral("point");
const QString SCRAP_ELEMENT_TYPE = QStringLiteral("scrap");

const QString XTHERION_SETTING_ID = QStringLiteral("xtherion");
const QString XTHERION_IMAGE_ELEMENT_TYPE = QStringLiteral("xtherionimagesetting");

const QString IMAGE_INSERT_XTHERION_SETTING = QStringLiteral("xth_me_image_insert");

const QString ENDAREA_KEYWORD = QStringLiteral("endarea");
const QString ENDLINE_KEYWORD = QStringLiteral("endline");
const QString ENDMULTILINE_COMMENT_KEYWORD = QStringLiteral("endcomment");
const QString ENDSCRAP_KEYWORD = QStringLiteral("endscrap");

const QString ALTITUDE_OPTION_ELEMENT_TYPE = QStringLiteral("altitudeoption");
const QString AUTHOR_OPTION_ELEMENT_TYPE = QStringLiteral("authoroption");
const QString CONTEXT_OPTION_ELEMENT_TYPE = QStringLiteral("contextoption");
const QString COPYRIGHT_OPTION_ELEMENT_TYPE = QStringLiteral("copyrightoption");
const QString DOUBLE_OPTION_ELEMENT_TYPE = QStringLiteral("doubleoption");
const QString EXTEND_OPTION_ELEMENT_TYPE = QStringLiteral("extendoption");
const QString FREETEXT_OPTION_ELEMENT_TYPE = QStringLiteral("freetextoption");
const QString LENGTH_OPTION_ELEMENT_TYPE = QStringLiteral("lengthoption");
const QString MARK_OPTION_ELEMENT_TYPE = QStringLiteral("markoption");
const QString ORIENTATION_OPTION_ELEMENT_TYPE = QStringLiteral("orientationoption");
const QString POINT_SCALE_OPTION_ELEMENT_TYPE = QStringLiteral("pointscaleoption");
const QString PROJECTION_OPTION_ELEMENT_TYPE = QStringLiteral("projectionoption");
const QString SCRAP_SCALE_OPTION_ELEMENT_TYPE = QStringLiteral("scrapscaleoption");
const QString SINGLE_SET_OPTION_ELEMENT_TYPE = QStringLiteral("singlesetoption");
const QString SKETCH_OPTION_ELEMENT_TYPE = QStringLiteral("sketchoption");
const QString STATION_NAMES_OPTION_ELEMENT_TYPE = QStringLiteral("stationnamesoption");
const QString SUBTYPE_OPTION_ELEMENT_TYPE = QStringLiteral("subtypeoption");
const QString VALUE_OPTION_ELEMENT_TYPE = QStringLiteral("valueoption");

const QString ALTITUDE_OPTION_NAME = QStringLiteral("altitude");
const QString AUTHOR_OPTION_NAME = QStringLiteral("author");
const QString CLIP_OPTION_NAME = QStringLiteral("clip");
const QString CONTEXT_OPTION_NAME = QStringLiteral("context");
const QString COPYRIGHT_OPTION_NAME = QStringLiteral("copyright");
const QString EXTEND_OPTION_NAME = QStringLiteral("extend");
const QString FREETEXT_OPTION_NAME = QStringLiteral("freetext");
const QString ID_OPTION_NAME = QStringLiteral("id");
const QString MARK_OPTION_NAME = QStringLiteral("mark");
const QString ORIENTATION_ALTERNATIVE_OPTION_NAME = QStringLiteral("orient");
const QString ORIENTATION_OPTION_NAME = QStringLiteral("orientation");
const QString POINT_SCALE_OPTION_NAME = QStringLiteral("scale");
const QString PROJECTION_OPTION_NAME = QStringLiteral("projection");
const QString SCRAP_SCALE_OPTION_NAME = QStringLiteral("scale");
const QString SKETCH_OPTION_NAME = QStringLiteral("sketch");
const QString STATION_OPTION_NAME = QStringLiteral("station");
const QString STATION_NAMES_OPTION_NAME = QStringLiteral("station-names");
const QString SUBTYPE_OPTION_NAME = QStringLiteral("subtype");
const QString VALUE_OPTION_NAME = QStringLiteral("value");

// TH options
const QString INPUT_ELEMENT_TYPE = QStringLiteral("input");

const QString ALTITUDE_POINT_TYPE = QStringLiteral("altitude");
const QString DATE_POINT_TYPE = QStringLiteral("date");
const QString DIMENSIONS_POINT_TYPE = QStringLiteral("dimensions");
const QString EXTRA_POINT_TYPE = QStringLiteral("extra");
const QString HEIGHT_POINT_TYPE = QStringLiteral("height");
const QString PASSAGE_HEIGHT_POINT_TYPE = QStringLiteral("passage-height");

const QString ELEVATION_PROJECTION_TYPE = QStringLiteral("elevation");
const QString NONE_PROJECTION_TYPE = QStringLiteral("none");
const QString PROJECTION_TYPE_INDEX_SEPARATOR = QStringLiteral(":");

const QString XVI_FILE_EXTENSION = QStringLiteral("xvi");

const QString FIX_KEYWORD = QStringLiteral("fix");

const QString UTF8_ENCODING = QStringLiteral("utf-8");

namespace MapiahEnums {

}
