/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "mapiahsupport.h"

namespace MapiahSupport
{
    /**
     * @brief TH2Writer::formatedFloat
     * @param n
     * @param maxDecimalPlaces
     * @return
     *
     * Returns as many decimal places as seem significant (as defined by
     * qFuzzyIsNull and qFussyCompare).
     */
    QString formatedFloatForWriting(qreal n, qint8 minIntPartSize)
    {
        QString asString;

        if (qFuzzyIsNull(n))
        {
            asString = QString("%1").arg(
                0,
                minIntPartSize,
                'f',
                0,
                QLatin1Char('0'));;
        }
        else
        {
            quint8 decimalPlaces = 0;
            qreal fromString;

            asString = QString("%1").arg(
                n,
                minIntPartSize,
                'f',
                decimalPlaces, QLatin1Char('0'));
            fromString = asString.toDouble();
            while(!qFuzzyCompare(n, fromString))
            {
                decimalPlaces++;
                asString = QString("%1").arg(
                    n,
                    minIntPartSize,
                    'f',
                    decimalPlaces,
                    QLatin1Char('0'));
                fromString = asString.toDouble();
            }
        }

        return asString;
    }
}
