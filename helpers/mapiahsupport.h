/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef MAPIAHSUPPORT_H
#define MAPIAHSUPPORT_H

#include "mapiahenums.h"

#include <QString>

#include <vector>
#include <algorithm>
#include <iterator>

namespace MapiahSupport
{
    QString formatedFloatForWriting(qreal n, qint8 minIntPartSize = 0);

    // std::vector helpers
    template<typename RandomIt, typename T = typename std::iterator_traits<RandomIt>::value_type, typename Val>
    T* find(RandomIt first, RandomIt last, const Val &aVal) {
        T* result = nullptr;
        auto iter = std::find(first, last, aVal);
        if (iter != last) {
            result = &(*iter);
        }
        return result;
    }

    template<typename T>
    T* find(const std::vector<T> &source, const T &aVal) {
        return find<typename std::vector<T>::iterator, T, T>(source.begin(), source.end(), aVal);
    }

    template<typename T>
    typename std::vector<T>::iterator::difference_type indexOf(const std::vector<T> &source, const T& aVal) {
        auto sBegin = source.begin();
        auto sEnd = source.end();
        auto iter = std::find(sBegin, sEnd, aVal);
        if (iter == sEnd) {
            return -1;
        }
        else{
            return iter - sBegin;
        }
    }

    template<typename T>
    bool contains(const std::vector<T> &source, const T &aVal) {
        auto sEnd = source.end();
        auto iter = std::find(source.begin(), sEnd, aVal);
        return (iter != sEnd);
    }


    template<typename T>
    std::vector<T> &insertAtBeginng(std::vector<T> &source, const T &aVal) {
        source.insert(source.begin(), aVal);
        return source;
    }
};

#endif // MAPIAHSUPPORT_H
