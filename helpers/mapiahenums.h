/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef MAPIAHENUMS_H
#define MAPIAHENUMS_H

#include <QString>

extern const QString CR_STRING;
extern const QString CR_LF_STRING;
extern const QString LF_STRING;
extern const QString LF_CR_STRING;

extern const QString BRACKETS_STRING;
extern const QString CLOSE_BRACKET_STRING;
extern const QString EMPTY_STRING;
extern const QString OPEN_BRACKET_STRING;
extern const QString PLUS_STRING;
extern const QString QUOTE_IN_QUOTE_STRING;
extern const QString QUOTE_STRING;
extern const QString SPACE_STRING;
extern const QString ZERO_STRING;

extern const QString AREA_ELEMENT_TYPE;
extern const QString COMMENT_ELEMENT_TYPE;
extern const QString ENCODING_ELEMENT_TYPE;
extern const QString LINEPOINT_ELEMENT_TYPE;
extern const QString LINE_ELEMENT_TYPE;
extern const QString MULTILINE_COMMENT_ELEMENT_TYPE;
extern const QString POINT_ELEMENT_TYPE;
extern const QString SCRAP_ELEMENT_TYPE;

extern const QString XTHERION_SETTING_ID;
extern const QString XTHERION_IMAGE_ELEMENT_TYPE;

extern const QString IMAGE_INSERT_XTHERION_SETTING;

extern const QString ENDAREA_KEYWORD;
extern const QString ENDLINE_KEYWORD;
extern const QString ENDMULTILINE_COMMENT_KEYWORD;
extern const QString ENDSCRAP_KEYWORD;

extern const QString ALTITUDE_OPTION_ELEMENT_TYPE;
extern const QString AUTHOR_OPTION_ELEMENT_TYPE;
extern const QString CONTEXT_OPTION_ELEMENT_TYPE;
extern const QString COPYRIGHT_OPTION_ELEMENT_TYPE;
extern const QString DOUBLE_OPTION_ELEMENT_TYPE;
extern const QString EXTEND_OPTION_ELEMENT_TYPE;
extern const QString FREETEXT_OPTION_ELEMENT_TYPE;
extern const QString LENGTH_OPTION_ELEMENT_TYPE;
extern const QString MARK_OPTION_ELEMENT_TYPE;
extern const QString ORIENTATION_OPTION_ELEMENT_TYPE;
extern const QString POINT_SCALE_OPTION_ELEMENT_TYPE;
extern const QString PROJECTION_OPTION_ELEMENT_TYPE;
extern const QString SCRAP_SCALE_OPTION_ELEMENT_TYPE;
extern const QString SINGLE_SET_OPTION_ELEMENT_TYPE;
extern const QString SKETCH_OPTION_ELEMENT_TYPE;
extern const QString STATION_NAMES_OPTION_ELEMENT_TYPE;
extern const QString SUBTYPE_OPTION_ELEMENT_TYPE;
extern const QString VALUE_OPTION_ELEMENT_TYPE;

extern const QString ALTITUDE_OPTION_NAME;
extern const QString AUTHOR_OPTION_NAME;
extern const QString CLIP_OPTION_NAME;
extern const QString CONTEXT_OPTION_NAME;
extern const QString COPYRIGHT_OPTION_NAME;
extern const QString EXTEND_OPTION_NAME;
extern const QString FREETEXT_OPTION_NAME;
extern const QString ID_OPTION_NAME;
extern const QString MARK_OPTION_NAME;
extern const QString ORIENTATION_ALTERNATIVE_OPTION_NAME;
extern const QString ORIENTATION_OPTION_NAME;
extern const QString POINT_SCALE_OPTION_NAME;
extern const QString PROJECTION_OPTION_NAME;
extern const QString SCRAP_SCALE_OPTION_NAME;
extern const QString SKETCH_OPTION_NAME;
extern const QString STATION_OPTION_NAME;
extern const QString STATION_NAMES_OPTION_NAME;
extern const QString SUBTYPE_OPTION_NAME;
extern const QString VALUE_OPTION_NAME;

// TH options
extern const QString INPUT_ELEMENT_TYPE;

extern const QString ALTITUDE_POINT_TYPE;
extern const QString DATE_POINT_TYPE;
extern const QString DIMENSIONS_POINT_TYPE;
extern const QString EXTRA_POINT_TYPE;
extern const QString HEIGHT_POINT_TYPE;
extern const QString PASSAGE_HEIGHT_POINT_TYPE;

extern const QString EXTEND_PREVIOUS_KEYWORD;
extern const QString EXTEND_PREVIOUS_ALTERNATE_KEYWORD;

extern const QString ELEVATION_PROJECTION_TYPE;
extern const QString NONE_PROJECTION_TYPE;
extern const QString PROJECTION_TYPE_INDEX_SEPARATOR;

extern const QString XVI_FILE_EXTENSION;

extern const QString FIX_KEYWORD;

extern const QString UTF8_ENCODING;

namespace MapiahEnums {
    enum class LineEndingType {
        Unset,
        LF,
        CR,
        LFCR,
        CRLF
    };
};

#endif // MAPIAHENUMS_H
