#!/bin/bash

# To be run on a directory with the new release, the desktop file 
# and the PNG icon.
QMAKE=/home/rodrigo/Qt-6.2.2/6.2.2/gcc_64/bin/qmake \
    LD_LIBRARY_PATH=~/Qt-6.2.2/6.2.2/gcc_64/lib \
    linuxdeploy-x86_64.AppImage \
        -d Mapiah.desktop \
        -i Mapiah.png \
        -e Mapiah \
        -p qt \
        -o appimage \
        --appdir=AppDir \
        -l /lib/x86_64-linux-gnu/libOpenGL.so.0
