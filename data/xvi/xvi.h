/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef XVIFILE_H
#define XVIFILE_H

#include <QList>
#include <QPointF>
#include <QString>

namespace XVI
{
    struct InitialGrid
    {
        double size;
        char unit;
    };

    struct Point
    {
        double x;
        double y;
    };

    struct Station
    {
        Point coordinates;
        std::string name;
    };

    typedef std::vector<Station> station_vec_t;

    struct Images
    {
    };

    struct Shot
    {
        Point from;
        Point to;
    };

    typedef std::vector<Shot> shot_vec_t;

    typedef std::vector<Point> point_vec_t;

    struct SketchLine
    {
        std::string color;
        point_vec_t points;
    };

    typedef std::vector<SketchLine> sketchline_vec_t;

    // Copied from Writeth2.py by Andrew Atkinson:
    //
    // Grid is {bottom left x, bottom left y,
    // x1 dist, y1 dist, x2 dist, y2 dist, number of x, number of y}
    // pXVIfile.write(str((maxmin['minx'] - 1) * cfactor)
    //                 + " " + str((maxmin['miny'] - 1) * cfactor)
    //                 + " " + str(grid * cfactor)
    //                 + " 0.0 0.0 " + str(grid * cfactor)
    //                 + " " + xsquares + " " + ysquares + "}")
    struct FinalGrid
    {
        Point bottom;
        double x_dist_1;
        double y_dist_1;
        double x_dist_2;
        double y_dist_2;
        int qt_x;
        int qt_y;
    };

    struct File
    {
        InitialGrid initialGrid;
        station_vec_t stations;
        Images images;
        shot_vec_t shots;
        sketchline_vec_t sketchlines;
        FinalGrid finalGrid;
    };

    QList<QPointF> *toQListOfQPointfs(const point_vec_t *aPointVector);
}

#endif // XVIFILE_H
