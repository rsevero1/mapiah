/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef INPUTFILE_H
#define INPUTFILE_H

#include "th2/th2element.h"
#include "th2/th2typedefs.h"

#include "../helpers/mapiahenums.h"

#include <QMultiHash>
#include <QString>
#include <QStringList>

#include <unordered_map>

class TH2Area;
class TH2Line;
class TH2LinePoint;
class TH2Option;

class QGraphicsScene;

class InputFile
{
    qint32 m_lastCodNumber = 0;

    QStringList m_elementCods;
    QStringList m_unrecognizedLineCods;

    QMultiHash<QString, QString> m_missingLinesOfAreas;

    typedef std::unordered_map<QString, th2element_up> elementByCodMap_t;
    elementByCodMap_t m_elementByCod;

    QMultiHash<QString, QString> m_codsByID;

    QString m_filename;

    QString m_encoding;

    MapiahEnums::LineEndingType m_lineEndingType = MapiahEnums::LineEndingType::LF;

    QString uniqueCod(TH2Element *newElement);

public:
    InputFile();

    void appendElement(th2element_up uniqueElement);
    void appendUnrecognizedLine(th2element_up uniqueUnrecognizedLine);
    void registerElement(th2element_up uniqueElement);
    bool registerID(const QString id, const QString cod);
    TH2Element *elementByID(const QString &aID) const;
    QString codByID(const QString &aID) const;
    bool exists(const QString &cod) const;
    bool existsByID(const QString &aID) const;
    const QString &filename() const;
    void setFilename(const QString &newFileName);

    TH2Element *element(const QString &cod) const;
    TH2Line *line(const QString &cod) const;
    TH2Line *lineByID(const QString &aID) const;
    TH2LinePoint *linePoint(const QString &cod) const;
    TH2Option *option(const QString &cod) const;
    TH2Area *area(const QString &cod) const;

    const QStringList &elementCods() const;
    qsizetype count() const;
    qsizetype countRegisteredElements() const;
    const QMultiHash<QString, QString> &missingLinesOfAreas() const;
    qsizetype countMissingLinesOfAreas() const;
    void addMissingLineOfArea(const QString &aLineID, const QString &aAreaCod);
    void removeMissingLineOfArea(const QString &aLineID);

    void draw(QGraphicsScene *aScene) const;
    MapiahEnums::LineEndingType lineEndingType() const;
    void setLineEndingType(MapiahEnums::LineEndingType newLineEndingType);
    const QString &encoding() const;
    void setEncoding(const QString &newEncoding);
};

#endif // INPUTFILE_H
