/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef THINPUT_H
#define THINPUT_H

#include "../th2/th2element.h"

#include "../th2/value/th2freetextvalue.h"

#include "../../helpers/mapiahenums.h"

#include "../../factories/th2factory.h"

class THInput : public TH2Element
{
protected:
    static const QString FILENAME_VALUE_NAME;

    TH2FreeTextValue m_filename;

    THInput(TH2Element *parent = nullptr, TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<THInput>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:    
    const TH2FreeTextValue &filename() const;
    QString filenameValue() const;
    bool setFilenameValue(const QString &newFilenameValue);

    // TH2Element interface
    const QString elementType() override;

    QString valueForFile() const;
};

#endif // THINPUT_H
