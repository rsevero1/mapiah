/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "thinput.h"

const QString THInput::FILENAME_VALUE_NAME = QStringLiteral("input-filename");

const TH2FreeTextValue &THInput::filename() const
{
    return m_filename;
}

QString THInput::filenameValue() const
{
    return m_filename.textValue();
}
bool THInput::setFilenameValue(const QString &newFilenameValue)
{
    return m_filename.setTextValue(newFilenameValue);
}

const QString THInput::elementType()
{
    return INPUT_ELEMENT_TYPE;
}

QString THInput::valueForFile() const
{
    return QString("%1 %2").arg(
        INPUT_ELEMENT_TYPE,
        m_filename.valueForFile());
}

THInput::THInput(TH2Element *parent, TH2File *th2file)
    : TH2Element(parent, th2file)
    , m_filename(this, FILENAME_VALUE_NAME)
{

}
