/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2linepoint.h"

#include "option/th2option.h"
#include "option/th2freetextoption.h"
#include "option/th2markoption.h"
#include "option/th2singlesetoption.h"

// Line point option types
//
// name | data-type | supported line types
//
// adjust | singleset | all*
// altitude | altitude | wall*
// direction | ONLY point | section*
// gradient | singleset | contour*
// l-size | double | all
// mark | mark | all*
// orientation | double limited | all
// r-size | double | all
// size | double | all
// smooth | singleset | all*
// subtype | subtype | wall, border, water-flow, survey, u

const TH2SupportedOptionNameCheckFuncMapType TH2LinePoint::SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP
{
    {QStringLiteral("adjust"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("altitude"), &TH2Option::isSupportedOption},
    {QStringLiteral("direction"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("gradient"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("l-size"), &TH2Option::isSupportedOption},
    {QStringLiteral("mark"), &TH2MarkOption::isSupportedOption},
    {QStringLiteral("orientation"), &TH2Option::isSupportedOption},
    {QStringLiteral("r-size"), &TH2Option::isSupportedOption},
    {QStringLiteral("size"), &TH2Option::isSupportedOption},
    {QStringLiteral("smooth"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("subtype"), &TH2Option::isSupportedOption},
};

QPointF TH2LinePoint::xy() const
{
    return m_xy;
}

void TH2LinePoint::setXY(const QPointF &newXY)
{
    m_xy = newXY;
}

bool TH2LinePoint::isLinePoint()
{
    return true;
}

TH2LinePoint::TH2LinePoint(TH2Element *parent, TH2File *th2file)
    : TH2Element(parent, th2file), TH2OptionList(th2file)
{

}
