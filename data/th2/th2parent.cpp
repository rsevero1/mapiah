/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2parent.h"

#include "th2file.h"

#include <memory>

const QStringList &TH2Parent::childCods() const
{
    return m_childCods;
}

TH2Parent::TH2Parent(TH2Element *parent, TH2File *th2file)
    : TH2Symbol(parent, th2file)
{

}

void TH2Parent::appendElement(th2element_up uniqueElement)
{
    TH2Element *newElement = uniqueElement.get();
    th2File()->registerElement(std::move(uniqueElement));
    m_childCods.append(newElement->cod());
}

qsizetype TH2Parent::count() const
{
    return m_childCods.count();
}

void TH2Parent::draw(QGraphicsScene *aScene)
{
    for(auto &aElementCod : m_childCods)
    {
        m_th2file->element(aElementCod)->draw(aScene);
    }
}
