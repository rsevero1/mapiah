/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2XTHERIONSETTING_H
#define TH2XTHERIONSETTING_H

#include "th2element.h"

#include <QString>

class TH2File;

class TH2XTherionSetting : public TH2Element
{
protected:
    QString m_xtherionParameters;

public:
    TH2XTherionSetting(
        QString aParameters,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    const QString xtherionParameters() const;

    const QString elementType() override;

    virtual const QString setting() const = 0;
};

#endif // TH2XTHERIONSETTING_H
