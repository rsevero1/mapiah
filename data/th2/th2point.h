/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2POINT_H
#define TH2POINT_H

#include "option/th2optionlist.h"
#include "th2symbol.h"

#include "th2typedefs.h"

#include "../../factories/th2factory.h"

#include <QGraphicsScene>
#include <QPointF>
#include <QString>

#include <unordered_map>
#include <unordered_set>

class TH2File;

class TH2Point : public TH2Symbol, public TH2OptionList
{
    QPointF m_xy;

protected:
    explicit TH2Point(
        QString aType,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createPoint(
        QString aType,
        QPointF coordinates,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    const QString elementType() override;
    const QPointF xy() const;
    void setXY(const QPointF &newXY);

    void draw(QGraphicsScene *aScene) override;

    // TH2Element interface
    bool isPoint() override;

    static bool isSupportedPointType(const QString &aType);
    static const TH2SupportedTypeListType &supportedTypes();

    static const TH2SupportedOptionNameCheckFuncMapType SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP;
    static const TH2SupportedTypeListType SUPPORTED_SYMBOL_TYPES_LIST;

    static bool isSupportedOptionType(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);
};

#endif // TH2POINT_H
