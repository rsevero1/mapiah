/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2ELEMENT_H
#define TH2ELEMENT_H

#include "../../helpers/errormessage.h"

#include <QGraphicsScene>
#include <QString>

class TH2File;

class TH2Element
{
protected:
    QString m_cod;
    qint32 m_lineNumber = 0;

    TH2Element *m_parent;
    TH2File *m_th2file;

public:
    explicit TH2Element(TH2Element *parent = nullptr, TH2File *th2file = nullptr);

    // Always make base class destructor as virtual in C++
    // https://codeyarns.com/tech/2016-12-28-always-make-base-class-destructor-as-virtual-in-c.html
    virtual ~TH2Element() {};

    QString cod() const;
    void setCod(const QString newCod);

    virtual const QString elementType() = 0;

    qint32 lineNumber() const;

    TH2File *th2File() const;

    TH2Element *parent() const;
    void setParent(TH2Element *newParent);

    void virtual draw(QGraphicsScene *aScene);

    virtual void valueChanged(const QString &valueName);

    virtual bool isPoint();
    virtual bool isLine();
    virtual bool isArea();
    virtual bool isScrap();
    virtual bool isLinePoint();
};

#endif // TH2ELEMENT_H
