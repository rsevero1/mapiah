/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2symbol.h"

#include "option/th2optionlist.h"

const QString &TH2Symbol::type() const
{
    return m_type;
}

void TH2Symbol::setType(const QString &newType)
{
    m_type = newType;
}

TH2Symbol::TH2Symbol(TH2Element *parent, TH2File *th2file)
    : TH2Element(parent, th2file)
{

}
