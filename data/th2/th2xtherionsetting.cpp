/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2xtherionsetting.h"

#include <QString>

const QString TH2XTherionSetting::xtherionParameters() const
{
    return m_xtherionParameters;
}

const QString TH2XTherionSetting::elementType()
{
    return "xtherionsetting";
}

TH2XTherionSetting::TH2XTherionSetting(
    QString aParameters,
    TH2Element *parent,
    TH2File *th2file)
    : TH2Element(parent, th2file)
    , m_xtherionParameters(aParameters)
{

}
