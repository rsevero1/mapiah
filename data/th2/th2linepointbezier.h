/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2LINEPOINTBEZIER_H
#define TH2LINEPOINTBEZIER_H

#include "th2linepoint.h"

#include "../../factories/th2factory.h"

#include <QPointF>
#include <QString>

class TH2File;

class TH2LinePointBezier : public TH2LinePoint
{
    QPointF m_c1;
    QPointF m_c2;

    friend th2element_up TH2Factory::createLinePointBezier(
        QPointF c1,
        QPointF c2,
        QPointF coordinates,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

protected:
    explicit TH2LinePointBezier(TH2Element *parent = nullptr, TH2File *th2file = nullptr);

public:
    const QString elementType() override;

    QPointF c1() const;
    void setC1(const QPointF &newC1);
    QPointF c2() const;
    void setC2(const QPointF &newC2);
};

#endif // TH2LINEPOINTBEZIER_H
