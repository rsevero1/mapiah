/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2scrap.h"

#include "th2file.h"

#include "option/th2authoroption.h"
#include "option/th2copyrightoption.h"
#include "option/th2freetextoption.h"
#include "option/th2projectionoption.h"
#include "option/th2scrapscaleoption.h"
#include "option/th2singlesetoption.h"
#include "option/th2sketchoption.h"
#include "option/th2stationnamesoption.h"

#include "../../helpers/mapiahenums.h"

// Scrap options
//
//author
//copyright
//cs
//flip
//projection
//sketch
//station-names
//stations
//title
//walls

// Scrap option types
//
// name | data-type | supported line types
//
// author | author | all*
// copyright | copyright | all*
// cs | freetext | all*
// flip | singleset | all*
// projection | projection | all*
// scale | scale | all*
// sketch | sketch | all*
// station-names | station-names | all*
// stations | freetext | all*
// title | freetext | all*
// walls | singleset | all*

const TH2SupportedOptionNameCheckFuncMapType TH2Scrap::SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP
{
    {QStringLiteral("author"), &TH2AuthorOption::isSupportedOption},
    {QStringLiteral("copyright"), &TH2CopyrightOption::isSupportedOption},
    {QStringLiteral("cs"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("flip"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("projection"), &TH2ProjectionOption::isSupportedOption},
    {QStringLiteral("scale"), &TH2ScrapScaleOption::isSupportedOption},
    {QStringLiteral("sketch"), &TH2SketchOption::isSupportedOption},
    {QStringLiteral("station-names"), &TH2StationNamesOption::isSupportedOption},
    {QStringLiteral("stations"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("title"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("walls"), &TH2SingleSetOption::isSupportedOption},
};

const QString &TH2Scrap::id() const
{
    return m_id;
}

void TH2Scrap::setID(const QString &newID)
{
    m_id = newID;
}

bool TH2Scrap::isScrap()
{
    return true;
}

void TH2Scrap::draw(QGraphicsScene *aScene)
{
    for(auto &sketchCod : m_images)
    {
        TH2SketchOption *aSketch =
            dynamic_cast<TH2SketchOption *>(th2File()->element(sketchCod));
        TH2GraphicsImage *aGraphicsImage =
            TH2Factory::createGraphicsImage(
                aSketch->filenameValue(),
                aSketch->xNumericValue(),
                aSketch->yNumericValue(),
                aSketch);

        aScene->addItem(aGraphicsImage);
        aGraphicsImage->dataChanged();

        aSketch->setImage(aGraphicsImage);
    }

    TH2Parent::draw(aScene);
}

void TH2Scrap::appendOption(th2element_up uniqueOption)
{
    TH2Option *newOption = dynamic_cast<TH2Option *>(uniqueOption.get());

    TH2OptionList::appendOption(std::move(uniqueOption));

    if (newOption->name() == SKETCH_OPTION_NAME)
    {
        addImage(newOption->cod());
    }
}

const QStringList &TH2Scrap::getImages() const
{
    return m_images;
}

void TH2Scrap::addImage(const QString &aCod)
{
    m_images.append(aCod);
}

TH2Scrap::TH2Scrap(QString aID, TH2Element *parent, TH2File *th2file)
    : TH2Parent(parent, th2file), TH2OptionList(th2file), m_id(aID)
{

}

const QString TH2Scrap::elementType()
{
    return SCRAP_ELEMENT_TYPE;
}
