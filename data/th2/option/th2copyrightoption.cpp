/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2copyrightoption.h"

const QString TH2CopyrightOption::DATE_VALUE_NAME = QStringLiteral("copyright-date");
const QString TH2CopyrightOption::NOTICE_VALUE_NAME = QStringLiteral("copyright-notice");

TH2DateValue &TH2CopyrightOption::date()
{
    return m_date;
}

QString TH2CopyrightOption::dateText() const
{
    return m_date.valueForFile();
}

bool TH2CopyrightOption::setDateFromText(const QString &newDateText)
{
    return m_date.setDateTimeFromText(newDateText);
}

TH2FreeTextValue &TH2CopyrightOption::notice()
{
    return m_notice;
}

QString TH2CopyrightOption::noticeText() const
{
    return m_notice.textValue();
}

bool TH2CopyrightOption::setNoticeText(const QString &newNoticeText)
{
    return m_notice.setTextValue(newNoticeText);
}

const QString TH2CopyrightOption::elementType()
{
    return COPYRIGHT_OPTION_ELEMENT_TYPE;
}

QString TH2CopyrightOption::valueForFile() const
{
    return QString("%1 %2").arg(
        m_date.valueForFile(),
                m_notice.valueForFile());
}

bool TH2CopyrightOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &,
    const QString &)
{
    return (aParentElementType == SCRAP_ELEMENT_TYPE);
}

TH2CopyrightOption::TH2CopyrightOption(TH2Element *parent, TH2File *th2file)
    : TH2Option(COPYRIGHT_OPTION_NAME, parent, th2file)
    , m_date(this, DATE_VALUE_NAME)
    , m_notice(this, NOTICE_VALUE_NAME)
{

}
