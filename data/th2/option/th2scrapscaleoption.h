/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2SCRAPSCALEOPTION_H
#define TH2SCRAPSCALEOPTION_H

#include "th2option.h"

#include "../value/th2lengthunit.h"
#include "../value/th2unlimiteddoublevalue.h"

#include <QString>

class TH2ScrapScaleOption : public TH2Option
{
protected:
    static const QString DRAWING_UNIT_SIZE_IN_REALITY_VALUE_NAME;
    static const QString AMOUNT_OF_DRAWING_UNITS_VALUE_NAME;
    static const QString X1_SCRAP_VALUE_NAME;
    static const QString Y1_SCRAP_VALUE_NAME;
    static const QString X2_SCRAP_VALUE_NAME;
    static const QString Y2_SCRAP_VALUE_NAME;
    static const QString X1_REALITY_VALUE_NAME;
    static const QString Y1_REALITY_VALUE_NAME;
    static const QString X2_REALITY_VALUE_NAME;
    static const QString Y2_REALITY_VALUE_NAME;
    static const QString LENGTH_UNIT_VALUE_NAME;

    TH2UnlimitedDoubleValue m_drawingUnitSizeInReality;
    TH2UnlimitedDoubleValue m_amountOfDrawingUnits;
    TH2UnlimitedDoubleValue m_x1Scrap;
    TH2UnlimitedDoubleValue m_y1Scrap;
    TH2UnlimitedDoubleValue m_x2Scrap;
    TH2UnlimitedDoubleValue m_y2Scrap;
    TH2UnlimitedDoubleValue m_x1Reality;
    TH2UnlimitedDoubleValue m_y1Reality;
    TH2UnlimitedDoubleValue m_x2Reality;
    TH2UnlimitedDoubleValue m_y2Reality;
    TH2LengthUnit m_lengthUnit;

    TH2ScrapScaleOption(
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2ScrapScaleOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    enum class Form {
        UNSET,
        SizeOfDrawingUnitInReality,
        AmountOfDrawingUnitsPerSizeInReality,
        Complete
    };

    const TH2UnlimitedDoubleValue &drawingUnitSizeInReality() const;
    TH2DoubleValueNumberType drawingUnitSizeInRealityNumericValue() const;
    bool setDrawingUnitSizeInRealityNumericValue(TH2DoubleValueNumberType aValue);
    bool setDrawingUnitSizeInRealityByText(const QString &aValueText);

    const TH2UnlimitedDoubleValue &amountOfDrawingUnits() const;
    TH2DoubleValueNumberType amountOfDrawingUnitsNumericValue() const;
    bool setAmountOfDrawingUnitsNumericValue(TH2DoubleValueNumberType aValue);
    bool setAmountOfDrawingUnitsByText(const QString &aValueText);

    const TH2UnlimitedDoubleValue &x1Scrap() const;
    TH2DoubleValueNumberType x1ScrapNumericValue() const;
    bool setX1ScrapNumericValue(TH2DoubleValueNumberType aValue);
    bool setX1ScrapByText(const QString &aValueText);

    const TH2UnlimitedDoubleValue &y1Scrap() const;
    TH2DoubleValueNumberType y1ScrapNumericValue() const;
    bool setY1ScrapNumericValue(TH2DoubleValueNumberType aValue);
    bool setY1ScrapByText(const QString &aValueText);

    const TH2UnlimitedDoubleValue &x2Scrap() const;
    TH2DoubleValueNumberType x2ScrapNumericValue() const;
    bool setX2ScrapNumericValue(TH2DoubleValueNumberType aValue);
    bool setX2ScrapByText(const QString &aValueText);

    const TH2UnlimitedDoubleValue &y2Scrap() const;
    TH2DoubleValueNumberType y2ScrapNumericValue() const;
    bool setY2ScrapNumericValue(TH2DoubleValueNumberType aValue);
    bool setY2ScrapByText(const QString &aValueText);

    const TH2UnlimitedDoubleValue &x1Reality() const;
    TH2DoubleValueNumberType x1RealityNumericValue() const;
    bool setX1RealityNumericValue(TH2DoubleValueNumberType aValue);
    bool setX1RealityByText(const QString &aValueText);

    const TH2UnlimitedDoubleValue &y1Reality() const;
    TH2DoubleValueNumberType y1RealityNumericValue() const;
    bool setY1RealityNumericValue(TH2DoubleValueNumberType aValue);
    bool setY1RealityByText(const QString &aValueText);

    const TH2UnlimitedDoubleValue &x2Reality() const;
    TH2DoubleValueNumberType x2RealityNumericValue() const;
    bool setX2RealityNumericValue(TH2DoubleValueNumberType aValue);
    bool setX2RealityByText(const QString &aValueText);

    const TH2UnlimitedDoubleValue &y2Reality() const;
    TH2DoubleValueNumberType y2RealityNumericValue() const;
    bool setY2RealityNumericValue(TH2DoubleValueNumberType aValue);
    bool setY2RealityByText(const QString &aValueText);

    const TH2LengthUnit &lengthUnit() const;
    TH2ValuesListSizeType lengthUnitID() const;
    QString lengthUnitText() const;
    bool setLengthUnitByID(TH2ValuesListSizeType aID);
    bool setLengthUnitByText(const QString &aText);

    Form form() const;
    void setForm(Form newForm);

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

protected:
    Form m_form;

};

#endif // TH2SCRAPSCALEOPTION_H
