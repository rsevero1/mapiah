/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2DOUBLEOPTION_H
#define TH2DOUBLEOPTION_H

#include "th2option.h"

#include "../value/th2doublevalue.h"

class TH2DoubleOption : public TH2Option
{
protected:
    TH2DoubleValue m_value;
    const TH2DoubleOptionDef &m_doubleOptionDef;

    explicit TH2DoubleOption(
        const TH2DoubleOptionDef &aDoubleOptionDef,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createTypedElement<TH2DoubleOption, TH2DoubleOptionDef>(
        const TH2DoubleOptionDef &aDoubleOptionDef,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    TH2DoubleValueNumberType numericValue() const;
    bool setNumericValue(TH2DoubleValueNumberType newDoubleValue);
    bool setNumericValueByText(const QString &newNumericValueText);

    // TH2Option interface
    QString valueForFile() const override;

    // TH2Element interface
    const QString elementType() override;

    const TH2DoubleValue &value() const;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);

};

#endif // TH2DOUBLEOPTION_H
