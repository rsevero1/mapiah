/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2projectionoption.h"

const QString TH2ProjectionOption::ELEVATION_AZIMUTH_VALUE_NAME = QStringLiteral("projection-elevation-direction");
const QString TH2ProjectionOption::INDEX_VALUE_NAME = QStringLiteral("projection-index");
const QString TH2ProjectionOption::PROJECTION_TYPE_VALUE_NAME = QStringLiteral("projection-type");

const TH2AzimuthValue &TH2ProjectionOption::elevationDirection() const
{
    return m_elevationDirection;
}

TH2DoubleValueNumberType TH2ProjectionOption::elevationDirectionNumericValue() const
{
    return m_elevationDirection.numericValue();
}

bool TH2ProjectionOption::setElevationDirectionNumericValueByValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_elevationDirection.setNumericValue(newNumericValue);
}

bool TH2ProjectionOption::setElevationDirectionNumericValueByText(const QString &newNumericValueText)
{
    return m_elevationDirection.setNumericValueByText(newNumericValueText);
}

QString TH2ProjectionOption::elevationDirectionUnitText() const
{
    return m_elevationDirection.unitText();
}

TH2ValuesListSizeType TH2ProjectionOption::elevationDirectionUnitID() const
{
    return m_elevationDirection.unitID();
}

bool TH2ProjectionOption::setElevationDirectionUnitByID(TH2ValuesListSizeType newUnitID)
{
    return m_elevationDirection.setUnitByID(newUnitID);
}

bool TH2ProjectionOption::setElevationDirectionUnitByText(const QString &newUnitText)
{
    return m_elevationDirection.setUnitByText(newUnitText);
}

const TH2KeywordValue &TH2ProjectionOption::index() const
{
    return m_index;
}

QString TH2ProjectionOption::indexText() const
{
    return m_index.keywordValue();
}

bool TH2ProjectionOption::setIndexText(const QString &newKeywordText)
{
    return m_index.setKeywordValue(newKeywordText);
}

const TH2SingleSetValue &TH2ProjectionOption::projectionType() const
{
    return m_projectionType;
}

QString TH2ProjectionOption::projectionTypeText() const
{
    return m_projectionType.singleSetValueText();
}

TH2ValuesListSizeType TH2ProjectionOption::projectionTypeID() const
{
    return m_projectionType.singleSetValueID();
}

bool TH2ProjectionOption::setProjectionTypeByText(const QString &newProjectionTypeText)
{
    return m_projectionType.setSingleSetValueByText(newProjectionTypeText);
}

bool TH2ProjectionOption::setProjectionTypeByID(TH2ValuesListSizeType newProjectionTypeID)
{
    return m_projectionType.setSingleSetValueByID(newProjectionTypeID);
}

const QString TH2ProjectionOption::elementType()
{
    return PROJECTION_OPTION_ELEMENT_TYPE;
}

QString TH2ProjectionOption::valueForFile() const
{
    QString value = m_index.keywordValue().isEmpty() ?
        m_projectionType.singleSetValueText() :
        QString("%1%2%3").arg(
            m_projectionType.singleSetValueText(),
            PROJECTION_TYPE_INDEX_SEPARATOR,
            m_index.keywordValue());

    if (m_hasElevationDirection
        && (m_projectionType.singleSetValueText() == ELEVATION_PROJECTION_TYPE))
    {
        value.append(QString(" %1").arg(m_elevationDirection.numericValue()));

        if (!m_elevationDirection.isDefaultUnit())
        {
            value.append(QString(" %1").arg(m_elevationDirection.unitText()));
        }

        value = QString("[%1]").arg(value);
    }

    return value;
}

bool TH2ProjectionOption::hasElevationDirection() const
{
    return m_hasElevationDirection;
}

bool TH2ProjectionOption::setHasElevationDirection(bool newHasElevationDirection)
{
    m_hasElevationDirection = newHasElevationDirection;

    return true;
}

TH2ProjectionOption::TH2ProjectionOption(TH2Element *parent, TH2File *th2file)
    : TH2Option(PROJECTION_OPTION_NAME, parent, th2file)
    , m_elevationDirection(this, ELEVATION_AZIMUTH_VALUE_NAME)
    , m_index(this, INDEX_VALUE_NAME)
    , m_projectionType(
        this,
        PROJECTION_TYPE_VALUE_NAME,
        TH2_PROJECTION_VALUE_DEF)
    , m_hasElevationDirection(false)

{

}
