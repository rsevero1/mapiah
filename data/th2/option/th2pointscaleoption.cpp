/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2pointscaleoption.h"

#include "../../../helpers/mapiahsupport.h"

const QString TH2PointScaleOption::VALUE_NAME_NUMBER =
    QStringLiteral("scale-numeric");
const QString TH2PointScaleOption::VALUE_NAME_PRESET =
    QStringLiteral("scale-preset");

bool TH2PointScaleOption::isNumeric() const
{
    return m_isNumeric;
}

TH2UnlimitedDoubleValue &TH2PointScaleOption::number()
{
    return m_number;
}

TH2DoubleValueNumberType TH2PointScaleOption::numericValue() const
{
    return m_number.doubleValue();
}

TH2SingleSetValue &TH2PointScaleOption::preset()
{
    return m_preset;
}

TH2ValuesListSizeType TH2PointScaleOption::presetID() const
{
    return m_preset.singleSetValueID();
}

const QString &TH2PointScaleOption::presetText() const
{
    return m_preset.singleSetValueText();
}

bool TH2PointScaleOption::setPresetByID(TH2ValuesListSizeType newPresetID)
{
    return m_preset.setSingleSetValueByID(newPresetID);
}

bool TH2PointScaleOption::setPresetByText(const QString &newPresetText)
{
    return m_preset.setSingleSetValueByText(newPresetText);
}

bool TH2PointScaleOption::setCurrentValueByText(const QString &newValue)
{
    if (setNumericValueByText(newValue))
    {
        return true;
    }

    return setPresetByText(newValue);
}

bool TH2PointScaleOption::setNumericValue(TH2DoubleValueNumberType newNumeric)
{
    return m_number.setDoubleValue(newNumeric);
}

bool TH2PointScaleOption::setNumericValueByText(const QString &newNumericText)
{
    return m_number.setDoubleValueByText(newNumericText);
}

const QString TH2PointScaleOption::elementType()
{
    return QStringLiteral("pointscaleoption");
}

QString TH2PointScaleOption::valueForFile() const
{
   return m_isNumeric
       ? m_number.valueForFile()
       : m_preset.valueForFile();
}

void TH2PointScaleOption::valueChanged(const QString &valueName)
{
    m_isNumeric = (valueName.startsWith(VALUE_NAME_NUMBER));
}

bool TH2PointScaleOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &,
    const QString &)
{
    return (aParentElementType == POINT_ELEMENT_TYPE);
}

TH2PointScaleOption::TH2PointScaleOption(
    TH2Element *parent,
    TH2File *th2file)
    : TH2Option(POINT_SCALE_OPTION_NAME, parent, th2file)
    , m_number(this, VALUE_NAME_NUMBER)
    , m_preset(this, VALUE_NAME_PRESET, TH2_POINT_SCALE_VALUE_DEF)
    , m_isNumeric(false)
{

}
