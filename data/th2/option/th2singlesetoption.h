/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2SINGLESETOPTION_H
#define TH2SINGLESETOPTION_H

#include "th2option.h"

#include "../value/th2singlesetvalue.h"
#include "../value/th2singlesetvaluedef.h"

#include "../th2typedefs.h"

#include "../../../factories/th2factory.h"

#include <QString>

class TH2File;

class TH2SingleSetOption : public TH2Option
{
private:
    TH2SingleSetValue m_value;

protected:
    static const QString VALUE_NAME;

    TH2SingleSetOption(
        const TH2SingleSetValueDef &aSingleSetValueDef,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createTypedElement<TH2SingleSetOption, TH2SingleSetValueDef>(
        const TH2SingleSetValueDef &aSingleSetValueDef,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

    static const TH2SupportedOptionNamesPerElementTypeMapType SUPPORTED_SUBTYPES_PER_ELEMENT_TYPE;

public:
    TH2ValuesListSizeType idByText(QString &aValueName) const;
    QString textByID(TH2ValuesListSizeType aValueID) const;
    TH2ValuesListSizeType valueID() const;
    bool setValueByID(TH2ValuesListSizeType newCurrentValueID);
    QString valueText() const;
    bool setValueByText(const QString &newCurrentValueText);

    bool isDefault() const;

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);

    const TH2SingleSetValueDef &singleSetValueDef() const;
    const TH2ValuesListType &values() const;
    const TH2ValueSubstitutionMapType &substitutionValues() const;

protected:
    static const TH2UnsupportedPointTypesSet UNSUPPORTED_POINT_TYPES_BY_CLIP_OPTION;
};

#endif // TH2SINGLESETOPTION_H
