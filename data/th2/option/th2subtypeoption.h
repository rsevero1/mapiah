#ifndef TH2SUBTYPEOPTION_H
#define TH2SUBTYPEOPTION_H

#include "th2option.h"

#include "../th2typedefs.h"

#include "../value/th2singlesetvalue.h"

#include "../../../factories/th2factory.h"

class TH2SubtypeOption : public TH2Option
{
protected:
    TH2SingleSetValue m_subtype;
    QString m_userSubType;
    bool m_isSubtypeInline;
    bool m_isSymbolTypeUser;

    TH2SubtypeOption(
        const QString &aSymbolType,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createTypedElement<TH2SubtypeOption, QString>(
        const QString &aType,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    static const QString SUBTYPE_SEPARATOR;
    static const QString VALUE_NAME;
    static const QString USER_SYMBOL_TYPE_NAME;

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    TH2SingleSetValue &subtype();
    TH2ValuesListSizeType subtypeID() const;
    const QString &subtypeText() const;
    bool setSubtypeByID(TH2ValuesListSizeType newSubtypeID);
    bool setSubtypeByText(const QString &newSubtypeText);

    bool isSubtypeInline() const;
    bool setIsSubtypeInline(bool newIsSubtypeInline);

    bool isSymbolTypeUser() const;

    bool isValid(const QString &newSubtype, TH2Element *aParent = nullptr);

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);
};

#endif // TH2SUBTYPEOPTION_H
