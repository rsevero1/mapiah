/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2sketchoption.h"

const QString TH2SketchOption::FILENAME_VALUE_NAME = QStringLiteral("sketch-filename");
const QString TH2SketchOption::X_VALUE_NAME = QStringLiteral("sketch-x");
const QString TH2SketchOption::Y_VALUE_NAME = QStringLiteral("sketch-y");

const TH2FreeTextValue &TH2SketchOption::filename() const
{
    return m_filename;
}

QString TH2SketchOption::filenameValue() const
{
    return m_filename.textValue();
}

bool TH2SketchOption::setFilenameValue(const QString &newFilename)
{
    return m_filename.setTextValue(newFilename);
}

const TH2UnlimitedDoubleValue &TH2SketchOption::x() const
{
    return m_x;
}

TH2DoubleValueNumberType TH2SketchOption::xNumericValue() const
{
    return m_x.doubleValue();
}

bool TH2SketchOption::setXNumericValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_x.setDoubleValue(newNumericValue);
}

bool TH2SketchOption::setXNumericValueByText(const QString &newNumericValueText)
{
    return m_x.setDoubleValueByText(newNumericValueText);
}

const TH2UnlimitedDoubleValue &TH2SketchOption::y() const
{
    return m_y;
}

TH2DoubleValueNumberType TH2SketchOption::yNumericValue() const
{
    return m_y.doubleValue();
}

bool TH2SketchOption::setYNumericValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_y.setDoubleValue(newNumericValue);
}

bool TH2SketchOption::setYNumericValueByText(const QString &newNumericValueText)
{
    return m_y.setDoubleValueByText(newNumericValueText);
}

const QString TH2SketchOption::elementType()
{
    return SKETCH_OPTION_ELEMENT_TYPE;
}

QString TH2SketchOption::valueForFile() const
{
    return QString("%1 %2 %3").arg(
        m_filename.valueForFile(),
        m_x.valueForFile(),
        m_y.valueForFile());
}

bool TH2SketchOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &,
    const QString &)
{
    return (aParentElementType == SCRAP_ELEMENT_TYPE);
}

TH2GraphicsImage *TH2SketchOption::image() const
{
    return m_image;
}

void TH2SketchOption::setImage(TH2GraphicsImage *newImage)
{
    m_image = newImage;
}

TH2SketchOption::TH2SketchOption(TH2Element *parent, TH2File *th2file)
    : TH2Option(SKETCH_OPTION_NAME, parent, th2file)
    , m_filename(this, FILENAME_VALUE_NAME)
    , m_x(this, X_VALUE_NAME)
    , m_y(this, Y_VALUE_NAME)
{

}
