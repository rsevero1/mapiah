/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2FREETEXTOPTION_H
#define TH2FREETEXTOPTION_H

#include "th2option.h"

#include "../value/th2freetextvalue.h"

#include "../th2typedefs.h"

#include "../../../factories/th2factory.h"

#include <unordered_map>
#include <unordered_set>

class TH2FreeTextOption : public TH2Option
{
protected:
    static const QString VALUE_NAME;

    TH2FreeTextValue m_text;

    TH2FreeTextOption(
        const QString &optionName,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createTypedElement<TH2FreeTextOption, QString>(
        const QString &aOptionType,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

    static const TH2SupportedOptionNamesPerElementTypeMapType SUPPORTED_SYMBOL_TYPES_PER_ELEMENT_TYPE;

public:
    const TH2FreeTextValue &text() const;
    bool setText(const QString &newText);
    const QString &textValue() const;

    // TH2Option interface
    QString valueForFile() const override;

    // TH2Element interface
    const QString elementType() override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);

};

#endif // TH2FREETEXTOPTION_H
