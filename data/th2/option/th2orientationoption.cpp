/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2orientationoption.h"

#include "../../../helpers/mapiahsupport.h"

const QString TH2OrientationOption::AZIMUTH_VALUE_NAME = QStringLiteral("orientation-azimuth");

const QString TH2OrientationOption::elementType()
{
    return "orientationoption";
}

bool TH2OrientationOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &,
    const QString &aOptionName)
{
   if ((aOptionName != ORIENTATION_OPTION_NAME)
        && (aOptionName != ORIENTATION_ALTERNATIVE_OPTION_NAME))
   {
       return false;
   }

   if ((aParentElementType != POINT_ELEMENT_TYPE)
        && (aParentElementType != LINEPOINT_ELEMENT_TYPE))
   {
       return false;
   }

   return true;
}

TH2AzimuthValue &TH2OrientationOption::azimuth()
{
    return m_azimuth;
}

TH2DoubleValue &TH2OrientationOption::number()
{
    return m_azimuth.number();
}

TH2DoubleValueNumberType TH2OrientationOption::numericValue() const
{
    return m_azimuth.numericValue();
}

bool TH2OrientationOption::setNumericValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_azimuth.setNumericValue(newNumericValue);
}

bool TH2OrientationOption::setNumericValueByText(const QString &newNumericValueByText)
{
    return m_azimuth.setNumericValueByText(newNumericValueByText);
}

TH2AzimuthUnit &TH2OrientationOption::unit()
{
    return m_azimuth.unit();
}

TH2ValuesListSizeType TH2OrientationOption::unitID() const
{
    return m_azimuth.unitID();
}

const QString &TH2OrientationOption::unitText() const
{
    return m_azimuth.unitText();
}

bool TH2OrientationOption::setUnitByID(TH2ValuesListSizeType newUnitID)
{
    return m_azimuth.setUnitByID(newUnitID);
}

bool TH2OrientationOption::setUnitByText(const QString &newUnitText)
{
    return m_azimuth.setUnitByText(newUnitText);
}

QString TH2OrientationOption::valueForFile() const
{
    QString value = m_azimuth.valueForFile();

    if (value.contains(SPACE_STRING))
    {
        value = QString("[%1]").arg(value);
    }

    return value;
}

TH2OrientationOption::TH2OrientationOption(TH2Element *parent, TH2File *th2file)
    : TH2Option(ORIENTATION_OPTION_NAME, parent, th2file)
    , m_azimuth(this, AZIMUTH_VALUE_NAME)
{

}
