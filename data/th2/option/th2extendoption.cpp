/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2extendoption.h"

const QString TH2ExtendOption::PREVIOUS_KEYWORD = QStringLiteral("previous");
const QString TH2ExtendOption::PREVIOUS_ALTERNATE_KEYWORD = QStringLiteral("prev");

const TH2KeywordValue &TH2ExtendOption::station() const
{
    return m_station;
}

const QString &TH2ExtendOption::stationName() const
{
    return m_station.keywordValue();
}

bool TH2ExtendOption::setStationName(const QString &newStationName)
{
    return m_station.setKeywordValue(newStationName);
}

const QString TH2ExtendOption::elementType()
{
    return "extendoption";
}

QString TH2ExtendOption::valueForFile() const
{
    return stationName().isEmpty() ?
        EMPTY_STRING :
        QString("previous %2").arg(stationName());
}

bool TH2ExtendOption::isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &)
{
    return ((aParentElementType == POINT_ELEMENT_TYPE)
        && (aSymbolType == STATION_OPTION_NAME));
}

TH2ExtendOption::TH2ExtendOption(TH2Element *parent, TH2File *th2file)
    : TH2Option(EXTEND_OPTION_NAME, parent, th2file)
    , m_station(this, EXTEND_OPTION_NAME)
{

}
