/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2altitudeoption.h"

const QString TH2AltitudeOption::LENGTH_VALUE_NAME = QStringLiteral("altitude-length");

bool TH2AltitudeOption::isFix() const
{
    return m_isFix;
}

void TH2AltitudeOption::setIsFix(bool newIsFix)
{
    m_isFix = newIsFix;
}

const TH2LengthValue &TH2AltitudeOption::length() const
{
    return m_length;
}

QString TH2AltitudeOption::lengthUnitText() const
{
    return m_length.unitText();
}

TH2ValuesListSizeType TH2AltitudeOption::lengthUnitID() const
{
    return m_length.unitID();
}

bool TH2AltitudeOption::setLengthUnitByText(const QString &newUnitText)
{
    return m_length.setUnitByText(newUnitText);
}

bool TH2AltitudeOption::setLengthUnitByID(TH2ValuesListSizeType newUnitID)
{
    return m_length.setUnitByID(newUnitID);
}

TH2DoubleValueNumberType TH2AltitudeOption::lengthNumericValue() const
{
    return m_length.numericValue();
}

bool TH2AltitudeOption::setLengthNumericValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_length.setNumericValue(newNumericValue);
}

bool TH2AltitudeOption::setLengthNumericValueByText(const QString &newNumericValueText)
{
    return m_length.setNumericValueByText(newNumericValueText);
}

QString TH2AltitudeOption::valueForFile() const
{
    QString value = m_length.valueForFile();

    if (m_isFix)
    {
        value = QString("%1 %2").arg(FIX_KEYWORD, value);
    }
    else
    {
        if (m_length.numericValue() > 0)
        {
            value.prepend(PLUS_STRING);
        }
    }

    if (value.contains(SPACE_STRING))
    {
        value = QString("[%1]").arg(value);
    }

    return value;
}

const QString TH2AltitudeOption::elementType()
{
    return ALTITUDE_OPTION_ELEMENT_TYPE;
}

bool TH2AltitudeOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &,
    const QString &)
{
    return (aParentElementType == LINEPOINT_ELEMENT_TYPE);
}

TH2AltitudeOption::TH2AltitudeOption(TH2Element *parent, TH2File *th2file)
    : TH2Option(ALTITUDE_OPTION_NAME, parent, th2file)
    , m_length(this, LENGTH_VALUE_NAME)
{

}
