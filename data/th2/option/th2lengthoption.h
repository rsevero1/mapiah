/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2LENGTHOPTION_H
#define TH2LENGTHOPTION_H

#include "th2option.h"

#include "../value/th2lengthvalue.h"

#include "../../../factories/th2factory.h"

class TH2LengthOption : public TH2Option
{
protected:
    TH2LengthValue m_length;

    const TH2LengthOptionSupportedSymbolTypesListType &m_supportedSymbolTypesList;

    TH2LengthOption(
        const QString &aOptionName,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createTypedElement<TH2LengthOption, QString>(
        const QString &aType,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    TH2LengthValue &dist();

    TH2DoubleValue &number();
    TH2DoubleValueNumberType numericValue();
    bool setNumericValue(TH2DoubleValueNumberType newNumber);
    bool setNumericValueByText(const QString &newNumberText);

    TH2LengthUnit &unit();
    const QString &unitText() const;
    TH2ValuesListSizeType unitID() const;
    bool setUnitByText(const QString &newUnitText);
    bool setUnitByID(TH2ValuesListSizeType newUnitID);
};

#endif // TH2LENGTHOPTION_H
