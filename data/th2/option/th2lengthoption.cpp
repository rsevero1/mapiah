/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2lengthoption.h"

#include "th2lengthoptiondef.h"

#include "../../../helpers/mapiahsupport.h"

bool TH2LengthOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &aSymbolType,
    const QString &aOptionName)
{
    if (TH2_LENGTH_OPTIONS.count(aParentElementType) == 0)
    {
        return false;
    }

    if (TH2_LENGTH_OPTIONS.at(aParentElementType).count(aOptionName) == 0)
    {
        return false;
    }

    return MapiahSupport::contains(
        TH2_LENGTH_OPTIONS.at(aParentElementType).at(aOptionName), aSymbolType);
}

const QString TH2LengthOption::elementType()
{
    return LENGTH_OPTION_ELEMENT_TYPE;
}

TH2LengthValue &TH2LengthOption::dist() {
    return m_length;
}

TH2DoubleValue &TH2LengthOption::number()
{
    return m_length.number();
}

QString TH2LengthOption::valueForFile() const
{
    QString value = m_length.valueForFile();

    if (value.contains(SPACE_STRING))
    {
        value = QString("[%1]").arg(value);
    }

    return value;
}

TH2DoubleValueNumberType TH2LengthOption::numericValue()
{
    return m_length.number().doubleValue();
}

const QString &TH2LengthOption::unitText() const
{
    return m_length.unitText();
}

TH2ValuesListSizeType TH2LengthOption::unitID() const
{
    return m_length.unitID();
}

bool TH2LengthOption::setNumericValue(TH2DoubleValueNumberType newNumber)
{
    return m_length.setNumericValue(newNumber);
}

bool TH2LengthOption::setNumericValueByText(const QString &newNumberText)
{
    return m_length.setNumericValueByText(newNumberText);
}

TH2LengthUnit &TH2LengthOption::unit()
{
    return m_length.unit();
}

bool TH2LengthOption::setUnitByText(const QString &newUnitText)
{
    return m_length.setUnitByText(newUnitText);
}

bool TH2LengthOption::setUnitByID(TH2ValuesListSizeType newUnitID)
{
    return m_length.setUnitByID(newUnitID);
}

TH2LengthOption::TH2LengthOption(
    const QString &aOptionName,
    TH2Element *parent,
    TH2File *th2file)
    : TH2Option(aOptionName, parent, th2file)
    , m_length(this, QString("%1-length").arg(aOptionName))
    , m_supportedSymbolTypesList(TH2_LENGTH_OPTIONS.at(parent->elementType()).at(aOptionName))
{

}
