/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2ALTITUDEOPTION_H
#define TH2ALTITUDEOPTION_H

#include "th2option.h"

#include "../value/th2lengthvalue.h"

#include "../../../factories/th2factory.h"

#include <QString>

class TH2AltitudeOption : public TH2Option
{
protected:
    static const QString LENGTH_VALUE_NAME;

    TH2LengthValue m_length;
    bool m_isFix = false;

    TH2AltitudeOption(
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2AltitudeOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:

    bool isFix() const;
    void setIsFix(bool newIsFix);

    const TH2LengthValue &length() const;
    QString lengthUnitText() const;
    TH2ValuesListSizeType lengthUnitID() const;
    bool setLengthUnitByText(const QString &newUnitText);
    bool setLengthUnitByID(TH2ValuesListSizeType newUnitID);
    TH2DoubleValueNumberType lengthNumericValue() const;
    bool setLengthNumericValue(TH2DoubleValueNumberType newNumericValue);
    bool setLengthNumericValueByText(const QString &newNumericValueText);

    // TH2Option interface
    QString valueForFile() const override;

    // TH2Element interface
    const QString elementType() override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);

};

#endif // TH2ALTITUDEOPTION_H
