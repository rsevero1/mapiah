/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2EXTENDOPTION_H
#define TH2EXTENDOPTION_H

#include "th2option.h"

#include "../value/th2keywordvalue.h"

#include "../../../factories/th2factory.h"

class TH2ExtendOption : public TH2Option
{
    TH2KeywordValue m_station;

protected:
    explicit TH2ExtendOption(TH2Element *parent, TH2File *th2file);

    friend th2element_up TH2Factory::createElement<TH2ExtendOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    static const QString PREVIOUS_KEYWORD;
    static const QString PREVIOUS_ALTERNATE_KEYWORD;

    const TH2KeywordValue &station() const;
    const QString &stationName() const;
    bool setStationName(const QString &newStationName);

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);
};

#endif // TH2EXTENDOPTION_H
