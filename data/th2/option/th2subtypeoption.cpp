#include "th2subtypeoption.h"

#include "../th2line.h"
#include "../th2point.h"

#include "../../../helpers/mapiahenums.h"

const QString TH2SubtypeOption::SUBTYPE_SEPARATOR = QStringLiteral(":");
const QString TH2SubtypeOption::VALUE_NAME = QStringLiteral("subtype-preset");
const QString TH2SubtypeOption::USER_SYMBOL_TYPE_NAME = QStringLiteral("u");

TH2SingleSetValue &TH2SubtypeOption::subtype()
{
    return m_subtype;
}

TH2ValuesListSizeType TH2SubtypeOption::subtypeID() const
{
    return m_subtype.singleSetValueID();
}

const QString &TH2SubtypeOption::subtypeText() const
{
    return  m_subtype.singleSetValueText();
}

bool TH2SubtypeOption::setSubtypeByID(TH2ValuesListSizeType newSubtypeID)
{
    // For user elements the subtype can only be set by text as there is no
    // preset list of allowed subtypes.
    if (m_isSymbolTypeUser)
    {
        return false;
    }

    return m_subtype.setSingleSetValueByID(newSubtypeID);
}

bool TH2SubtypeOption::setSubtypeByText(const QString &newSubtypeText)
{
    if (!m_isSymbolTypeUser)
    {
        return m_subtype.setSingleSetValueByText(newSubtypeText);
    };

    if (newSubtypeText.isEmpty())
    {
        return false;
    }

    m_userSubType = newSubtypeText;

    return true;
}

bool TH2SubtypeOption::isSubtypeInline() const
{
    return m_isSubtypeInline;
}

bool TH2SubtypeOption::setIsSubtypeInline(bool newIsSubtypeInline)
{
    m_isSubtypeInline = newIsSubtypeInline;

    return true;
}

bool TH2SubtypeOption::isValid(const QString &newSubtype, TH2Element *aParent)
{
    if (aParent == nullptr)
    {
        aParent = parent();
        if (aParent == nullptr)
        {
            qInfo() << "Can't identify parent of this option in TH2SubtypeOption::setSubtype";
            return false;
        }
    }

    TH2Symbol *aParentSymbol = dynamic_cast<TH2Symbol *>(aParent);

    if (aParentSymbol == nullptr)
    {
        qInfo() << "dynamic_cast<TH2Symbol *>(aParent) failed in TH2SubtypeOption::isValid";
        return false;
    }

    QString aElementType = aParent->elementType();
    QString aSymbolType = aParentSymbol->type();

    if (!TH2SubtypeOption::isSupportedOption(
        aElementType,
        aSymbolType,
        SUBTYPE_OPTION_NAME))
    {
        return false;
    }

    if (aSymbolType == QStringLiteral("u"))
    {
        if (newSubtype.isEmpty())
        {
            qInfo() << "Subtype for user type can't be empty in TH2SubtypeOption::isValid";
            return false;
        }
    }
    else if (!MapiahSupport::contains(
         TH2_SUBTYPE_SINGLE_SET_VALUE_DEFS.at(aElementType).at(aSymbolType).valueNames,
         newSubtype))
    {
        qInfo() << QString("Subtype '%1' unsupported for %2 of type '%3' in TH2SubtypeOption::isValid")
                   .arg(newSubtype, aElementType, aSymbolType);
        return false;
    }

    return true;
}

bool TH2SubtypeOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &aSymbolType,
    const QString &)
{
    if ((aParentElementType != POINT_ELEMENT_TYPE)
       && (aParentElementType != LINE_ELEMENT_TYPE))
    {
        return false;
    }

    if (aSymbolType == USER_SYMBOL_TYPE_NAME)
    {
        return true;
    }

    // Point/line has unsupported type.
    if (TH2_SUBTYPE_SINGLE_SET_VALUE_DEFS.at(aParentElementType).count(aSymbolType) == 0)
    {
        return false;
    }

    return true;
}

bool TH2SubtypeOption::isSymbolTypeUser() const
{
    return m_isSymbolTypeUser;
}

TH2SubtypeOption::TH2SubtypeOption(
    const QString &aSymbolType,
    TH2Element *parent,
    TH2File *th2file)
    : TH2Option(
          SUBTYPE_OPTION_NAME,
          parent,
          th2file)
    , m_subtype(
          this,
          VALUE_NAME,
          TH2_SUBTYPE_SINGLE_SET_VALUE_DEFS.at(parent->elementType()).at(aSymbolType))
    , m_isSubtypeInline(false)
    , m_isSymbolTypeUser(aSymbolType == USER_SYMBOL_TYPE_NAME)
{

}

const QString TH2SubtypeOption::elementType()
{
    return QStringLiteral("subtypeoption");
}

QString TH2SubtypeOption::valueForFile() const
{
    return m_isSymbolTypeUser ?
        m_userSubType :
        m_subtype.valueForFile();
}
