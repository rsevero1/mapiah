/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2optionlist.h"

#include "../th2file.h"
#include "th2freetextoption.h"

#include <memory>

const QStringList &TH2OptionList::optionCods() const
{
    return m_optionCods;
}

qsizetype TH2OptionList::countOptions() const
{
    return m_optionCods.count();
}

void TH2OptionList::clearOptions()
{
    m_optionCods.clear();
    m_optionTypes.clear();
}

const QHash<QString, QString> &TH2OptionList::optionTypes() const
{
    return m_optionTypes;
}

TH2OptionList::TH2OptionList(TH2File *th2file) : m_th2File(th2file)
{

}

void TH2OptionList::appendOption(th2element_up uniqueOption)
{
    TH2Option *newOption = dynamic_cast<TH2Option *>(uniqueOption.get());
    m_th2File->registerElement(std::move(uniqueOption));
    m_optionCods.append(newOption->cod());
    m_optionTypes[newOption->name()] = newOption->cod();
}

bool TH2OptionList::existsOptionByType(const QString &optionType) const
{
    return m_optionTypes.contains(optionType);
}

TH2Option *TH2OptionList::getOptionByType(const QString &optionType) const
{
    TH2Option *option;

    if (!m_optionTypes.contains(optionType))
    {
        return nullptr;
    }
    auto cod = m_optionTypes.value(optionType);
    option = (TH2Option *) m_th2File->element(cod);

    return option;
}
