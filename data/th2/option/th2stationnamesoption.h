/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2STATIONNAMESOPTION_H
#define TH2STATIONNAMESOPTION_H

#include "th2option.h"

#include "../value/th2freetextvalue.h"

#include "../../../factories/th2factory.h"

#include <QString>

class TH2StationNamesOption : public TH2Option
{
protected:
    static const QString PREFIX_VALUE_NAME;
    static const QString SUFFIX_VALUE_NAME;

    TH2FreeTextValue m_prefix;
    TH2FreeTextValue m_suffix;

    TH2StationNamesOption(
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2StationNamesOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    const TH2FreeTextValue &prefix() const;
    QString prefixText() const;
    bool setPrefixText(const QString &newPrefixText);

    const TH2FreeTextValue &suffix() const;
    QString suffixText() const;
    bool setSuffixText(const QString &newSuffixText);

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);
};

#endif // TH2STATIONNAMESOPTION_H
