/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2singlesetoption.h"

#include "../../../helpers/mapiahsupport.h"

const QString TH2SingleSetOption::VALUE_NAME = QStringLiteral("%1-singleset");

const TH2SupportedOptionNamesPerElementTypeMapType
    TH2SingleSetOption::SUPPORTED_SUBTYPES_PER_ELEMENT_TYPE
{
    {
        QStringLiteral("area"),
        {
            {
                QStringLiteral("clip"),
                {}
            },
            {
                QStringLiteral("place"),
                {}
            },
            {
                QStringLiteral("visibility"),
                {}
            },
        },
    },
    {
        QStringLiteral("line"),
        {
            {
                QStringLiteral("anchors"),
                {
                    QStringLiteral("rope"),
                }
            },
            {
                QStringLiteral("border"),
                {
                    QStringLiteral("slope"),
                }
            },
            {
                QStringLiteral("clip"),
                {}
            },
            {
                QStringLiteral("close"),
                {}
            },
            {
                QStringLiteral("direction"),
                {
                    QStringLiteral("section"),
                }
            },
            {
                QStringLiteral("gradient"),
                {
                    QStringLiteral("contour"),
                }
            },
            {
                QStringLiteral("head"),
                {
                    QStringLiteral("arrow"),
                }
            },
            {
                QStringLiteral("outline"),
                {}
            },
            {
                QStringLiteral("place"),
                {}
            },
            {
                QStringLiteral("rebelays"),
                {
                    QStringLiteral("rope"),
                }
            },
            {
                QStringLiteral("reverse"),
                {}
            },
            {
                QStringLiteral("visibility"),
                {}
            },
        },
    },
    {
        QStringLiteral("linepoint"),
        {
            {
                QStringLiteral("adjust"),
                {}
            },
            {
                QStringLiteral("direction"),
                {
                    QStringLiteral("section"),
                }
            },
            {
                QStringLiteral("gradient"),
                {
                    QStringLiteral("contour"),
                }
            },
            {
                QStringLiteral("smooth"),
                {}
            },
        },
    },
    {
        QStringLiteral("point"),
        {
            {
                QStringLiteral("align"),
                {}
            },
            {
                QStringLiteral("clip"),
                {}
            },
            {
                QStringLiteral("context"),
                {}
            },
            {
                QStringLiteral("place"),
                {}
            },
            {
                QStringLiteral("visibility"),
                {}
            },
        },
    },
    {
        QStringLiteral("scrap"),
        {
            {
                QStringLiteral("flip"),
                {}
            },
            {
                QStringLiteral("walls"),
                {}
            },
        },
    },
};

const TH2UnsupportedPointTypesSet TH2SingleSetOption::UNSUPPORTED_POINT_TYPES_BY_CLIP_OPTION
{
    QStringLiteral("altitude"),
    QStringLiteral("date"),
    QStringLiteral("height"),
    QStringLiteral("label"),
    QStringLiteral("passage-height"),
    QStringLiteral("remark"),
    QStringLiteral("station"),
    QStringLiteral("station-name"),
};

TH2ValuesListSizeType TH2SingleSetOption::valueID() const
{
    return m_value.singleSetValueID();
}

bool TH2SingleSetOption::setValueByID(TH2ValuesListSizeType newCurrentValueID)
{
    return m_value.setSingleSetValueByID(newCurrentValueID);
}

QString TH2SingleSetOption::valueText() const
{
    return m_value.singleSetValueText();
}

bool TH2SingleSetOption::setValueByText(const QString &newCurrentValueText)
{
    return m_value.setSingleSetValueByText(newCurrentValueText);
}

bool TH2SingleSetOption::isDefault() const
{
    return m_value.isDefault();
}

const TH2SingleSetValueDef &TH2SingleSetOption::singleSetValueDef() const
{
    return m_value.singleSetOptionDef();
}

const TH2ValuesListType &TH2SingleSetOption::values() const
{
    return m_value.singleSetOptionDef().valueNames;
}

const TH2ValueSubstitutionMapType &TH2SingleSetOption::substitutionValues() const
{
    return m_value.singleSetOptionDef().valueSubstitutions;
}

bool TH2SingleSetOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &aSymbolType,
    const QString &aOptionName)
{
    // Checking if the element type is supported.
    if (SUPPORTED_SUBTYPES_PER_ELEMENT_TYPE.count(aParentElementType) == 0)
    {
        return false;
    }

    // The clip option is only supported for point types not listed in
    // unsupportedPointTypesForClipOption
    if ((aParentElementType == POINT_ELEMENT_TYPE)
        && (aOptionName == CLIP_OPTION_NAME))
    {
        if (UNSUPPORTED_POINT_TYPES_BY_CLIP_OPTION.count(aSymbolType) == 1)
        {
            return false;
        }
    }

    // Checking if the option name is supported for this element type.
    if (SUPPORTED_SUBTYPES_PER_ELEMENT_TYPE.at(aParentElementType).count(aOptionName) == 0)
    {
        return false;
    }

    // Checking if there is a list of supported symbol types for this option name.
    if (SUPPORTED_SUBTYPES_PER_ELEMENT_TYPE.at(aParentElementType).at(aOptionName).size() != 0)
    {
        // Checking if aSymbolType is listed in it.
        if (SUPPORTED_SUBTYPES_PER_ELEMENT_TYPE.at(aParentElementType).at(aOptionName).count(aSymbolType) == 0)
        {
            return false;
        }
    }

    return true;
}

TH2SingleSetOption::TH2SingleSetOption(
    const TH2SingleSetValueDef &aSingleSetValueDef,
    TH2Element *parent,
    TH2File *th2file)
    : TH2Option(aSingleSetValueDef.optionName, parent, th2file)
    , m_value(this, VALUE_NAME, aSingleSetValueDef)
{

}

TH2ValuesListSizeType TH2SingleSetOption::idByText(QString &aValueName) const
{
    return m_value.idByText(aValueName);
}

QString TH2SingleSetOption::textByID(TH2ValuesListSizeType aValueID) const
{
    return m_value.textByID(aValueID);
}

const QString TH2SingleSetOption::elementType()
{
    return "singlesetoption";
}

QString TH2SingleSetOption::valueForFile() const
{
    return valueText();
}
