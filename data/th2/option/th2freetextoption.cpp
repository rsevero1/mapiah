/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2freetextoption.h"

#include "../../../helpers/mapiahenums.h"

const QString TH2FreeTextOption::VALUE_NAME = QStringLiteral("freetext-text");

const TH2SupportedOptionNamesPerElementTypeMapType
    TH2FreeTextOption::SUPPORTED_SYMBOL_TYPES_PER_ELEMENT_TYPE
{
    {
        QStringLiteral("area"),
        {
            {
                QStringLiteral("cs"),
                {}
            },
            {
                QStringLiteral("id"),
                {}
            },
            {
                QStringLiteral("stations"),
                {}
            },
            {
                QStringLiteral("title"),
                {}
            },
        },
    },
    {
        QStringLiteral("line"),
        {
            {
                QStringLiteral("id"),
                {}
            },
            {
                QStringLiteral("text"),
                {
                    QStringLiteral("label"),
                }
            },
        },
    },
    {
        QStringLiteral("linepoint"),
        {
            {
                QStringLiteral("mark"),
                {}
            },
        },
    },
    {
        QStringLiteral("point"),
        {
            {
                QStringLiteral("from"),
                {
                    QStringLiteral("extra"),
                }
            },
            {
                QStringLiteral("id"),
                {}
            },
            {
                QStringLiteral("name"),
                {
                    QStringLiteral("station"),
                }
            },
            {
                QStringLiteral("scrap"),
                {
                    QStringLiteral("section"),
                }
            },
            {
                QStringLiteral("text"),
                {
                    QStringLiteral("continuation"),
                    QStringLiteral("label"),
                    QStringLiteral("remark"),
                }
            },
        },
    },
    {
        QStringLiteral("scrap"),
        {
            {
                QStringLiteral("cs"),
                {}
            },
            {
                QStringLiteral("stations"),
                {}
            },
            {
                QStringLiteral("title"),
                {}
            },
        },
    },
};

bool TH2FreeTextOption::setText(const QString &newText)
{
    return m_text.setTextValue(newText);
}

QString TH2FreeTextOption::valueForFile() const
{
    return m_text.valueForFile();
}

const QString TH2FreeTextOption::elementType()
{
    return FREETEXT_OPTION_ELEMENT_TYPE;
}

const QString &TH2FreeTextOption::textValue() const
{
    return m_text.textValue();
}

bool TH2FreeTextOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &aSymbolType,
    const QString &aOptionName)
{
    if (SUPPORTED_SYMBOL_TYPES_PER_ELEMENT_TYPE.count(aParentElementType) == 0)
    {
        return false;
    }

    if (SUPPORTED_SYMBOL_TYPES_PER_ELEMENT_TYPE.at(aParentElementType).count(aOptionName) == 0)
    {
        return false;
    }

    if (SUPPORTED_SYMBOL_TYPES_PER_ELEMENT_TYPE.at(aParentElementType).at(aOptionName).size() != 0)
    {
        if (SUPPORTED_SYMBOL_TYPES_PER_ELEMENT_TYPE.at(aParentElementType).at(aOptionName).count(aSymbolType) == 0)
        {
            return false;
        }
    }

    return true;
}

const TH2FreeTextValue &TH2FreeTextOption::text() const
{
    return m_text;
}

TH2FreeTextOption::TH2FreeTextOption(const QString &optionName,
        TH2Element *parent,
        TH2File *th2file)
    : TH2Option(optionName, parent, th2file)
    , m_text(this, VALUE_NAME)
{

}
