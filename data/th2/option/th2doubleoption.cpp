/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2doubleoption.h"

#include "th2doubleoptiondef.h"

#include "../../../helpers/mapiahsupport.h"

#include <QString>

const TH2DoubleValue &TH2DoubleOption::value() const
{
    return m_value;
}

bool TH2DoubleOption::isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName)
{
    if (TH2_DOUBLE_VALUE_DEFS.count(aParentElementType) == 0)
    {
        return false;
    }

    if (TH2_DOUBLE_VALUE_DEFS.at(aParentElementType).count(aOptionName) == 0)
    {
        return false;
    }

    if ((TH2_DOUBLE_VALUE_DEFS.at(aParentElementType).at(aOptionName).supportedSymbolTypes.size() != 0)
        && (TH2_DOUBLE_VALUE_DEFS.at(aParentElementType).at(aOptionName).supportedSymbolTypes.count(aSymbolType) == 0))
    {
        return false;
    }

    return true;
}

TH2DoubleValueNumberType TH2DoubleOption::numericValue() const
{
    return m_value.doubleValue();
}

bool TH2DoubleOption::setNumericValue(TH2DoubleValueNumberType newDoubleValue)
{
    return m_value.setDoubleValue(newDoubleValue);
}

bool TH2DoubleOption::setNumericValueByText(const QString &newNumericValueText)
{
    return m_value.setDoubleValueByText(newNumericValueText);
}

QString TH2DoubleOption::valueForFile() const
{
    return m_value.valueForFile();
}

TH2DoubleOption::TH2DoubleOption(
    const TH2DoubleOptionDef &aDoubleOptionDef,
    TH2Element *parent,
    TH2File *th2file)
    : TH2Option(aDoubleOptionDef.optionName, parent, th2file)
    , m_value(
          this,
          QString("%1-double").arg(aDoubleOptionDef.optionName),
          aDoubleOptionDef.isLowerLimited,
          aDoubleOptionDef.isUpperLimited,
          aDoubleOptionDef.lowerLimit,
          aDoubleOptionDef.upperLimit)
    , m_doubleOptionDef(aDoubleOptionDef)
{

}

const QString TH2DoubleOption::elementType()
{
    return DOUBLE_OPTION_ELEMENT_TYPE;
}
