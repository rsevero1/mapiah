/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2MARKOPTION_H
#define TH2MARKOPTION_H

#include "th2option.h"

#include "../value/th2keywordvalue.h"

#include "../../../factories/th2factory.h"

#include <QString>

class TH2MarkOption : public TH2Option
{
protected:
    static const QString KEYWORD_VALUE_NAME;

    TH2KeywordValue m_keyword;

    TH2MarkOption(
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2MarkOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    const TH2KeywordValue &keyword() const;
    QString keywordText() const;
    bool setKeywordText(const QString &newText);

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);

};

#endif // TH2MARKOPTION_H
