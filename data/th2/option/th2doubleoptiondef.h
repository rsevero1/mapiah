/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2DOUBLEOPTIONDEF_H
#define TH2DOUBLEOPTIONDEF_H

#include "../value/th2doublevalue.h"

#include "../th2typedefs.h"

#include <QString>

struct TH2DoubleOptionDef
{
    const QString optionName;
    TH2DoubleValue::Limited isLowerLimited;
    TH2DoubleValue::Limited isUpperLimited;
    TH2DoubleValueNumberType lowerLimit;
    TH2DoubleValueNumberType upperLimit;
    const QStringList &supportedSymbolTypes;

    TH2DoubleOptionDef(
        const QString &aOptionName,
        TH2DoubleValue::Limited aIsLowerLimited,
        TH2DoubleValue::Limited aIsUpperLimited,
        TH2DoubleValueNumberType aLowerLimit,
        TH2DoubleValueNumberType aUpperLimit,
        const QStringList &aSupportedSymbolTypesList = {});

};

extern const TH2DoubleOptionDefsPerElementTypeMapType TH2_DOUBLE_VALUE_DEFS;

#endif // TH2DOUBLEOPTIONDEF_H
