/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2ORIENTATIONOPTION_H
#define TH2ORIENTATIONOPTION_H

#include "th2option.h"

#include "../value/th2azimuthvalue.h"

#include "../../../factories/th2factory.h"

class TH2OrientationOption : public TH2Option
{
protected:
    TH2AzimuthValue m_azimuth;

    TH2OrientationOption(TH2Element *parent = nullptr, TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2OrientationOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    static const QString AZIMUTH_VALUE_NAME;

    // TH2Element interface
    const QString elementType() override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &,
        const QString &aOptionName);

    TH2AzimuthValue &azimuth();

    TH2DoubleValue &number();
    TH2DoubleValueNumberType numericValue() const;
    bool setNumericValue(TH2DoubleValueNumberType newNumericValue);
    bool setNumericValueByText(const QString &newNumericValueByText);

    TH2AzimuthUnit &unit();
    TH2ValuesListSizeType unitID() const;
    const QString &unitText() const;
    bool setUnitByID(TH2ValuesListSizeType newUnitID);
    bool setUnitByText(const QString &newUnitText);

    // TH2Option interface
    QString valueForFile() const override;

};

#endif // TH2ORIENTATIONOPTION_H
