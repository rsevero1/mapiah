/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2COPYRIGHTOPTION_H
#define TH2COPYRIGHTOPTION_H

#include "th2option.h"

#include "../value/th2datevalue.h"
#include "../value/th2freetextvalue.h"

#include "../../../factories/th2factory.h"

class TH2CopyrightOption : public TH2Option
{
protected:
    static const QString DATE_VALUE_NAME;
    static const QString NOTICE_VALUE_NAME;

    TH2DateValue m_date;
    TH2FreeTextValue m_notice;

    TH2CopyrightOption(
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2CopyrightOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    TH2DateValue &date();
    QString dateText() const;
    bool setDateFromText(const QString &newDateText);

    TH2FreeTextValue &notice();
    QString noticeText() const;
    bool setNoticeText(const QString &newNoticeText);

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);
};

#endif // TH2COPYRIGHTOPTION_H
