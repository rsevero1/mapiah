/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2VALUEOPTION_H
#define TH2VALUEOPTION_H

#include "th2option.h"

#include "../value/th2datevalue.h"
#include "../value/th2doublevalue.h"
#include "../value/th2lengthunit.h"
#include "../value/th2unlimiteddoublevalue.h"

#include "../../../factories/th2factory.h"

class TH2ValueOption : public TH2Option
{
protected:
    static const QString DATE_VALUE_NAME;
    static const QString SINGLE_NUMBER_VALUE_NAME;
    static const QString ABOVE_NUMBER_VALUE_NAME;
    static const QString BELOW_NUMBER_VALUE_NAME;
    static const QString UNIT_VALUE_NAME;
    static const TH2DoubleValueNumberType LOWER_LIMIT;
    static const QString FIX_KEYWORD;
    static const QString PRESUMED_KEYWORD;

    TH2ValueOption(TH2Element *parent = nullptr, TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2ValueOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    enum class Sign {
        UNSET,
        Depth,
        DepthHeight,
        Height,
    };

    enum class PassageHeight {
        UNSET,
        Depth,
        DepthHeight,
        Distance,
        Height,
    };

private:
    TH2DateValue m_date;
    TH2UnlimitedDoubleValue m_number;
    TH2DoubleValue m_above;
    TH2DoubleValue m_below;
    TH2LengthUnit m_unit;
    bool m_isFix = false;
    bool m_isPresumed = false;
    Sign m_sign = Sign::UNSET;
    PassageHeight m_passageHeight = PassageHeight::UNSET;

public:
    const TH2UnlimitedDoubleValue &number() const;
    TH2DoubleValueNumberType numericValue() const;
    bool setNumericValue(TH2DoubleValueNumberType newNumericValue);
    bool setNumericValueByText(const QString &newNumericValueText);

    const TH2DoubleValue &above() const;
    TH2DoubleValueNumberType aboveValue() const;
    bool setAboveValue(TH2DoubleValueNumberType newNumericValue);
    bool setAboveValueByText(const QString &newNumericValueText);

    const TH2DoubleValue &below() const;
    TH2DoubleValueNumberType belowValue() const;
    bool setBelowValue(TH2DoubleValueNumberType newNumericValue);
    bool setBelowValueByText(const QString &newNumericValueText);

    const TH2LengthUnit &unit() const;
    TH2ValuesListSizeType unitID() const;
    const QString &unitText() const;
    bool setUnitByID(TH2ValuesListSizeType newUnitID);
    bool setUnitByText(const QString &newUnitText);

    bool isPresumed() const;
    bool setIsPresumed(bool newIsPresumed);

    bool isFix() const;
    bool setIsFix(bool newIsFix);

    Sign sign() const;
    bool setSign(Sign newSign);

    PassageHeight passageHeight() const;
    bool setPassageHeight(PassageHeight newPassageHeight);

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);

    TH2DateValue &date();
    const QString dateAsText() const;
    bool setDateFromText(const QString &newDateText);
};

#endif // TH2VALUEOPTION_H
