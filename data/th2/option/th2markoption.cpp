/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2markoption.h"

const QString TH2MarkOption::KEYWORD_VALUE_NAME = QStringLiteral("mark-keyword");

const TH2KeywordValue &TH2MarkOption::keyword() const
{
    return m_keyword;
}

QString TH2MarkOption::keywordText() const
{
    return m_keyword.keywordValue();
}

bool TH2MarkOption::setKeywordText(const QString &newText)
{
    return m_keyword.setKeywordValue(newText);
}

const QString TH2MarkOption::elementType()
{
    return MARK_OPTION_ELEMENT_TYPE;
}

QString TH2MarkOption::valueForFile() const
{
    return m_keyword.valueForFile();
}

bool TH2MarkOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &,
    const QString &)
{
    return (aParentElementType == LINEPOINT_ELEMENT_TYPE);
}

TH2MarkOption::TH2MarkOption(
    TH2Element *parent,
    TH2File *th2file)
    : TH2Option(
          MARK_OPTION_NAME,
          parent,
          th2file)
    , m_keyword(this, KEYWORD_VALUE_NAME)
{

}
