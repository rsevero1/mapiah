/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2scrapscaleoption.h"

const QString TH2ScrapScaleOption::DRAWING_UNIT_SIZE_IN_REALITY_VALUE_NAME = QStringLiteral("scrapscale-drawingunitsizeinreality");
const QString TH2ScrapScaleOption::AMOUNT_OF_DRAWING_UNITS_VALUE_NAME = QStringLiteral("scrapscale-amountofdrawingunits");
const QString TH2ScrapScaleOption::X1_SCRAP_VALUE_NAME = QStringLiteral("scrapscale-x1scrap");
const QString TH2ScrapScaleOption::Y1_SCRAP_VALUE_NAME = QStringLiteral("scrapscale-y1scrap");
const QString TH2ScrapScaleOption::X2_SCRAP_VALUE_NAME = QStringLiteral("scrapscale-x2scrap");
const QString TH2ScrapScaleOption::Y2_SCRAP_VALUE_NAME = QStringLiteral("scrapscale-y2scrap");
const QString TH2ScrapScaleOption::X1_REALITY_VALUE_NAME = QStringLiteral("scrapscale-x1reality");
const QString TH2ScrapScaleOption::Y1_REALITY_VALUE_NAME = QStringLiteral("scrapscale-y1reality");
const QString TH2ScrapScaleOption::X2_REALITY_VALUE_NAME = QStringLiteral("scrapscale-x2reality");
const QString TH2ScrapScaleOption::Y2_REALITY_VALUE_NAME = QStringLiteral("scrapscale-y2reality");
const QString TH2ScrapScaleOption::LENGTH_UNIT_VALUE_NAME = QStringLiteral("scrapscale-lengthunit");

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::drawingUnitSizeInReality() const
{
    return m_drawingUnitSizeInReality;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::drawingUnitSizeInRealityNumericValue() const
{
    return m_drawingUnitSizeInReality.doubleValue();
}

bool TH2ScrapScaleOption::setDrawingUnitSizeInRealityNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_drawingUnitSizeInReality.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setDrawingUnitSizeInRealityByText(const QString &aValueText)
{
    return m_drawingUnitSizeInReality.setDoubleValueByText(aValueText);
}

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::amountOfDrawingUnits() const
{
    return m_amountOfDrawingUnits;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::amountOfDrawingUnitsNumericValue() const
{
    return m_amountOfDrawingUnits.doubleValue();
}

bool TH2ScrapScaleOption::setAmountOfDrawingUnitsNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_amountOfDrawingUnits.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setAmountOfDrawingUnitsByText(const QString &aValueText)
{
    return m_amountOfDrawingUnits.setDoubleValueByText(aValueText);
}

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::x1Scrap() const
{
    return m_x1Scrap;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::x1ScrapNumericValue() const
{
    return m_x1Scrap.doubleValue();
}

bool TH2ScrapScaleOption::setX1ScrapNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_x1Scrap.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setX1ScrapByText(const QString &aValueText)
{
    return m_x1Scrap.setDoubleValueByText(aValueText);
}

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::y1Scrap() const
{
    return m_y1Scrap;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::y1ScrapNumericValue() const
{
    return m_y1Scrap.doubleValue();
}

bool TH2ScrapScaleOption::setY1ScrapNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_y1Scrap.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setY1ScrapByText(const QString &aValueText)
{
    return m_y1Scrap.setDoubleValueByText(aValueText);
}

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::x2Scrap() const
{
    return m_x2Scrap;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::x2ScrapNumericValue() const
{
    return m_x2Scrap.doubleValue();
}

bool TH2ScrapScaleOption::setX2ScrapNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_x2Scrap.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setX2ScrapByText(const QString &aValueText)
{
    return m_x2Scrap.setDoubleValueByText(aValueText);
}

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::y2Scrap() const
{
    return m_y2Scrap;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::y2ScrapNumericValue() const
{
    return m_y2Scrap.doubleValue();
}

bool TH2ScrapScaleOption::setY2ScrapNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_y2Scrap.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setY2ScrapByText(const QString &aValueText)
{
    return m_y2Scrap.setDoubleValueByText(aValueText);
}

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::x1Reality() const
{
    return m_x1Reality;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::x1RealityNumericValue() const
{
    return m_x1Reality.doubleValue();
}

bool TH2ScrapScaleOption::setX1RealityNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_x1Reality.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setX1RealityByText(const QString &aValueText)
{
    return m_x1Reality.setDoubleValueByText(aValueText);
}

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::y1Reality() const
{
    return m_y1Reality;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::y1RealityNumericValue() const
{
    return m_y1Reality.doubleValue();
}

bool TH2ScrapScaleOption::setY1RealityNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_y1Reality.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setY1RealityByText(const QString &aValueText)
{
    return m_y1Reality.setDoubleValueByText(aValueText);
}

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::x2Reality() const
{
    return m_x2Reality;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::x2RealityNumericValue() const
{
    return m_x2Reality.doubleValue();
}

bool TH2ScrapScaleOption::setX2RealityNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_x2Reality.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setX2RealityByText(const QString &aValueText)
{
    return m_x2Reality.setDoubleValueByText(aValueText);
}

const TH2UnlimitedDoubleValue &TH2ScrapScaleOption::y2Reality() const
{
    return m_y2Reality;
}

TH2DoubleValueNumberType TH2ScrapScaleOption::y2RealityNumericValue() const
{
    return m_y2Reality.doubleValue();
}

bool TH2ScrapScaleOption::setY2RealityNumericValue(TH2DoubleValueNumberType aValue)
{
    return m_y2Reality.setDoubleValue(aValue);
}

bool TH2ScrapScaleOption::setY2RealityByText(const QString &aValueText)
{
    return m_y2Reality.setDoubleValueByText(aValueText);
}

const TH2LengthUnit &TH2ScrapScaleOption::lengthUnit() const
{
    return m_lengthUnit;
}

TH2ValuesListSizeType TH2ScrapScaleOption::lengthUnitID() const
{
    return m_lengthUnit.lengthUnitValueID();
}

QString TH2ScrapScaleOption::lengthUnitText() const
{
    return m_lengthUnit.lengthUnitValueByText();
}

bool TH2ScrapScaleOption::setLengthUnitByID(TH2ValuesListSizeType aID)
{
    return m_lengthUnit.setLengthUnitValueByID(aID);
}

bool TH2ScrapScaleOption::setLengthUnitByText(const QString &aText)
{
    return m_lengthUnit.setLengthUnitValueByText(aText);
}

TH2ScrapScaleOption::Form TH2ScrapScaleOption::form() const
{
    return m_form;
}

void TH2ScrapScaleOption::setForm(Form newForm)
{
    m_form = newForm;
}

const QString TH2ScrapScaleOption::elementType()
{
    return SCRAP_SCALE_OPTION_ELEMENT_TYPE;
}

QString TH2ScrapScaleOption::valueForFile() const
{
    QString value;

    switch(m_form)
    {
    case Form::SizeOfDrawingUnitInReality:
        if (m_lengthUnit.isDefault())
        {
            value = m_drawingUnitSizeInReality.valueForFile();
        }
        else
        {
            value = QString("[%1 %2]").arg(
                m_drawingUnitSizeInReality.valueForFile(),
                m_lengthUnit.valueForFile());
        }
        break;
    case Form::AmountOfDrawingUnitsPerSizeInReality:
        value = QString("[%1 %2 %3]").arg(
            m_amountOfDrawingUnits.valueForFile(),
            m_drawingUnitSizeInReality.valueForFile(),
            m_lengthUnit.valueForFile());
        break;
    case Form::Complete:
        value = QString("[%1 %2 %3 %4 %5 %6 %7 %8 %9]").arg(
            m_x1Scrap.valueForFile(),
            m_y1Scrap.valueForFile(),
            m_x2Scrap.valueForFile(),
            m_y2Scrap.valueForFile(),
            m_x1Reality.valueForFile(),
            m_y1Reality.valueForFile(),
            m_x2Reality.valueForFile(),
            m_y2Reality.valueForFile(),
            m_lengthUnit.valueForFile());
        break;
    case Form::UNSET:
        break;
    }

    return value;
}

TH2ScrapScaleOption::TH2ScrapScaleOption(TH2Element *parent, TH2File *th2file)
     : TH2Option(
       SCRAP_SCALE_OPTION_NAME,
       parent,
       th2file)
     , m_drawingUnitSizeInReality(this, DRAWING_UNIT_SIZE_IN_REALITY_VALUE_NAME)
     , m_amountOfDrawingUnits(this, AMOUNT_OF_DRAWING_UNITS_VALUE_NAME)
     , m_x1Scrap(this, X1_SCRAP_VALUE_NAME)
     , m_y1Scrap(this, Y1_SCRAP_VALUE_NAME)
     , m_x2Scrap(this, X2_SCRAP_VALUE_NAME)
     , m_y2Scrap(this, Y2_SCRAP_VALUE_NAME)
     , m_x1Reality(this, X1_REALITY_VALUE_NAME)
     , m_y1Reality(this, Y1_REALITY_VALUE_NAME)
     , m_x2Reality(this, X2_REALITY_VALUE_NAME)
     , m_y2Reality(this, Y2_REALITY_VALUE_NAME)
     , m_lengthUnit(this, LENGTH_UNIT_VALUE_NAME)
     , m_form(Form::UNSET)
{

}
