/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2contextoption.h"

const QString TH2ContextOption::SYMBOL_ELEMENT_TYPE_VALUE_NAME = QStringLiteral("context-symbol_element_type");
const QString TH2ContextOption::SYMBOL_TYPE_VALUE_NAME = QStringLiteral("context-symbol_type");

TH2ContextOption::TH2ContextOption(
    TH2Element *parent,
    TH2File *th2file)
    : TH2Option(CONTEXT_OPTION_NAME, parent, th2file)
    , m_symbolElementType(
      this,
      SYMBOL_ELEMENT_TYPE_VALUE_NAME,
      TH2_CONTEXT_VALUE_DEF)
    , m_symbolType(
      this,
      SYMBOL_TYPE_VALUE_NAME)
{

}

TH2SingleSetValue &TH2ContextOption::symbolElementType()
{
    return m_symbolElementType;
}

bool TH2ContextOption::setSymbolElementTypeByText(const QString &newSymbolElementType)
{
    return m_symbolElementType.setSingleSetValueByText(newSymbolElementType);
}

bool TH2ContextOption::setSymbolElementTypeByID(TH2ValuesListSizeType newSymbolElementType)
{
    return m_symbolElementType.setSingleSetValueByID(newSymbolElementType);
}

const QString &TH2ContextOption::symbolElementTypeText() const
{
    return m_symbolElementType.singleSetValueText();
}

TH2ValuesListSizeType TH2ContextOption::symbolElementTypeID() const
{
    return m_symbolElementType.singleSetValueID();
}

TH2FreeTextValue &TH2ContextOption::symbolType()
{
    return m_symbolType;
}

bool TH2ContextOption::setSymbolTypeText(const QString &newSymbolType)
{
    return m_symbolType.setTextValue(newSymbolType);
}

const QString &TH2ContextOption::symbolTypeText()
{
    return m_symbolType.textValue();
}

const QString TH2ContextOption::elementType()
{
    return "contextoption";
}

QString TH2ContextOption::valueForFile() const
{
    QString value = QString("%1 %2")
        .arg(m_symbolElementType.valueForFile(), m_symbolType.valueForFile());

    return value;
}

bool TH2ContextOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &,
    const QString &)
{
    return ((aParentElementType == POINT_ELEMENT_TYPE)
        || (aParentElementType == LINE_ELEMENT_TYPE)
        || (aParentElementType == AREA_ELEMENT_TYPE));
}
