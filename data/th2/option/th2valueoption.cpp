/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2valueoption.h"

#include "../th2point.h"

const QString TH2ValueOption::DATE_VALUE_NAME = QStringLiteral("value-date");
const QString TH2ValueOption::SINGLE_NUMBER_VALUE_NAME = QStringLiteral("value-single");
const QString TH2ValueOption::ABOVE_NUMBER_VALUE_NAME = QStringLiteral("value-above");
const QString TH2ValueOption::BELOW_NUMBER_VALUE_NAME = QStringLiteral("value-below");
const QString TH2ValueOption::UNIT_VALUE_NAME = QStringLiteral("value-unit");
const TH2DoubleValueNumberType TH2ValueOption::LOWER_LIMIT = 0;
const QString TH2ValueOption::FIX_KEYWORD = QStringLiteral("fix");
const QString TH2ValueOption::PRESUMED_KEYWORD = QStringLiteral("?");

const TH2UnlimitedDoubleValue &TH2ValueOption::number() const
{
    return m_number;
}

TH2DoubleValueNumberType TH2ValueOption::numericValue() const
{
    return m_number.doubleValue();
}

bool TH2ValueOption::setNumericValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_number.setDoubleValue(newNumericValue);
}

bool TH2ValueOption::setNumericValueByText(const QString &newNumericValueText)
{
    return m_number.setDoubleValueByText(newNumericValueText);
}

const TH2DoubleValue &TH2ValueOption::above() const
{
    return m_above;
}

TH2DoubleValueNumberType TH2ValueOption::aboveValue() const
{
    return m_above.doubleValue();
}

bool TH2ValueOption::setAboveValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_above.setDoubleValue(newNumericValue);
}

bool TH2ValueOption::setAboveValueByText(const QString &newNumericValueText)
{
    return m_above.setDoubleValueByText(newNumericValueText);
}

const TH2DoubleValue &TH2ValueOption::below() const
{
    return m_below;
}

TH2DoubleValueNumberType TH2ValueOption::belowValue() const
{
    return m_below.doubleValue();
}

bool TH2ValueOption::setBelowValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_below.setDoubleValue(newNumericValue);
}

bool TH2ValueOption::setBelowValueByText(const QString &newNumericValueText)
{
    return m_below.setDoubleValueByText(newNumericValueText);
}

const TH2LengthUnit &TH2ValueOption::unit() const
{
    return m_unit;
}

TH2ValuesListSizeType TH2ValueOption::unitID() const
{
    return m_unit.lengthUnitValueID();
}

const QString &TH2ValueOption::unitText() const
{
    return m_unit.lengthUnitValueByText();
}

bool TH2ValueOption::setUnitByID(TH2ValuesListSizeType newUnitID)
{
    return m_unit.setLengthUnitValueByID(newUnitID);
}

bool TH2ValueOption::setUnitByText(const QString &newUnitText)
{
    return m_unit.setLengthUnitValueByText(newUnitText);
}

bool TH2ValueOption::isPresumed() const
{
    return m_isPresumed;
}

bool TH2ValueOption::setIsPresumed(bool newIsPresumed)
{
    m_isPresumed = newIsPresumed;

    return true;
}

const QString TH2ValueOption::elementType()
{
    return VALUE_OPTION_ELEMENT_TYPE;
}

QString TH2ValueOption::valueForFile() const
{
    TH2Point *parentPoint = dynamic_cast<TH2Point *>(parent());
    assert(parentPoint != nullptr);
    const QString symbolType = parentPoint->type();
    QString value;

    if (symbolType == ALTITUDE_POINT_TYPE)
    {
        value = m_number.valueForFile();
        if (m_isFix)
        {
            value = QString("%1 %2").arg(FIX_KEYWORD, value);
        }
    }
    else if (symbolType == DATE_POINT_TYPE)
    {
        value = m_date.valueForFile();
    }
    else if (symbolType == DIMENSIONS_POINT_TYPE)
    {
        value = QString("%1 %2").arg(
            m_above.valueForFile(),
            m_below.valueForFile());
    }
    else if (symbolType == EXTRA_POINT_TYPE)
    {
        value = m_number.valueForFile();
    }
    else if (symbolType == HEIGHT_POINT_TYPE)
    {
        if (m_sign == Sign::Height)
        {
            value = "+";
        }
        else if (m_sign == Sign::Depth)
        {
            value = "-";
        }

        value.append(m_number.valueForFile());

        if (isPresumed())
        {
            value.append(PRESUMED_KEYWORD);
        }
    }
    else if (symbolType == PASSAGE_HEIGHT_POINT_TYPE)
    {
        switch(m_passageHeight)
        {
        case PassageHeight::Depth:
            value = QString("-%1").arg(m_number.valueForFile());
            break;
        case PassageHeight::Distance:
            value = m_number.valueForFile();
            break;
        case PassageHeight::Height:
            value = QString("+%1").arg(m_number.valueForFile());
            break;
        case PassageHeight::DepthHeight:
            value = QString("+%1 -%2").arg(
                m_above.valueForFile(),
                m_below.valueForFile());
            break;
        case PassageHeight::UNSET:
            return EMPTY_STRING;
        }
    }

    if (symbolType != DATE_POINT_TYPE)
    {
        if (m_unit.isSet())
        {
            value = QString("%1 %2").arg(value, m_unit.valueForFile());
        }

        if (value.contains(SPACE_STRING))
        {
            value = QString("[%1]").arg(value);
        }
    }

    return value;
}

bool TH2ValueOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &aSymbolType,
    const QString &)
{
    if (aParentElementType != POINT_ELEMENT_TYPE)
    {
        return false;
    }

    if ((aSymbolType != ALTITUDE_POINT_TYPE)
        && (aSymbolType != DATE_POINT_TYPE)
        && (aSymbolType != DIMENSIONS_POINT_TYPE)
        && (aSymbolType != EXTRA_POINT_TYPE)
        && (aSymbolType != HEIGHT_POINT_TYPE)
        && (aSymbolType != PASSAGE_HEIGHT_POINT_TYPE))
    {
        return false;
    }

    return true;
}

bool TH2ValueOption::isFix() const
{
    return m_isFix;
}

bool TH2ValueOption::setIsFix(bool newIsFix)
{
    m_isFix = newIsFix;

    return true;
}

TH2ValueOption::TH2ValueOption(
    TH2Element *parent,
    TH2File *th2file)
    : TH2Option (VALUE_OPTION_NAME, parent, th2file)
    , m_date(this, DATE_VALUE_NAME)
    , m_number(this, SINGLE_NUMBER_VALUE_NAME)
    , m_above(
          this,
          ABOVE_NUMBER_VALUE_NAME,
          TH2DoubleValue::Limited::Yes,
          TH2DoubleValue::Limited::No,
          LOWER_LIMIT,
          TH2DoubleValue::UNLIMITED_BOUND)
    , m_below(
          this,
          BELOW_NUMBER_VALUE_NAME,
          TH2DoubleValue::Limited::Yes,
          TH2DoubleValue::Limited::No,
          LOWER_LIMIT,
          TH2DoubleValue::UNLIMITED_BOUND)
    , m_unit(this, UNIT_VALUE_NAME)
{

}

TH2DateValue &TH2ValueOption::date()
{
    return m_date;
}

const QString TH2ValueOption::dateAsText() const
{
    return m_date.asText();
}

bool TH2ValueOption::setDateFromText(const QString &newDateText)
{
    return m_date.setDateTimeFromText(newDateText);
}

TH2ValueOption::Sign TH2ValueOption::sign() const
{
    return m_sign;
}

bool TH2ValueOption::setSign(Sign newSign)
{
    m_sign = newSign;

    return true;
}

TH2ValueOption::PassageHeight TH2ValueOption::passageHeight() const
{
    return m_passageHeight;
}

bool TH2ValueOption::setPassageHeight(TH2ValueOption::PassageHeight newPassageHeight)
{
    m_passageHeight = newPassageHeight;

    return true;
}
