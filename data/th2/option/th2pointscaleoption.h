/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2POINTSCALEOPTION_H
#define TH2POINTSCALEOPTION_H

#include "th2option.h"

#include "../th2typedefs.h"

#include "../value/th2singlesetvalue.h"
#include "../value/th2unlimiteddoublevalue.h"

#include "../../../factories/th2factory.h"

class TH2PointScaleOption : public TH2Option
{
    TH2UnlimitedDoubleValue m_number;
    TH2SingleSetValue m_preset;
    bool m_isNumeric;

protected:
    TH2PointScaleOption(
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2PointScaleOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    static const QString OPTION_NAME;
    static const QString VALUE_NAME_NUMBER;
    static const QString VALUE_NAME_PRESET;

    bool isNumeric() const;

    TH2UnlimitedDoubleValue &number();
    TH2DoubleValueNumberType numericValue() const;
    bool setNumericValue(TH2DoubleValueNumberType newNumeric);
    bool setNumericValueByText(const QString &newNumericText);

    TH2SingleSetValue &preset();
    TH2ValuesListSizeType presetID() const;
    const QString &presetText() const;
    bool setPresetByID(TH2ValuesListSizeType newPresetID);
    bool setPresetByText(const QString &newPresetText);

    bool setCurrentValueByText(const QString &newValue);

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;
    void valueChanged(const QString &valueName) override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &,
        const QString &);

};

#endif // TH2POINTSCALEOPTION_H
