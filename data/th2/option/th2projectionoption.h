/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2PROJECTIONOPTION_H
#define TH2PROJECTIONOPTION_H

#include "th2option.h"

#include "../value/th2azimuthvalue.h"
#include "../value/th2keywordvalue.h"
#include "../value/th2singlesetvalue.h"
#include "../value/th2singlesetvaluedef.h"

#include "../../../factories/th2factory.h"

class TH2ProjectionOption : public TH2Option
{
protected:
    static const QString ELEVATION_AZIMUTH_VALUE_NAME;
    static const QString INDEX_VALUE_NAME;
    static const QString PROJECTION_TYPE_VALUE_NAME;

    TH2AzimuthValue m_elevationDirection;
    TH2KeywordValue m_index;
    TH2SingleSetValue m_projectionType;
    bool m_hasElevationDirection;

    TH2ProjectionOption(
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2ProjectionOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    const TH2AzimuthValue &elevationDirection() const;
    TH2DoubleValueNumberType elevationDirectionNumericValue() const;
    bool setElevationDirectionNumericValueByValue(TH2DoubleValueNumberType newNumericValue);
    bool setElevationDirectionNumericValueByText(const QString &newNumericValueText);
    QString elevationDirectionUnitText() const;
    TH2ValuesListSizeType elevationDirectionUnitID() const;
    bool setElevationDirectionUnitByID(TH2ValuesListSizeType newUnitID);
    bool setElevationDirectionUnitByText(const QString &newUnitText);

    const TH2KeywordValue &index() const;
    QString indexText() const;
    bool setIndexText(const QString &newKeywordText);

    const TH2SingleSetValue &projectionType() const;
    QString projectionTypeText() const;
    TH2ValuesListSizeType projectionTypeID() const;
    bool setProjectionTypeByText(const QString &newProjectionTypeText);
    bool setProjectionTypeByID(TH2ValuesListSizeType newProjectionTypeID);

    bool hasElevationDirection() const;
    bool setHasElevationDirection(bool newHasElevationDirection);

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

};

#endif // TH2PROJECTIONOPTION_H
