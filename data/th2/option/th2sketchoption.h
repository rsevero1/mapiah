/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2SKETCHOPTION_H
#define TH2SKETCHOPTION_H

#include "th2option.h"

#include "../value/th2unlimiteddoublevalue.h"
#include "../value/th2freetextvalue.h"

#include "../../../graphics/th2graphicsimage.h"

#include "../../../helpers/mapiahenums.h"

#include "../../../factories/th2factory.h"

#include <QString>

class TH2SketchOption : public TH2Option
{
protected:
    static const QString FILENAME_VALUE_NAME;
    static const QString X_VALUE_NAME;
    static const QString Y_VALUE_NAME;

    TH2FreeTextValue m_filename;
    TH2UnlimitedDoubleValue m_x;
    TH2UnlimitedDoubleValue m_y;

    TH2GraphicsImage *m_image;

    TH2SketchOption(TH2Element *parent = nullptr,TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2SketchOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    const TH2FreeTextValue &filename() const;
    QString filenameValue() const;
    bool setFilenameValue(const QString &newFilename);

    const TH2UnlimitedDoubleValue &x() const;
    TH2DoubleValueNumberType xNumericValue() const;
    bool setXNumericValue(TH2DoubleValueNumberType newNumericValue);
    bool setXNumericValueByText(const QString &newNumericValueText);

    const TH2UnlimitedDoubleValue &y() const;
    TH2DoubleValueNumberType yNumericValue() const;
    bool setYNumericValue(TH2DoubleValueNumberType newNumericValue);
    bool setYNumericValueByText(const QString &newNumericValueText);

    void setImage(TH2GraphicsImage *newImage);
    TH2GraphicsImage *image() const;

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);

};

#endif // TH2SKETCHOPTION_H
