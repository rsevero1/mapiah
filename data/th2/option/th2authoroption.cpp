/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2authoroption.h"

#include "../../../helpers/mapiahenums.h"

const QString TH2AuthorOption::DATE_VALUE_NAME = QStringLiteral("author-date");
const QString TH2AuthorOption::PERSON_VALUE_NAME = QStringLiteral("author-person");

TH2DateValue &TH2AuthorOption::date()
{
    return m_date;
}

QString TH2AuthorOption::dateTimeAsText() const
{
    return m_date.valueForFile();
}

bool TH2AuthorOption::setDateTimeFromText(const QString &newDateTimeText)
{
    return m_date.setDateTimeFromText(newDateTimeText);
}

TH2PersonValue &TH2AuthorOption::person()
{
    return m_person;
}

QString TH2AuthorOption::fullname() const
{
    return m_person.valueForFile();
}

bool TH2AuthorOption::setFullName(const QString &newPersonFullName)
{
    return m_person.setFullName(newPersonFullName);
}

const QString TH2AuthorOption::elementType()
{
    return AUTHOR_OPTION_ELEMENT_TYPE;
}

QString TH2AuthorOption::valueForFile() const
{
    return QString("%1 %2").arg(
        m_date.valueForFile(),
                m_person.valueForFile());
}

bool TH2AuthorOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &,
    const QString &)
{
    return (aParentElementType == SCRAP_ELEMENT_TYPE);
}

TH2AuthorOption::TH2AuthorOption(TH2Element *parent, TH2File *th2file)
    : TH2Option(AUTHOR_OPTION_NAME, parent = nullptr, th2file)
    , m_date(this, DATE_VALUE_NAME)
    , m_person(this, PERSON_VALUE_NAME)
{

}
