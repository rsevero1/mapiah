/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2stationnamesoption.h"

const QString TH2StationNamesOption::PREFIX_VALUE_NAME = QStringLiteral("stationnames-prefix");
const QString TH2StationNamesOption::SUFFIX_VALUE_NAME = QStringLiteral("stationnames-suffix");

const TH2FreeTextValue &TH2StationNamesOption::prefix() const
{
    return m_prefix;
}

QString TH2StationNamesOption::prefixText() const
{
    return m_prefix.textValue();
}

bool TH2StationNamesOption::setPrefixText(const QString &newPrefixText)
{
    return m_prefix.setTextValue(newPrefixText);
}

const TH2FreeTextValue &TH2StationNamesOption::suffix() const
{
    return m_suffix;
}

QString TH2StationNamesOption::suffixText() const
{
    return m_suffix.textValue();
}

bool TH2StationNamesOption::setSuffixText(const QString &newSuffixText)
{
    return m_suffix.setTextValue(newSuffixText);
}

const QString TH2StationNamesOption::elementType()
{
    return STATION_NAMES_OPTION_ELEMENT_TYPE;
}

QString TH2StationNamesOption::valueForFile() const
{
    QString value;

    value = m_prefix.textValue().isEmpty() ?
        BRACKETS_STRING :
        m_prefix.textValue();

    value.append(SPACE_STRING);

    value.append(m_suffix.textValue().isEmpty() ?
         BRACKETS_STRING :
         m_suffix.textValue());

    return value;
}

bool TH2StationNamesOption::isSupportedOption(
    const QString &aParentElementType,
    const QString &,
    const QString &)
{
    return (aParentElementType == SCRAP_ELEMENT_TYPE);
}

TH2StationNamesOption::TH2StationNamesOption(
    TH2Element *parent,
    TH2File *th2file)
    : TH2Option(
          STATION_NAMES_OPTION_NAME,
          parent,
          th2file)
    , m_prefix(this, PREFIX_VALUE_NAME)
    , m_suffix(this, SUFFIX_VALUE_NAME)
{

}
