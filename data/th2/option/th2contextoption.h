/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2CONTEXTOPTION_H
#define TH2CONTEXTOPTION_H

#include "th2option.h"

#include "../value/th2singlesetvalue.h"
#include "../value/th2freetextvalue.h"

#include "../../../factories/th2factory.h"

class TH2ContextOption : public TH2Option
{
protected:
    TH2SingleSetValue m_symbolElementType;
    TH2FreeTextValue m_symbolType;

    static const QString SYMBOL_ELEMENT_TYPE_VALUE_NAME;
    static const QString SYMBOL_TYPE_VALUE_NAME;

    explicit TH2ContextOption(
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createElement<TH2ContextOption>(
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    TH2SingleSetValue &symbolElementType();

    bool setSymbolElementTypeByText(const QString &newSymbolElementType);
    bool setSymbolElementTypeByID(TH2ValuesListSizeType newSymbolElementType);
    const QString &symbolElementTypeText() const;
    TH2ValuesListSizeType symbolElementTypeID() const;

    TH2FreeTextValue &symbolType();

    bool setSymbolTypeText(const QString &newSymbolType);
    const QString &symbolTypeText();

    // TH2Element interface
    const QString elementType() override;

    // TH2Option interface
    QString valueForFile() const override;

    static bool isSupportedOption(
        const QString &aParentElementType,
        const QString &,
        const QString &);
};

#endif // TH2CONTEXTOPTION_H
