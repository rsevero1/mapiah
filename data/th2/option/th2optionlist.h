/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2OPTIONLIST_H
#define TH2OPTIONLIST_H

#include "th2option.h"

#include "../th2typedefs.h"

#include "../../../helpers/errormessage.h"

#include <QHash>
#include <QList>
#include <QString>
#include <QStringList>

class TH2File;
class TH2Option;

class TH2OptionList
{
    QStringList m_optionCods;
    QHash<QString, QString> m_optionTypes;

    TH2File *m_th2File;

public:
    explicit TH2OptionList(TH2File *th2file = nullptr);

    virtual void appendOption(th2element_up uniqueOption);
    bool existsOptionByType(const QString &optionType) const;
    TH2Option *getOptionByType(const QString &optionType) const;
    TH2File *th2file() const;
    const QStringList &optionCods() const;
    qsizetype countOptions() const;
    void clearOptions();
    const QHash<QString, QString> &optionTypes() const;
};

#endif // TH2OPTIONLIST_H
