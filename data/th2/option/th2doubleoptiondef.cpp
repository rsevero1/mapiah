/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2doubleoptiondef.h"

TH2DoubleOptionDef::TH2DoubleOptionDef(
        const QString &aOptionName,
        TH2DoubleValue::Limited aIsLowerLimited,
        TH2DoubleValue::Limited aIsUpperLimited,
        TH2DoubleValueNumberType aLowerLimit,
        TH2DoubleValueNumberType aUpperLimit,
        const QStringList &aSupportedSymbolTypesList)
    : optionName(aOptionName)
    , isLowerLimited(aIsLowerLimited)
    , isUpperLimited(aIsUpperLimited)
    , lowerLimit(aLowerLimit)
    , upperLimit(aUpperLimit)
    , supportedSymbolTypes(aSupportedSymbolTypesList)
{

}

const TH2DoubleOptionDefsPerElementTypeMapType TH2_DOUBLE_VALUE_DEFS
{
    {
        QStringLiteral("linepoint"),
        {
            {
                QStringLiteral("l-size"),
                {
                    QStringLiteral("l-size"),
                    TH2DoubleValue::Limited::Yes,
                    TH2DoubleValue::Limited::No,
                    0,
                    TH2DoubleValue::UNLIMITED_BOUND
                }
            },
            {
                QStringLiteral("r-size"),
                {
                    QStringLiteral("r-size"),
                    TH2DoubleValue::Limited::Yes,
                    TH2DoubleValue::Limited::No,
                    0,
                    TH2DoubleValue::UNLIMITED_BOUND
                }
            },
            {
                QStringLiteral("size"),
                {
                    QStringLiteral("size"),
                    TH2DoubleValue::Limited::Yes,
                    TH2DoubleValue::Limited::No,
                    0,
                    TH2DoubleValue::UNLIMITED_BOUND
                }
            },
        }
    }
};
