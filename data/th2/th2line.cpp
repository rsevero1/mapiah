/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2line.h"

#include "th2area.h"
#include "th2file.h"
#include "th2linepoint.h"

#include "option/th2contextoption.h"
#include "option/th2freetextoption.h"
#include "option/th2lengthoption.h"
#include "option/th2singlesetoption.h"

#include "../../graphics/th2graphicsline.h"

//abyss-entrance
//arrow
//border
//ceiling-meander
//ceiling-step
//chimney
//contour
//dripline
//fault
//floor-meander
//floor-step
//flowstone
//joint
//label
//low-ceiling
//map-connection
//moonmilk
//overhang
//pit
//pit-chimney
//rimstone-dam
//rimstone-pool
//rock-border
//rock-edge
//rope
//section
//slope
//survey
//u
//walkway
//wall
//water-flow

const TH2SupportedTypeListType TH2Line::SUPPORTED_SYMBOL_TYPES_LIST
{
    QStringLiteral("abyss-entrance"),
    QStringLiteral("arrow"),
    QStringLiteral("border"),
    QStringLiteral("ceiling-meander"),
    QStringLiteral("ceiling-step"),
    QStringLiteral("chimney"),
    QStringLiteral("contour"),
    QStringLiteral("dripline"),
    QStringLiteral("fault"),
    QStringLiteral("fixed-ladder"),
    QStringLiteral("floor-meander"),
    QStringLiteral("floor-step"),
    QStringLiteral("flowstone"),
    QStringLiteral("gradient"),
    QStringLiteral("joint"),
    QStringLiteral("label"),
    QStringLiteral("low-ceiling"),
    QStringLiteral("map-connection"),
    QStringLiteral("moonmilk"),
    QStringLiteral("overhang"),
    QStringLiteral("pit"),
    QStringLiteral("pitch"),
    QStringLiteral("pit-chimney"),
    QStringLiteral("rimstone-dam"),
    QStringLiteral("rimstone-pool"),
    QStringLiteral("rock-border"),
    QStringLiteral("rock-edge"),
    QStringLiteral("rope"),
    QStringLiteral("rope-ladder"),
    QStringLiteral("section"),
    QStringLiteral("slope"),
    QStringLiteral("steps"),
    QStringLiteral("survey"),
    QStringLiteral("u"),
    QStringLiteral("via-ferrata"),
    QStringLiteral("walkway"),
    QStringLiteral("wall"),
    QStringLiteral("water-flow"),
};

// Line option types
//
// name | data-type | supported line types
//
// border | singleset | slope*
// clip | singleset | all*
// close | singleset | all*
// context | context | all*
// direction | singleset | section*
// head | singleset | arrow*
// height | double limited | pit, pitch wall:pit and wall:pitch*
// id | freetext | all*
// outline | singleset | all*
// place | singleset | all*
// reverse | singleset | all*
// text | freetext | label*
// visibility | singleset | all*

const TH2SupportedOptionNameCheckFuncMapType TH2Line::SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP
{
    {QStringLiteral("border"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("clip"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("close"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("context"), &TH2ContextOption::isSupportedOption},
    {QStringLiteral("direction"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("head"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("height"), &TH2LengthOption::isSupportedOption},
    {QStringLiteral("id"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("outline"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("place"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("reverse"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("text"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("visibility"), &TH2SingleSetOption::isSupportedOption},
};

quint32 TH2Line::end_line_number() const
{
    return m_end_line_number;
}

void TH2Line::setEnd_line_number(const qint32 newEnd_line_number)
{
    m_end_line_number = newEnd_line_number;
}

void TH2Line::appendElement(th2element_up uniqueElement)
{
    TH2Element *newElement = uniqueElement.get();
    th2File()->registerElement(std::move(uniqueElement));
    m_childCods.append(newElement->cod());
    TH2LinePoint *aLinePoint = dynamic_cast<TH2LinePoint *>(newElement);
    if (aLinePoint != nullptr)
    {
        m_linepointCods.append(aLinePoint->cod());
    }
}

void TH2Line::draw(QGraphicsScene *aScene)
{
    TH2GraphicsLine *graphicLine = new TH2GraphicsLine(this);
    aScene->addItem(graphicLine);
}

bool TH2Line::isLine()
{
    return true;
}

const QList<TH2LinePoint *> TH2Line::linePoints() const
{
    QList<TH2LinePoint *> linePointsList;

    for(auto &aCod : m_linepointCods)
    {
        linePointsList.append(th2File()->linePoint(aCod));
    }
    return linePointsList;
}

TH2Area *TH2Line::borderToArea() const
{
    return m_borderToArea;
}

void TH2Line::setBorderToArea(TH2Area *newBorderToArea)
{
    m_borderToArea = newBorderToArea;
}

const QList<QString> &TH2Line::linepointCods() const
{
    return m_linepointCods;
}

qsizetype TH2Line::countLinePoints() const
{
    return m_linepointCods.count();
}

TH2Line::TH2Line(QString aType, TH2Element *parent, TH2File *th2file)
    : TH2Parent(parent, th2file), TH2OptionList(th2file)
{
    setType(aType);
}

const QString TH2Line::elementType()
{
    return "line";
}

bool TH2Line::isSupportedType(const QString &aType)
{
    return (SUPPORTED_SYMBOL_TYPES_LIST.count(aType) == 1);
}

const TH2SupportedTypeListType &TH2Line::supportedTypes()
{
    return SUPPORTED_SYMBOL_TYPES_LIST;
}
