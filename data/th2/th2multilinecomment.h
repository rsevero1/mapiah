/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2MULTILINECOMMENT_H
#define TH2MULTILINECOMMENT_H

#include "th2element.h"

#include "th2typedefs.h"

#include "../../factories/th2factory.h"

#include <QString>

class TH2File;

class TH2MultiLineComment : public TH2Element
{
    QString m_content;

    explicit TH2MultiLineComment(
        const QString &newContent,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createTypedElement<TH2MultiLineComment, QString>(
        const QString &content,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    // TH2Element interface
    const QString elementType() override;
    const QString &content() const;
};

#endif // TH2MULTILINECOMMENT_H
