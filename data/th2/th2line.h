/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2LINE_H
#define TH2LINE_H

#include "option/th2optionlist.h"
#include "th2parent.h"

#include "th2typedefs.h"

#include "../../factories/th2factory.h"

#include <QList>
#include <QMap>

#include <unordered_set>

class TH2Area;
class TH2File;
class TH2LinePoint;

class TH2Line : public TH2Parent, public TH2OptionList
{
    qint32 m_end_line_number = 0;

    QStringList m_linepointCods;

    TH2Area *m_borderToArea;

    explicit TH2Line(
        QString aType,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createTypedElement<TH2Line, QString>(
        const QString &aType,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    const QString elementType() override;

    quint32 end_line_number() const;
    void setEnd_line_number(const qint32 newEnd_line_number);

    const QList<TH2LinePoint *>linePoints() const;
    TH2Area *borderToArea() const;
    void setBorderToArea(TH2Area *newBorderToArea);
    const QList<QString> &linepointCods() const;

    qsizetype countLinePoints() const;

    // TH2Parent interface
    void appendElement(th2element_up uniqueElement) override;

    // TH2Element interface
    void draw(QGraphicsScene *aScene) override;

    // TH2Element interface
    bool isLine() override;

    static bool isSupportedType(const QString &aType);
    static const TH2SupportedTypeListType &supportedTypes();

    static const TH2SupportedOptionNameCheckFuncMapType SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP;
    static const TH2SupportedTypeListType SUPPORTED_SYMBOL_TYPES_LIST;
};

#endif // TH2LINE_H
