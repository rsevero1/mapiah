/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2linepointbezier.h"

const QString TH2LinePointBezier::elementType()
{
    return "linepointbezier";
}

QPointF TH2LinePointBezier::c1() const
{
    return m_c1;
}

void TH2LinePointBezier::setC1(const QPointF &newC1)
{
    m_c1 = newC1;
}

QPointF TH2LinePointBezier::c2() const
{
    return m_c2;
}

void TH2LinePointBezier::setC2(const QPointF &newC2)
{
    m_c2 = newC2;
}

TH2LinePointBezier::TH2LinePointBezier(TH2Element *parent, TH2File *th2file)
    : TH2LinePoint(parent, th2file)
{

}
