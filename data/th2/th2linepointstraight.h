/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2LINEPOINTSTRAIGHT_H
#define TH2LINEPOINTSTRAIGHT_H

#include "th2linepoint.h"

#include "../../factories/th2factory.h"

#include <QPointF>
#include <QString>

class TH2File;

class TH2LinePointStraight : public TH2LinePoint
{
    friend th2element_up TH2Factory::createLinePointStraight(
        QPointF coordinates,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

    explicit TH2LinePointStraight(TH2Element *parent = nullptr, TH2File *th2file = nullptr);

public:
    const QString elementType() override;
};

#endif // TH2LINEPOINTSTRAIGHT_H
