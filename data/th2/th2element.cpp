/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2element.h"
#include "th2file.h"


qint32 TH2Element::lineNumber() const
{
    return m_lineNumber;
}

QString TH2Element::cod() const
{
    return m_cod;
}

void TH2Element::setCod(const QString newCod)
{
    m_cod = newCod;
}

TH2File *TH2Element::th2File() const
{
    return m_th2file;
}

TH2Element *TH2Element::parent() const
{
    return m_parent;
}

void TH2Element::draw(QGraphicsScene *)
{

}

void TH2Element::valueChanged(const QString &)
{

}

bool TH2Element::isPoint()
{
    return false;
}

bool TH2Element::isLine()
{
    return false;
}

bool TH2Element::isArea()
{
    return false;
}

bool TH2Element::isScrap()
{
    return false;
}

bool TH2Element::isLinePoint()
{
    return false;
}

void TH2Element::setParent(TH2Element *newParent)
{
    m_parent = newParent;
}

TH2Element::TH2Element(TH2Element *parent, TH2File *th2file)
    : m_parent(parent), m_th2file(th2file)
{

}
