/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2TYPEDEFS_H
#define TH2TYPEDEFS_H

#include <QHashFunctions>
#include <QString>

#include <memory>
#include <unordered_map>
#include <unordered_set>

class TH2Element;
typedef std::unique_ptr<TH2Element> th2element_up;

class TH2File;
typedef std::unique_ptr<TH2File> th2file_up;

typedef qreal TH2DoubleValueNumberType;

typedef QString TH2ParamType;

typedef QString TH2OptionNameType;
typedef QString TH2SymbolTypeType;
typedef QString TH2ElementTypeType;

typedef std::unordered_set<QString> TH2SupportedOptionTypeListType;
typedef std::unordered_set<QString> TH2SupportedTypeListType;

// Set of typedefs for mapping element type -> option names -> element type types
typedef std::unordered_set<TH2SymbolTypeType> TH2SupportedSymbolTypeSetType;
typedef std::unordered_map<TH2OptionNameType, TH2SupportedSymbolTypeSetType> TH2SupportedSymbolTypePerOptionNameMapType;
typedef std::unordered_map<TH2ElementTypeType, TH2SupportedSymbolTypePerOptionNameMapType> TH2SupportedOptionNamesPerElementTypeMapType;

// Function ptrs to isSupportedOption()
typedef bool (*TH2CheckSupportedOptionPerElementFuncType)(
    const QString &aParentElementType,
    const QString &aParentType,
    const QString &aOptionName);

// Maps option names to function ptrs to isSupportedOption()
typedef std::unordered_map<TH2OptionNameType, TH2CheckSupportedOptionPerElementFuncType> TH2SupportedOptionNameCheckFuncMapType;

// Set of unsupported point types.
typedef std::unordered_set<TH2SymbolTypeType> TH2UnsupportedPointTypesSet;

// Used in TH2SingleSetValueDef
typedef std::vector<TH2ParamType> TH2ValuesListType;
typedef TH2ValuesListType::difference_type TH2ValuesListSizeType;
typedef std::unordered_map<TH2ParamType, TH2ParamType> TH2ValueSubstitutionMapType;
struct TH2SingleSetValueDef;
typedef std::unordered_map<TH2OptionNameType, TH2SingleSetValueDef> TH2SingleSetValueDefPerOptionNameMapType;
typedef std::unordered_map<TH2ElementTypeType, TH2SingleSetValueDefPerOptionNameMapType> TH2SingleSetValueDefsPerElementTypeMapType;

// Used by TH2LengthOptionDef
typedef std::vector<TH2SymbolTypeType> TH2LengthOptionSupportedSymbolTypesListType;
typedef std::unordered_map<TH2OptionNameType, TH2LengthOptionSupportedSymbolTypesListType> TH2LengthOptionSupportedOptionNamePerSymbolTypeMapType;
typedef std::unordered_map<TH2ElementTypeType, TH2LengthOptionSupportedOptionNamePerSymbolTypeMapType> TH2LengthOptionSupportedOptionNamePerElementTypeMapType;

// Used by TH2DoubleOptionDef
struct TH2DoubleOptionDef;
typedef std::unordered_map<TH2OptionNameType, TH2DoubleOptionDef> TH2DoubleOptionDefPerOptionNameMapType;
typedef std::unordered_map<TH2ElementTypeType, TH2DoubleOptionDefPerOptionNameMapType> TH2DoubleOptionDefsPerElementTypeMapType;

#endif // TH2TYPEDEFS_H
