/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2AREA_H
#define TH2AREA_H

#include "option/th2optionlist.h"
#include "th2parent.h"

#include "th2typedefs.h"

#include "../../factories/th2factory.h"

#include <QList>

class TH2Line;

class TH2Area : public TH2Parent, public TH2OptionList
{
    QStringList m_borderLineIDs;

protected:
    friend th2element_up TH2Factory::createTypedElement<TH2Area, QString>(
        const QString &aType,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

    explicit TH2Area(
        QString aType,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

public:
    const QString elementType() override;

    QList<TH2Line *> borderLines() const;
    const QList<QString> &borderLineIDs() const;
    bool usesLineID(const QString &aLineID) const;

    qsizetype countBorderLineIDs() const;

    // TH2Parent interface
    void appendElement(th2element_up uniqueElement) override;

    // TH2Element interface
    bool isArea() override;

    static bool isSupportedType(const QString &aType);
    static const TH2SupportedTypeListType &supportedTypes();

    static const TH2SupportedOptionNameCheckFuncMapType SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP;
    static const TH2SupportedTypeListType SUPPORTED_SYMBOL_TYPES_LIST;

    static bool isSupportedOptionType(
        const QString &aParentElementType,
        const QString &aSymbolType,
        const QString &aOptionName);
};

#endif // TH2AREA_H
