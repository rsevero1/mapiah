/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2XTHERIONUNKNOWNSETTING_H
#define TH2XTHERIONUNKNOWNSETTING_H

#include "th2xtherionsetting.h"

#include "th2typedefs.h"

#include "../../factories/th2factory.h"

#include <QString>

class TH2File;

class TH2XTherionUnknownSetting : public TH2XTherionSetting
{
protected:
    QString m_setting;

    TH2XTherionUnknownSetting(QString aParameters, TH2Element *parent = nullptr, TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createXTherionUnknownSetting(
        QString aSetting,
        QString aParameters,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    const QString setting() const override;

    // TH2Element interface
    const QString elementType() override;
};

#endif // TH2XTHERIONUNKNOWNSETTING_H
