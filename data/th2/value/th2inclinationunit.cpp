/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2inclinationunit.h"

TH2InclinationUnit::TH2InclinationUnit(TH2Element *parent, const QString &newValueName)
    : TH2SingleSetValue(parent, newValueName, TH2_INCLINATION_VALUE_DEF)
{

}

const QString TH2InclinationUnit::inclinationText(qsizetype newInclinationID) const
{
    return textByID(newInclinationID);
}

qsizetype TH2InclinationUnit::inclinationValueID() const
{
    return singleSetValueID();
}

bool TH2InclinationUnit::setInclinationValueByID(qsizetype newInclinationValueID)
{
    return setSingleSetValueByID(newInclinationValueID);
}

const QString &TH2InclinationUnit::inclinationValueText() const
{
    return singleSetValueText();
}

bool TH2InclinationUnit::setInclinationValueByText(const QString &newInclinationValueByText)
{
    return setSingleSetValueByText(newInclinationValueByText);
}
