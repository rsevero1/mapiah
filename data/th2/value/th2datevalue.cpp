/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2datevalue.h"

#include "../../../helpers/mapiahenums.h"
#include "../../../helpers/mapiahsupport.h"

const QString TH2DateValue::DATETIME_FRACTIONAL_SECONDS_AS_TEXT = QStringLiteral("z");
const QString TH2DateValue::DATETIME_NO_FRACTIONAL_SECONDS_AS_TEXT = QStringLiteral("yyyy.MM.dd@HH:mm:ss");
const QString TH2DateValue::DATETIME_WITH_FRACTIONAL_SECONDS_AS_TEXT = QStringLiteral("yyyy.MM.dd@HH:mm:ss.z");

const QString TH2DateValue::DATETIME_RANGE_SEPARATOR = QStringLiteral("-");
const QString TH2DateValue::UNSPECIFIED_TEXT = QStringLiteral("-");

const QRegularExpression TH2DateValue::rangeDateTimeRegex = QRegularExpression(R"RX(
^
    (?<start_year>\d{4})
        (?:\.(?<start_month>\d{1,2})
            (?:\.(?<start_day>\d{1,2})
                (?:@(?<start_hours>\d{1,2})
                    (?::(?<start_minutes>\d{1,2})
                        (?::(?<start_seconds>\d{1,2}(?:.\d+)?))?
                    )?
                )?
            )?
        )?
    (?:\s*-\s*(?<end_year>\d{4})
        (?:\.(?<end_month>\d{1,2})
            (?:\.(?<end_day>\d{1,2})
                (?:@(?<end_hours>\d{1,2})
                    (?::(?<end_minutes>\d{1,2})
                        (?::(?<end_seconds>\d{1,2}(?:.\d+)?))?
                    )?
                )?
            )?
        )?
    )?
$
)RX",
    QRegularExpression::ExtendedPatternSyntaxOption);
const QRegularExpression TH2DateValue::singleDateTimeRegex = QRegularExpression(R"RX(
^
    (?<year>\d{4})
        (?:\.(?<month>\d{1,2})
            (?:\.(?<day>\d{1,2})
                (?:@(?<hours>\d{1,2})
                    (?::(?<minutes>\d{1,2})
                        (?::(?<seconds>\d{1,2}(?:.\d+)?))?
                    )?
                )?
            )?
        )?
$
)RX",
    QRegularExpression::ExtendedPatternSyntaxOption);

const QString TH2DateValue::asText() const
{
    if (m_isSpecified == Specified::No)
    {
        return UNSPECIFIED_TEXT;
    }

    QString value = QString("%1").arg(m_startYear, 4, 10, QLatin1Char('0'));

    if (m_startMonth != -1)
    {
        value.append(QString(".%1").arg(m_startMonth, 2, 10, QLatin1Char('0')));
        if (m_startDay != -1)
        {
            value.append(QString(".%1").arg(m_startDay, 2, 10, QLatin1Char('0')));
            if (m_startHour != -1)
            {
                value.append(QString("@%1").arg(m_startHour, 2, 10, QLatin1Char('0')));
                if (m_startMinute != -1)
                {
                    value.append(QString(":%1").arg(m_startMinute, 2, 10, QLatin1Char('0')));
                    if (m_startSeconds != -1)
                    {
                        value.append(QString(":%1").arg(
                            MapiahSupport::formatedFloatForWriting(m_startSeconds, 2)));
                    }
                }
            }
        }
    }

    if (m_endYear != -1)
    {
        value.append(QString(" - %1").arg(m_endYear, 4, 10, QLatin1Char('0')));
        if (m_endMonth != -1)
        {
            value.append(QString(".%1").arg(m_endMonth, 2, 10, QLatin1Char('0')));
            if (m_endDay != -1)
            {
                value.append(QString(".%1").arg(m_endDay, 2, 10, QLatin1Char('0')));
                if (m_endHour != -1)
                {
                    value.append(QString("@%1").arg(m_endHour, 2, 10, QLatin1Char('0')));
                    if (m_endMinute != -1)
                    {
                        value.append(QString(":%1").arg(m_endMinute, 2, 10, QLatin1Char('0')));
                        if (m_endSeconds != -1)
                        {
                            value.append(QString(":%1").arg(
                                MapiahSupport::formatedFloatForWriting(m_endSeconds, 2)));
                        }
                    }
                }
            }
        }
    }

    return value;
}

TH2DateValue::TH2DateValue(TH2Element *newParent, const QString &newValueName)
    : TH2Value(newParent, newValueName)
    , m_isSpecified(Specified::No)
    , m_isInterval(Interval::UNSET)
{

}

bool TH2DateValue::setDateTimeFromText(const QString &newDateTimeText)
{
    if (newDateTimeText == UNSPECIFIED_TEXT)
    {
        setIsSpecified(Specified::No);
        m_isInterval = Interval::UNSET;

        // Classes that derived directly from TH2Value should call parent option
        // valueChanged method.
        m_parent->valueChanged(valueName());

        return true;
    }

    QRegularExpressionMatch match = rangeDateTimeRegex.match(newDateTimeText);

    if (match.hasMatch())
    {
        if (match.captured("start_year").isEmpty())
        {
            return false;
        }

        m_isInterval = Interval::No;
        m_isSpecified = Specified::Yes;
        m_startYear = match.captured("start_year").toInt();
        m_startMonth = -1;
        m_startDay = -1;
        m_startHour = -1;
        m_startMinute = -1;
        m_startSeconds = -1;
        m_endYear = -1;
        m_endMonth = -1;
        m_endDay = -1;
        m_endHour = -1;
        m_endMinute = -1;
        m_endSeconds = -1;

        if (!match.captured("start_month").isEmpty())
        {
            m_startMonth = match.captured("start_month").toInt();
            if ((m_startMonth < 1) || (m_startMonth > 12))
            {
                return false;
            }
            if (!match.captured("start_day").isEmpty())
            {
                m_startDay = match.captured("start_day").toInt();
                if ((m_startDay < 1) || (m_startDay > 31))
                {
                    return false;
                }
                if (!match.captured("start_hours").isEmpty())
                {
                    m_startHour = match.captured("start_hours").toInt();
                    if ((m_startHour < 0) || (m_startHour > 23))
                    {
                        return false;
                    }
                    if (!match.captured("start_minutes").isEmpty())
                    {
                        m_startMinute = match.captured("start_minutes").toInt();
                        if ((m_startMinute < 0) || (m_startMinute > 59))
                        {
                            return false;
                        }
                        if (!match.captured("start_seconds").isEmpty())
                        {
                            m_startSeconds = match.captured("start_seconds").toDouble();
                            if ((m_startSeconds < 0) || (m_startSeconds >= 60))
                            {
                                return false;
                            }
                        }
                    }
                }
            }
        }

    }

    if (!match.captured("end_year").isEmpty())
    {
        m_isInterval = Interval::Yes;
        m_endYear = match.captured("end_year").toInt();
        if (!match.captured("end_month").isEmpty())
        {
            m_endMonth = match.captured("end_month").toInt();
            if ((m_endMonth < 1) || (m_endMonth > 12))
            {
                return false;
            }
            if (!match.captured("end_day").isEmpty())
            {
                m_endDay = match.captured("end_day").toInt();
                if ((m_endDay < 1) || (m_endDay > 31))
                {
                    return false;
                }
                if (!match.captured("end_hours").isEmpty())
                {
                    m_endHour = match.captured("end_hours").toInt();
                    if ((m_endHour < 0) || (m_endHour > 23))
                    {
                        return false;
                    }
                    if (!match.captured("end_minutes").isEmpty())
                    {
                        m_endMinute = match.captured("end_minutes").toInt();
                        if ((m_endMinute < 0) || (m_endMinute > 59))
                        {
                            return false;
                        }
                        if (!match.captured("end_seconds").isEmpty())
                        {
                            m_endSeconds = match.captured("end_seconds").toDouble();
                            if ((m_endSeconds < 0) || (m_endSeconds >= 60))
                            {
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }

    m_parent->valueChanged(valueName());

    return true;
}

TH2DateValue::Specified TH2DateValue::isSpecified() const
{
    return m_isSpecified;
}

bool TH2DateValue::setIsSpecified(Specified newIsSpecified)
{
    m_isSpecified = newIsSpecified;

    return true;
}

const QString TH2DateValue::valueForFile() const
{
    return asText();
}

TH2DateValue::Interval TH2DateValue::isInterval() const
{
    return m_isInterval;
}
