/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2SINGLESETVALUEDEF_H
#define TH2SINGLESETVALUEDEF_H

#include "../th2typedefs.h"

#include "../../../helpers/mapiahsupport.h"

#include <QString>

#include <unordered_map>
#include <vector>

extern const TH2ParamType TH2_SINGLE_SET_VALUE_INVALID;
extern const TH2ParamType TH2_SINGLE_SET_VALUE_UNSET;

struct TH2SingleSetValueDef {
    const QString optionName;
    const QString defaultValueName;
    TH2ValuesListType valueNames;
    TH2ValueSubstitutionMapType valueSubstitutions;
    TH2SingleSetValueDef(
        QString aOptionName,
        QString aDefaultValueName,
        TH2ValuesListType aListOfValueNames,
        TH2ValueSubstitutionMapType aValueSubstitutions = {}
    )
        : optionName(aOptionName)
        , defaultValueName(aDefaultValueName)
        , valueNames(MapiahSupport::insertAtBeginng(aListOfValueNames, TH2_SINGLE_SET_VALUE_UNSET))
        , valueSubstitutions(aValueSubstitutions)
          {};
};

extern const TH2SingleSetValueDefsPerElementTypeMapType TH2_SINGLE_SET_VALUE_DEFS;
extern const TH2SingleSetValueDefsPerElementTypeMapType TH2_SUBTYPE_SINGLE_SET_VALUE_DEFS;

extern const TH2SingleSetValueDef TH2_LENGTH_VALUE_DEF;
extern const TH2SingleSetValueDef TH2_AZIMUTH_VALUE_DEF;
extern const TH2SingleSetValueDef TH2_CONTEXT_VALUE_DEF;
extern const TH2SingleSetValueDef TH2_INCLINATION_VALUE_DEF;
extern const TH2SingleSetValueDef TH2_POINT_SCALE_VALUE_DEF;
extern const TH2SingleSetValueDef TH2_PROJECTION_VALUE_DEF;

#endif // TH2SINGLESETVALUEDEF_H
