/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2VALUE_H
#define TH2VALUE_H

#include "../th2element.h"

#include <QString>

class TH2Value
{
protected:
    TH2Element *m_parent;
    QString m_valueName;

public:
    TH2Value(TH2Element *newParent, const QString &newValueName);

    // Always make base class destructor as virtual in C++
    // https://codeyarns.com/tech/2016-12-28-always-make-base-class-destructor-as-virtual-in-c.html
    virtual ~TH2Value() {};

    TH2Element *parent() const;

    const QString &valueName() const;

    virtual const QString valueForFile() const = 0;
};

#endif // TH2VALUE_H
