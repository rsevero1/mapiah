/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2azimuthvalue.h"

TH2DoubleValue &TH2AzimuthValue::number()
{
    return m_number;
}

TH2AzimuthUnit &TH2AzimuthValue::unit()
{
    return m_unit;
}

bool TH2AzimuthValue::setNumericValue(TH2DoubleValueNumberType newNumber)
{
    return m_number.setDoubleValue(newNumber);
}

bool TH2AzimuthValue::setNumericValueByText(const QString &newNumber)
{
    return m_number.setDoubleValueByText(newNumber);
}

bool TH2AzimuthValue::setUnitByText(const QString &newUnit)
{
    return m_unit.setAzimuthValueByText(newUnit);
}

bool TH2AzimuthValue::setUnitByID(TH2ValuesListSizeType newUnitID)
{
    return m_unit.setAzimuthValueByID(newUnitID);
}

bool TH2AzimuthValue::isDefaultUnit() const
{
    return m_unit.isDefault();
}

TH2DoubleValueNumberType TH2AzimuthValue::numericValue() const
{
    return m_number.doubleValue();
}

const QString &TH2AzimuthValue::unitText() const
{
    return m_unit.azimuthValueByText();
}

TH2ValuesListSizeType TH2AzimuthValue::unitID() const
{
    return m_unit.azimuthValueID();
}

const QString TH2AzimuthValue::valueForFile() const
{
    if (qFuzzyIsNull(m_number.doubleValue()))
    {
        return m_number.valueForFile();
    }
    else
    {
        return QString("%1 %2")
            .arg(m_number.valueForFile(), m_unit.valueForFile());
    }
}

TH2AzimuthValue::TH2AzimuthValue(TH2Element *newParent, const QString &newValueName)
    : TH2Value(newParent, newValueName)
    , m_number(
          newParent,
          QString("%1-azimuth-value").arg(newValueName),
          TH2DoubleValue::Limited::Yes,
          TH2DoubleValue::Limited::Yes,
          MINIMUM_AZIMUTH,
          MAXIMUM_AZIMUTH)
    , m_unit(newParent, QString("%1-azimuth-unit").arg(newValueName))
{

}
