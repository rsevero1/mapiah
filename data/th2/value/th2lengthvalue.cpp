/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2lengthvalue.h"

TH2UnlimitedDoubleValue &TH2LengthValue::number()
{
    return m_number;
}

TH2DoubleValueNumberType TH2LengthValue::numericValue() const
{
    return m_number.doubleValue();
}

bool TH2LengthValue::setNumericValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_number.setDoubleValue(newNumericValue);
}

bool TH2LengthValue::setNumericValueByText(const QString &newNumericValueByText)
{
    return m_number.setDoubleValueByText(newNumericValueByText);
}

TH2LengthUnit &TH2LengthValue::unit()
{
    return m_unit;
}

TH2ValuesListSizeType TH2LengthValue::unitID() const
{
    return m_unit.lengthUnitValueID();
}

const QString &TH2LengthValue::unitText() const
{
    return m_unit.lengthUnitValueByText();
}

bool TH2LengthValue::setUnitByID(TH2ValuesListSizeType newUnitID)
{
    return m_unit.setLengthUnitValueByID(newUnitID);
}

bool TH2LengthValue::setUnitByText(const QString &newUnitText)
{
    return m_unit.setLengthUnitValueByText(newUnitText);
}

bool TH2LengthValue::isDefaultUnit() const
{
    return m_unit.isDefault();
}

TH2LengthValue::TH2LengthValue(TH2Element *newParent, const QString &newValueName)
    : TH2Value(newParent, newValueName)
    , m_number(newParent, QString("%1-length-value").arg(newValueName))
    , m_unit(newParent, QString("%1-length-unit").arg(newValueName))
{

}

const QString TH2LengthValue::valueForFile() const
{
    if (qFuzzyIsNull(m_number.doubleValue()))
    {
        return m_number.valueForFile();
    }
    else
    {
        return QString("%1 %2")
            .arg(m_number.valueForFile(), m_unit.valueForFile());
    }
}
