/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2regexedvalue.h"

TH2RegexedValue::TH2RegexedValue(
    const QRegularExpression &newRegularExpression,
    TH2Element *newParent,
    const QString &newValueName)
    : TH2Value(newParent, newValueName)
    , m_regularExpression(newRegularExpression)
{

}

const QString &TH2RegexedValue::regexedValue() const
{
    return m_regexedValue;
}

bool TH2RegexedValue::setRegexedValue(const QString &newRegexedValue)
{
    if (!isValidValue(newRegexedValue))
    {
        return false;
    }

    m_regexedValue = newRegexedValue;

    // Classes that derived directly from TH2Value should call parent option
    // valueChanged method.
    m_parent->valueChanged(valueName());

    return true;
}

const QString TH2RegexedValue::valueForFile() const
{
    return m_regexedValue;
}

bool TH2RegexedValue::isValidValue(const QString &aTextValue) const
{
    QRegularExpressionMatch match = m_regularExpression.match(
        aTextValue,
        0,
        QRegularExpression::NormalMatch);

    return match.hasMatch();
}
