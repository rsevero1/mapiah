/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2REGEXEDVALUE_H
#define TH2REGEXEDVALUE_H

#include "th2value.h"

#include <QRegularExpression>

class TH2RegexedValue : public TH2Value
{
protected:
    QString m_regexedValue;

    const QRegularExpression &m_regularExpression;

    bool isValidValue(const QString &aTextValue) const;

public:
    TH2RegexedValue(
        const QRegularExpression &newRegularExpression,
        TH2Element *newParent,
        const QString &newValueName);

    const QString &regexedValue() const;
    bool setRegexedValue(const QString &newRegexedValue);

    // TH2Value interface
    const QString valueForFile() const override;
};

#endif // TH2REGEXEDVALUE_H
