/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2AZIMUTHUNIT_H
#define TH2AZIMUTHUNIT_H

#include "th2singlesetvalue.h"

class TH2AzimuthUnit : public TH2SingleSetValue
{
public:
    TH2AzimuthUnit(
        TH2Element *parent,
        const QString &newValueName);

    const QString azimuthText(TH2ValuesListSizeType newAzimuthID) const;
    TH2ValuesListSizeType azimuthValueID() const;
    bool setAzimuthValueByID(TH2ValuesListSizeType newAzimuthValueID);
    const QString &azimuthValueByText() const;
    bool setAzimuthValueByText(const QString &newAzimuthValueByText);
};

#endif // TH2AZIMUTHUNIT_H
