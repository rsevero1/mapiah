/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2inclinationvalue.h"

TH2DoubleValue &TH2InclinationValue::number()
{
    return m_number;
}

TH2DoubleValueNumberType TH2InclinationValue::numericValue() const
{
    return m_number.doubleValue();
}

bool TH2InclinationValue::setNumericValue(TH2DoubleValueNumberType newNumericValue)
{
    return m_number.setDoubleValue(newNumericValue);
}

bool TH2InclinationValue::setNumericValueByText(const QString &newNumericValueText)
{
    return m_number.setDoubleValueByText(newNumericValueText);
}

TH2InclinationUnit &TH2InclinationValue::unit()
{
    return m_unit;
}

TH2ValuesListSizeType TH2InclinationValue::unitID() const
{
    return m_unit.inclinationValueID();
}

QString TH2InclinationValue::unitText() const
{
    return m_unit.inclinationValueText();
}

bool TH2InclinationValue::setUnitByID(TH2ValuesListSizeType newUnitID)
{
    return m_unit.setInclinationValueByID(newUnitID);
}

bool TH2InclinationValue::setUnitByText(const QString &newUnitText)
{
    return m_unit.setInclinationValueByText(newUnitText);
}

bool TH2InclinationValue::isDefaultUnit() const
{
    return m_unit.isDefault();
}

const QString TH2InclinationValue::valueForFile() const
{
    if (qFuzzyIsNull(m_number.doubleValue()))
    {
        return m_number.valueForFile();
    }
    else
    {
        return QString("%1 %2")
            .arg(m_number.valueForFile(), m_unit.valueForFile());
    }
}

TH2InclinationValue::TH2InclinationValue(TH2Element *parent, const QString &newValueName)
    : TH2Value(parent, newValueName)
    , m_number(
          parent,
          QString("%1-inclination-value").arg(newValueName),
          TH2DoubleValue::Limited::Yes,
          TH2DoubleValue::Limited::Yes,
          MINIMUM_INCLINATION,
          MAXIMUM_INCLINATION)
    , m_unit(parent, QString("%1-inclination-unit").arg(newValueName))
{

}
