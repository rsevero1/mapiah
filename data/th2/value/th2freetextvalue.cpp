/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2freetextvalue.h"

#include "../../../helpers/mapiahenums.h"

const QString &TH2FreeTextValue::textValue() const
{
    return m_textValue;
}

bool TH2FreeTextValue::setTextValue(const QString &newTextValue)
{
    m_textValue = newTextValue;

    // Classes that derived directly from TH2Value should call parent option
    // valueChanged method.
    m_parent->valueChanged(valueName());

    return true;
}

const QString TH2FreeTextValue::valueForFile() const
{
    QString outputText = m_textValue;

    outputText.replace(QUOTE_STRING, QUOTE_IN_QUOTE_STRING);

    if (outputText.contains(SPACE_STRING)
        || outputText.contains(QUOTE_STRING)
        || outputText.contains(LF_STRING)
        || outputText.contains(CR_STRING))
    {
        outputText = QString(R"RX("%1")RX").arg(outputText);
    }

    return outputText;
}

TH2FreeTextValue::TH2FreeTextValue(TH2Element *parent, const QString &newValueName)
    : TH2Value(parent, newValueName)
{

}
