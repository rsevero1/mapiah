/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2SINGLESETVALUE_H
#define TH2SINGLESETVALUE_H

#include "th2value.h"

#include "th2singlesetvaluedef.h"

class TH2SingleSetValue : public TH2Value
{
protected:
    static const TH2ValuesListSizeType INVALID_ID;

    TH2ValuesListSizeType m_singleSetValue;

    const TH2SingleSetValueDef &m_singleSetOptionDef;

    bool isValidID(TH2ValuesListSizeType aValueID) const;
    bool isValidText(const QString &aValueByText) const;
    bool isValidValue(const QString &aTextValue) const;

public:
    TH2SingleSetValue(
        TH2Element *parent,
        const QString &newValueName,
        const TH2SingleSetValueDef &newSingleSetOptionDef);

    const QString &textByID(TH2ValuesListSizeType newValueID) const;
    TH2ValuesListSizeType idByText(const QString &aValueByText) const;
    const QString &substitutedText(const QString &aText) const;
    TH2ValuesListSizeType singleSetValueID() const;
    bool setSingleSetValueByID(TH2ValuesListSizeType newSingleSetValueID);
    const QString &singleSetValueText() const;
    bool setSingleSetValueByText(const QString &newValueByText);

    bool isDefault() const;
    bool isSet() const;

    // TH2Value interface
    const QString valueForFile() const override;

    const TH2SingleSetValueDef &singleSetOptionDef() const;

};

#endif // TH2SINGLESETVALUE_H
