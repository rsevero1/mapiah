/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2lengthunit.h"

const TH2ValuesListSizeType TH2LengthUnit::DEFAULT_VALUE = 0;

TH2LengthUnit::TH2LengthUnit(
    TH2Element *parent,
    const QString &newValueName)
    : TH2SingleSetValue(parent, newValueName, TH2_LENGTH_VALUE_DEF)
{

}

const QString TH2LengthUnit::lengthUnitText(TH2ValuesListSizeType aLengthUnitID) const
{
    return textByID(aLengthUnitID);
}

TH2ValuesListSizeType TH2LengthUnit::lengthUnitValueID() const
{
    return singleSetValueID();
}

bool TH2LengthUnit::setLengthUnitValueByID(TH2ValuesListSizeType newLengthUnitValueID)
{
    return setSingleSetValueByID(newLengthUnitValueID);
}

const QString &TH2LengthUnit::lengthUnitValueByText() const
{
    return singleSetValueText();
}

bool TH2LengthUnit::setLengthUnitValueByText(const QString &newLengthUnitValueByText)
{
    return setSingleSetValueByText(newLengthUnitValueByText);
}
