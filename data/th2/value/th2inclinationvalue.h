/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2INCLINATIONVALUE_H
#define TH2INCLINATIONVALUE_H

#include "th2value.h"

#include "th2doublevalue.h"
#include "th2inclinationunit.h"

class TH2InclinationValue : public TH2Value
{
protected:
    TH2DoubleValue m_number;
    TH2InclinationUnit m_unit;

    static constexpr const TH2DoubleValueNumberType MINIMUM_INCLINATION = -90;
    static constexpr const TH2DoubleValueNumberType MAXIMUM_INCLINATION = 90;

public:
    TH2InclinationValue(TH2Element *parent, const QString &newValueName);

    TH2DoubleValue &number();
    TH2DoubleValueNumberType numericValue() const;
    bool setNumericValue(TH2DoubleValueNumberType newNumericValue);
    bool setNumericValueByText(const QString &newNumericValueText);

    TH2InclinationUnit &unit();
    TH2ValuesListSizeType unitID() const;
    QString unitText() const;
    bool setUnitByID(TH2ValuesListSizeType newUnitID);
    bool setUnitByText(const QString &newUnitText);

    bool isDefaultUnit() const;

    // TH2Value interface
    const QString valueForFile() const override;
};

#endif // TH2INCLINATIONVALUE_H
