/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2AZIMUTHVALUE_H
#define TH2AZIMUTHVALUE_H

#include "th2value.h"

#include "th2doublevalue.h"
#include "th2azimuthunit.h"

class TH2AzimuthValue : public TH2Value
{
protected:
    TH2DoubleValue m_number;
    TH2AzimuthUnit m_unit;

    constexpr static TH2DoubleValueNumberType MINIMUM_AZIMUTH = 0;
    constexpr static TH2DoubleValueNumberType MAXIMUM_AZIMUTH = 360;

public:
    TH2AzimuthValue(TH2Element *newParent, const QString &newValueName);

    TH2DoubleValue &number();
    TH2DoubleValueNumberType numericValue() const;
    bool setNumericValue(TH2DoubleValueNumberType newNumber);
    bool setNumericValueByText(const QString &newNumber);

    TH2AzimuthUnit &unit();
    const QString &unitText() const;
    TH2ValuesListSizeType unitID() const;
    bool setUnitByText(const QString &newUnit);
    bool setUnitByID(TH2ValuesListSizeType newUnitID);
    bool isDefaultUnit() const;

    // TH2Value interface
    const QString valueForFile() const override;
};

#endif // TH2AZIMUTHVALUE_H
