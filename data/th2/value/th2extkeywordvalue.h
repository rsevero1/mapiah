/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2EXTKEYWORDVALUE_H
#define TH2EXTKEYWORDVALUE_H

#include "th2regexedvalue.h"

class TH2ExtKeywordValue : public TH2RegexedValue
{
protected:
    static const QRegularExpression supportedCharsRegex;

public:
    TH2ExtKeywordValue(TH2Element *newParent, const QString &newValueName);

    const QString &extKeywordValue() const;
    bool setExtKeywordValue(const QString &newExtKeywordValue);
};

#endif // TH2EXTKEYWORDVALUE_H
