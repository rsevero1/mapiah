/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2azimuthunit.h"

TH2AzimuthUnit::TH2AzimuthUnit(TH2Element *parent, const QString &newValueName)
    : TH2SingleSetValue(parent, newValueName, TH2_AZIMUTH_VALUE_DEF)
{

}

const QString TH2AzimuthUnit::azimuthText(TH2ValuesListSizeType newAzimuthID) const
{
    return textByID(newAzimuthID);
}

TH2ValuesListSizeType TH2AzimuthUnit::azimuthValueID() const
{
    return singleSetValueID();
}

bool TH2AzimuthUnit::setAzimuthValueByID(TH2ValuesListSizeType newAzimuthValueID)
{
    return setSingleSetValueByID(newAzimuthValueID);
}

const QString &TH2AzimuthUnit::azimuthValueByText() const
{
    return singleSetValueText();
}

bool TH2AzimuthUnit::setAzimuthValueByText(const QString &newAzimuthValueByText)
{
    return setSingleSetValueByText(newAzimuthValueByText);
}
