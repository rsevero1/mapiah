/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2INCLINATIONUNIT_H
#define TH2INCLINATIONUNIT_H

#include "th2singlesetvalue.h"

class TH2InclinationUnit : public TH2SingleSetValue
{
public:
    TH2InclinationUnit(
        TH2Element *parent,
        const QString &newValueName);

    const QString inclinationText(qsizetype newInclinationID) const;
    qsizetype inclinationValueID() const;
    bool setInclinationValueByID(qsizetype newInclinationValueID);
    const QString &inclinationValueText() const;
    bool setInclinationValueByText(const QString &newInclinationValueByText);
};

#endif // TH2INCLINATIONUNIT_H
