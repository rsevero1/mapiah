/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2singlesetvalue.h"

#include "../../../helpers/mapiahsupport.h"

const TH2ValuesListSizeType TH2SingleSetValue::INVALID_ID = -1;

TH2ValuesListSizeType TH2SingleSetValue::singleSetValueID() const
{
    return m_singleSetValue;
}

bool TH2SingleSetValue::setSingleSetValueByID(TH2ValuesListSizeType newSingleSetValueID)
{
    if (!isValidID(newSingleSetValueID))
    {
        return false;
    }

    m_singleSetValue = newSingleSetValueID;

    // Classes that derived directly from TH2Value should call parent option
    // valueChanged method.
    m_parent->valueChanged(valueName());

    return true;
}

const QString &TH2SingleSetValue::singleSetValueText() const
{
    return textByID(m_singleSetValue);
}

bool TH2SingleSetValue::setSingleSetValueByText(const QString &newValueByText)
{
    if (!isValidValue(newValueByText))
    {
        return false;
    }

    return setSingleSetValueByID(idByText(newValueByText));
}

bool TH2SingleSetValue::isDefault() const
{
    return ((m_singleSetValue == INVALID_ID)
        || (m_singleSetValue == idByText(m_singleSetOptionDef.defaultValueName)));
}

const QString TH2SingleSetValue::valueForFile() const
{
    if (isDefault())
    {
        return m_singleSetOptionDef.defaultValueName;
    }
    else
    {
        return singleSetValueText();
    }
}

bool TH2SingleSetValue::isValidValue(const QString &aTextValue) const
{
    QString aText = substitutedText(aTextValue);

    return MapiahSupport::contains(m_singleSetOptionDef.valueNames, aText);
}

TH2SingleSetValue::TH2SingleSetValue(TH2Element *parent,
    const QString &newValueName,
    const TH2SingleSetValueDef &newSingleSetOptionDef)
    : TH2Value(parent, newValueName)
    , m_singleSetValue(INVALID_ID)
    , m_singleSetOptionDef(newSingleSetOptionDef)
{

}

const QString &TH2SingleSetValue::textByID(TH2ValuesListSizeType newValueID) const
{
    if (!isValidID(newValueID))
    {
        return TH2_SINGLE_SET_VALUE_INVALID;
    }

    return m_singleSetOptionDef.valueNames.at(newValueID);
}

TH2ValuesListSizeType TH2SingleSetValue::idByText(const QString &aValueByText) const
{
    if (!isValidValue(aValueByText))
    {
        return -1;
    }

    QString aText = substitutedText(aValueByText);

    return MapiahSupport::indexOf(m_singleSetOptionDef.valueNames, aText);
}

const TH2SingleSetValueDef &TH2SingleSetValue::singleSetOptionDef() const
{
    return m_singleSetOptionDef;
}

bool TH2SingleSetValue::isSet() const
{
    return isValidID(m_singleSetValue);
}

bool TH2SingleSetValue::isValidID(TH2ValuesListSizeType aValueID) const
{
    return ((aValueID >= 0)
        && (aValueID < (TH2ValuesListSizeType)(m_singleSetOptionDef.valueNames.size())));
}

bool TH2SingleSetValue::isValidText(const QString &aValueByText) const
{
    return isValidValue(aValueByText);
}

const QString &TH2SingleSetValue::substitutedText(const QString &aText) const
{
    if (m_singleSetOptionDef.valueSubstitutions.count(aText) == 0)
    {
        return aText;
    }
    else
    {
        return m_singleSetOptionDef.valueSubstitutions.at(aText);
    }
}
