/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2doublevalue.h"

#include "../option/th2option.h"

#include "../../../helpers/mapiahsupport.h"

TH2DoubleValueNumberType TH2DoubleValue::doubleValue() const
{
    return m_doubleValue;
}

bool TH2DoubleValue::setDoubleValue(TH2DoubleValueNumberType newDoubleValue)
{
    if (!withinLimits(newDoubleValue))
    {
        return false;
    }

    m_doubleValue = newDoubleValue;

    // Classes that derived directly from TH2Value should call parent option
    // valueChanged method.
    m_parent->valueChanged(valueName());

    return true;
}

bool TH2DoubleValue::setDoubleValueByText(const QString &newStringDoubleValue)
{
    if (!isValidValue(newStringDoubleValue))
    {
        return false;
    }

    TH2DoubleValueNumberType aDoubleValue = newStringDoubleValue.toDouble();

    return setDoubleValue(aDoubleValue);
}

const QString TH2DoubleValue::valueForFile() const
{
    return MapiahSupport::formatedFloatForWriting(m_doubleValue);
}

bool TH2DoubleValue::isValidValue(const QString &aTextValue) const
{
    bool convertResult;

    TH2DoubleValueNumberType aDoubleValue = aTextValue.toDouble(&convertResult);

    if (!convertResult)
    {
        return false;
    }

    return withinLimits(aDoubleValue);
}

bool TH2DoubleValue::withinLimits(TH2DoubleValueNumberType aDouble) const
{
    if ((m_isLowerLimited == Limited::Yes)
        && (aDouble < m_lowerLimit))
    {
        return false;
    }

    if ((m_is_UpperLimited == Limited::Yes)
        && (aDouble > m_upperLimit))
    {
        return false;
    }

    return true;
}

TH2DoubleValue::TH2DoubleValue(TH2Element *parent,
    const QString &newValueName,
    Limited newIsLowerLimited,
    Limited newIsUpperLimited,
    TH2DoubleValueNumberType newLowerLimit,
    TH2DoubleValueNumberType newUpperLimit)
    :TH2Value(parent, newValueName)
    , m_doubleValue(DEFAULT_VALUE)
    , m_isLowerLimited(newIsLowerLimited)
    , m_is_UpperLimited(newIsUpperLimited)
    , m_lowerLimit(newLowerLimit)
    , m_upperLimit(newUpperLimit)
{

}
