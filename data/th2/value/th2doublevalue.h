/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2DOUBLEVALUE_H
#define TH2DOUBLEVALUE_H

#include "th2value.h"

#include "../th2typedefs.h"

#include <QtGlobal>

class TH2DoubleValue : public TH2Value
{
    TH2DoubleValueNumberType m_doubleValue;

    bool isValidValue(const QString &aTextValue) const;

public:
    // Making ctor calls in derived classes clearer.
    enum class Limited {
        No,
        Yes
    };

    TH2DoubleValue(
        TH2Element *parent,
        const QString &newValueName,
        Limited newIsLowerLimited,
        Limited newIsUpperLimited,
        TH2DoubleValueNumberType newLowerLimit,
        TH2DoubleValueNumberType newUpperLimit);

    TH2DoubleValueNumberType doubleValue() const;
    bool setDoubleValue(TH2DoubleValueNumberType newDoubleValue);
    bool setDoubleValueByText(const QString &newStringDoubleValue);

    // TH2Value interface
    const QString valueForFile() const override;

    bool withinLimits(TH2DoubleValueNumberType aDouble) const;

    // Convenience constant so derived classes don't have to declare their own
    // unbounded constants.
    static constexpr TH2DoubleValueNumberType UNLIMITED_BOUND = 0;

    static constexpr TH2DoubleValueNumberType DEFAULT_VALUE = 0;

protected:
    const Limited m_isLowerLimited;
    const Limited m_is_UpperLimited;
    const TH2DoubleValueNumberType m_lowerLimit;
    const TH2DoubleValueNumberType m_upperLimit;

};

#endif // TH2DOUBLEVALUE_H
