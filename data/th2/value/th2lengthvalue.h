/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2LENGTHVALUE_H
#define TH2LENGTHVALUE_H

#include "th2value.h"
#include "th2unlimiteddoublevalue.h"
#include "th2lengthunit.h"

class TH2LengthValue : public TH2Value
{
protected:
    TH2UnlimitedDoubleValue m_number;
    TH2LengthUnit m_unit;

    bool parse(const QString &toParse, qreal &value, QString &unit) const;

public:
    TH2LengthValue(TH2Element *newParent, const QString &newValueName);

    // TH2Value interface
    const QString valueForFile() const override;

    TH2UnlimitedDoubleValue &number();
    TH2DoubleValueNumberType numericValue() const;
    bool setNumericValue(TH2DoubleValueNumberType newNumericValue);
    bool setNumericValueByText(const QString &newNumericValueByText);

    TH2LengthUnit &unit();
    TH2ValuesListSizeType unitID() const;
    const QString &unitText() const;
    bool setUnitByID(TH2ValuesListSizeType newUnitID);
    bool setUnitByText(const QString &newUnitText);

    bool isDefaultUnit() const;
};

#endif // TH2LENGTHVALUE_H
