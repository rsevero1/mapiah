/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2DATEVALUE_H
#define TH2DATEVALUE_H

#include "th2value.h"

#include <QRegularExpression>

class TH2DateValue : public TH2Value
{
protected:
    static const QString DATETIME_FRACTIONAL_SECONDS_AS_TEXT;
    static const QString DATETIME_NO_FRACTIONAL_SECONDS_AS_TEXT;
    static const QString DATETIME_WITH_FRACTIONAL_SECONDS_AS_TEXT;

public:
    static const QString DATETIME_RANGE_SEPARATOR;
    static const QString UNSPECIFIED_TEXT;

    static const QRegularExpression rangeDateTimeRegex;
    static const QRegularExpression singleDateTimeRegex;

    enum class Specified {
        Yes,
        No,
    };

    enum class Interval {
        UNSET,
        Yes,
        No,
    };

    TH2DateValue(TH2Element *newParent, const QString &newValueName);

    bool setDateTimeFromText(const QString &newDateTimeText);

    Specified isSpecified() const;
    bool setIsSpecified(Specified newIsSpecified);

    // TH2Value interface
    const QString valueForFile() const override;

    Interval isInterval() const;

    const QString &startDateTime() const;
    const QString &endDateTime() const;

    const QString asText() const;

protected:
    qint16 m_startYear = -1;
    qint8 m_startMonth = -1;
    qint8 m_startDay = -1;
    qint8 m_startHour = -1;
    qint8 m_startMinute = -1;
    qreal m_startSeconds = -1;
    qint16 m_endYear = -1;
    qint8 m_endMonth = -1;
    qint8 m_endDay = -1;
    qint8 m_endHour = -1;
    qint8 m_endMinute = -1;
    qreal m_endSeconds = -1;
    Specified m_isSpecified;
    Interval m_isInterval;

};

#endif // TH2DATEVALUE_H
