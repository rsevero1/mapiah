/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2singlesetvaluedef.h"

const TH2SingleSetValueDefsPerElementTypeMapType TH2_SINGLE_SET_VALUE_DEFS
{
    {
        QStringLiteral("area"),
        {
            {
                QStringLiteral("clip"),
                TH2SingleSetValueDef(
                    QStringLiteral("clip"),
                    QStringLiteral("on"),
                    TH2ValuesListType() = {
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
            {
                QStringLiteral("context"),
                TH2SingleSetValueDef(
                    QStringLiteral("context"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral("point"),
                        QStringLiteral("line"),
                        QStringLiteral("area"),
                    }
                )
            },
            {
                QStringLiteral("place"),
                TH2SingleSetValueDef(
                    QStringLiteral("place"),
                    QStringLiteral("default"),
                    TH2ValuesListType() = {
                        QStringLiteral("bottom"),
                        QStringLiteral("default"),
                        QStringLiteral("top"),
                    }
                )
            },
            {
                QStringLiteral("visibility"),
                TH2SingleSetValueDef(
                    QStringLiteral("visibility"),
                    QStringLiteral("on"),
                    TH2ValuesListType() = {
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
        },
    },
    {
        QStringLiteral("line"),
        {
            {
                QStringLiteral("anchors"),
                TH2SingleSetValueDef(
                    QStringLiteral("anchors"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
            {
                QStringLiteral("border"),
                TH2SingleSetValueDef(
                    QStringLiteral("border"),
                    QStringLiteral("on"),
                    TH2ValuesListType() = {
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
            {
                QStringLiteral("clip"),
                TH2SingleSetValueDef(
                    QStringLiteral("clip"),
                    QStringLiteral("on"),
                    TH2ValuesListType() = {
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
            {
                QStringLiteral("close"),
                TH2SingleSetValueDef(
                    QStringLiteral("close"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral("auto"),
                        QStringLiteral("off"),
                        QStringLiteral("on"),
                    }
                )
            },
            {
                QStringLiteral("context"),
                TH2SingleSetValueDef(
                    QStringLiteral("context"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral("point"),
                        QStringLiteral("line"),
                        QStringLiteral("area"),
                    }
                )
            },
            {
                QStringLiteral("direction"),
                TH2SingleSetValueDef(
                    QStringLiteral("direction"),
                    QStringLiteral("none"),
                    TH2ValuesListType() = {
                        QStringLiteral("begin"),
                        QStringLiteral("both"),
                        QStringLiteral("end"),
                        QStringLiteral("none"),
                    }
                )
            },
            {
                QStringLiteral("gradient"),
                TH2SingleSetValueDef(
                    QStringLiteral("gradient"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral("none"),
                        QStringLiteral("center"),
                    }
                )
            },
            {
                QStringLiteral("head"),
                TH2SingleSetValueDef(
                    QStringLiteral("head"),
                    QStringLiteral("end"),
                    TH2ValuesListType() = {
                        QStringLiteral("begin"),
                        QStringLiteral("both"),
                        QStringLiteral("end"),
                        QStringLiteral("none"),
                    }
                )
            },
            {
                QStringLiteral("outline"),
                TH2SingleSetValueDef(
                    QStringLiteral("outline"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral("in"),
                        QStringLiteral("out"),
                        QStringLiteral("none"),
                    }
                )
            },
            {
                QStringLiteral("place"),
                TH2SingleSetValueDef(
                    QStringLiteral("place"),
                    QStringLiteral("default"),
                    TH2ValuesListType() = {
                        QStringLiteral("bottom"),
                        QStringLiteral("default"),
                        QStringLiteral("top"),
                    }
                )
            },
            {
                QStringLiteral("rebelays"),
                TH2SingleSetValueDef(
                    QStringLiteral("rebelays"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
            {
                QStringLiteral("reverse"),
                TH2SingleSetValueDef(
                    QStringLiteral("reverse"),
                    QStringLiteral("off"),
                    TH2ValuesListType() = {
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
            {
                QStringLiteral("visibility"),
                TH2SingleSetValueDef(
                    QStringLiteral("visibility"),
                    QStringLiteral("on"),
                    TH2ValuesListType() = {
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
        },
    },
    {
        QStringLiteral("linepoint"),
        {
            {
                QStringLiteral("adjust"),
                TH2SingleSetValueDef(
                    QStringLiteral("adjust"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral("horizontal"),
                        QStringLiteral("vertical"),
                    }
                )
            },
            {
                QStringLiteral("direction"),
                TH2SingleSetValueDef(
                    QStringLiteral("direction"),
                    QStringLiteral("point"),
                    TH2ValuesListType() = {
                        QStringLiteral("point"),
                    }
                )
            },
            {
                QStringLiteral("gradient"),
                TH2SingleSetValueDef(
                    QStringLiteral("gradient"),
                    QStringLiteral("point"),
                    TH2ValuesListType() = {
                        QStringLiteral("point"),
                    }
                )
            },
            {
                QStringLiteral("smooth"),
                TH2SingleSetValueDef(
                    QStringLiteral("smooth"),
                    QStringLiteral("auto"),
                    TH2ValuesListType() = {
                        QStringLiteral("auto"),
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
        }
    },
    {
        QStringLiteral("point"),
        {
            {
                QStringLiteral("align"),
                TH2SingleSetValueDef(
                    QStringLiteral("align"),
                    QStringLiteral(""),
                    TH2ValuesListType {
                        QStringLiteral("bottom"),
                        QStringLiteral("bottom-left"),
                        QStringLiteral("bottom-right"),
                        QStringLiteral("center"),
                        QStringLiteral("left"),
                        QStringLiteral("right"),
                        QStringLiteral("top"),
                        QStringLiteral("top-left"),
                        QStringLiteral("top-right"),
                    },
                    TH2ValueSubstitutionMapType {
                        {QStringLiteral("b"), QStringLiteral("bottom")},
                        {QStringLiteral("bl"), QStringLiteral("bottom-left")},
                        {QStringLiteral("br"), QStringLiteral("bottom-right")},
                        {QStringLiteral("c"), QStringLiteral("center")},
                        {QStringLiteral("l"), QStringLiteral("left")},
                        {QStringLiteral("r"), QStringLiteral("right")},
                        {QStringLiteral("t"), QStringLiteral("top")},
                        {QStringLiteral("tl"), QStringLiteral("top-left")},
                        {QStringLiteral("tr"), QStringLiteral("top-right")},
                    }
                    )
            },
            {
                QStringLiteral("clip"),
                TH2SingleSetValueDef(
                    QStringLiteral("clip"),
                    QStringLiteral("on"),
                    TH2ValuesListType() = {
                        QStringLiteral("on"),
                        QStringLiteral("off"),
                    }
                )
            },
            {
                QStringLiteral("context"),
                TH2SingleSetValueDef(
                    QStringLiteral("context"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral("point"),
                        QStringLiteral("line"),
                        QStringLiteral("area"),
                    }
                )
            },
            {
                QStringLiteral("place"),
                TH2SingleSetValueDef(
                    QStringLiteral("place"),
                    QStringLiteral("default"),
                    TH2ValuesListType() = {
                        QStringLiteral("bottom"),
                        QStringLiteral("default"),
                        QStringLiteral("top"),
                    }
                )
            },
            {
                QStringLiteral("scale"),
                TH2SingleSetValueDef(
                    QStringLiteral("scale"),
                    QStringLiteral(""),
                    TH2ValuesListType {
                        QStringLiteral("tiny"),
                        QStringLiteral("small"),
                        QStringLiteral("normal"),
                        QStringLiteral("large"),
                        QStringLiteral("huge"),
                    },
                    TH2ValueSubstitutionMapType {
                        {QStringLiteral("xs"), QStringLiteral("tiny")},
                        {QStringLiteral("s"), QStringLiteral("small")},
                        {QStringLiteral("m"), QStringLiteral("normal")},
                        {QStringLiteral("n"), QStringLiteral("normal")},
                        {QStringLiteral("l"), QStringLiteral("large")},
                        {QStringLiteral("xl"), QStringLiteral("huge")},
                    }
                 )
            },
        }
    },
    {
        QStringLiteral("scrap"),
        {
            {
                QStringLiteral("flip"),
                TH2SingleSetValueDef(
                    QStringLiteral("flip"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                        QStringLiteral(""),
                        QStringLiteral("horizontal"),
                        QStringLiteral("vertical"),
                    }
                )
            },
            {
                QStringLiteral("walls"),
                TH2SingleSetValueDef(
                    QStringLiteral("walls"),
                    QStringLiteral("auto"),
                    TH2ValuesListType() = {
                        QStringLiteral("auto"),
                        QStringLiteral("off"),
                        QStringLiteral("on"),
                    }
                )
            },
        },
    },
};

const TH2SingleSetValueDefsPerElementTypeMapType TH2_SUBTYPE_SINGLE_SET_VALUE_DEFS
{
    {
        QStringLiteral("line"),
        {
            {
                QStringLiteral("border"),
                TH2SingleSetValueDef(
                    QStringLiteral("border"),
                    QStringLiteral("visible"),
                    TH2ValuesListType() = {
                        QStringLiteral("invisible"),
                        QStringLiteral("presumed"),
                        QStringLiteral("temporary"),
                        QStringLiteral("visible"),
                    }
                )
            },
            {
                QStringLiteral("survey"),
                TH2SingleSetValueDef(
                    QStringLiteral("survey"),
                    QStringLiteral("cave"),
                    TH2ValuesListType() = {
                        QStringLiteral("cave"),
                        QStringLiteral("surface"),
                    }
                )
            },
            {
                QStringLiteral("u"),
                TH2SingleSetValueDef(
                    QStringLiteral("u"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                    }
                )
            },
            {
                QStringLiteral("wall"),
                TH2SingleSetValueDef(
                    QStringLiteral("wall"),
                    QStringLiteral("bedrock"),
                    TH2ValuesListType() = {
                        QStringLiteral("bedrock"),
                        QStringLiteral("blocks"),
                        QStringLiteral("clay"),
                        QStringLiteral("debris"),
                        QStringLiteral("flowstone"),
                        QStringLiteral("ice"),
                        QStringLiteral("invisible"),
                        QStringLiteral("moonmilk"),
                        QStringLiteral("overlying"),
                        QStringLiteral("pebbles"),
                        QStringLiteral("pit"),
                        QStringLiteral("presumed"),
                        QStringLiteral("sand"),
                        QStringLiteral("underlying"),
                        QStringLiteral("unsurveyed"),
                    }
                )
            },
            {
                QStringLiteral("water-flow"),
                TH2SingleSetValueDef(
                    QStringLiteral("water-flow"),
                    QStringLiteral("permanent"),
                    TH2ValuesListType() = {
                        QStringLiteral("conjectural"),
                        QStringLiteral("intermittent"),
                        QStringLiteral("permanent"),
                    }
                )
            },
        },
    },
    {
        QStringLiteral("point"),
        {
            {
                QStringLiteral("air-draught"),
                TH2SingleSetValueDef(
                    QStringLiteral("air-draught"),
                    QStringLiteral("undefined"),
                    TH2ValuesListType() = {
                        QStringLiteral("summer"),
                        QStringLiteral("undefined"),
                        QStringLiteral("winter"),
                    }
                )
            },
            {
                QStringLiteral("station"),
                TH2SingleSetValueDef(
                    QStringLiteral("station"),
                    QStringLiteral("temporary"),
                    TH2ValuesListType() = {
                        QStringLiteral("fixed"),
                        QStringLiteral("natural"),
                        QStringLiteral("painted"),
                        QStringLiteral("temporary"),
                    }
                )
            },
            {
                QStringLiteral("u"),
                TH2SingleSetValueDef(
                    QStringLiteral("u"),
                    QStringLiteral(""),
                    TH2ValuesListType() = {
                    }
                )
            },
            {
                QStringLiteral("water-flow"),
                TH2SingleSetValueDef(
                    QStringLiteral("water-flow"),
                    QStringLiteral("permanent"),
                    TH2ValuesListType() = {
                        QStringLiteral("intermittent"),
                        QStringLiteral("paleo"),
                        QStringLiteral("permanent"),
                    }
                )
            },
        },
    },
};

const TH2SingleSetValueDef TH2_AZIMUTH_VALUE_DEF(
    QStringLiteral("azimuth"),
    QStringLiteral("deg"),
    TH2ValuesListType {
        QStringLiteral("deg"),
        QStringLiteral("min"),
        QStringLiteral("grad"),
        QStringLiteral("mil"),
    },
    TH2ValueSubstitutionMapType {
        {QStringLiteral("degree"), QStringLiteral("deg")},
        {QStringLiteral("degree"), QStringLiteral("deg")},
        {QStringLiteral("minute"), QStringLiteral("min")},
        {QStringLiteral("minutes"), QStringLiteral("min")},
        {QStringLiteral("grads"), QStringLiteral("grad")},
        {QStringLiteral("mils"), QStringLiteral("mil")},
    }
 );

const TH2SingleSetValueDef TH2_CONTEXT_VALUE_DEF(
    QStringLiteral("context"),
    QStringLiteral(""),
    TH2ValuesListType() = {
        QStringLiteral("point"),
        QStringLiteral("line"),
        QStringLiteral("area")
    }
);

const TH2SingleSetValueDef TH2_INCLINATION_VALUE_DEF(
    QStringLiteral("inclination"),
    QStringLiteral("deg"),
    TH2ValuesListType {
        QStringLiteral("deg"),
        QStringLiteral("min"),
        QStringLiteral("grad"),
        QStringLiteral("mil"),
        QStringLiteral("percent"),
    },
    TH2ValueSubstitutionMapType {
        {QStringLiteral("degree"), QStringLiteral("deg")},
        {QStringLiteral("degrees"), QStringLiteral("deg")},
        {QStringLiteral("minute"), QStringLiteral("min")},
        {QStringLiteral("minutes"), QStringLiteral("min")},
        {QStringLiteral("grads"), QStringLiteral("grad")},
        {QStringLiteral("mils"), QStringLiteral("mil")},
        {QStringLiteral("percentage"), QStringLiteral("percent")},
    }
 );

const TH2SingleSetValueDef TH2_LENGTH_VALUE_DEF(
    QStringLiteral("length"),
    QStringLiteral("m"),
    TH2ValuesListType {
        QStringLiteral("m"),
        QStringLiteral("cm"),
        QStringLiteral("in"),
        QStringLiteral("ft"),
        QStringLiteral("yd"),
    },
    TH2ValueSubstitutionMapType {
        {QStringLiteral("meter"), QStringLiteral("m")},
        {QStringLiteral("meters"), QStringLiteral("m")},
        {QStringLiteral("centimeter"), QStringLiteral("cm")},
        {QStringLiteral("centimeters"), QStringLiteral("cm")},
        {QStringLiteral("inch"), QStringLiteral("in")},
        {QStringLiteral("inches"), QStringLiteral("in")},
        {QStringLiteral("feet"), QStringLiteral("ft")},
        {QStringLiteral("feets"), QStringLiteral("ft")},
        {QStringLiteral("yard"), QStringLiteral("yd")},
        {QStringLiteral("yards"), QStringLiteral("yd")},
    }
 );

const TH2SingleSetValueDef TH2_POINT_SCALE_VALUE_DEF(
    QStringLiteral("scale"),
    QStringLiteral(""),
    TH2ValuesListType {
        QStringLiteral("tiny"),
        QStringLiteral("small"),
        QStringLiteral("normal"),
        QStringLiteral("large"),
        QStringLiteral("huge"),
    },
    TH2ValueSubstitutionMapType {
        {QStringLiteral("xs"), QStringLiteral("tiny")},
        {QStringLiteral("s"), QStringLiteral("small")},
        {QStringLiteral("m"), QStringLiteral("normal")},
        {QStringLiteral("n"), QStringLiteral("normal")},
        {QStringLiteral("l"), QStringLiteral("large")},
        {QStringLiteral("xl"), QStringLiteral("huge")},
    }
);

const TH2SingleSetValueDef TH2_PROJECTION_VALUE_DEF(
    QStringLiteral("projection"),
    QStringLiteral(""),
    TH2ValuesListType {
        QStringLiteral("elevation"),
        QStringLiteral("extended"),
        QStringLiteral("none"),
        QStringLiteral("plan"),
    }
);


const TH2ParamType TH2_SINGLE_SET_VALUE_INVALID = QStringLiteral("INVALID");
const TH2ParamType TH2_SINGLE_SET_VALUE_UNSET = QStringLiteral("UNSET");
