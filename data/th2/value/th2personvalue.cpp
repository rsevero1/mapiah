/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2personvalue.h"

#include "../../../helpers/mapiahenums.h"

const QString TH2PersonValue::JOIN_CHAR = QStringLiteral("/");

const QRegularExpression TH2PersonValue::fullNameRegex = QRegularExpression(R"RX(^(?<name>\S+)\s*(?<surname>\S+)?$)RX");

TH2PersonValue::TH2PersonValue(TH2Element *newParent,
    const QString &newValueName)
    : TH2Value(newParent, newValueName)
{

}

bool TH2PersonValue::setFullName(const QString &newFullName)
{
    QRegularExpressionMatch match = fullNameRegex.match(newFullName);

    if (match.hasMatch())
    {
        m_name = match.captured("name");
        m_surname = match.captured("surname");
    }
    else
    {
        m_name = newFullName;
    }

    // Classes that derived directly from TH2Value should call parent option
    // valueChanged method.
    m_parent->valueChanged(valueName());

    return true;
}

const QString TH2PersonValue::valueForFile() const
{
    QString nameValue = m_name;

    if (!m_surname.isEmpty())
    {
       nameValue.append(SPACE_STRING);
       nameValue.append(m_surname);
    }

    if (nameValue.count(SPACE_STRING) != 1)
    {
        nameValue = QString(R"RX("%1")RX").arg(nameValue);
    }

    return nameValue;
}
