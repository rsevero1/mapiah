/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2point.h"

#include "option/th2contextoption.h"
#include "option/th2extendoption.h"
#include "option/th2freetextoption.h"
#include "option/th2lengthoption.h"
#include "option/th2orientationoption.h"
#include "option/th2pointscaleoption.h"
#include "option/th2singlesetoption.h"
#include "option/th2subtypeoption.h"
#include "option/th2valueoption.h"

#include "../../graphics/th2graphicspoint.h"

//air-draught
//altar
//altitude
//anastomosis
//anchor
//aragonite
//archeo-excavation
//archeo-material
//audio
//bat
//bedrock
//blocks
//bones
//breakdown-choke
//bridge
//camp
//cave-pearl
//clay
//clay-choke
//clay-tree
//continuation
//crystal
//curtain
//curtains
//danger
//date
//debris
//dig
//dimensions
//disc-pillar
//disc-pillars
//disc-stalactite
//disc-stalactites
//disc-stalagmite
//disc-stalagmites
//disk
//electric-light
//entrance
//ex-voto
//extra
//fixed-ladder
//flowstone
//flowstone-choke
//flute
//gate
//gradient
//guano
//gypsum
//gypsum-flower
//height
//helictite
//helictites
//human-bones
//ice
//ice-pillar
//ice-stalactite
//ice-stalagmite
//karren
//label
//low-end
//map-connection
//masonry
//moonmilk
//mud
//mudcrack
//nameplate
//narrow-end
//no-equipment
//no-wheelchair
//paleo-material
//passage-height
//pebbles
//pendant
//photo
//pillar
//pillar-with-curtains
//pillars-with-curtains
//popcorn
//raft
//raft-cone
//remark
//rimstone-dam
//rimstone-pool
//root
//rope
//rope-ladder
//sand
//scallop
//section
//seed-germination
//sink
//snow
//soda-straw
//spring
//stalactite
//stalactite-stalagmite
//stalactites-stalagmites
//stalagmite
//station
//station-name
//steps
//traverse
//tree-trunk
//u
//vegetable-debris
//volcano
//walkway
//wall-calcite
//water
//water-drip
//water-flow
//wheelchair

const TH2SupportedTypeListType TH2Point::SUPPORTED_SYMBOL_TYPES_LIST
{
    QStringLiteral("air-draught"),
    QStringLiteral("altar"),
    QStringLiteral("altitude"),
    QStringLiteral("anastomosis"),
    QStringLiteral("anchor"),
    QStringLiteral("aragonite"),
    QStringLiteral("archeo-excavation"),
    QStringLiteral("archeo-material"),
    QStringLiteral("audio"),
    QStringLiteral("bat"),
    QStringLiteral("bedrock"),
    QStringLiteral("blocks"),
    QStringLiteral("bones"),
    QStringLiteral("breakdown-choke"),
    QStringLiteral("bridge"),
    QStringLiteral("camp"),
    QStringLiteral("cave-pearl"),
    QStringLiteral("clay"),
    QStringLiteral("clay-choke"),
    QStringLiteral("clay-tree"),
    QStringLiteral("continuation"),
    QStringLiteral("crystal"),
    QStringLiteral("curtain"),
    QStringLiteral("curtains"),
    QStringLiteral("danger"),
    QStringLiteral("date"),
    QStringLiteral("debris"),
    QStringLiteral("dig"),
    QStringLiteral("dimensions"),
    QStringLiteral("disc-pillar"),
    QStringLiteral("disc-pillars"),
    QStringLiteral("disc-stalactite"),
    QStringLiteral("disc-stalactites"),
    QStringLiteral("disc-stalagmite"),
    QStringLiteral("disc-stalagmites"),
    QStringLiteral("disk"),
    QStringLiteral("electric-light"),
    QStringLiteral("entrance"),
    QStringLiteral("ex-voto"),
    QStringLiteral("extra"),
    QStringLiteral("fixed-ladder"),
    QStringLiteral("flowstone"),
    QStringLiteral("flowstone-choke"),
    QStringLiteral("flute"),
    QStringLiteral("gate"),
    QStringLiteral("gradient"),
    QStringLiteral("guano"),
    QStringLiteral("gypsum"),
    QStringLiteral("gypsum-flower"),
    QStringLiteral("handrail"),
    QStringLiteral("height"),
    QStringLiteral("helictite"),
    QStringLiteral("helictites"),
    QStringLiteral("human-bones"),
    QStringLiteral("ice"),
    QStringLiteral("ice-pillar"),
    QStringLiteral("ice-stalactite"),
    QStringLiteral("ice-stalagmite"),
    QStringLiteral("karren"),
    QStringLiteral("label"),
    QStringLiteral("low-end"),
    QStringLiteral("map-connection"),
    QStringLiteral("masonry"),
    QStringLiteral("moonmilk"),
    QStringLiteral("mud"),
    QStringLiteral("mudcrack"),
    QStringLiteral("nameplate"),
    QStringLiteral("narrow-end"),
    QStringLiteral("no-equipment"),
    QStringLiteral("no-wheelchair"),
    QStringLiteral("paleo-material"),
    QStringLiteral("passage-height"),
    QStringLiteral("pebbles"),
    QStringLiteral("pendant"),
    QStringLiteral("photo"),
    QStringLiteral("pillar"),
    QStringLiteral("pillar-with-curtains"),
    QStringLiteral("pillars"),
    QStringLiteral("pillars-with-curtains"),
    QStringLiteral("popcorn"),
    QStringLiteral("raft"),
    QStringLiteral("raft-cone"),
    QStringLiteral("remark"),
    QStringLiteral("rimstone-dam"),
    QStringLiteral("rimstone-pool"),
    QStringLiteral("root"),
    QStringLiteral("rope"),
    QStringLiteral("rope-ladder"),
    QStringLiteral("sand"),
    QStringLiteral("scallop"),
    QStringLiteral("section"),
    QStringLiteral("seed-germination"),
    QStringLiteral("sink"),
    QStringLiteral("snow"),
    QStringLiteral("soda-straw"),
    QStringLiteral("spring"),
    QStringLiteral("stalactite"),
    QStringLiteral("stalactite-stalagmite"),
    QStringLiteral("stalactites"),
    QStringLiteral("stalactites-stalagmites"),
    QStringLiteral("stalagmite"),
    QStringLiteral("stalagmites"),
    QStringLiteral("station"),
    QStringLiteral("station-name"),
    QStringLiteral("steps"),
    QStringLiteral("traverse"),
    QStringLiteral("tree-trunk"),
    QStringLiteral("u"),
    QStringLiteral("vegetable-debris"),
    QStringLiteral("via-ferrata"),
    QStringLiteral("volcano"),
    QStringLiteral("walkway"),
    QStringLiteral("wall-calcite"),
    QStringLiteral("water"),
    QStringLiteral("water-drip"),
    QStringLiteral("water-flow"),
    QStringLiteral("wheelchair"),
};

// Point option types
//
// name - data type - supported point types
//
// align - SingleSet - all
// clip - SingleSet - Not{station,  station-name, label, remark, date, altitude, height, passage-height}
// context - Context - all
// dist - Length - extra
// explored - Length - continuation
// extend - Extend - station
// from - FreeText - extra
// id - FreeText - all
// name - FreeText - station
// orient - Azimuth - all
// orientation - Azimuth - all
// place - SingleSet - all
// scale - Scale - all
// scrap - FreeText - section
// subtype - Subtype - station, air-draught, water-flow
// text - FreeText -  label, remark, continuation
// value - Value - height, passage-height, altitude, dimensions
// visibility - SingleSet - all

// Option data types
//
// Azimuth*
// Context*
// Double*
// Double limited*
// Extend*
// FreeText*
// Length*
// Scale*
// SingleSet*
// Subtype*
// Value*

const TH2SupportedOptionNameCheckFuncMapType TH2Point::SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP
{
    {QStringLiteral("align"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("clip"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("context"), &TH2ContextOption::isSupportedOption},
    {QStringLiteral("dist"), &TH2LengthOption::isSupportedOption},
    {QStringLiteral("explored"), &TH2LengthOption::isSupportedOption},
    {QStringLiteral("extend"), &TH2ExtendOption::isSupportedOption},
    {QStringLiteral("from"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("id"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("name"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("orient"), &TH2OrientationOption::isSupportedOption},
    {QStringLiteral("orientation"), &TH2OrientationOption::isSupportedOption},
    {QStringLiteral("place"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("scale"), &TH2PointScaleOption::isSupportedOption},
    {QStringLiteral("scrap"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("subtype"), &TH2SubtypeOption::isSupportedOption},
    {QStringLiteral("text"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("value"), &TH2ValueOption::isSupportedOption},
    {QStringLiteral("visibility"), &TH2SingleSetOption::isSupportedOption},
};

const QPointF TH2Point::xy() const
{
    return m_xy;
}

void TH2Point::setXY(const QPointF &newXY)
{
    m_xy = newXY;
}

void TH2Point::draw(QGraphicsScene *aScene)
{
    TH2GraphicsPoint *graphicPoint = new TH2GraphicsPoint(this);
    aScene->addItem(graphicPoint);
}

bool TH2Point::isPoint()
{
    return true;
}

bool TH2Point::isSupportedPointType(const QString &aType)
{
    return (SUPPORTED_SYMBOL_TYPES_LIST.count(aType) == 1);
}

const TH2SupportedTypeListType &TH2Point::supportedTypes()
{
    return SUPPORTED_SYMBOL_TYPES_LIST;
}

bool TH2Point::isSupportedOptionType(const QString &aParentElementType, const QString &aSymbolType, const QString &aOptionName)
{
    if (SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP.count(aOptionName) == 0)
    {
        return false;
    }

    return SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP.at(aOptionName)(
        aParentElementType,
        aSymbolType,
        aOptionName);
}

TH2Point::TH2Point(QString aType, TH2Element *parent, TH2File *th2file)
    : TH2Symbol(parent, th2file), TH2OptionList(th2file)
{
    setType(aType);
}

const QString TH2Point::elementType()
{
    return POINT_ELEMENT_TYPE;
}
