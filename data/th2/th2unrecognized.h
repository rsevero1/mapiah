/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2UNRECOGNIZED_H
#define TH2UNRECOGNIZED_H

#include "th2element.h"

#include "th2typedefs.h"

#include "../../factories/th2factory.h"

#include <QString>

class TH2File;

class TH2Unrecognized : public TH2Element
{
protected:
    QString m_content;
    QString m_reason;

    explicit TH2Unrecognized(TH2Element *parent = nullptr, TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createUnrecognizableLine(
        QString lineContent,
        QString errorReason,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:

    const QString elementType() override;

    const QString &content() const;
    const QString &reason() const;
};

#endif // TH2UNRECOGNIZED_H
