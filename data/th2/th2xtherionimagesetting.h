/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2XTHERIONIMAGESETTING_H
#define TH2XTHERIONIMAGESETTING_H

#include "th2xtherionsetting.h"

#include "th2typedefs.h"

#include "../../graphics/th2graphicsimage.h"

#include "../../factories/th2factory.h"

#include <QGraphicsScene>
#include <QString>

class TH2File;

class TH2XTherionImageSetting : public TH2XTherionSetting
{
    qint32 m_vsb;
    qreal m_igamma;
    qreal m_xx;
    bool m_isXVI;
    qreal m_yy;
    QString m_XVIroot;
    QString m_fname;
    qint32 m_iidx;
    bool m_ximage;
    QString m_imgx;
    QString m_xdata;

    TH2GraphicsImage *m_image;

protected:
    TH2XTherionImageSetting(
        QString aParameters,
        TH2Element *parent = nullptr,
        TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createTypedElement<TH2XTherionImageSetting, QString>(
        const QString &aParameters,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

    // TH2XTherionSetting interface
public:
    const QString setting() const override;

    const qint32 &vsb() const;
    void setVsb(qint32 newVsb);
    qreal igamma() const;
    void setIgamma(const qreal newIgamma);
    qreal xx() const;
    void setXX(const qreal newXX);
    bool isXVI() const;
    void setIsXVI(const bool newIsXVI);
    qreal yy() const;
    void setYY(const qreal newYY);
    const QString &XVIroot() const;
    void setXVIroot(const QString &newXVIroot);
    const QString &fname() const;
    void setFname(const QString &newFname);
    const qint32 &iidx() const;
    void setIidx(const qint32 &newIidx);
    bool ximage() const;
    void setXimage(const bool newXimage);
    const QString &imgx() const;
    void setImgx(const QString &newImgx);
    const QString &xdata() const;
    void setXdata(const QString &newXdata);

    // TH2Element interface
    void draw(QGraphicsScene *aScene) override;
    const QString elementType() override;
    TH2GraphicsImage *image() const;
};

#endif // TH2XTHERIONIMAGESETTING_H
