/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2LINEPOINT_H
#define TH2LINEPOINT_H

#include "th2element.h"
#include "option/th2optionlist.h"

#include "th2typedefs.h"

#include <QPointF>

class TH2File;
class TH2OptionList;

class TH2LinePoint : public TH2Element, public TH2OptionList
{
    QPointF m_xy;

protected:
    explicit TH2LinePoint(TH2Element *parent = nullptr, TH2File *th2file = nullptr);

public:

    QPointF xy() const;
    void setXY(const QPointF &newXY);

    // TH2Element interface
    bool isLinePoint() override;

    static const TH2SupportedOptionNameCheckFuncMapType SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP;

};

#endif // TH2LINEPOINT_H
