/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2SCRAP_H
#define TH2SCRAP_H

#include "option/th2optionlist.h"
#include "th2parent.h"

#include "th2typedefs.h"

#include "../../factories/th2factory.h"

#include <QString>
#include <QStringList>

class TH2File;

class TH2Scrap : public TH2Parent, public TH2OptionList
{
protected:
    QString m_id;
    QStringList m_images;

    explicit TH2Scrap(QString aID, TH2Element *parent = nullptr, TH2File *th2file = nullptr);

    friend th2element_up TH2Factory::createTypedElement<TH2Scrap, QString>(
        const QString &aID,
        TH2Element *parent,
        TH2File *th2file,
        qint32 lineNumber);

public:
    const QString elementType() override;

    const QString &id() const;
    void setID(const QString &newID);

    const QStringList &getImages() const;
    void addImage(const QString &aCod);

    // TH2Element interface
    bool isScrap() override;
    void draw(QGraphicsScene *aScene) override;

    // TH2OptionList interface
    void appendOption(th2element_up uniqueOption) override;

    static const TH2SupportedOptionNameCheckFuncMapType SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP;

};

#endif // TH2SCRAP_H
