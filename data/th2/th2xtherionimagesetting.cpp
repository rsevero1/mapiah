/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2xtherionimagesetting.h"

#include "../../graphics/th2graphicsraster.h"
#include "../../graphics/th2graphicsxvi.h"

#include <QFileInfo>

const qint32 &TH2XTherionImageSetting::vsb() const
{
    return m_vsb;
}

void TH2XTherionImageSetting::setVsb(qint32 newVsb)
{
    m_vsb = newVsb;
}

qreal TH2XTherionImageSetting::igamma() const
{
    return m_igamma;
}

void TH2XTherionImageSetting::setIgamma(const qreal newIgamma)
{
    m_igamma = newIgamma;
}

qreal TH2XTherionImageSetting::xx() const
{
    return m_xx;
}

void TH2XTherionImageSetting::setXX(const qreal newXX)
{
    m_xx = newXX;
}

bool TH2XTherionImageSetting::isXVI() const
{
    return m_isXVI;
}

void TH2XTherionImageSetting::setIsXVI(const bool newIsXVI)
{
    m_isXVI = newIsXVI;
}

qreal TH2XTherionImageSetting::yy() const
{
    return m_yy;
}

void TH2XTherionImageSetting::setYY(const qreal newYY)
{
    m_yy = newYY;
}

const QString &TH2XTherionImageSetting::XVIroot() const
{
    return m_XVIroot;
}

void TH2XTherionImageSetting::setXVIroot(const QString &newXVIroot)
{
    m_XVIroot = newXVIroot;
}

const QString &TH2XTherionImageSetting::fname() const
{
    return m_fname;
}

void TH2XTherionImageSetting::setFname(const QString &newFname)
{
    m_fname = newFname;
}

const qint32 &TH2XTherionImageSetting::iidx() const
{
    return m_iidx;
}

void TH2XTherionImageSetting::setIidx(const qint32 &newIidx)
{
    m_iidx = newIidx;
}

bool TH2XTherionImageSetting::ximage() const
{
    return m_ximage;
}

void TH2XTherionImageSetting::setXimage(const bool newXimage)
{
    m_ximage = newXimage;
}

const QString &TH2XTherionImageSetting::imgx() const
{
    return m_imgx;
}

void TH2XTherionImageSetting::setImgx(const QString &newImgx)
{
    m_imgx = newImgx;
}

const QString &TH2XTherionImageSetting::xdata() const
{
    return m_xdata;
}

void TH2XTherionImageSetting::setXdata(const QString &newXdata)
{
    m_xdata = newXdata;
}

void TH2XTherionImageSetting::draw(QGraphicsScene *aScene)
{
    m_image = TH2Factory::createGraphicsImage(fname(), xx(), yy(), this);

    aScene->addItem(m_image);
    m_image->dataChanged();
}

const QString TH2XTherionImageSetting::elementType()
{
    return XTHERION_IMAGE_ELEMENT_TYPE;
}

TH2GraphicsImage *TH2XTherionImageSetting::image() const
{
    return m_image;
}

TH2XTherionImageSetting::TH2XTherionImageSetting(
    QString aParameters,
    TH2Element *parent,
    TH2File *th2file)
    : TH2XTherionSetting(aParameters, parent, th2file)
    , m_vsb(1)
    , m_igamma(1.0)
    , m_isXVI(false)
    , m_ximage(false)
{

}

const QString TH2XTherionImageSetting::setting() const
{
    return IMAGE_INSERT_XTHERION_SETTING;
}
