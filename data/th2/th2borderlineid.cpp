/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2borderlineid.h"

const QString &TH2BorderLineID::borderLineID() const
{
    return m_borderLineID;
}

void TH2BorderLineID::setBorderLineID(const QString &newBorderLineID)
{
    m_borderLineID = newBorderLineID;
}

const QString TH2BorderLineID::elementType()
{
    return "borderlineid";
}

TH2BorderLineID::TH2BorderLineID(const QString &aBorderLineID, TH2Element *parent, TH2File *th2file)
    : TH2Element(parent, th2file)
{
    setBorderLineID(aBorderLineID);
}
