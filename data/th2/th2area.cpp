/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2area.h"

#include "th2borderlineid.h"
#include "th2file.h"

#include "option/th2contextoption.h"
#include "option/th2freetextoption.h"
#include "option/th2singlesetoption.h"

//bedrock
//blocks
//clay
//debris
//flowstone
//ice
//moonmilk
//mudcrack
//pebbles
//pillar
//pillar-with-curtains
//sand
//snow
//stalactite
//stalactite-stalagmite
//stalagmite
//sump
//u
//water

const TH2SupportedTypeListType TH2Area::SUPPORTED_SYMBOL_TYPES_LIST
{
    QStringLiteral("bedrock"),
    QStringLiteral("blocks"),
    QStringLiteral("clay"),
    QStringLiteral("debris"),
    QStringLiteral("flowstone"),
    QStringLiteral("ice"),
    QStringLiteral("moonmilk"),
    QStringLiteral("mudcrack"),
    QStringLiteral("pebbles"),
    QStringLiteral("pillar"),
    QStringLiteral("pillar-with-curtains"),
    QStringLiteral("sand"),
    QStringLiteral("snow"),
    QStringLiteral("stalactite"),
    QStringLiteral("stalactite-stalagmite"),
    QStringLiteral("stalagmite"),
    QStringLiteral("sump"),
    QStringLiteral("u"),
    QStringLiteral("water"),
};

// Area option types
//
// name | data-type | supported line types
//
// clip | singleset | all*
// context | context | all*
// id | freetext | all*
// place | singleset | all*
// visibility | singleset | all*

const TH2SupportedOptionNameCheckFuncMapType TH2Area::SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP
{
    {QStringLiteral("clip"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("context"), &TH2ContextOption::isSupportedOption},
    {QStringLiteral("id"), &TH2FreeTextOption::isSupportedOption},
    {QStringLiteral("place"), &TH2SingleSetOption::isSupportedOption},
    {QStringLiteral("visibility"), &TH2SingleSetOption::isSupportedOption},
};

const QList<QString> &TH2Area::borderLineIDs() const
{
    return m_borderLineIDs;
}

bool TH2Area::usesLineID(const QString &aLineID) const
{
    return m_borderLineIDs.contains(aLineID);
}

qsizetype TH2Area::countBorderLineIDs() const
{
    return m_borderLineIDs.count();
}

void TH2Area::appendElement(th2element_up uniqueElement)
{
    TH2Element *newElement = uniqueElement.get();

    th2File()->registerElement(std::move(uniqueElement));
    m_childCods.append(newElement->cod());
    TH2BorderLineID *newBorderLineID = dynamic_cast<TH2BorderLineID *>(newElement);
    if (newBorderLineID != nullptr)
    {
        m_borderLineIDs.append(newBorderLineID->borderLineID());
    }
}

bool TH2Area::isArea()
{
    return true;
}

TH2Area::TH2Area(QString aType, TH2Element *parent, TH2File *th2file)
    : TH2Parent(parent, th2file), TH2OptionList(th2file)
{
    setType(aType);
}

const QString TH2Area::elementType()
{
    return "area";
}

QList<TH2Line *> TH2Area::borderLines() const
{
    QList<TH2Line *> currentLines;

    for (auto &aID : m_borderLineIDs)
    {
        currentLines.append(th2File()->lineByID(aID));
    }

    return currentLines;
}

bool TH2Area::isSupportedType(const QString &aType)
{
    return (SUPPORTED_SYMBOL_TYPES_LIST.count(aType) == 1);
}

const TH2SupportedTypeListType &TH2Area::supportedTypes()
{
    return SUPPORTED_SYMBOL_TYPES_LIST;
}

bool TH2Area::isSupportedOptionType(const QString &aParentElementType, const QString &aSymbolType, const QString &aOptionName)
{
    if (SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP.count(aOptionName) == 0)
    {
        return false;
    }

    return SUPPORTED_OPTION_NAME_CHECK_FUNC_MAP.at(aOptionName)(
        aParentElementType,
        aSymbolType,
        aOptionName);
}
