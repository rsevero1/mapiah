/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "inputfile.h"

#include "th2/th2area.h"
#include "th2/th2element.h"
#include "th2/th2line.h"
#include "th2/th2linepoint.h"
#include "th2/th2unrecognized.h"

InputFile::InputFile()
    : m_encoding(UTF8_ENCODING)
{

}

const QStringList &InputFile::elementCods() const
{
    return m_elementCods;
}

qsizetype InputFile::count() const
{
    return m_elementCods.count();
}

qsizetype InputFile::countRegisteredElements() const
{
    return m_elementByCod.size();
}

const QMultiHash<QString, QString> &InputFile::missingLinesOfAreas() const
{
    return m_missingLinesOfAreas;
}

qsizetype InputFile::countMissingLinesOfAreas() const
{
    return m_missingLinesOfAreas.count();
}

void InputFile::addMissingLineOfArea(const QString &aLineID, const QString &aAreaCod)
{
    m_missingLinesOfAreas.insert(aLineID, aAreaCod);
}

void InputFile::removeMissingLineOfArea(const QString &aLineID)
{
    m_missingLinesOfAreas.remove(aLineID);
}

void InputFile::draw(QGraphicsScene *aScene) const
{
    for(auto &aElementCod : m_elementCods)
    {
        element(aElementCod)->draw(aScene);
    }
}

QString InputFile::uniqueCod(TH2Element *newElement)
{
    ++m_lastCodNumber;
    QString newCod = QString("%1-%2")
        .arg(newElement->elementType())
        .arg(m_lastCodNumber);
    return newCod;
}

void InputFile::setEncoding(const QString &newEncoding)
{
    m_encoding = newEncoding;
}

const QString &InputFile::encoding() const
{
    return m_encoding;;
}

MapiahEnums::LineEndingType InputFile::lineEndingType() const
{
    return m_lineEndingType;
}

void InputFile::setLineEndingType(MapiahEnums::LineEndingType newLineEndingType)
{
    m_lineEndingType = newLineEndingType;
}

const QString &InputFile::filename() const
{
    return m_filename;
}

void InputFile::setFilename(const QString &newFileName)
{
    m_filename = newFileName;
}

/**
 * Appends the new element in the list of elements directly attached to this file, i.e.,
 * the elements appended to the file, have a defined order inside the main structure of
 * the file.
 *
 * @brief TH2File::appendElement
 * @param newElement
 * @return
 */
void InputFile::appendElement(th2element_up uniqueElement)
{
    TH2Element *newElement = uniqueElement.get();

    registerElement(std::move(uniqueElement));

    m_elementCods.append(newElement->cod());
}

void InputFile::appendUnrecognizedLine(th2element_up uniqueUnrecognizedLine)
{
    TH2Unrecognized *newUnrecognizedLine =
            dynamic_cast<TH2Unrecognized*>(uniqueUnrecognizedLine.get());

    appendElement(std::move(uniqueUnrecognizedLine));

    m_unrecognizedLineCods.append(newUnrecognizedLine->cod());
}

/**
 * Register the element as a file element, i.e., available in the file. It's automatically
 * done at TH2Elements constructor.
 *
 * @brief TH2File::registerElement
 * @param newElement
 * @return
 */
void InputFile::registerElement(th2element_up uniqueElement)
{
    TH2Element *newElement = uniqueElement.get();

    newElement->setCod(uniqueCod(newElement));

    m_elementByCod[newElement->cod()] = std::move(uniqueElement);
}

bool InputFile::registerID(const QString id, const QString cod)
{
    m_codsByID.insert(id, cod);
    return true;
}

TH2Element *InputFile::element(const QString &cod) const
{
    if (m_elementByCod.count(cod) == 0)
    {
        return nullptr;
    }

    TH2Element *aElement = m_elementByCod.at(cod).get();

    if (aElement == nullptr)
    {
        qInfo() << QString("Failure getting element for cod '%1'").arg(cod);
    }

    return aElement;
}

TH2Line *InputFile::line(const QString &cod) const
{
    TH2Line *aLine = dynamic_cast<TH2Line *>(element(cod));

    if (aLine == nullptr)
    {
        qInfo() << QString("dynamic_cast to TH2Line failure for cod '%1'").arg(cod);
    }

    return aLine;
}

TH2Line *InputFile::lineByID(const QString &aID) const
{
    auto aCod = codByID(aID);
    auto aLine = line(aCod);
    return aLine;
}

TH2LinePoint *InputFile::linePoint(const QString &cod) const
{
    TH2LinePoint *aLinePoint = dynamic_cast<TH2LinePoint *>(element(cod));

    if (aLinePoint == nullptr)
    {
        qInfo() << QString("dynamic_cast to TH2LinePoint failure for cod '%1'").arg(cod);
    }

    return aLinePoint;
}

TH2Option *InputFile::option(const QString &cod) const
{
    TH2Option *aOption = dynamic_cast<TH2Option *>(element(cod));

    if (aOption == nullptr)
    {
        qInfo() << QString("dynamic_cast to TH2Option failure for cod '%1'").arg(cod);
    }

    return aOption;
}

TH2Area *InputFile::area(const QString &cod) const
{
    TH2Area *aArea = dynamic_cast<TH2Area *>(element(cod));

    if (aArea == nullptr)
    {
        qInfo() << QString("dynamic_cast to TH2Area failure for cod '%1'").arg(cod);
    }

    return aArea;
}

TH2Element *InputFile::elementByID(const QString &aID) const
{
    return element(codByID(aID));
}

QString InputFile::codByID(const QString &aID) const
{
    if (!m_codsByID.contains(aID))
    {
        qInfo() << "ID not found: '" << aID << "'";
        return nullptr;
    }

    if (m_codsByID.count(aID) == 1)
    {
        return m_codsByID.value(aID);
    }
    else
    {
        qInfo() << "Multiple cods for ID: '" << aID << "'";
        return nullptr;
    }
}

bool InputFile::exists(const QString &cod) const
{
    return (m_elementByCod.count(cod) != 0);
}

bool InputFile::existsByID(const QString &aID) const
{
    if (!m_codsByID.contains(aID))
    {
        return false;
    }

    return exists(codByID(aID));
}
