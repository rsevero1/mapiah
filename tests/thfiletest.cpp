/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include <catch2/catch_test_macros.hpp>

#include "../data/th2/th2file.h"

#include "../data/th/thinput.h"

#include "../parsers/th2parser.h"

#include <QFileInfo>
#include <QString>

const QString fullPathTHFile(const QString &aFilename)
{
    QString projectPath = QFileInfo("./").canonicalPath();

    return projectPath + "/Testing/auxiliary/thfile/" + aFilename;
}

th2file_up parseTHFile(
        QString aFilename,
        qint32 elementCount,
        std::string encoding,
        bool parsingShouldSucceed = true)
{
    TH2Parser aParser = TH2Parser();

    aParser.setFilename(fullPathTHFile(aFilename));
    th2file_up newTHFile_u = aParser.parseTH2();

    REQUIRE(aParser.parsingSucceeded() == parsingShouldSucceed);

    if (parsingShouldSucceed)
    {
        CHECK(newTHFile_u->count() == elementCount);
        CHECK(newTHFile_u->encoding().toStdString() == encoding);
    }

    return newTHFile_u;
}
