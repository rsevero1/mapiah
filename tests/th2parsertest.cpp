/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include <catch2/catch_test_macros.hpp>

#include "../data/th/thinput.h"

#include "../data/th2/th2area.h"
#include "../data/th2/th2comment.h"
#include "../data/th2/th2file.h"
#include "../data/th2/th2line.h"
#include "../data/th2/th2linepointbezier.h"
#include "../data/th2/th2linepointstraight.h"
#include "../data/th2/th2multilinecomment.h"
#include "../data/th2/th2point.h"
#include "../data/th2/th2scrap.h"
#include "../data/th2/th2xtherionimagesetting.h"
#include "../data/th2/th2xtherionunknownsetting.h"

#include "../data/th2/option/th2orientationoption.h"
#include "../data/th2/option/th2pointscaleoption.h"
#include "../data/th2/option/th2subtypeoption.h"

#include "../parsers/th2parser.h"

#include <QFileInfo>
#include <QString>

const QString fullPathTH2Parser(const QString &aFilename)
{
    QString projectPath = QFileInfo("./").canonicalPath();

    return projectPath + "/Testing/auxiliary/th2parser/" + aFilename;
}

th2file_up parseTH2File(
        QString aFilename,
        qint32 elementCount,
        std::string encoding,
        bool parsingShouldSucceed = true)
{
    TH2Parser aParser = TH2Parser();

    aParser.setFilename(fullPathTH2Parser(aFilename));
    th2file_up newTH2File_u = aParser.parseTH2();

    REQUIRE(aParser.parsingSucceeded() == parsingShouldSucceed);

    if (parsingShouldSucceed)
    {
        CHECK(newTH2File_u->count() == elementCount);
        CHECK(newTH2File_u->encoding().toStdString() == encoding);
    }

    return newTH2File_u;
}

void checkArea(
        TH2File *aTH2File,
        QString aCod,
        std::string areaType,
        qint32 childCount,
        qint32 optionCount,
        QStringList aListBorderLineIDs = {})
{
    TH2Area *aArea;

    REQUIRE(aTH2File->exists(aCod) == true);
    aArea = dynamic_cast<TH2Area *>(aTH2File->element(aCod));
    CHECK(aArea->type().toStdString() == areaType);
    CHECK(aArea->count() == childCount);
    CHECK(aArea->countOptions() == optionCount);
    CHECK(aArea->countBorderLineIDs() == aListBorderLineIDs.count());
    QStringList parsedBorderLineIDs = aArea->borderLineIDs();
    for(auto &aBorderLineID : aListBorderLineIDs)
    {
        CHECK(parsedBorderLineIDs.contains(aBorderLineID));
    }
}

void checkBasicOption(
        TH2File *aTH2File,
        QString aCod,
        std::string optionName,
        std::string optionSaveValue)
{
    TH2Option *aOption;

    REQUIRE(aTH2File->exists(aCod) == true);
    aOption = dynamic_cast<TH2Option *>(aTH2File->element(aCod));
    CHECK(aOption->name().toStdString() == optionName);
    CHECK(aOption->valueForFile().toStdString() == optionSaveValue);
}

void checkComment(
        TH2File *aTH2File,
        QString aCod,
        std::string aContent,
        bool isPreviousLineEndComment = false)
{
    TH2Comment *aComment;

    REQUIRE(aTH2File->exists(aCod) == true);
    aComment = dynamic_cast<TH2Comment *>(aTH2File->element(aCod));
    CHECK(aComment->content().toStdString() == aContent);
    CHECK(aComment->isPreviousLineEndComment() == isPreviousLineEndComment);
}

void checkFreeTextOption(
        TH2File *aTH2File,
        QString aCod,
        std::string optionName,
        std::string optionSaveValue)
{
    checkBasicOption(aTH2File, aCod, optionName, optionSaveValue);
}

void checkInput(
        TH2File *aTH2File,
        QString aCod,
        std::string aContent)
{
    THInput *aInput;

    REQUIRE(aTH2File->exists(aCod) == true);
    aInput = dynamic_cast<THInput *>(aTH2File->element(aCod));
    CHECK(aInput->filenameValue().toStdString() == aContent);
}

void checkLine(
        TH2File *aTH2File,
        QString aCod,
        std::string lineType,
        qint32 childCount,
        qint32 optionCount)
{
    TH2Line *aLine;

    REQUIRE(aTH2File->exists(aCod) == true);
    aLine = dynamic_cast<TH2Line *>(aTH2File->element(aCod));
    CHECK(aLine->count() == childCount);
    CHECK(aLine->type().toStdString() == lineType);
    CHECK(aLine->countOptions() == optionCount);
}

void checkLinePointBezier(
        TH2File *aTH2File,
        QString aCod,
        QPointF c1,
        QPointF c2,
        QPointF xy,
        qint32 optionCount)
{
    TH2LinePointBezier *aLinePointBezier;

    REQUIRE(aTH2File->exists(aCod) == true);
    aLinePointBezier = dynamic_cast<TH2LinePointBezier *>(aTH2File->element(aCod));
    CHECK(aLinePointBezier->c1() == c1);
    CHECK(aLinePointBezier->c2() == c2);
    CHECK(aLinePointBezier->xy() == xy);
    CHECK(aLinePointBezier->countOptions() == optionCount);
}

void checkLinePointStraight(
        TH2File *aTH2File,
        QString aCod,
        QPointF coordinates,
        qint32 optionCount)
{
    TH2LinePointStraight *aLinePointStraight;

    REQUIRE(aTH2File->exists(aCod) == true);
    aLinePointStraight = dynamic_cast<TH2LinePointStraight *>(aTH2File->element(aCod));
    CHECK(aLinePointStraight->xy() == coordinates);
    CHECK(aLinePointStraight->countOptions() == optionCount);
}

void checkMultiLineComment(
        TH2File *aTH2File,
        QString aCod,
        std::string aContent)
{
    TH2MultiLineComment *aMultiLineComment;

    REQUIRE(aTH2File->exists(aCod) == true);
    aMultiLineComment = dynamic_cast<TH2MultiLineComment *>(aTH2File->element(aCod));
    CHECK(aMultiLineComment->content().toStdString() == aContent);
}

void checkPoint(
        TH2File *aTH2File,
        QString aCod,
        std::string pointType,
        QPointF coodinates,
        qint32 optionCount)
{
    TH2Point *aPoint;

    REQUIRE(aTH2File->exists(aCod) == true);
    aPoint = dynamic_cast<TH2Point *>(aTH2File->element(aCod));
    CHECK(aPoint->type().toStdString() == pointType);
    CHECK(aPoint->xy() == coodinates);
    CHECK(aPoint->countOptions() == optionCount);
}

void checkScrap(
        TH2File *aTH2File,
        QString aCod,
        std::string aScrapID,
        qint32 elementCount,
        qint32 optionCount)
{
    TH2Scrap *aScrap;

    REQUIRE(aTH2File->exists(aCod) == true);
    aScrap = dynamic_cast<TH2Scrap *>(aTH2File->element(aCod));
    CHECK(aScrap->id().toStdString() == aScrapID);
    CHECK(aScrap->count() == elementCount);
    CHECK(aScrap->countOptions() == optionCount);
}

void checkSingleSetOption(
        TH2File *aTH2File,
        QString aCod,
        std::string optionName,
        std::string optionSaveValue)
{
    checkBasicOption(aTH2File, aCod, optionName, optionSaveValue);
}

void checkSubtypeOption(
        TH2File *aTH2File,
        QString aCod,
        std::string optionSaveValue,
        bool aIsInlineSubtype = false)
{
    checkBasicOption(aTH2File, aCod, "subtype", optionSaveValue);

    TH2SubtypeOption *aSubTypeOption;

    if (aTH2File->exists(aCod))
    {
        aSubTypeOption = dynamic_cast<TH2SubtypeOption *>(aTH2File->element(aCod));
        CHECK(aSubTypeOption->isSubtypeInline() == aIsInlineSubtype);
    }
}

void checkXTherionUnknownSetting(TH2File *aTH2File, QString aCod, std::string aName, std::string aParameter)
{
    TH2XTherionUnknownSetting *aXTherionUnknownSetting;

    REQUIRE(aTH2File->exists(aCod) == true);
    aXTherionUnknownSetting = dynamic_cast<TH2XTherionUnknownSetting *>(aTH2File->element(aCod));
    CHECK(aXTherionUnknownSetting->setting().toStdString() == aName);
    CHECK(aXTherionUnknownSetting->xtherionParameters().toStdString() == aParameter);
}

void checkXTherionImageSetting(
        TH2File *aTH2File,
        QString aCod,
        qreal xx,
        qreal yy,
        std::string filename)
{
    TH2XTherionImageSetting *aXTherionImageSetting;

    REQUIRE(aTH2File->exists(aCod) == true);
    aXTherionImageSetting = dynamic_cast<TH2XTherionImageSetting *>(aTH2File->element(aCod));
    CHECK(aXTherionImageSetting->setting().toStdString() == "xth_me_image_insert");
    CHECK(aXTherionImageSetting->xx() == xx);
    CHECK(aXTherionImageSetting->vsb() == 1);
    CHECK(aXTherionImageSetting->igamma() == 1.0);
    CHECK(aXTherionImageSetting->yy() == yy);
    CHECK(aXTherionImageSetting->XVIroot().toStdString() == "{}");
    CHECK(aXTherionImageSetting->fname().toStdString() == filename);
    CHECK(aXTherionImageSetting->iidx() == 0);
    CHECK(aXTherionImageSetting->imgx().toStdString() == "");
}

SCENARIO( "We have a TH2Parser", "[th2parser]" ) {

    GIVEN("a file with an encoding command and trailing space") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0011-encoding_with_trailing_space.th2",
            0,
            "utf-8");
    }

    GIVEN("a file with an encoding command and a comment on the same line") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0012-encoding_with_trailing_comment.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in both elements elements") {
            checkComment(aTH2File, "comment-1", "# end of line comment", true);
        }
    }

    GIVEN("a file with an encoding not UTF-8") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0013-iso8859-1_encoding.th2",
            1,
            "ISO8859-1");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in right encoding name and proper char translation") {
            checkComment(aTH2File, "comment-1", "# ISO8859-1 comment: àáâãäåç");
        }
    }

    GIVEN("a file with an encoding command") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0019-encoding_only.th2",
            0,
            "utf-8");
    }

    GIVEN("a file with only a XTHERION setting") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0020-xtherionsetting_only.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in a XTHERION setting element") {
            checkXTherionUnknownSetting(
                aTH2File,
                "xtherionunknownsetting-1",
                "xth_me_area_adjust",
                "-164 -2396 4206 1508");
        }
    }

    GIVEN("a file with encoding and a XTHERION setting") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0030-adding_xtherionsetting.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with elements representing the parsed file") {
            checkXTherionUnknownSetting(
                aTH2File,
                "xtherionunknownsetting-1",
                "xth_me_area_adjust",
                "-164 -2396 4206 1508");
        }
    }

    GIVEN("a file with encoding, an empty line and a XTHERION setting") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0031-with_empty_line.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with elements representing the parsed file") {
            checkXTherionUnknownSetting(
                aTH2File,
                "xtherionunknownsetting-1",
                "xth_me_area_adjust",
                "-164 -2396 4206 1508");
        }
    }

    GIVEN("a file with a XTHERION image setting") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0035-xtherionimagesetting_only.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkXTherionImageSetting(
                aTH2File,
                "xtherionimagesetting-1",
                -36,
                28,
                "croquis/croqui-007.jpg");
        }
    }

    GIVEN("a file with encoding and several XTHERION settings") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0040-adding_several_xtherionsettings.th2", 4, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with elements representing the parsed file") {
            checkXTherionUnknownSetting(
                aTH2File,
                "xtherionunknownsetting-1",
                "xth_me_area_adjust",
                "-164 -2396 4206 1508");
            checkXTherionUnknownSetting(
                aTH2File,
                "xtherionunknownsetting-2",
                "xth_me_area_zoom_to",
                "100");
            checkXTherionImageSetting(
                aTH2File,
                "xtherionimagesetting-3",
                1890,
                1380,
                "croquis/croqui-006.jpg");
            checkXTherionImageSetting(
                aTH2File,
                "xtherionimagesetting-4",
                -36,
                28,
                "croquis/croqui-007.jpg");
        }
    }

    GIVEN("a file with a opening scrap command without closing") {
        TH2Parser aParser;
        aParser.setFilename(fullPathTH2Parser("th2parser-0060-scrap_without_endscrap-parse_failure.th2"));
        auto result = aParser.parse();

        CHECK(result == false);
    }

    GIVEN("a file with a scrap command") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0061-scrap_and_endscrap.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "poco_surubim_SCP01", 0, 1);
            checkBasicOption(
                aTH2File,
                "scrapscaleoption-2",
                "scale",
                "[-164 -2396 3308 -2396 0 0 88.1888 0 m]");
        }
    }

    GIVEN("a file with a point command") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0070-point_only.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "debris",
                QPointF(2596, -468),
                0);
        }
    }

    GIVEN("a file with a point with an option") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0071-point_with_option.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "label",
                QPointF(2316, -548),
                1);
            checkFreeTextOption(
                aTH2File,
                "freetextoption-2",
                "text",
                R"RX("200 m")RX");
        }
    }

    GIVEN("a file with a point with an option and an ID") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0072-point_with_option_and_id.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "station",
                QPointF(782, -1740),
                3);
            checkSubtypeOption(aTH2File, "subtypeoption-2", "fixed");
            checkFreeTextOption(
                aTH2File,
                "freetextoption-3",
                "name",
                "A2@final_de_semana_26_e_27_de_setembro_de_2015");
            checkFreeTextOption(aTH2File, "freetextoption-4", "id", "A2");
        }
    }

    GIVEN("a file with encoding and a scrap with 4 points") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0073-scrap_with_points.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "poco_surubim_SCP01", 4, 1);
            checkBasicOption(
                aTH2File,
                "scrapscaleoption-2",
                "scale",
                "[-164 -2396 3308 -2396 0 0 88.1888 0 m]");
            checkPoint(
                aTH2File,
                "point-3",
                "debris",
                QPointF(2596, -468),
                0);
            checkPoint(
                aTH2File,
                "point-4",
                "station",
                QPointF(782, -1740),
                3);
            checkSubtypeOption(aTH2File, "subtypeoption-5", "fixed");
            checkFreeTextOption(
                aTH2File,
                "freetextoption-6",
                "name",
                "A2@final_de_semana_26_e_27_de_setembro_de_2015");
            checkFreeTextOption(aTH2File, "freetextoption-7", "id", "A2");
            checkPoint(
                aTH2File,
                "point-8",
                "label",
                QPointF(2316, -548),
                1);
            checkFreeTextOption(aTH2File, "freetextoption-9", "text", R"RX("200 m")RX");
            checkPoint(
                aTH2File,
                "point-10",
                "station",
                QPointF(2547.70696983, 6236.61014023),
                1);
            checkFreeTextOption(aTH2File, "freetextoption-11", "name", "5.18");
        }
    }

    GIVEN("a file with a point ending in a quoted string with windows line endings") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0074-point_ending_in_quoted_string_with_windows_line_endings.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "label",
                QPointF(278, 5488),
                2);
            checkBasicOption(
                aTH2File,
                "pointscaleoption-2",
                "scale",
                "huge");
            checkFreeTextOption(aTH2File, "freetextoption-3", "text", R"RX("The Frozen Deep")RX");
        }
    }

    GIVEN("a file with a line command") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0080-line_only.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "wall", 0, 1);
            checkSingleSetOption(aTH2File, "singlesetoption-2", "reverse", "on");
        }
    }

    GIVEN("a file with a line command") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0081-line_with_last_line_with_spaces_only.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "wall", 0, 1);
            checkSingleSetOption(aTH2File, "singlesetoption-2", "reverse", "on");
        }
    }

    GIVEN("a file with a line with internal data including an invalid inside line option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0082-line_with_internal_data_including_an_invalid_inside_line_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            CHECK(aTH2File->countRegisteredElements() == 8);

            checkLine(aTH2File, "line-1", "wall", 4, 2);
            checkFreeTextOption(aTH2File, "freetextoption-2", "id", "prd03_SCP03");
            checkSubtypeOption(aTH2File, "subtypeoption-3", "presumed");
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-4",
                QPointF(1758, -1030),
                0);
            checkLinePointBezier(
                aTH2File,
                "linepointbezier-5",
                QPointF(2147.74, -1120.48),
                QPointF(1834.24, -1042.2),
                QPointF(2668, -1364),
                1);
            checkSingleSetOption(aTH2File, "singlesetoption-6", "smooth", "on");
        }
    }

    GIVEN("a file with an area command") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0090-area_only.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkArea(
                aTH2File,
                "area-1",
                "clay",
                0,
                1);
            checkSingleSetOption(aTH2File, "singlesetoption-2", "clip", "on");

            CHECK(aTH2File->countMissingLinesOfAreas() == 0);
        }
    }

    GIVEN("a file with an area command") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0091-area_with_line_id.th2", 1, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkArea(
                aTH2File,
                "area-1",
                "clay",
                1,
                1,
                QStringList {
                    "l85-3732--20"
                });
            checkSingleSetOption(aTH2File, "singlesetoption-2", "clip", "on");

            CHECK(aTH2File->countMissingLinesOfAreas() == 1);
            CHECK(aTH2File->missingLinesOfAreas().value("l85-3732--20").toStdString() == "area-1");
        }
    }

    GIVEN("a file with an area command") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0092-area_with_line_id_and_line.th2",
            2,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkArea(
                aTH2File,
                "area-1",
                "clay",
                1,
                1,
                QStringList {
                    "l85-3732--20"
                });
            checkSingleSetOption(aTH2File, "singlesetoption-2", "clip", "on");
            checkLine(aTH2File, "line-4", "border", 5, 3);
            checkFreeTextOption(aTH2File, "freetextoption-5", "id", "l85-3732--20");
            checkSingleSetOption(aTH2File, "singlesetoption-6", "close", "on");
            checkSingleSetOption( aTH2File, "singlesetoption-7", "visibility", "off");

            CHECK(aTH2File->countMissingLinesOfAreas() == 0);
        }
    }

    GIVEN("a file with a line inside a scrap") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0099-file_with_line_inside_scrap.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "poco_surubim_SCP02", 1, 0);
            checkLine(aTH2File, "line-2", "border", 3, 1);
            checkLinePointBezier(
                aTH2File,
                "linepointbezier-6",
                QPointF(3233.22, 101.65),
                QPointF(3066.45, -131.93),
                QPointF(3204, -332),
                1);
            checkSingleSetOption(aTH2File, "singlesetoption-7", "smooth", "on");
        }
    }

    GIVEN("a file with 2 scraps") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0100-file_with_2_scraps.th2", 2, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "poco_surubim_SCP01", 1, 1);
            checkScrap(aTH2File, "scrap-4", "poco_surubim_SCP02", 2, 1);
        }
    }

    GIVEN("a file with comment blocks") {
        th2file_up aTH2File_u = parseTH2File("th2parser-0110-comment_block.th2", 2, "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkMultiLineComment(
                aTH2File,
                "multilinecomment-1",
R"RX(The scrap below is really complex.
Take care!!
)RX");
            checkScrap(aTH2File, "scrap-2", "poco_surubim_SCP01", 2, 1);
            checkMultiLineComment(
                aTH2File,
                "multilinecomment-5",
R"RX(
Another comment block.

But this one is inside a scrap!

)RX");
        }
    }

    GIVEN("a file with a point with a single set option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0120-point_with_single_set_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "station",
                QPointF(-423.45, 3365.54),
                1);
            checkSingleSetOption(
                aTH2File,
                "singlesetoption-2",
                "align",
                "right");
        }
    }

    GIVEN("a file with a point with a substituted single set option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0121-point_with_single_set_option_substituted.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkSingleSetOption(
                aTH2File,
                "singlesetoption-2",
                "align",
                "right");
        }
    }

    GIVEN("a file with a point with a inline subtype") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0122-point_with_inline_subtype.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkSubtypeOption(
                aTH2File,
                "subtypeoption-2",
                "paleo",
                true);
        }
    }

    GIVEN("a file with a point with a user type") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0123-point_with_user_type.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "u",
                QPointF(432, 911),
                1);
            checkSubtypeOption(
                aTH2File,
                "subtypeoption-2",
                "jabuticaba",
                true);
        }
    }

    GIVEN("a file with a point with an invalid subtype for it's type") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0124-point_with_invalid_subtype_for_type.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "air-draught",
                QPointF(432, 911),
                0);
        }
    }

    GIVEN("a file with a point of a type with no support for subtype") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0125-point_with_subtype_for_type_with_no_support.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "scallop",
                QPointF(42, 91),
                0);
        }
    }

    GIVEN("a file with a point with an orientation option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0126-point_with_orientation_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "ex-voto",
                QPointF(122.0031, 321.9712),
                1);
            checkBasicOption(
                aTH2File,
                "orientationoption-2",
                "orientation",
                "[173.23 deg]");
        }
    }

    GIVEN("a file with a point with an alternative ID for the orientation option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0127-point_with_alternative_id_for_orientation_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "electric-light",
                QPointF(4122, 9321),
                1);
            checkBasicOption(
                aTH2File,
                "orientationoption-2",
                "orientation",
                "[297 deg]");
        }
    }

    GIVEN("a file with a point with an invalid orientation option value") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0128-point_with_invalid_orientation_option_value.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "vegetable-debris",
                QPointF(42, 91),
                0);
        }
    }

    GIVEN("a file with a point with an orientation option with non default unit") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0129-point_with_orientation_option_with_unit.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "passage-height",
                QPointF(3962, -3653),
                1);
            checkBasicOption(
                aTH2File,
                "orientationoption-2",
                "orientation",
                "[245 mil]");
        }
    }

    GIVEN("a file with a point with a preset scale value") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0130-point_with_preset_scale_value.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "altitude",
                QPointF(-949.68, -354.61),
                1);
            checkBasicOption(
                aTH2File,
                "pointscaleoption-2",
                "scale",
                "tiny");
        }
    }

    GIVEN("a file with a point with a numeric scale value") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0131-point_with_numeric_scale_value.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "date",
                QPointF(717.49, 552.12),
                1);
            checkBasicOption(
                aTH2File,
                "pointscaleoption-2",
                "scale",
                "2.84");
        }
    }

    GIVEN("a file with a point with an invalid scale value") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0132-point_with_invalid_scale_value.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "height",
                QPointF(266.2, 346.58),
                0);
        }
    }

    GIVEN("a file with a point with a context option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0140-point_with_context_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "date",
                QPointF(-104.04, 75),
                1);
            checkBasicOption(
                aTH2File,
                "contextoption-2",
                "context",
                "line extra-info");
        }
    }

    GIVEN("a file with a point with an invalid context type") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0141-point_with_invalid_context_type.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "height",
                QPointF(361, 648),
                0);
        }
    }

    GIVEN("a file with a point with an incomplete context option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0142-point_with_incomplete_context_option.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "label",
                QPointF(527, 632),
                0);
        }
    }

    GIVEN("a file with a point with a dist option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0150-point_with_dist_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "extra",
                QPointF(-4882, -2163),
                1);
            checkBasicOption(
                aTH2File,
                "lengthoption-2",
                "dist",
                "[18 m]");
        }
    }

    GIVEN("a file with a point with a dist option with non default unit") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0151-point_with_dist_option_with_unit.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "extra",
                QPointF(-4381, -253),
                1);
            checkBasicOption(
                aTH2File,
                "lengthoption-2",
                "dist",
                "[2450 cm]");
        }
    }

    GIVEN("a file with a point with an invalid dist option: unsupported unit") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0152-point_with_invalid_dist_option_unsupported_unit.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "extra",
                QPointF(1454, 2646),
                0);
        }
    }

    GIVEN("a file with a point with an invalid dist option: no number") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0153-point_with_invalid_dist_option_no_number.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "extra",
                QPointF(3706, 4846),
                0);
        }
    }

    GIVEN("a file with a point with an invalid type for dist option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0154-point_with_invalid_type_for_dist_option.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "remark",
                QPointF(3149, -1199),
                0);
        }
    }

    GIVEN("a file with a point with an extend option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0160-point_with_extend_option_without_previous_station.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "station",
                QPointF(-2675, 2206),
                1);
            checkBasicOption(
                aTH2File,
                "extendoption-2",
                "extend",
                "");
        }
    }

    GIVEN("a file with a point with an extend option and a previous station") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0161-point_with_extend_option_with_previous_station.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "station",
                QPointF(-3218, -2379),
                1);
            checkBasicOption(
                aTH2File,
                "extendoption-2",
                "extend",
                "previous a12");
        }
    }

    GIVEN("a file with a point with an extend option and an alternative previous station") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0162-point_with_extend_option_with_alternate_previous_station.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "station",
                QPointF(-4747, -2577),
                1);
            checkBasicOption(
                aTH2File,
                "extendoption-2",
                "extend",
                "previous c33");
        }
    }

    GIVEN("a file with an altitude point with a value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0170-point_with_value_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "altitude",
                QPointF(-2885, 604),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "46");
        }
    }

    GIVEN("a file with an altitude point with a value option and a fix") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0171-altitude_point_with_fix.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "altitude",
                QPointF(3852, -2159),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "[fix 59]");
        }
    }

    GIVEN("a file with an altitude point with a value option and a fix and an unit") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0172-altitude_point_with_fix_and_unit.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "altitude",
                QPointF(2438, -2933),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "[fix 107.1 in]");
        }
    }

    GIVEN("a file with an altitude point with a value option and an unit") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0173-altitude_point_with_unit.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "altitude",
                QPointF(-4884, -1045),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "[31.8 yd]");
        }
    }

    GIVEN("a file with a dimensions point with an above and below value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0174-dimensions_point_with_above_and_below.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "dimensions",
                QPointF(-4156, 1012),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "[32 61]");
        }
    }

    GIVEN("a file with a dimensions point with an above, below and unit value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0175-dimensions_point_with_above_below_and_unit.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "dimensions",
                QPointF(1387, 2663),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "[82 2 ft]");
        }
    }

    GIVEN("a file with a height point with a height value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0176-height_point_with_height.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "height",
                QPointF(-2085, 3128),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "+15");
        }
    }

    GIVEN("a file with a height point with a depth value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0177-height_point_with_depth.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "height",
                QPointF(-1935, -3691),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "-23");
        }
    }

    GIVEN("a file with a height point with a depth and height value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0178-height_point_with_depth_height.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "height",
                QPointF(3990, 4746),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "25");
        }
    }

    GIVEN("a file with a height point with a height and unit value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0179-height_point_with_height_and_unit.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "height",
                QPointF(-3847, 4540),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "[+35 ft]");
        }
    }

    GIVEN("a file with a passage-height point with a height value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0180-passage_height_point_with_height.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "passage-height",
                QPointF(-3717, 3423),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "+6");
        }
    }

    GIVEN("a file with a passage-height point with a depth value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0181-passage_height_point_with_depth.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "passage-height",
                QPointF(3853, -1100),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "-15");
        }
    }

    GIVEN("a file with a passage-height point with a distance value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0182-passage_height_point_with_distance.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "passage-height",
                QPointF(2163, -814),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "17");
        }
    }

    GIVEN("a file with a passage-height point with a height and depth value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0183-passage_height_point_with_height_depth.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "passage-height",
                QPointF(4502, -365),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "[+81 -1471]");
        }
    }

    GIVEN("a file with a passage-height point with a height, depth and unit value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0184-passage_height_point_with_height_depth_unit.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "passage-height",
                QPointF(1051, -2210),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "[+6 -71 in]");
        }
    }

    GIVEN("a file with a passage-height point with an INVALID value option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0185-passage_height_point_with_value_option_without_brakcets-INVALID.th2",
            1,
            "utf-8",
            false);
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "passage-height",
                QPointF(-3081, 799),
                1);
        }
    }

    GIVEN("a file with a line with a context option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0190-line_with_context_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            CHECK(aTH2File->countRegisteredElements() == 4);

            checkLine(aTH2File, "line-1", "contour", 2, 1);
            checkBasicOption(aTH2File, "contextoption-2", "context", "line extra");
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-3",
                QPointF(2802, -969),
                0);
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-4",
                QPointF(3804, 3512),
                0);
        }
    }

    GIVEN("a file with a line with a text option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0200-line_with_text_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            CHECK(aTH2File->countRegisteredElements() == 4);

            checkLine(aTH2File, "line-1", "label", 2, 1);
            checkFreeTextOption(aTH2File, "freetextoption-2", "text", R"RX("Important info regarding this line")RX");
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-3",
                QPointF(-3068, 3038),
                0);
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-4",
                QPointF(717, -3448),
                0);
        }
    }

    GIVEN("a file with a line with a height option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0210-line_with_height_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            CHECK(aTH2File->countRegisteredElements() == 4);

            checkLine(aTH2File, "line-1", "pit", 2, 1);
            checkBasicOption(
                aTH2File,
                "lengthoption-2",
                "height",
                "[4.44 m]");
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-3",
                QPointF(-1201, 4317),
                0);
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-4",
                QPointF(2446, 191),
                0);
        }
    }

    GIVEN("a file with a scrap command with a cs option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0220-scrap_with_cs_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras1", 0, 1);
            checkFreeTextOption(
                aTH2File,
                "freetextoption-2",
                "cs",
                "long-lat");
        }
    }

    GIVEN("a file with a scrap command with a stations option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0230-scrap_with_stations_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras2", 0, 1);
            checkFreeTextOption(
                aTH2File,
                "freetextoption-2",
                "stations",
                "a2,c4");
        }
    }

    GIVEN("a file with a scrap command with a title option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0240-scrap_with_title_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras3", 0, 1);
            checkFreeTextOption(
                aTH2File,
                "freetextoption-2",
                "title",
                R"RX("Buraco das Araras - Salão Seco")RX");
        }
    }

    GIVEN("a file with a scrap command with an walls option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0250-scrap_with_walls_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras4", 0, 1);
            checkFreeTextOption(
                aTH2File,
                "singlesetoption-2",
                "walls",
                "off");
        }
    }

    GIVEN("a file with a scrap command with an author option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0260-scrap_with_author_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras5", 0, 1);
            checkBasicOption(
                aTH2File,
                "authoroption-2",
                "author",
                "2022.02.13@11:27:32 Rodrigo Severo");
        }
    }

    GIVEN("a file with a scrap command with an author option with fractional seconds") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0261-scrap_with_author_option_with_fractional_seconds.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras6", 0, 1);
            checkBasicOption(
                aTH2File,
                "authoroption-2",
                "author",
                "2022.02.13@11:27:32.12 Rodrigo Severo");
        }
    }

    GIVEN("a file with a scrap command with an author option with a date range") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0262-scrap_with_author_option_with_date_range.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras7", 0, 1);
            checkBasicOption(
                aTH2File,
                "authoroption-2",
                "author",
                R"RX(2022.02.13@11:27:32 - 2022.02.13@11:58:00 Rodrigo Severo)RX");
        }
    }

    GIVEN("a file with a scrap command with an author option with a quoted name") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0263-scrap_with_author_option_with_quoted_name.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras8", 0, 1);
            checkBasicOption(
                aTH2File,
                "authoroption-2",
                "author",
                R"RX(2022.02.13@11:58:00 "Rodrigo Severo Cambará")RX");
        }
    }

    GIVEN("a file with a scrap command with an author option with a date range without spaces") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0264-scrap_with_author_option_with_date_range_without_spaces.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras7", 0, 1);
            checkBasicOption(
                aTH2File,
                "authoroption-2",
                "author",
                R"RX(2022.02.13@11:27:32 - 2022.02.13@11:58:00 Rodrigo Severo)RX");
        }
    }

    GIVEN("a file with a scrap command with a copyright option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0270-scrap_with_copyright_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras9", 0, 1);
            checkBasicOption(
                aTH2File,
                "copyrightoption-2",
                "copyright",
                R"RX(2022.02.13@14:50:52 "Todos os diretos reservados pra mim®")RX");
        }
    }

    GIVEN("a file with a scrap command with a flip option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0280-scrap_with_flip_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras10", 0, 1);
            checkBasicOption(
                aTH2File,
                "singlesetoption-2",
                "flip",
                "horizontal");
        }
    }

    GIVEN("a file with a scrap command with an empty flip option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0281-scrap_with_flip_empty_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras11", 0, 1);
            checkBasicOption(
                aTH2File,
                "singlesetoption-2",
                "flip",
                "");
        }
    }

    GIVEN("a file with a scrap command with a projection option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0290-scrap_with_projection_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras12", 0, 1);
            checkBasicOption(
                aTH2File,
                "projectionoption-2",
                "projection",
                "none");
        }
    }

    GIVEN("a file with a scrap command with a projection option with an index") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0291-scrap_with_projection_option_with_index.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras13", 0, 1);
            checkBasicOption(
                aTH2File,
                "projectionoption-2",
                "projection",
                "plan:main");
        }
    }

    GIVEN("a file with a scrap command with a projection option with an elevation with a direction") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0292-scrap_with_projection_option_with_elevation_with_direction.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras14", 0, 1);
            checkBasicOption(
                aTH2File,
                "projectionoption-2",
                "projection",
                "[elevation:alternative 273 grad]");
        }
    }

    GIVEN("a file with a scrap command with a sketch option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0300-scrap_with_sketch_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras15", 0, 1);
            checkBasicOption(
                aTH2File,
                "sketchoption-2",
                "sketch",
                "./FrozenDeep_p.xvi 12 32");
        }
    }

    GIVEN("a file with a scrap command with a station-names option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0310-scrap_with_station-names_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "araras16", 0, 1);
            checkBasicOption(
                aTH2File,
                "stationnamesoption-2",
                "station-names",
                "[] 2021-12-28.reveillon2021");
        }
    }

    GIVEN("a file with a scrap command with a scale option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0320-scrap_with_scale_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "andorinhas1", 0, 1);
            checkBasicOption(
                aTH2File,
                "scrapscaleoption-2",
                "scale",
                "793.112");
        }
    }

    GIVEN("a file with a scrap command with a scale option and length unit") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0321-scrap_with_scale_option_with_length_unit.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "andorinhas2", 0, 1);
            checkBasicOption(
                aTH2File,
                "scrapscaleoption-2",
                "scale",
                "[183.092 cm]");
        }
    }

    GIVEN("a file with a scrap command with a scale option and amount of drawing units") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0322-scrap_with_scale_option_amount_drawing_units.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkScrap(aTH2File, "scrap-1", "andorinhas3", 0, 1);
            checkBasicOption(
                aTH2File,
                "scrapscaleoption-2",
                "scale",
                "[252 584.2 yd]");
        }
    }

    GIVEN("a file with a line command with adjust line point option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0330-linepoint_with_adjust_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "floor-step", 2, 0);
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-2",
                QPointF(1758, -1030),
                0);
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-3",
                QPointF(2147.74, -1120.48),
                1);
            checkBasicOption(
                aTH2File,
                "singlesetoption-4",
                "adjust",
                "horizontal");
        }
    }

    GIVEN("a file with a line command with direction line point option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0331-linepoint_with_direction_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "pit", 2, 0);
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-2",
                QPointF(1758, -1030),
                1);
            checkBasicOption(
                aTH2File,
                "singlesetoption-3",
                "direction",
                "point");
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-4",
                QPointF(2147.74, -1120.48),
                0);
        }
    }

    GIVEN("a file with a line command with a gradient line point option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0332-linepoint_with_gradient_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "contour", 2, 0);
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-2",
                QPointF(1758, -1030),
                1);
            checkBasicOption(
                aTH2File,
                "singlesetoption-3",
                "gradient",
                "point");
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-4",
                QPointF(2147.74, -1120.48),
                0);
        }
    }

    GIVEN("a file with a line command with a smooth line point option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0333-linepoint_with_smooth_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "ceiling-step", 2, 0);
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-2",
                QPointF(1758, -1030),
                0);
            checkLinePointStraight(
                aTH2File,
                "linepointstraight-3",
                QPointF(2147.74, -1120.48),
                1);
            checkBasicOption(
                aTH2File,
                "singlesetoption-4",
                "smooth",
                "off");
        }
    }

    GIVEN("a file with a line command with an altitude line point option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0350-linepoint_with_altitude_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "overhang", 2, 0);
            checkBasicOption(
                aTH2File,
                "altitudeoption-4",
                "altitude",
                "[+4 m]");
        }
    }

    GIVEN("a file with a line command with an altitude line point option with a fix") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0351-linepoint_with_altitude_option_with_fix.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "ceiling-meander", 2, 0);
            checkBasicOption(
                aTH2File,
                "altitudeoption-4",
                "altitude",
                "[fix 1510 ft]");
        }
    }

    GIVEN("a file with a line command with an altitude line point option with a fix") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0352-linepoint_with_altitude_option_set_as_nan.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "ceiling-meander", 2, 0);
            checkBasicOption(
                aTH2File,
                "altitudeoption-4",
                "altitude",
                "0");
        }
    }

    GIVEN("a file with a altitude point command with a NaN value") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0353-altitude_point_with_value_option_set_as_nan.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "altitude",
                QPointF(1758, -1030),
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "0");
        }
    }

    GIVEN("a file with a line command with a l-size line point option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0360-linepoint_with_l-size_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "floor-meander", 2, 0);
            checkBasicOption(
                aTH2File,
                "doubleoption-3",
                "l-size",
                "3");
        }
    }

    GIVEN("a file with a line command with a r-size line point option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0361-linepoint_with_r-size_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "low-ceiling", 2, 0);
            checkBasicOption(
                aTH2File,
                "doubleoption-4",
                "r-size",
                "1.5");
        }
    }

    GIVEN("a file with a line command with a size line point option") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0362-linepoint_with_size_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "pit-chimney", 2, 0);
            checkBasicOption(
                aTH2File,
                "doubleoption-4",
                "size",
                "4");
        }
    }

    GIVEN("a file with a line of type rope command") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0370-line_of_type_rope.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(aTH2File, "line-1", "rope", 2, 1);
            checkBasicOption(
                aTH2File,
                "singlesetoption-2",
                "reverse",
                "on");
        }
    }

    GIVEN("a file with a point of type date and a value option with only a date") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0380-point_of_type_date_with_date_only_value_option.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "date",
                QPointF(2282, 80),
                3);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "2022.02.05");
        }
    }

    GIVEN("a file with an input") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0390-input.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkInput(
                aTH2File,
                "input-1",
                "./testfile.th");
        }
    }

    GIVEN("a file with a date point with an incomplete date") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0400-date_point_with_incomplete_date.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "date",
                {-2885, 604},
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "2021.02");
        }
    }

    GIVEN("a file with a date point with a complete range") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0401-date_point_with_complete_range.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "date",
                {-2885, 604},
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "2021.12.23@08:30:01 - 2022.02.09@02:30:07");
        }
    }

    GIVEN("a file with a date point with an incomplete range") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0402-date_point_with_incomplete_range.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "date",
                {-2885, 604},
                1);
            checkBasicOption(
                aTH2File,
                "valueoption-2",
                "value",
                "2021.12.23 - 2022.02.09@02:30");
        }
    }

    GIVEN("a file with a handrail point") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0410-handrail_point.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkPoint(
                aTH2File,
                "point-1",
                "handrail",
                {-2885, 604},
                0);
        }
    }

    GIVEN("a file with a via-ferrata line command") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0411-via-ferrata_line.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(
                aTH2File,
                "line-1",
                "via-ferrata",
                2,
                0);
        }
    }

    GIVEN("a file with a rope line command with anchors and rebelays options") {
        th2file_up aTH2File_u = parseTH2File(
            "th2parser-0412-rope_line_with_anchors_and_rebelays_options.th2",
            1,
            "utf-8");
        TH2File *aTH2File = aTH2File_u.get();

        THEN("results in TH2File with a element representing the parsed file") {
            checkLine(
                aTH2File,
                "line-1",
                "rope",
                3,
                2);
            checkSingleSetOption(
                aTH2File,
                "singlesetoption-2",
                "rebelays",
                "off");
            checkSingleSetOption(
                aTH2File,
                "singlesetoption-3",
                "anchors",
                "on");
        }
    }
}
