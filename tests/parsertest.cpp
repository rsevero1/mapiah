/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifdef BASE_PARSER_DEBUG
#include <catch2/catch_test_macros.hpp>

#include "../parsers/parser.h"

#include <QFileInfo>
#include <QString>

const QString fullPathParser(const QString aFilename) {
    QString projectPath = QFileInfo("./").canonicalPath();

    return projectPath + "/Testing/auxiliary/parser/" + aFilename;
}

void parseTH2File(Parser &aParser, QString aFilename, qint32 elementCount)
{
    aParser.setFilename(fullPathParser(aFilename));
    auto result = aParser.parse();

    REQUIRE(result == true);

    CHECK(aParser.m_parsedWords.count() == elementCount);
}

SCENARIO( "We have a Parser", "[parser]" ) {
    Parser aParser = Parser();

    GIVEN("an empty file") {
        parseTH2File(aParser, "parser-000-empty.th2", 0);
    }

    GIVEN("an encoding only file with a quoted type") {
        parseTH2File(aParser, "parser-020-quoted-strings_only.th2", 2);

        THEN("has encoding only contents") {
            CHECK(aParser.m_parsedWords[1]->type == Parser::WordType::Quoted);
            CHECK(aParser.m_parsedWords[1]->word.toStdString() == "utf-8");
        }
    }

    GIVEN("an encoding file with a multiline quoted string and some doubles") {
        parseTH2File(aParser, "parser-030-multiline-quoted-strings_only.th2", 4);

        THEN("has encoding only contents") {
            CHECK(aParser.m_parsedWords[1]->word.startsWith("utf-8") == true);
            CHECK(aParser.m_parsedWords[1]->word.endsWith("lines") == true);
            CHECK(aParser.m_parsedWords[1]->type == Parser::WordType::Quoted);
            CHECK(aParser.m_parsedWords[2]->word.toStdString() == "3.122");
            CHECK(aParser.m_parsedWords[2]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[3]->word.toStdString() == "4.322");
            CHECK(aParser.m_parsedWords[3]->type == Parser::WordType::Regular);
        }
    }

    GIVEN("a file with a bracketed 'string'") {
        parseTH2File(aParser, "parser-040-bracketed-string_only.th2", 8);

        THEN("has 3 regular words and a final bracketed one") {
            CHECK(aParser.m_parsedWords[0]->word.toStdString() == "scrap");
            CHECK(aParser.m_parsedWords[0]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[1]->word.toStdString() == "FrozenDeepSPl1");
            CHECK(aParser.m_parsedWords[1]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[2]->word.toStdString() == "-scale");
            CHECK(aParser.m_parsedWords[2]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[7]->word.toStdString() == "0 0 10 m");
            CHECK(aParser.m_parsedWords[7]->type == Parser::WordType::Bracketed);
        }
    }

    GIVEN("a file with a multiline bracketed 'string'") {
        parseTH2File(aParser, "parser-050-multiline-bracketed-string_only.th2", 8);

        THEN("has 3 regular words and a final bracketed one") {
            CHECK(aParser.m_parsedWords[0]->word.toStdString() == "scrap");
            CHECK(aParser.m_parsedWords[0]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[1]->word.toStdString() == "FrozenDeepSPl1");
            CHECK(aParser.m_parsedWords[1]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[2]->word.toStdString() == "-scale");
            CHECK(aParser.m_parsedWords[2]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[7]->word.toStdString() == "12 13 10 m");
            CHECK(aParser.m_parsedWords[7]->type == Parser::WordType::Bracketed);
        }
    }

    GIVEN("a file with a bracketed 'string' with a multiline quoted string inside it") {
        parseTH2File(aParser, "parser-060-multiline-bracketed-string_with_multiline_quoted_string.th2", 8);

        THEN("has 3 regular words and a final bracketed one") {
            CHECK(aParser.m_parsedWords[0]->word.toStdString() == "scrap");
            CHECK(aParser.m_parsedWords[0]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[1]->word.toStdString() == "FrozenDeepSPl1");
            CHECK(aParser.m_parsedWords[1]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[2]->word.toStdString() == "-scale");
            CHECK(aParser.m_parsedWords[2]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[5]->type == Parser::WordType::Quoted);
            CHECK(aParser.m_parsedWords[7]->word.toStdString() == R"RX(12 13 "m means meters
    ft means feet" m)RX");
            CHECK(aParser.m_parsedWords[7]->type == Parser::WordType::Bracketed);
        }
    }

    GIVEN("a file with a multiline bracketed 'string' with a multiline quoted string inside it") {
        parseTH2File(aParser, "parser-070-multiline-bracketed-string_with_multiline_quoted_string.th2", 10);

        THEN("has 3 regular words and a final bracketed one") {
            CHECK(aParser.m_parsedWords[0]->word.toStdString() == "scrap");
            CHECK(aParser.m_parsedWords[0]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[1]->word.toStdString() == "FrozenDeepSPl1");
            CHECK(aParser.m_parsedWords[1]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[2]->word.toStdString() == "-scale");
            CHECK(aParser.m_parsedWords[2]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[5]->type == Parser::WordType::Quoted);
            CHECK(aParser.m_parsedWords[6]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[7]->type == Parser::WordType::Quoted);
            CHECK(aParser.m_parsedWords[8]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[9]->word.toStdString() == "12 13 \"m means meters\n    ft means feet\" m \"another \nmultiline quoted string\" b");
            CHECK(aParser.m_parsedWords[9]->type == Parser::WordType::Bracketed);
        }
    }

    GIVEN("an erroneous file with a multiline bracketed 'string' with a multiline quoted string inside it") {
        aParser.setFilename(fullPathParser("parser-080-multiline-bracketed-string_with_multiline_quoted_string_with_error.th2"));
        auto result = aParser.parse();

        THEN("parsing fails") {
            CHECK(result == false);
        }
    }

    GIVEN("a file with a comment") {
        parseTH2File(aParser, "parser-090-comment.th2", 1);

        THEN("has one comment type word") {
            CHECK(aParser.m_parsedWords[0]->word.toStdString() == "# Comment example");
            CHECK(aParser.m_parsedWords[0]->type == Parser::WordType::Comment);
        }
    }

    GIVEN("a file with a curly 'string' inside it") {
        parseTH2File(aParser, "parser-100-curly_string_only.th2", 4);

        THEN("has 1 regular word and a final curly one") {
            CHECK(aParser.m_parsedWords[0]->word.toStdString() == "setXVIgrids");
            CHECK(aParser.m_parsedWords[0]->type == Parser::WordType::Regular);
            CHECK(aParser.m_parsedWords[3]->word.toStdString() == "1.0 m");
            CHECK(aParser.m_parsedWords[3]->type == Parser::WordType::Curly);
        }
    }
}
#endif
