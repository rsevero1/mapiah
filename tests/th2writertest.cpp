/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include <catch2/catch_test_macros.hpp>

#include "../data/th2/th2file.h"

#include "../functional/th2writer.h"

#include "../parsers/th2parser.h"

#include <QFileInfo>
#include <QString>

const QString fullPathTH2Writer(const QString &aFilename)
{
    QString projectPath = QFileInfo("./").canonicalPath();

    return projectPath + "/Testing/auxiliary/th2parser/" + aFilename;
}

th2file_up th2WriterParseFile(TH2Parser &aParser, QString aTH2FileName)
{
    aParser.setFilename(fullPathTH2Writer(aTH2FileName));
    th2file_up aTH2File = aParser.parseTH2();
    REQUIRE(aTH2File.get() != nullptr);
    return aTH2File;
}

QByteArray th2WriterWriteFile(TH2File *aFile, bool showOutput = false)
{
    TH2Writer aWriter = TH2Writer(aFile, "");
    auto result = aWriter.write();

    REQUIRE(result == true);

    QByteArray output = aWriter.output();

    if (showOutput)
    {
        qInfo() << output;
    }

    return output;
}

SCENARIO( "We have a TH2Writer with an empty TH2File object", "[th2writer]" ) {
    TH2File aTH2File = TH2File();

    QByteArray output = th2WriterWriteFile(&aTH2File);

    THEN("results in the expected output") {
        CHECK(output == QByteArray("encoding utf-8\n"));
    }
}

SCENARIO( "We have a TH2Writer", "[th2writer]" ) {
    TH2Parser aParser = TH2Parser();

    GIVEN("a file with an encoding command and trailing space") {
        th2file_up aTH2File = th2WriterParseFile(aParser, "th2parser-0011-encoding_with_trailing_space.th2");

        THEN("the resulting output matches the original content") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray("encoding utf-8\n"));
        }
    }

    GIVEN("a file with an encoding not UTF-8") {
        th2file_up aTH2File = th2WriterParseFile(aParser, "th2parser-0013-iso8859-1_encoding.th2");

        THEN("results in right encoding name and proper char translation") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            // The hex codes below represent the folowing chars in ISO8859-1: àáâãäåç
            CHECK(output == QByteArray("encoding ISO8859-1\n# ISO8859-1 comment: \xE0\xE1\xE2\xE3\xE4\xE5\xE7\n"));
        }
    }

    GIVEN("a file with encoding and a XTHERION setting") {
        th2file_up aTH2File = th2WriterParseFile(aParser, "th2parser-0030-adding_xtherionsetting.th2");

        THEN("results in TH2File with elements representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
##XTHERION## xth_me_area_adjust -164 -2396 4206 1508
)RX"));

        }
    }

    GIVEN("a file with encoding, an empty line and a XTHERION setting") {
        aParser.setFilename(fullPathTH2Writer("th2parser-0031-with_empty_line.th2"));

        WHEN("read discarding empty lines") {
            th2file_up aTH2File = aParser.parseTH2();
            CHECK(aTH2File.get() != nullptr);

            THEN("results in TH2File with elements representing the parsed file") {
                QByteArray output = th2WriterWriteFile(aTH2File.get());

                CHECK(output == QByteArray(
R"RX(encoding utf-8
##XTHERION## xth_me_area_adjust -164 -2396 4206 1508
)RX"));
            }
        }
    }

    GIVEN("a file with encoding and a scrap with 4 points") {
        th2file_up aTH2File = th2WriterParseFile(aParser, "th2parser-0073-scrap_with_points.th2");

        WHEN("written respecting the original double values representations") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            THEN("results in TH2File with a element representing the parsed file") {
                CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap poco_surubim_SCP01 -scale [-164 -2396 3308 -2396 0 0 88.1888 0 m]
	point 2596 -468 debris
	point 782 -1740 station -subtype fixed -name A2@final_de_semana_26_e_27_de_setembro_de_2015 -id A2
	point 2316 -548 label -text "200 m"
	point 2547.70696983 6236.61014023 station -name 5.18
endscrap
)RX"));
            }
        }
    }

    GIVEN("a file with a point ending in a quoted string with windows line endings") {
        th2file_up aTH2File = th2WriterParseFile(aParser, "th2parser-0074-point_ending_in_quoted_string_with_windows_line_endings.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray("encoding utf-8\r\npoint 278 5488 label -scale huge -text \"The Frozen Deep\"\r\n"));
        }
    }

    GIVEN("a file with a line with internal data including an invalid inside line option") {
        th2file_up aTH2File = th2WriterParseFile(aParser, "th2parser-0082-line_with_internal_data_including_an_invalid_inside_line_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line wall -id prd03_SCP03 -subtype presumed
	1758 -1030
		smooth on
	2147.74 -1120.48 1834.24 -1042.2 2668 -1364
	# The 'smooth off' option below is created by XTherion sometimes but
	# has no real effect. TH2Parser discards it.
endline
)RX"));
        }
    }

    GIVEN("a file with a line inside a scrap") {
        th2file_up aTH2File = th2WriterParseFile(aParser, "th2parser-0099-file_with_line_inside_scrap.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap poco_surubim_SCP02
	line border -close on
		3592 208
		3539.45 249.03 3447.39 245.1 3392 208
			smooth on
		3233.22 101.65 3066.45 -131.93 3204 -332
	endline
endscrap
)RX"));
        }
    }

    GIVEN("a file with 2 scraps") {
        th2file_up aTH2File = th2WriterParseFile(aParser, "th2parser-0100-file_with_2_scraps.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap poco_surubim_SCP01 -scale [-164 -2396 3308 -2396 0 0 88.1888 0 m]
	point 521 -1840 entrance
endscrap
scrap poco_surubim_SCP02 -scale [-164 -2396 3308 -2396 0 0 88.1888 0 m]
	area clay -clip on
		l85-3732--20
	endarea
	line border -id l85-3732--20 -close on -visibility off
		3592 208
		3539.45 249.03 3447.39 245.1 3392 208
		3233.22 101.65 3066.45 -131.93 3204 -332
		3266.87 -423.45 3365.54 -513.28 3476 -524
		3929.86 -568.03 3743.42 89.77 3592 208
	endline
endscrap
)RX"));
        }
    }

    GIVEN("a file with comment blocks") {
        th2file_up aTH2File = th2WriterParseFile(aParser, "th2parser-0110-comment_block.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
comment
The scrap below is really complex.
Take care!!
endcomment
scrap poco_surubim_SCP01 -scale [-164 -2396 3308 -2396 0 0 88.1888 0 m]
	point 2596 -468 debris
comment

Another comment block.

But this one is inside a scrap!

endcomment
endscrap
)RX"));
        }
    }

    GIVEN("a file with a point with a single set option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0120-point_with_single_set_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -423.45 3365.54 station -align right
)RX"));
        }
    }

    GIVEN("a file with a point with a substituted single set option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0121-point_with_single_set_option_substituted.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -423.45 3365.54 station -align right
)RX"));
        }
    }

    GIVEN("a file with a point with a inline subtype option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0122-point_with_inline_subtype.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 43 336 water-flow:paleo
)RX"));
        }
    }

    GIVEN("a file with a point with a inline subtype option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0123-point_with_user_type.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 432 911 u:jabuticaba
)RX"));
        }
    }

    GIVEN("a file with a point with an orientation option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0126-point_with_orientation_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 122.0031 321.9712 ex-voto -orientation [173.23 deg]
)RX"));
        }
    }

    GIVEN("a file with a point with an alternative ID for the orientation option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0127-point_with_alternative_id_for_orientation_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 4122 9321 electric-light -orientation [297 deg]
)RX"));
        }
    }

    GIVEN("a file with a point with a preset scale value") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0130-point_with_preset_scale_value.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -949.68 -354.61 altitude -scale tiny
)RX"));
        }
    }

    GIVEN("a file with a point with a numeric scale value") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0131-point_with_numeric_scale_value.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 717.49 552.12 date -scale 2.84
)RX"));
        }
    }

    GIVEN("a file with a point with a context option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0140-point_with_context_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -104.04 75 date -context line extra-info
)RX"));
        }
    }

    GIVEN("a file with a point with a dist option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0150-point_with_dist_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -4882 -2163 extra -dist [18 m]
)RX"));
        }
    }

    GIVEN("a file with a point with a dist option with non default unit") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0151-point_with_dist_option_with_unit.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -4381 -253 extra -dist [2450 cm]
)RX"));
        }
    }

    GIVEN("a file with a point with an extend option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0160-point_with_extend_option_without_previous_station.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -2675 2206 station -extend
)RX"));
        }
    }

    GIVEN("a file with a point with an extend option and a previous station") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0161-point_with_extend_option_with_previous_station.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -3218 -2379 station -extend previous a12
)RX"));
        }
    }


    GIVEN("a file with a point with an extend option and an alternative previous station") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0162-point_with_extend_option_with_alternate_previous_station.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -4747 -2577 station -extend previous c33
)RX"));
        }
    }

    GIVEN("a file with an altitude point with a value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0170-point_with_value_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -2885 604 altitude -value 46
)RX"));
        }
    }

    GIVEN("a file with an altitude point with a value option and a fix") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0171-altitude_point_with_fix.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 3852 -2159 altitude -value [fix 59]
)RX"));
        }
    }

    GIVEN("a file with an altitude point with a value option and a fix and an unit") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0172-altitude_point_with_fix_and_unit.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 2438 -2933 altitude -value [fix 107.1 in]
)RX"));
        }
    }

    GIVEN("a file with an altitude point with a value option and an unit") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0173-altitude_point_with_unit.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -4884 -1045 altitude -value [31.8 yd]
)RX"));
        }
    }

    GIVEN("a file with a dimensions point with an above and below value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0174-dimensions_point_with_above_and_below.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -4156 1012 dimensions -value [32 61]
)RX"));
        }
    }

    GIVEN("a file with a dimensions point with an above, below and unit value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0175-dimensions_point_with_above_below_and_unit.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 1387 2663 dimensions -value [82 2 ft]
)RX"));
        }
    }

    GIVEN("a file with a height point with a height value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0176-height_point_with_height.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -2085 3128 height -value +15
)RX"));
        }
    }

    GIVEN("a file with a height point with a depth value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0177-height_point_with_depth.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -1935 -3691 height -value -23
)RX"));
        }
    }

    GIVEN("a file with a height point with a depth and height value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0178-height_point_with_depth_height.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 3990 4746 height -value 25
)RX"));
        }
    }

    GIVEN("a file with a height point with a height and unit value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0179-height_point_with_height_and_unit.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -3847 4540 height -value [+35 ft]
)RX"));
        }
    }

    GIVEN("a file with a passage-height point with a height value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0180-passage_height_point_with_height.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -3717 3423 passage-height -value +6
)RX"));
        }
    }

    GIVEN("a file with a passage-height point with a depth value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0181-passage_height_point_with_depth.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 3853 -1100 passage-height -value -15
)RX"));
        }
    }

    GIVEN("a file with a passage-height point with a distance value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0182-passage_height_point_with_distance.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 2163 -814 passage-height -value 17
)RX"));
        }
    }

    GIVEN("a file with a passage-height point with a height and depth value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0183-passage_height_point_with_height_depth.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 4502 -365 passage-height -value [+81 -1471]
)RX"));
        }
    }

    GIVEN("a file with a passage-height point with a height, depth and unit value option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0184-passage_height_point_with_height_depth_unit.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 1051 -2210 passage-height -value [+6 -71 in]
)RX"));
        }
    }

    GIVEN("a file with a line with a context option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0190-line_with_context_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line contour -context line extra
	2802 -969
	3804 3512
endline
)RX"));
        }
    }

    GIVEN("a file with a line with a text option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0200-line_with_text_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line label -text "Important info regarding this line"
	-3068 3038
	717 -3448
endline
)RX"));
        }
    }

    GIVEN("a file with a line with a height option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0210-line_with_height_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line pit -height [4.44 m]
	-1201 4317
	2446 191
endline
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a cs option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0220-scrap_with_cs_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras1 -cs long-lat
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a stations option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0230-scrap_with_stations_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras2 -stations a2,c4
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a title option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0240-scrap_with_title_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras3 -title "Buraco das Araras - Salão Seco"
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with an walls option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0250-scrap_with_walls_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras4 -walls off
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with an author option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0260-scrap_with_author_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras5 -author 2022.02.13@11:27:32 Rodrigo Severo
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with an author option with fractional seconds") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0261-scrap_with_author_option_with_fractional_seconds.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras6 -author 2022.02.13@11:27:32.12 Rodrigo Severo
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with an author option with a date range") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0262-scrap_with_author_option_with_date_range.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras7 -author 2022.02.13@11:27:32 - 2022.02.13@11:58:00 Rodrigo Severo
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with an author option with a quoted name") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0263-scrap_with_author_option_with_quoted_name.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras8 -author 2022.02.13@11:58:00 "Rodrigo Severo Cambará"
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a copyright option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0270-scrap_with_copyright_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras9 -copyright 2022.02.13@14:50:52 "Todos os diretos reservados pra mim®"
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a flip option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0280-scrap_with_flip_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras10 -flip horizontal
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with an empty flip option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0281-scrap_with_flip_empty_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras11 -flip
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a projection option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0290-scrap_with_projection_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras12 -projection none
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a projection option with an index") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0291-scrap_with_projection_option_with_index.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras13 -projection plan:main
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a projection option with an elevation with a direction") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0292-scrap_with_projection_option_with_elevation_with_direction.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras14 -projection [elevation:alternative 273 grad]
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a sketch option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0300-scrap_with_sketch_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras15 -sketch ./FrozenDeep_p.xvi 12 32
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a station-names option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0310-scrap_with_station-names_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap araras16 -station-names [] 2021-12-28.reveillon2021
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a scale option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0320-scrap_with_scale_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap andorinhas1 -scale 793.112
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a scale option and length unit") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0321-scrap_with_scale_option_with_length_unit.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap andorinhas2 -scale [183.092 cm]
endscrap
)RX"));
        }
    }

    GIVEN("a file with a scrap command with a scale option and amount of drawing units") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0322-scrap_with_scale_option_amount_drawing_units.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
scrap andorinhas3 -scale [252 584.2 yd]
endscrap
)RX"));
        }
    }

    GIVEN("a file with a line command with adjust line point option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0330-linepoint_with_adjust_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line floor-step
	1758 -1030
		adjust horizontal
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line command with direction line point option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0331-linepoint_with_direction_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line pit
		direction point
	1758 -1030
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line command with a gradient line point option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0332-linepoint_with_gradient_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line contour
		gradient point
	1758 -1030
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line command with a smooth line point option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0333-linepoint_with_smooth_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line ceiling-step
	1758 -1030
		smooth off
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line command with a mark line point option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0340-linepoint_with_mark_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line chimney
	1758 -1030
		mark dirt-passage
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line command with an altitude line point option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0350-linepoint_with_altitude_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line overhang
	1758 -1030
		altitude [+4 m]
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line command with an altitude line point option with a fix") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0351-linepoint_with_altitude_option_with_fix.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line ceiling-meander
	1758 -1030
		altitude [fix 1510 ft]
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line command with an altitude line point option with a fix") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0352-linepoint_with_altitude_option_set_as_nan.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line ceiling-meander
	1758 -1030
		altitude 0
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a altitude point command with a NaN value") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0353-altitude_point_with_value_option_set_as_nan.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 1758 -1030 altitude -value 0
)RX"));
        }
    }

    GIVEN("a file with a line command with a l-size line point option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0360-linepoint_with_l-size_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line floor-meander
		l-size 3
	1758 -1030
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line command with a r-size line point option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0361-linepoint_with_r-size_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line low-ceiling
	1758 -1030
		r-size 1.5
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line command with a size line point option") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0362-linepoint_with_size_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line pit-chimney
	1758 -1030
		size 4
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a line of type rope command") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0370-line_of_type_rope.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line rope -reverse on
	2802 -969
	3804 3512
endline
)RX"));
        }
    }

    GIVEN("a file with a point of type date and a value option with only a date") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0380-point_of_type_date_with_date_only_value_option.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point 2282 80 date -value 2022.02.05 -scale tiny -align bottom-left
)RX"));
        }
    }

    GIVEN("a file with an input") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0390-input.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
input ./testfile.th
)RX"));
        }
    }

    GIVEN("a file with a date point with an incomplete date") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0400-date_point_with_incomplete_date.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -2885 604 date -value 2021.02
)RX"));
        }
    }

    GIVEN("a file with a date point with a complete range") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0401-date_point_with_complete_range.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -2885 604 date -value 2021.12.23@08:30:01 - 2022.02.09@02:30:07
)RX"));
        }
    }

    GIVEN("a file with a date point with an incomplete range") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0402-date_point_with_incomplete_range.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -2885 604 date -value 2021.12.23 - 2022.02.09@02:30
)RX"));
        }
    }

    GIVEN("a file with a handrail point") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0410-handrail_point.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
point -2885 604 handrail
)RX"));
        }
    }

    GIVEN("a file with a via-ferrata line command") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0411-via-ferrata_line.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line via-ferrata
	1758 -1030
	2147.74 -1120.48
endline
)RX"));
        }
    }

    GIVEN("a file with a rope line command with anchors and rebelays options") {
        th2file_up aTH2File = th2WriterParseFile(
            aParser,
            "th2parser-0412-rope_line_with_anchors_and_rebelays_options.th2");

        THEN("results in TH2File with a element representing the parsed file") {
            QByteArray output = th2WriterWriteFile(aTH2File.get());

            CHECK(output == QByteArray(
R"RX(encoding utf-8
line rope -rebelays off -anchors on
	82 -4
	82 -4 59 -24 60 -49
	61 -74 87 -91 87 -91
endline
)RX"));
        }
    }

}
