/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include <catch2/catch_test_macros.hpp>

#include "../data/th2/th2point.h"

#include <QStringList>

SCENARIO( "We have lists of Point types", "[th2file][th2point]" ) {
    THEN("valid point types are supported") {
        QStringList validPointTypes {
            "disk",
            "ex-voto",
            "stalagmite",
            "station",
        };

        for(auto &aPointType : validPointTypes)
        {
            CHECK(TH2Point::isSupportedPointType(aPointType) == true);
        }
    }

    THEN("invalid point types are not supported") {
        QStringList invalidPointTypes {
            "alien-bones",
            "dripline",
            "sump",
            "user",
            "wormhole",
        };

        for(auto &aPointType : invalidPointTypes)
        {
            CHECK(TH2Point::isSupportedPointType(aPointType) == false);
        }
    }
}
