/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include <catch2/catch_test_macros.hpp>

#include "../data/th2/th2point.h"

#include "../data/th2/option/th2contextoption.h"
#include "../data/th2/option/th2freetextoption.h"
#include "../data/th2/option/th2lengthoption.h"
#include "../data/th2/option/th2option.h"
#include "../data/th2/option/th2orientationoption.h"
#include "../data/th2/option/th2pointscaleoption.h"
#include "../data/th2/option/th2singlesetoption.h"
#include "../data/th2/option/th2subtypeoption.h"

#include <QHash>
#include <QString>
#include <QStringList>

typedef bool(*supportsPointTypeFuncPtrType)(const QString &, const QString &, const QString &);

QHash<QString, supportsPointTypeFuncPtrType> optionFuncs {
    {"align", &TH2SingleSetOption::isSupportedOption },
    {"clip", &TH2SingleSetOption::isSupportedOption },
    {"context", &TH2ContextOption::isSupportedOption },
    {"dist", &TH2LengthOption::isSupportedOption },
    {"explored", &TH2LengthOption::isSupportedOption },
    {"extend", &TH2Option::isSupportedOption },
    {"from", &TH2FreeTextOption::isSupportedOption },
    {"id", &TH2FreeTextOption::isSupportedOption },
    {"name", &TH2FreeTextOption::isSupportedOption },
    {"orientation", &TH2OrientationOption::isSupportedOption },
    {"scale", &TH2PointScaleOption::isSupportedOption },
    {"scrap", &TH2FreeTextOption::isSupportedOption },
    {"subtype", &TH2SubtypeOption::isSupportedOption },
    {"text", &TH2FreeTextOption::isSupportedOption },
    {"value", &TH2Option::isSupportedOption },
};

void checkSupportedTypesForOptionName(
    const QString &aElementType,
    const QStringList &pointTypes,
    const QString &optionName)
{
    supportsPointTypeFuncPtrType supportPointTypeFunc;

    if (aElementType == "point")
    {
        supportPointTypeFunc = optionFuncs[optionName];
    }

    for(auto &aPointTypeFromList : pointTypes)
    {
        CAPTURE(aPointTypeFromList.toStdString());
        CAPTURE(optionName.toStdString());
        CHECK(supportPointTypeFunc(aElementType, aPointTypeFromList, optionName) == true);
    }
}

void checkUnsupportedTypesForOptionName(
    const QString &aElementType,
    const QStringList &pointTypes,
    const QString &optionName,
    const QStringList &pointTypesToIgnore)
{
    supportsPointTypeFuncPtrType supportPointTypeFunc;

    if (aElementType == "point")
    {
        supportPointTypeFunc = optionFuncs[optionName];
    }

    for(auto &aPointTypeFromList : pointTypes)
    {
        if (pointTypesToIgnore.contains(aPointTypeFromList))
        {
            continue;
        }
        CAPTURE(aPointTypeFromList.toStdString());
        CAPTURE(optionName.toStdString());
        CHECK(supportPointTypeFunc(aElementType, aPointTypeFromList, optionName) == false);
    }
}

void checkElementTypesForOptionName(
    const QString &aElementType,
    const QStringList &pointTypes,
    const QString &optionName,
    const QStringList &pointTypesWithSupport)
{
    checkSupportedTypesForOptionName(aElementType, pointTypesWithSupport, optionName);
    checkUnsupportedTypesForOptionName(aElementType, pointTypes, optionName, pointTypesWithSupport);
}

SCENARIO( "We have some pairs of Point and Option types", "[th2file][th2point][th2option]" ) {

    QStringList pointTypes {
        "altitude",
        "continuation",
        "extra",
        "section",
        "stalactite",
        "station",
        "steps",
    };

    THEN("singleset options supported by all point types are reported as supported") {
        QStringList optionNames {
            "align",
        };

        for (auto &optionName : optionNames)
        {
            checkSupportedTypesForOptionName("point", pointTypes, optionName);
        }
    }

    THEN("context option which is supported by all point types are reported as supported") {
        QStringList optionNames {
            "context",
        };

        for (auto &optionName : optionNames)
        {
            checkSupportedTypesForOptionName("point", pointTypes, optionName);
        }
    }

    THEN("freetext options supported by all point types are reported as supported") {
        QStringList optionNames {
            "id",
        };

        for (auto &optionName : optionNames)
        {
            checkSupportedTypesForOptionName("point", pointTypes, optionName);
        }
    }

    THEN("orientation option which is supported by all point types are reported as supported") {
        QStringList optionNames {
            "orientation",
        };

        for (auto &optionName : optionNames)
        {
            checkSupportedTypesForOptionName("point", pointTypes, optionName);
        }
    }

    THEN("scale option which is supported by all point types are reported as supported") {
        QStringList optionNames {
            "scale",
        };

        for (auto &optionName : optionNames)
        {
            checkSupportedTypesForOptionName("point", pointTypes, optionName);
        }
    }

//    THEN("options supported only for 'altitude' point type are reported as supported") {
//        QStringList optionNamesAltitude {
//            "value",
//        };

//        QStringList altitudePointType {
//            "altitude"
//        };

//        checkSupportedPointTypesForOptionNames(altitudePointType, optionNamesAltitude);
//        checkUnsupportedPointTypesForOptionNames(pointTypes, optionNamesAltitude, altitudePointType);
//    }

//    THEN("options supported only for 'continuation' point type are reported as supported") {
//        QStringList optionNamesContinuation {
//            "explored",
//        };

//        QStringList continuationPointType = {
//            "continuation"
//        };

//        checkSupportedPointTypesForOptionNames(
//            continuationPointType,
//            optionNamesContinuation);
//        checkUnsupportedPointTypesForOptionNames(
//            pointTypes,
//            optionNamesContinuation,
//            continuationPointType);

//    }

    THEN("freetext options supported only for 'extra' point type are reported as supported") {
        QStringList optionNames {
            "from",
        };

        QStringList supportedPointType = {
            "extra"
        };

        for (auto &optionName : optionNames)
        {
            checkElementTypesForOptionName(
                "point",
                pointTypes,
                optionName,
                supportedPointType);
        }
    }

    THEN("double options supported only for 'extra' point type are reported as supported") {
        QStringList optionNames {
            "dist",
        };

        QStringList supportedPointType = {
            "extra"
        };

        for (auto &optionName : optionNames)
        {
            checkElementTypesForOptionName(
                "point",
                pointTypes,
                optionName,
                supportedPointType);
        }
    }

    THEN("options supported only for 'section' point type are reported as supported") {
        QStringList optionNames {
            "scrap",
        };

        QStringList supportedPointType = {
            "section"
        };

        for (auto &optionName : optionNames)
        {
            checkElementTypesForOptionName(
                "point",
                pointTypes,
                optionName,
                supportedPointType);
        }
    }

//    THEN("extend option which is supported only for 'station' point type are reported as supported") {
//        QStringList optionNames {
//            "extend",
//        };

//        QStringList supportedPointType = {
//            "station"
//        };

//        for (auto &optionName : optionNames)
//        {
//            checkElementTypesForOptionName(
//                "point",
//                pointTypes,
//                optionName,
//                supportedPointType);
//        }
//    }

    THEN("freetext options supported only for 'station' point type are reported as supported") {
        QStringList optionNames {
            "name",
        };

        QStringList supportedPointType = {
            "station"
        };

        for (auto &optionName : optionNames)
        {
            checkElementTypesForOptionName(
                "point",
                pointTypes,
                optionName,
                supportedPointType);
        }
    }

    THEN("subtype option which is supported only for 'station' point type are reported as supported") {
        QStringList optionNames {
            "subtype",
        };

        QStringList supportedPointType = {
            "station"
        };

        for (auto &optionName : optionNames)
        {
            checkElementTypesForOptionName(
                "point",
                pointTypes,
                optionName,
                supportedPointType);
        }
    }

}
