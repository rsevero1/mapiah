/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef TH2PARSER_H
#define TH2PARSER_H

#include "parser.h"

#include "../data/th2/th2element.h"
#include "../data/th2/th2typedefs.h"

#include "../factories/th2factory.h"

#include <QFile>
#include <QRegularExpression>
#include <QString>

#include <functional>
#include <typeinfo>
#include <typeindex>
#include <vector>

class THInput;

class TH2Area;
class TH2File;
class TH2Line;
class TH2LinePoint;
class TH2Point;
class TH2Scrap;
class TH2Symbol;

class TH2AltitudeOption;
class TH2AuthorOption;
class TH2ContextOption;
class TH2CopyrightOption;
class TH2DoubleOption;
class TH2ExtendOption;
class TH2FreeTextOption;
class TH2LengthOption;
class TH2MarkOption;
class TH2Option;
class TH2OptionList;
class TH2PointScaleOption;
class TH2ProjectionOption;
class TH2ScrapScaleOption;
class TH2SingleSetOption;
class TH2SketchOption;
class TH2StationNamesOption;
class TH2SubtypeOption;
class TH2ValueOption;

class TH2AzimuthValue;
class TH2DateValue;
class TH2LengthValue;
class TH2PersonValue;

class TH2Parser : public Parser
{
    enum ParseAllowedWordResult {
        Success,
        Failure,
        SectionEnd
    };

    QString xtherionSetting;
    QString xtherionParameters;

    TH2Option *currentOption;
    QString currentElementType;

    TH2Point *newPoint;
    TH2LinePoint * newLinePoint;

    TH2Line *newLine;
    QList<qreal> linePointCoordinates;
    QStringList linePointOriginalRepresentations;

    TH2Area *newArea;
    QMultiHash<QString, QString> linesXareas;

    TH2Scrap *newScrap;
    bool optionIsID;
    QString currentID;

    TH2File *m_th2File;
    th2file_up u_th2File;

    QList<TH2Element *> m_ancestor;

    bool m_parsingSucceeded;

    typedef std::vector<th2element_up> th2element_vector;
    th2element_vector nextLinePointOptions;

    TH2Element *currentParent() const;

    void addNewParent(TH2Element *newAncestor);
    void removeCurrentParent();

    void appendElement(th2element_up uniqueElement);

    void initializeTH2Parser();

    static const QString XTHERION_SETTING_ID;
    static const QString XTHERION_IMAGE_SETTING_ID;

    static constexpr qint32 MINUTES_PER_DEGREE = 60;
    static constexpr qint32 SECONDS_PER_DEGREE = 3600;

    static const QRegularExpression altitudeValueRegex;
    static const QRegularExpression degreeSpecialNotationRegex;
    static const QRegularExpression dimensionsValueRegex;
    static const QRegularExpression heightValueRegex;
    static const QRegularExpression isNaNRegex;
    static const QRegularExpression measurementWithOptionalUnitRegex;
    static const QRegularExpression passageHeightDoubleValueRegex;
    static const QRegularExpression passageHeightSingleValueRegex;
    static const QRegularExpression projectionElevationRegex;
    static const QRegularExpression projectionGeneralRegex;
    static const QRegularExpression scrapScaleDrawingUnitSizeRegex;
    static const QRegularExpression scrapScaleAmountDrawingUnitsRegex;
    static const QRegularExpression scrapScaleCompleteRegex;

    static const QString COMMENT_PREFIX;
    static const QString OPTION_NAME_PREFIX;

    static const QString DEFAULT_ENCODING;

    static const QString DEGREES_REGEX_NAME;
    static const QString MINUTES_REGEX_NAME;
    static const QString NUMBER_REGEX_NAME;
    static const QString SECONDS_REGEX_NAME;
    static const QString UNIT_REGEX_NAME;

    static const QString ABOVE_HEIGHT_SIGN;
    static const QString BELOW_DEPTH_SIGN;

    // Parser interface
protected:
    bool internalParse() override;

    void unrecognizedLine(const QString reason);
    bool parseEndOfLine();

    bool parseLineContents();
    bool parseAreaContents();

    bool parseLineLine();
    bool parseAreaLine();

    bool parseEncoding();

    bool parseCommentWordType();
    bool parseComment(const bool aIsPreviousLineEndComment = false);
    bool parseMultiLineComment();

    bool parseXTherionSetting();
    bool parseXTherionImageSetting();

    bool parseArea();
    bool parseLine();
    bool parsePoint();
    bool parseScrap();

    bool parseInput();

    bool parseLinePointOption();

    // TH2Parser::optionsInLine expects a method that will parse this set
    // of options for the current symbol type.
    // The signature
    // optionsInLine call example:
    //
    // optionsInLine(std::bind(&TH2Parser::parseScrapOption, this));
    typedef bool optionsInLineParseFuncReturnType;
    typedef std::function<optionsInLineParseFuncReturnType()> optionsInLineParseFuncType;
    bool optionsInLine(optionsInLineParseFuncType symbolParseOptionFunc);
    // Another implementation technique using templates + lambdas:
    //
    // template <typename F>
    // void optionsInLine(F&& symbolParseOptionFunc);
    //
    // and optionsInLine would be called like this:
    //
    // optionsInLine([this]() { parseScrapOption(); });
    //
    // Please observe that if we use this template + lambda solution
    // we have to include TH2Parse::optionsInLine implementation here is
    // parse.h header file because templates have to be completely
    // defined in the header file. Or not. To better understand take
    // a look at https://isocpp.org/wiki/faq/templates#templates-defn-vs-decl
    optionsInLineParseFuncReturnType parseOption(
        const QString &aElementType,
        const QString &aSymbolType,
        TH2OptionList *aOptionList = nullptr);
    optionsInLineParseFuncReturnType parseAreaOption();
    optionsInLineParseFuncReturnType parseLineOption();
    optionsInLineParseFuncReturnType parsePointOption();
    optionsInLineParseFuncReturnType parseScrapOption();

    typedef ParseAllowedWordResult parseLinesParseAllowedWordsFuncReturnType;
    typedef std::function<parseLinesParseAllowedWordsFuncReturnType()> parseLinesParseAllowedWordsFuncType;
    bool parseLines(parseLinesParseAllowedWordsFuncType parseAllowedWords);
    parseLinesParseAllowedWordsFuncReturnType parseWordGeneral();
    parseLinesParseAllowedWordsFuncReturnType parseWordScrap();

    enum class FetchNextWord {
        Yes,
        No
    };

    template<typename OptionPtrType>
    bool setOptionValue(FetchNextWord fetchNextWord = FetchNextWord::Yes)
    {
        OptionPtrType newOption = dynamic_cast<OptionPtrType>(currentOption);

        if (newOption == nullptr)
        {
            parseError(
                QString("Failure casting newOption to <%1 *>.")
                    .arg(QString::fromStdString(typeid(OptionPtrType).name())),
                QStringLiteral("TH2Parser::setOptionValue()"));
            return false;
        }

        if ((fetchNextWord == FetchNextWord::Yes) && (!nextWord()))
        {
            parseError(
                QString("Failure reading %1 option value.")
                    .arg(QString::fromStdString(typeid(OptionPtrType).name())),
                QStringLiteral("TH2Parser::setOptionValue()"));
            return false;
        }

        return setOptionValuePerType(newOption);
    }

    bool setOptionValuePerType(TH2AltitudeOption *newAltitudeOption);
    bool setOptionValuePerType(TH2AuthorOption *newAuthorOption);
    bool setOptionValuePerType(TH2ContextOption *newContextOption);
    bool setOptionValuePerType(TH2CopyrightOption *newCopyrightOption);
    bool setOptionValuePerType(TH2DoubleOption *newDoubleOption);
    bool setOptionValuePerType(TH2ExtendOption *newExtendOption);
    bool setOptionValuePerType(TH2FreeTextOption *newFreeTextOption);
    bool setOptionValuePerType(TH2LengthOption *newLengthOption);
    bool setOptionValuePerType(TH2MarkOption *newMarkOption);
    bool setOptionValuePerType(TH2OrientationOption *newOrientationOption);
    bool setOptionValuePerType(TH2PointScaleOption *newPointScaleOption);
    bool setOptionValuePerType(TH2ProjectionOption *newProjectionOption);
    bool setOptionValuePerType(TH2ScrapScaleOption *newScrapScaleOption);
    bool setOptionValuePerType(TH2SingleSetOption *newSingleSetOption);
    bool setOptionValuePerType(TH2SketchOption *newSketchOption);
    bool setOptionValuePerType(TH2StationNamesOption *newStationNamesOption);
    bool setOptionValuePerType(TH2SubtypeOption *newSubtypeOption);
    bool setOptionValuePerType(TH2ValueOption *newValueOption);

    bool setAzimuthValue(TH2AzimuthValue *aAzimuthValue);
    bool setDatetimeValue(TH2DateValue *aDateValue);
    bool setLengthValue(TH2LengthValue *aLengthValue);
    bool setPersonValue(TH2PersonValue *aPersonValue);

    bool createLinePointBezier();
    bool createLinePointStraight();
    bool appendLinePoint(th2element_up uniqueElement);

    void joinLinesToAreas();

    void appendLinePointOptions(TH2LinePoint *aLinePoint);

    bool isNaN(const QString &valueText);

public:
    TH2Parser(const QString &aFilename);
    TH2Parser();

    th2file_up parseTH2();

    void setFilename(const QString &newFilename);
    bool parsingSucceeded() const;
};

#endif // TH2PARSER_H

