/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "parser.h"

#include <QApplication>
#include <QMessageBox>

#include <iostream>

#ifdef BASE_PARSER_DEBUG
#include <QDebug>
#include <string>
#endif

Parser::Brace::Brace()
{

};

Parser::Brace::Brace(QString aBeginChar, QString aEndChar, WordType aWordType)
    : beginChar(aBeginChar), endChar(aEndChar), finalWordType(aWordType)
{
    endBraceRegex = QRegularExpression("(.*?)" + endChar);
}

const QString &Parser::filename() const
{
    return m_filename;
}

void Parser::setFilename(const QString &newFilename)
{
    m_filename = newFilename;
}

bool Parser::justPeek()
{
    if (eol)
    {
        return false;
    }

    QRegularExpressionMatch match = justPeekRegex.match(
        currentLine,
        currentLinePos,
        QRegularExpression::NormalMatch,
        QRegularExpression::AnchorAtOffsetMatchOption
    );

    if (!match.hasMatch())
    {
        return false;
    }

    justPeekWord = match.captured(1);
    return true;
}

bool Parser::nextWord()
{
    if (eol)
    {
        return false;
    }

    QString aWord;
    qint32 currentPosUpdate;

#ifdef TH2_DEBUG
    qDebug() << "search string pre space skipper in Parser::nextWord():";
    qDebug() << currentLine.sliced(currentLinePos);
#endif
    QRegularExpressionMatch match = spaceSkipperRegex.match(
        currentLine,
        currentLinePos,
        QRegularExpression::NormalMatch,
        QRegularExpression::AnchorAtOffsetMatchOption
    );

    if (match.hasMatch())
    {
        currentLinePos += match.captured(0).length();
    }

#ifdef TH2_DEBUG
    qDebug() << "search string for wordRegex in Parser::nextWord():";
    qDebug() << currentLine.sliced(currentLinePos);
#endif
    match = wordRegex.match(
        currentLine,
        currentLinePos,
        QRegularExpression::NormalMatch,
        QRegularExpression::AnchorAtOffsetMatchOption
    );

#ifdef TH2_DEBUG
    qDebug() << "wordMatch inside Parser::nextWord():";
    qDebug() << match;
#endif

    if (match.hasMatch())
    {
        aWord = match.captured(1);
        if (currentLinePos == (currentLine.length() - match.captured(0).length()))
        {
            // If we matched until the end of the string, skip till the end.
            currentPosUpdate = match.captured(0).length();
        }
        else
        {
            // otherwise skip only the word we will actually process.
            currentPosUpdate = match.captured(1).length();
        }
    }
    else
    {
        currentPosUpdate = currentLine.size() - currentLinePos;
        aWord = currentLine.last(currentPosUpdate);
    }

    if (aWord.length() > 0)
    {
        char firstChar = aWord[0].toLatin1();
        switch(firstChar)
        {
        case '"':
            currentWordType = QuotedInConstruction;
            return nextQuotedString();
        case '[':
            currentWordType = BracketedInConstruction;
            return nextBracedString('[');
        case '{':
            currentWordType = BracketedInConstruction;
            return nextBracedString('{');
        case '#':
            // Return rest of line as comment.
            currentWord = currentLine.sliced(currentLinePos);
            currentWordType = WordType::Comment;
#ifdef BASE_PARSER_DEBUG
            m_parsedWords.append(new ParsedWord(currentWord, currentWordType));
#endif
            eol = true;
            return true;
        }

        currentLinePos += currentPosUpdate;
        updateEOL();

        if (eol && (aWord.last(1) == R"RX(\)RX"))
        {
            backslashedLine = true;
            aWord.chop(1);
            nextLine();
            eol = false;

            if (aWord.isEmpty())
            {
                return nextWord();
            }
        }
        else
        {
            backslashedLine = false;
        }
        currentWordType = WordType::Regular;
        currentWord = aWord;
    }
    else
    {
        currentWordType = WordType::Empty;
        currentWord = aWord;
        return false;
    }

#ifdef BASE_PARSER_DEBUG
    m_parsedWords.append(new ParsedWord(currentWord, currentWordType));
#endif
    return true;
}

bool Parser::nextQuotedString()
{
    // Skipping initial quote.
    currentLinePos += 1;

    QString aQuotedString;

    bool foundEndingQuote = false;
    QRegularExpressionMatch quoteMatch;

    while (!foundEndingQuote && !eof)
    {
        quoteMatch = quoteRegex.match(
            currentLine,
            currentLinePos,
            QRegularExpression::NormalMatch,
            QRegularExpression::AnchorAtOffsetMatchOption
        );

#ifdef TH2_DEBUG
        qDebug() << "quoteMatch inside Parser::nextQuotedString():";
        qDebug() << quoteMatch;
#endif

        if (quoteMatch.hasMatch())
        {
            // Adding the pre-quote match.
            aQuotedString += quoteMatch.captured(1);
            if (quoteMatch.captured(2) == R"RX("")RX")
            {
                // Adding the inside quote.
                aQuotedString += R"RX(")RX";
            }
            else
            {
                foundEndingQuote = true;
            }
            // We always add the full captured(0) length to currentLinePos as
            // we want to discard all matched characters, even the final quote
            // that was not actually included in currentWord.
            currentLinePos += quoteMatch.captured(0).length();
        }
        else
        {
            // Manually adding the final newline.
            aQuotedString += currentLine.sliced(currentLinePos) + "\n";
            nextLine();
        }
    }

    if (!foundEndingQuote)
    {
#ifdef TH2_DEBUG
        qInfo() << "No closing quote found at Parser::nextQuotedString().";
#endif
        currentWordType = WordType::UnrecognizedWord;
        return false;
    }

    updateEOL();

    currentWord = aQuotedString;
    currentWordType = WordType::Quoted;
#ifdef BASE_PARSER_DEBUG
    m_parsedWords.append(new ParsedWord(currentWord, currentWordType));
#endif
    return true;
}

bool Parser::nextBracedString(const char brace)
{
    // Skipping initial brace.
    currentLinePos += 1;

    bool foundEndBrace = false;
    QString aBracedString;
    QRegularExpressionMatch bracedMatch;

    while (!foundEndBrace && !eof)
    {
        // A braced string can have regular words, quoted and other braced
        // strings inside.
        if (!nextWord())
        {
#ifdef TH2_DEBUG
            qInfo() << "No word identified at Parser::nextBracedString().";
#endif
            currentWordType = WordType::UnrecognizedWord;
            return false;
        }

        switch(currentWordType)
        {
        case WordType::Regular:
            bracedMatch = braces[brace].endBraceRegex.match(
                currentWord,
                0,
                QRegularExpression::NormalMatch,
                QRegularExpression::AnchorAtOffsetMatchOption
            );

            if (bracedMatch.hasMatch())
            {
                foundEndBrace = true;
#ifdef TH2_DEBUG
//                qInfo() << "bracedMatch.captured(0):" << bracedMatch.captured(0);
//                qInfo() << "bracedMatch.captured(1):" << bracedMatch.captured(1);
#endif

                if (aBracedString.isEmpty())
                {
                    aBracedString = bracedMatch.captured(1);
                }
                else
                {
                    // Only add the captured text if it not empty to prevent
                    // inclusion of a unnecessary spaces.
                    if (!bracedMatch.captured(1).isEmpty())
                    {
                        aBracedString += " " + bracedMatch.captured(1);
                    }
                }


                // Rewinding currentLinePos in case we didn't use all of currentWord;
                currentLinePos -= currentWord.length() - bracedMatch.captured(0).length();
            }
            else
            {
                if (aBracedString.isEmpty())
                {
                    aBracedString = currentWord;
                }
                else
                {
                    // Only add the captured text if it not empty to prevent
                    // inclusion of a unnecessary spaces.
                    if (!currentWord.isEmpty())
                    {
                        aBracedString += " " + currentWord;
                    }
                }
            }

            break;
        case WordType::Quoted:
            if (aBracedString.isEmpty())
            {
                aBracedString = R"RX(")RX" + currentWord + R"RX(")RX";
            }
            else
            {
                aBracedString += R"RX( ")RX" + currentWord + R"RX(")RX";
            }
            break;
        case WordType::Bracketed:
            if (aBracedString.isEmpty())
            {
                aBracedString = braces['['].beginChar + currentWord + braces['['].endChar;
            }
            else
            {
                aBracedString += " " + braces['['].beginChar + currentWord + braces['['].endChar;
            }
            break;
        case WordType::Curly:
            if (aBracedString.isEmpty())
            {
                aBracedString = braces['{'].beginChar + currentWord + braces['{'].endChar;
            }
            else
            {
                aBracedString += " " + braces['{'].beginChar + currentWord + braces['{'].endChar;
            }
            break;
        default:
#ifdef TH2_DEBUG
            qInfo() << "Unsupported word type at Parser::nextBracedString().";
#endif
            currentWordType = WordType::UnrecognizedWord;
            return false;
        }
    }

    updateEOL();

    currentWord = aBracedString;
    currentWordType = braces[brace].finalWordType;
#ifdef BASE_PARSER_DEBUG
    m_parsedWords.append(new ParsedWord(currentWord, currentWordType));
#endif
    return true;
}

void Parser::updateEOL()
{
    eol = (currentLinePos >= (currentLine.size()));
}

bool Parser::nextLine(bool returnEmptyLines)
{
    while(true) {
        if (file.atEnd())
        {
            eof = true;
            eol = true;
            return false;
        }
        encodedLine = file.readLine();
        if (lineEndingType == MapiahEnums::LineEndingType::Unset)
        {
            identifyLineEndingType();
        }
        currentLine = codec->toUnicode(encodedLine);
        currentLineNumber++;
        if (!currentLine.trimmed().isEmpty() || returnEmptyLines)
        {
            break;
        }
    };

    switch (lineEndingType) {
    case MapiahEnums::LineEndingType::CR:
        if ((currentLine.length() >= 1) && (currentLine.last(1) == "\r"))
        {
            currentLine.chop(1);
        }
        break;
    case MapiahEnums::LineEndingType::CRLF:
        if ((currentLine.length() >= 2) && (currentLine.last(2) == "\r\n"))
        {
            currentLine.chop(2);
        }
        break;
    case MapiahEnums::LineEndingType::LFCR:
        if ((currentLine.length() >= 2) && (currentLine.last(2) == "\n\r"))
        {
            currentLine.chop(2);
        }
        break;
    default:
        if ((currentLine.length() >= 1) && (currentLine.last(1) == "\n"))
        {
            currentLine.chop(1);
        }
    }

    currentLinePos = 0;
    updateEOL();

    return true;
}

void Parser::parseError(const QString errorMessage, const QString methodName)
{
    QString detailedText = QString("File '%1'\nLine:\n'%2'\nLine number %3\nLine pos %4\nLast identified word: '%5'\nLast identified word type: %6")
            .arg(m_filename)
            .arg(currentLine)
            .arg(currentLineNumber)
            .arg(currentLinePos)
            .arg(currentWord)
            .arg(currentWordType);
    QApplication *guiApp = dynamic_cast<QApplication *>(QCoreApplication::instance());

    if (guiApp == nullptr)
    {
        // This is a console app. Probably this is the test app.
        QString consoleMsg = QString("WARNING!\nParse error at %1\n%2\n%3\n")
                .arg(methodName, errorMessage, detailedText);
        std::cout << consoleMsg.toStdString() << std::endl;
    }
    else
    {
        // We are in a proper GuiApp. Probably regular user.
        QMessageBox errorMessageBox;

        errorMessageBox.setText("Parse error at " + methodName);
        errorMessageBox.setInformativeText(errorMessage);
        errorMessageBox.setIcon(QMessageBox::Warning);
        errorMessageBox.setDetailedText(detailedText);
        errorMessageBox.exec();
    }
}

void Parser::identifyLineEndingType()
{
    if ((encodedLine.length() >= 2) && (encodedLine.last(2) == "\r\n"))
    {
        lineEndingType = MapiahEnums::LineEndingType::CRLF;
        lineEnding = "\r\n";
    }
    else if ((encodedLine.length() >= 2) && (encodedLine.last(2) == "\n\r"))
    {
        lineEndingType = MapiahEnums::LineEndingType::LFCR;
        lineEnding = "\n\r";
    }
    else if ((encodedLine.length() >= 1) && (encodedLine.last(1) == "\r"))
    {
        lineEndingType = MapiahEnums::LineEndingType::CR;
        lineEnding = "\r";
    }
    else if ((encodedLine.length() >= 1) && (encodedLine.last(1) == "\n"))
    {
        lineEndingType = MapiahEnums::LineEndingType::LF;
        lineEnding = "\n";
    }
}

#ifdef BASE_PARSER_DEBUG
// This implementation exists only for debug the base parser itself without
// interference of top level parsers like TH2Parser, THParser and XVIParser.
bool Parser::internalParse()
{
    nextLine();
    while (!eof)
    {
        while (!eol)
        {
            if (!nextWord())
            {
                return false;
            }
        }
#ifdef TH2_DEBUG
        qInfo() << "eol reached.";
#endif
        nextLine();
    }

#ifdef TH2_DEBUG
    qInfo() << "Parsed words:";
    for(ParsedWord* parsedWord : m_parsedWords) {
        qInfo() << "Word:" << parsedWord->word << " - Type: " << parsedWord->type;
    }
#endif

    return true;
}
#endif

void Parser::initializeParser()
{
    // Initializing support QHash for generalized braced string support
    // (bracketed and curly) to be used in Parser::nextBracedString().
    braces.insert(
        '[',
        Brace( "[", "]", WordType::Bracketed)
    );
    braces.insert(
        '{',
        Brace( "{", "}", WordType::Curly)
    );
}

Parser::Parser(const QString &aFilename)
    : m_filename((aFilename))
    , currentLineNumber(0)
    , currentLinePos(0)
    , backslashedLine(false)
    , eol(false)
    , eof(false)
    , lineEndingType(MapiahEnums::LineEndingType::Unset)
{
    initializeParser();
}

Parser::Parser()
    : currentLineNumber(0)
    , currentLinePos(0)
    , backslashedLine(false)
    , eol(false)
    , eof(false)
    , lineEndingType(MapiahEnums::LineEndingType::Unset)
{
    initializeParser();
}

bool Parser::parse()
{
    if (m_filename.isEmpty())
    {
#ifdef TH2_DEBUG
        qInfo() << "Can't parse with an empty file name." ;
#endif
        return false;
    }
    file.setFileName(m_filename);
    if (!file.open(QIODevice::ReadOnly))
    {
#ifdef TH2_DEBUG
        qInfo() << file.errorString();
#endif
        return false;
    }

    file.seek(0);

    return internalParse();
}
