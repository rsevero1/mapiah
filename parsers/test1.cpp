#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

namespace client
{
    namespace fusion = boost::fusion;
    namespace phoenix = boost::phoenix;
    namespace qi = boost::spirit::qi;
    namespace ascii = boost::spirit::ascii;

    ///////////////////////////////////////////////////////////////////////////
    //  Our structs
    ///////////////////////////////////////////////////////////////////////////
    struct XVIPoint
    {
        double x;
        double y;
    };

    struct XVIGrids
    {
        double distance;
        std::string unit;
    };

    struct XVIStation
    {
        XVIPoint coordinates;
        std::string name;
    }; 

    typedef std::vector<XVIStation> xvistation_vec_t;

    struct XVIStations
    {
        xvistation_vec_t stations;
    };

    struct XVIShot
    {
        XVIPoint from;
        XVIPoint to;
    };

    typedef std::vector<XVIShot> xvishot_vec_t;

    struct XVIShots
    {
        xvishot_vec_t shots;
    };

    typedef std::vector<XVIPoint> xvipoint_vec_t;

    struct XVISketchline
    {
        std::string color;
        xvipoint_vec_t points;
    };

    typedef std::vector<XVISketchline> xvisketchline_vec_t;

    struct XVISketchlines
    {
        xvisketchline_vec_t sketchlines;
    };

    // Copied from Writeth2.py by Andrew Atkinson:
    //
    // #Grid is{bottom left x, bottom left y,
    // #x1 dist, y1 dist, x2 dist, y2 dist, number of x, number of y}
    // pXVIfile.write(str((maxmin['minx'] - 1) * cfactor)
    //                 + " " + str((maxmin['miny'] - 1) * cfactor)
    //                 + " " + str(grid * cfactor)
    //                 + " 0.0 0.0 " + str(grid * cfactor)
    //                 + " " + xsquares + " " + ysquares + "}")
    struct XVIGrid
    {
        XVIPoint bottom;
        double x_dist_1;
        double y_dist_1;
        double x_dist_2;
        double y_dist_2;
        double qt_x;
        double qt_y;
    };

    struct XVIFile
    {
        XVIGrids m_XVIGrids;
        XVIStations m_XVIStations;
        XVIShots m_XVIShots;
        XVISketchlines m_XVISketchlines;
        XVIGrid m_XVIGrid;
    };
}

// We need to tell fusion about our structs
// to make them first-class fusion citizens. This has to
// be in global scope.
BOOST_FUSION_ADAPT_STRUCT(
    client::XVIPoint,
    x,
    y
)

BOOST_FUSION_ADAPT_STRUCT(
    client::XVIGrids,
    distance,
    unit
)

BOOST_FUSION_ADAPT_STRUCT(
    client::XVIStation,
    coordinates,
    name
)

BOOST_FUSION_ADAPT_STRUCT(
    client::XVIStations,
    stations
)

BOOST_FUSION_ADAPT_STRUCT(
    client::XVIShot,
    from,
    to
)

BOOST_FUSION_ADAPT_STRUCT(
    client::XVIShots,
    shots
)

BOOST_FUSION_ADAPT_STRUCT(
    client::XVISketchline,
    color,
    points
)

BOOST_FUSION_ADAPT_STRUCT(
    client::XVISketchlines,
    sketchlines
)

BOOST_FUSION_ADAPT_STRUCT(
    client::XVIGrid,
    bottom,
    x_dist_1,
    y_dist_1,
    x_dist_2,
    y_dist_2,
    qt_x,
    qt_y
)

BOOST_FUSION_ADAPT_STRUCT(
    client::XVIFile,
    m_XVIGrids,
    m_XVIStations,
    m_XVIShots,
    m_XVISketchlines,
    m_XVIGrid
)

namespace client
{
    ///////////////////////////////////////////////////////////////////////////
    //  Print out the XVIFile
    ///////////////////////////////////////////////////////////////////////////
    int const tabsize = 4;

    void tab(int indent)
    {
        for (int i = 0; i < indent; ++i)
            std::cout << ' ';
    }

    struct xvifile_printer
    {
        xvifile_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVIFile const& aXVIFile) const;

        int indent;
    };

    struct xvifile_xvipoint_printer
    {
        xvifile_xvipoint_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVIPoint const& aXVIPoint) const
        {
            if (indent > 0)
                tab(indent + tabsize);
            std::cout << '(' << aXVIPoint.x << ", " << aXVIPoint.y << ')';
            if (indent > 0)
                std::cout << std::endl;
        }

        int indent;
    };

    struct xvifile_xvistation_printer
    {
        xvifile_xvistation_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVIStation const& aXVIStation) const
        {
            tab(indent + tabsize);
            std::cout << "XVIStation '" << aXVIStation.name << "': ";
            xvifile_xvipoint_printer()(aXVIStation.coordinates);
            std::cout << std::endl;
        }

        int indent;
    };

    struct xvifile_xvistations_printer
    {
        xvifile_xvistations_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVIStations const& aXVIStations) const
        {
            tab(indent + tabsize);
            std::cout << "XVIStations:" << std::endl;
            xvistation_vec_t aStations = aXVIStations.stations;
            for (XVIStation aStation: aStations) {
                xvifile_xvistation_printer(indent + (2 * tabsize))(aStation);
            }
        }

        int indent;
    };  

    struct xvifile_xvishot_printer
    {
        xvifile_xvishot_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVIShot const& aXVIShot) const
        {
            tab(indent + tabsize);
            std::cout << "XVIShot:";
            std::cout << " from ";
            xvifile_xvipoint_printer()(aXVIShot.from);
            std::cout << " to ";
            xvifile_xvipoint_printer()(aXVIShot.to);
            std::cout << std::endl;
        }

        int indent;
    }; 

    struct xvifile_xvishots_printer
    {
        xvifile_xvishots_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVIShots const& aXVIShots) const
        {
            tab(indent + tabsize);
            std::cout << "XVIShots:" << std::endl;
            xvishot_vec_t aShots = aXVIShots.shots;
            for (XVIShot aShot: aShots) {
                xvifile_xvishot_printer(indent + (2 * tabsize))(aShot);
            }
        }

        int indent;
    };

    struct xvifile_xvisketchline_printer
    {
        xvifile_xvisketchline_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVISketchline const& aXVISketchline) const
        {
            tab(indent + tabsize);
            std::cout << "XVISketchline: " << aXVISketchline.color << std::endl;
            xvipoint_vec_t aPoints = aXVISketchline.points;
            for (XVIPoint aPoint: aPoints) {
                xvifile_xvipoint_printer(indent + (2 * tabsize))(aPoint);
            }
        }

        int indent;
    }; 

    struct xvifile_xvisketchlines_printer
    {
        xvifile_xvisketchlines_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVISketchlines const& aXVISketchlines) const
        {
            tab(indent + tabsize);
            std::cout << "XVISketchlines: '" << std::endl;
            xvisketchline_vec_t aSketchlines = aXVISketchlines.sketchlines;
            for (XVISketchline aSketchline: aSketchlines) {
                xvifile_xvisketchline_printer(indent + (2 * tabsize))(aSketchline);
            }
        }

        int indent;
    };  

    struct xvifile_xvigrids_printer
    {
        xvifile_xvigrids_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVIGrids const& aXVIGrids) const
        {
            tab(indent + tabsize);
            std::cout << "XVIGrids:" << std::endl;
            tab(indent + (2 * tabsize));
            std::cout << "distance: " << aXVIGrids.distance << std::endl;
            tab(indent + (2 * tabsize));
            std::cout << "unit: " << aXVIGrids.unit << std::endl;
        }

        int indent;
    };   

    struct xvifile_xvigrid_printer
    {
        xvifile_xvigrid_printer(int indent = 0)
          : indent(indent)
        {
        }

        void operator()(XVIGrid const& aXVIGrid) const
        {
            tab(indent + tabsize);
            std::cout << "XVIGrid:" << std::endl;
            tab(indent + (2 * tabsize));
            std::cout << "bottom: ";
            xvifile_xvipoint_printer()(aXVIGrid.bottom);
            std::cout << std::endl;
            tab(indent + (2 * tabsize));
            std::cout << "dists:" << std::endl;
            tab(indent + (3 * tabsize));
            std::cout << "X1: " << aXVIGrid.x_dist_1 << " - Y1: " << aXVIGrid.y_dist_1;
            std::cout << " - X2: " << aXVIGrid.x_dist_2 << " - Y2: " << aXVIGrid.y_dist_2 << std::endl;
            tab(indent + (2 * tabsize));
            std::cout << "quantity: " << aXVIGrid.qt_x << ' ' << aXVIGrid.qt_y << ' ' << std::endl;
        }

        int indent;
    }; 

    void xvifile_printer::operator()(XVIFile const& aXVIFile) const
    {
        tab(indent);
        std::cout << "XVIFile: " << std::endl;

        xvifile_xvigrids_printer(indent).operator()(aXVIFile.m_XVIGrids);
        xvifile_xvistations_printer(indent).operator()(aXVIFile.m_XVIStations);
        xvifile_xvishots_printer(indent).operator()(aXVIFile.m_XVIShots);
        xvifile_xvisketchlines_printer(indent).operator()(aXVIFile.m_XVISketchlines);
        xvifile_xvigrid_printer(indent).operator()(aXVIFile.m_XVIGrid);
    }

    template <typename Iterator>
    struct XVIGrammar : qi::grammar<Iterator, XVIFile(), ascii::space_type>
    {
        XVIGrammar() : XVIGrammar::base_type(xvifile, "xvifile")
        {
            using qi::lit;
            using qi::lexeme;
            using qi::double_;
            using qi::no_case;
            using ascii::char_;
            using ascii::alnum;

            using namespace qi::labels;

            grids = 
                no_case[lit("set")]
                >> no_case[lit("xvigrids")]
                >> '{'
                >> double_
                >> char_
                >> '}'
            ;

            point = 
                double_
                >> double_
            ;

            station_name =
                lexeme[+(alnum | char_(".,;-()[]+"))]
            ;

            station = 
                '{'
                >> point
                >> station_name
                >> '}'
            ;

            stations = 
                no_case[lit("set")]
                >> no_case[lit("xvistations")]
                >> '{'
                >> +(station)
                >> '}'
            ;

            shot = 
                '{'
                >> point
                >> point
                >> '}'
            ;

            shots = 
                no_case[lit("set")]
                >> no_case[lit("xvishots")]
                >> '{'
                >> +(shot)
                >> '}'
            ;

            color_name = 
                lexeme[+(alnum | char_('-'))]
            ;

            sketchline = 
                '{'
                >> color_name
                >> +(point)
                >> '}' 
            ;

            sketchlines = 
                no_case[lit("set")]
                >> no_case[lit("xvisketchlines")]
                >> '{'
                >> +(sketchline)
                >> '}'
            ;

            grid = 
                no_case[lit("set")]
                >> no_case[lit("xvigrid")]
                >> '{'
                >> point
                >> double_
                >> double_
                >> double_
                >> double_
                >> double_
                >> double_
                >> '}'
            ;

            xvifile = 
                (
                    (grids)
                    ^ (stations)
                    ^ (shots)
                    ^ (sketchlines)
                    ^ (grid)
                )
            ;

            xvifile.name("xvifile");
            grids.name("grids");
            station_name.name("station_name");
            point.name("point");
            station.name("station");
            stations.name("stations");
            shot.name("shot");
            shots.name("shots");
            color_name.name("color_name");
            sketchline.name("sketchline");
            sketchlines.name("sketchlines");
            grid.name("grid");
        }

        qi::rule<Iterator, XVIGrids(), ascii::space_type> grids;
        qi::rule<Iterator, XVIPoint(), ascii::space_type> point;
        qi::rule<Iterator, std::string(), ascii::space_type> station_name;
        qi::rule<Iterator, XVIStation(), ascii::space_type> station;
        qi::rule<Iterator, XVIStations(), ascii::space_type> stations;
        qi::rule<Iterator, XVIShot(), ascii::space_type> shot;
        qi::rule<Iterator, XVIShots(), ascii::space_type> shots;
        qi::rule<Iterator, std::string(), ascii::space_type> color_name;
        qi::rule<Iterator, XVISketchline(), ascii::space_type> sketchline;
        qi::rule<Iterator, XVISketchlines(), ascii::space_type> sketchlines;
        qi::rule<Iterator, XVIGrid(), ascii::space_type> grid;
        qi::rule<Iterator, XVIFile(), ascii::space_type> xvifile;
    };
}

///////////////////////////////////////////////////////////////////////////////
//  Main program
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
    char const* filename;
    if (argc > 1)
    {
        filename = argv[1];
    }
    else
    {
        std::cerr << "Error: No input file provided." << std::endl;
        return 1;
    }

    std::ifstream in(filename, std::ios_base::in);

    if (!in)
    {
        std::cerr << "Error: Could not open input file: "
            << filename << std::endl;
        return 1;
    }

    std::string storage; // We will read the contents here.
    in.unsetf(std::ios::skipws); // No white space skipping!
    std::copy(
        std::istream_iterator<char>(in),
        std::istream_iterator<char>(),
        std::back_inserter(storage));

    typedef client::XVIGrammar<std::string::const_iterator> XVIGrammar;
    XVIGrammar xvi; // Our grammar
    client::XVIFile m_XVIFile; // Our tree

    using boost::spirit::ascii::space;
    std::string::const_iterator iter = storage.begin();
    std::string::const_iterator end = storage.end();
    bool r = phrase_parse(iter, end, xvi, space, m_XVIFile);

    if (r && iter == end)
    {
        std::cout << "-------------------------\n";
        std::cout << "Parsing succeeded\n";
        std::cout << "-------------------------\n";
        client::xvifile_printer printer;
        printer(m_XVIFile);
        return 0;
    }
    else
    {
        std::cout << "-------------------------\n";
        std::cout << "Parsing failed\n";
        std::cout << "-------------------------\n";
        return 1;
    }
}



