/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef PARSER_H
#define PARSER_H

#include "../helpers/mapiahenums.h"

#include <QFile>
#include <QRegularExpression>
#include <QSet>
#include <QString>
#include <QTextCodec>

#ifdef BASE_PARSER_DEBUG
#include <QList>
#endif

class Parser
{
    void initializeParser();

public:
    enum WordType {
        Regular,
        Quoted,
        Bracketed,
        Curly,
        Comment,
        QuotedInConstruction,
        BracketedInConstruction,
        CurlyInConstruction,
        Empty,
        UnrecognizedWord
    };

protected:
    struct Brace
    {
        QString beginChar;
        QString endChar;
        QRegularExpression endBraceRegex;
        WordType finalWordType;

        Brace();
        Brace(QString aBeginChar, QString aEndChar, WordType aWordType);
    };

    typedef struct Brace Brace;

    QHash<char, Brace> braces;

    QString m_filename;
    QFile file;

    QString currentLine;
    qint32 currentLineNumber;
    qint32 currentLinePos;
    bool backslashedLine;
    bool eol;
    bool eof;
    QString currentWord;
    QString justPeekWord;
    WordType currentWordType = WordType::Regular;

    MapiahEnums::LineEndingType lineEndingType;
    QString lineEnding;

    QRegularExpression spaceSkipperRegex = QRegularExpression(R"RX(\s*)RX");
    QRegularExpression spaceSplitterRegex = QRegularExpression(R"RX(\s+)RX");
    QRegularExpression wordRegex = QRegularExpression(R"RX((\S+)\s+)RX");
    QRegularExpression justPeekRegex = QRegularExpression(R"RX(\s*(\S+)\s*)RX");
    QRegularExpression quoteRegex = QRegularExpression(R"RX((.+?)("{1,2}))RX");

    QString optionID;

    // Different encodings support
    QByteArray encodedLine;
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");

    void updateEOL();
    bool nextWord();
    bool nextQuotedString();
    bool nextBracedString(const char brace);
    bool nextLine(bool returnEmptyLines = false);
    bool justPeek();

    void parseError(const QString errorMessage, const QString methodName);

    void identifyLineEndingType();

#ifdef BASE_PARSER_DEBUG
    virtual bool internalParse();
public:
    bool parse();
protected:
#else
    virtual bool internalParse() = 0;
    bool parse();
#endif

public:
    Parser(const QString &aFilename);
    Parser();

    const QString &filename() const;
    void setFilename(const QString &newFilename);

#ifdef BASE_PARSER_DEBUG
    struct ParsedWord {
        QString word;
        WordType type;

        ParsedWord(const QString &aWord, const WordType aType) : word(aWord), type(aType) {}
    };

    typedef struct ParsedWord ParsedWord;
    typedef QList<ParsedWord*> listParsedWord_t;

    listParsedWord_t m_parsedWords;
    ParsedWord* parsedWord;
#endif
};

#endif // PARSER_H
