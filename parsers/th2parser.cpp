/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "th2parser.h"

#include "../data/th/thinput.h"

#include "../data/th2/th2area.h"
#include "../data/th2/th2borderlineid.h"
#include "../data/th2/th2comment.h"
#include "../data/th2/th2element.h"
#include "../data/th2/th2file.h"
#include "../data/th2/th2line.h"
#include "../data/th2/th2linepointbezier.h"
#include "../data/th2/th2linepointstraight.h"
#include "../data/th2/th2multilinecomment.h"
#include "../data/th2/th2parent.h"
#include "../data/th2/th2point.h"
#include "../data/th2/th2scrap.h"
#include "../data/th2/th2unrecognized.h"
#include "../data/th2/th2xtherionimagesetting.h"
#include "../data/th2/th2xtherionunknownsetting.h"

#include "../data/th2/option/th2altitudeoption.h"
#include "../data/th2/option/th2authoroption.h"
#include "../data/th2/option/th2contextoption.h"
#include "../data/th2/option/th2copyrightoption.h"
#include "../data/th2/option/th2doubleoption.h"
#include "../data/th2/option/th2extendoption.h"
#include "../data/th2/option/th2freetextoption.h"
#include "../data/th2/option/th2lengthoption.h"
#include "../data/th2/option/th2markoption.h"
#include "../data/th2/option/th2orientationoption.h"
#include "../data/th2/option/th2pointscaleoption.h"
#include "../data/th2/option/th2projectionoption.h"
#include "../data/th2/option/th2scrapscaleoption.h"
#include "../data/th2/option/th2singlesetoption.h"
#include "../data/th2/option/th2sketchoption.h"
#include "../data/th2/option/th2stationnamesoption.h"
#include "../data/th2/option/th2subtypeoption.h"
#include "../data/th2/option/th2valueoption.h"

#include "../helpers/mapiahenums.h"

#include "../factories/th2factory.h"

#include <QList>

#include <typeinfo>

#ifdef TH2_DEBUG
#include <QDebug>
#endif

const QString TH2Parser::XTHERION_SETTING_ID = QStringLiteral("##XTHERION##");
const QString TH2Parser::XTHERION_IMAGE_SETTING_ID = QStringLiteral("xth_me_image_insert");

const QRegularExpression TH2Parser::altitudeValueRegex = QRegularExpression(R"RX(
^
    (?<fix>fix(?:\s+))?
    (?<number>(?:[-+]?\d+(?:\.(?:\d+))?)|\.|-|nan)
    (?:\s*(?<unit>[a-zA-Z]+))?
$
)RX",
    QRegularExpression::ExtendedPatternSyntaxOption
    | QRegularExpression::CaseInsensitiveOption);

const QRegularExpression TH2Parser::degreeSpecialNotationRegex = QRegularExpression(R"RX(
^
    (?<degrees>[-+]?\d+)
        :
    (?<minutes>\d+)
    (?::(?<seconds>\d+(?:\.(?:\d+))?))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QRegularExpression TH2Parser::dimensionsValueRegex = QRegularExpression(R"RX(
^
    (?<above>\d+(?:\.(?:\d+))?)
        \s+
    (?<below>\d+(?:\.(?:\d+))?)
    (?:\s*(?<unit>[a-zA-Z]+))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QRegularExpression TH2Parser::heightValueRegex = QRegularExpression(R"RX(
^
    (?<sign>[+\-])?
    (?<number>-?\d+(?:\.(?:\d+))?)
        \s*
    (?<presumed>\?)?
    (?:\s*(?<unit>[a-zA-Z]+))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QRegularExpression TH2Parser::isNaNRegex = QRegularExpression(R"RX(
^
    \.|-|nan
$
)RX",
    QRegularExpression::ExtendedPatternSyntaxOption
    | QRegularExpression::CaseInsensitiveOption);

const QRegularExpression TH2Parser::measurementWithOptionalUnitRegex = QRegularExpression(R"RX(
^
    (?<number>[-+]?\d+(?:\.(?:\d+))?)
    (?:\s*(?<unit>[a-zA-Z]+))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QRegularExpression TH2Parser::passageHeightDoubleValueRegex = QRegularExpression(R"RX(
^
    \+(?<above>\d+(?:\.(?:\d+))?)
        \s+
    -(?<below>\d+(?:\.(?:\d+))?)
    (?:\s*(?<unit>[a-zA-Z]+))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QRegularExpression TH2Parser::passageHeightSingleValueRegex = QRegularExpression(R"RX(
^
    (?<sign>[+-])?(?<number>\d+(?:\.(?:\d+))?)
    (?:\s*(?<unit>[a-zA-Z]+))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QRegularExpression TH2Parser::projectionElevationRegex = QRegularExpression(R"RX(
^
    elevation(?::(?<index>[a-zA-Z0-9_/][a-zA-Z0-9_/-]*))?
        \s+
    (?<number>[-+]?\d+(?:\.(?:\d+))?)
    (?:\s+(?<unit>[a-zA-Z]+))?
$
)RX",
    QRegularExpression::ExtendedPatternSyntaxOption
    | QRegularExpression::CaseInsensitiveOption);

const QRegularExpression TH2Parser::projectionGeneralRegex = QRegularExpression(R"RX(
^
    (?<type>[a-zA-Z]+)
    (?::(?<index>[a-zA-Z0-9_/][a-zA-Z0-9_/-]*))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QRegularExpression TH2Parser::scrapScaleDrawingUnitSizeRegex = QRegularExpression(R"RX(
^
    (?<drawingUnitSize>[-+]?\d+(?:\.(?:\d+))?)
    (?:\s*(?<unit>[a-zA-Z]+))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QRegularExpression TH2Parser::scrapScaleAmountDrawingUnitsRegex = QRegularExpression(R"RX(
^
    (?<amountOfDrawingUnits>[-+]?\d+(?:\.(?:\d+))?)
        \s+
    (?<drawingUnitSize>[-+]?\d+(?:\.(?:\d+))?)
    (?:\s*(?<unit>[a-zA-Z]+))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QRegularExpression TH2Parser::scrapScaleCompleteRegex = QRegularExpression(R"RX(
^
    (?<x1scrap>[-+]?\d+(?:\.(?:\d+))?)
        \s+
    (?<y1scrap>[-+]?\d+(?:\.(?:\d+))?)
        \s+
    (?<x2scrap>[-+]?\d+(?:\.(?:\d+))?)
        \s+
    (?<y2scrap>[-+]?\d+(?:\.(?:\d+))?)
        \s+
    (?<x1reality>[-+]?\d+(?:\.(?:\d+))?)
        \s+
    (?<y1reality>[-+]?\d+(?:\.(?:\d+))?)
        \s+
    (?<x2reality>[-+]?\d+(?:\.(?:\d+))?)
        \s+
    (?<y2reality>[-+]?\d+(?:\.(?:\d+))?)
        \s*
    (?:\s*(?<unit>[a-zA-Z]+))?
$
)RX", QRegularExpression::ExtendedPatternSyntaxOption);

const QString TH2Parser::COMMENT_PREFIX = QStringLiteral("#");
const QString TH2Parser::OPTION_NAME_PREFIX = QStringLiteral("-");

const QString TH2Parser::DEFAULT_ENCODING = QStringLiteral("utf-8");

const QString TH2Parser::DEGREES_REGEX_NAME = QStringLiteral("degrees");
const QString TH2Parser::MINUTES_REGEX_NAME = QStringLiteral("minutes");
const QString TH2Parser::NUMBER_REGEX_NAME = QStringLiteral("number");
const QString TH2Parser::SECONDS_REGEX_NAME = QStringLiteral("seconds");
const QString TH2Parser::UNIT_REGEX_NAME = QStringLiteral("unit");

const QString TH2Parser::ABOVE_HEIGHT_SIGN = QStringLiteral("+");
const QString TH2Parser::BELOW_DEPTH_SIGN = QStringLiteral("-");

th2file_up TH2Parser::parseTH2()
{
    m_parsingSucceeded = parse();

    return std::move(u_th2File);
}

void TH2Parser::setFilename(const QString &newFilename)
{
    if (m_filename == newFilename)
    {
        return;
    }

    u_th2File = std::make_unique<TH2File>();
    m_th2File = u_th2File.get();
    m_th2File->setFilename(newFilename);
    m_filename = newFilename;
}

TH2Parser::ParseAllowedWordResult TH2Parser::parseWordGeneral()
{
    if (currentWordType == WordType::Comment)
    {
        return parseCommentWordType() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }

    const QString command = currentWord.toLower();

    if (command == AREA_ELEMENT_TYPE)
    {
        // There is no 'command == "endarea"' here because parseAreaContents()
        // already consumes the 'endarea' line;
        return parseArea() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == COMMENT_ELEMENT_TYPE)
    {
        return parseMultiLineComment() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == ENCODING_ELEMENT_TYPE)
    {
        return parseEncoding() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == ENDSCRAP_KEYWORD)
    {
        return ParseAllowedWordResult::SectionEnd;
    }
    else if (command == INPUT_ELEMENT_TYPE)
    {
        return parseInput() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == LINE_ELEMENT_TYPE)
    {
        // There is no 'command == "endline"' here because parseLineContents()
        // already consumes the 'endline' line;
        return parseLine() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == POINT_ELEMENT_TYPE)
    {
        return parsePoint() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == SCRAP_ELEMENT_TYPE)
    {
        return parseScrap() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }

    unrecognizedLine(QStringLiteral("Unrecognized command/first word."));

    return ParseAllowedWordResult::Failure;
}

TH2Parser::ParseAllowedWordResult TH2Parser::parseWordScrap()
{
    if (currentWordType == WordType::Comment)
    {
        return parseCommentWordType() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }

    const QString command = currentWord.toLower();

    if (command == AREA_ELEMENT_TYPE)
    {
        // There is no 'command == "endarea"' here because parseAreaContents()
        // already consumes the 'endarea' line;
        return parseArea() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == COMMENT_ELEMENT_TYPE)
    {
        return parseMultiLineComment() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == ENDSCRAP_KEYWORD)
    {
        return ParseAllowedWordResult::SectionEnd;
    }
    else if (command == INPUT_ELEMENT_TYPE)
    {
        return parseInput() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == LINE_ELEMENT_TYPE)
    {
        // There is no 'command == "endline"' here because parseLineContents()
        // already consumes the 'endline' line;
        return parseLine() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }
    else if (command == POINT_ELEMENT_TYPE)
    {
        return parsePoint() ? ParseAllowedWordResult::Success : ParseAllowedWordResult::Failure;
    }

    unrecognizedLine(QStringLiteral("Unrecognized command/first word."));
    return ParseAllowedWordResult::Failure;
}

bool TH2Parser::setOptionValuePerType(TH2AltitudeOption *newAltitudeOption)
{
    QRegularExpressionMatch match = altitudeValueRegex.match(currentWord);

    if (!match.hasMatch())
    {
        return false;
    }

    newAltitudeOption->setIsFix(!match.captured("fix").isEmpty());

    if (isNaN(match.captured("number")))
    {
        if (!newAltitudeOption->setLengthNumericValue(0))
        {
            return false;
        }
    }
    else if (!newAltitudeOption->setLengthNumericValueByText(match.captured("number")))
    {
        return false;
    }

    if (!match.captured("unit").isEmpty())
    {
        if (!newAltitudeOption->setLengthUnitByText(match.captured("unit")))
        {
            return false;
        }
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2AuthorOption *newAuthorOption)
{
    // Date
    if (!setDatetimeValue(&(newAuthorOption->date())))
    {
        return false;
    }

    // Person
    if (!setPersonValue(&(newAuthorOption->person())))
    {
        return false;
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2FreeTextOption *newFreeTextOption)
{
    newFreeTextOption->setText(currentWord);

    if (optionID == ID_OPTION_NAME)
    {
        optionIsID = true;
        currentID = newFreeTextOption->textValue();
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2ExtendOption *newExtendOption)
{
    // Extend option without 'previous station' setting.
    if (!justPeek() || justPeekWord.startsWith(OPTION_NAME_PREFIX))
    {
        return true;
    }

    if ((justPeekWord != TH2ExtendOption::PREVIOUS_KEYWORD)
        && (justPeekWord != TH2ExtendOption::PREVIOUS_ALTERNATE_KEYWORD))
    {
        return false;
    }

    // Discarding the extend previous keyword.
    if (!nextWord())
    {
        return false;
    }

    // Expecting a station name.
    if (!nextWord())
    {
        return false;
    }

    newExtendOption->setStationName(currentWord);

    return true;
}

void TH2Parser::unrecognizedLine(const QString reason)
{
    th2element_up newUnrecognizedLine =
        TH2Factory::createUnrecognizableLine(
            currentLine,
            reason,
            currentParent(),
            m_th2File,
            currentLineNumber);

    m_th2File->appendUnrecognizedLine(std::move(newUnrecognizedLine));

    parseError(reason, QStringLiteral("TH2Parser::unrecognizedLine"));
}

bool TH2Parser::parseEndOfLine()
{
    if (eol)
    {
        return true;
    }

    auto startingLinePos = currentLinePos;
    while (nextWord())
    {
        if (currentWordType == WordType::Comment)
        {
            return parseComment(true);
        }
        else
        {
            parseError(
                QString("Unsupported word type %1 at end of line. Expecting %2 (WordType::Comment). Started parsing at line pos %3.")
                        .arg(currentWordType)
                        .arg(WordType::Comment)
                        .arg(startingLinePos),
                QStringLiteral("TH2Parser::parseEndOfLine")
            );
            return false;
        }
    }

    return true;
}

void TH2Parser::appendLinePointOptions(TH2LinePoint *aLinePoint)
{
    auto rit = nextLinePointOptions.begin();

    for (; rit != nextLinePointOptions.end(); ++rit) {
        aLinePoint->appendOption(std::move(*rit));
    }

    nextLinePointOptions.clear();
}

bool TH2Parser::isNaN(const QString &valueText)
{
    QRegularExpressionMatch match = isNaNRegex.match(valueText);

    return match.hasMatch();
}

bool TH2Parser::createLinePointStraight()
{
    th2element_up uniqueLinePoint =
        TH2Factory::createLinePointStraight(
            QPointF(linePointCoordinates.value(0), linePointCoordinates.value(1)),
            currentParent(),
            m_th2File,
            currentLineNumber);

    return appendLinePoint(std::move(uniqueLinePoint));
}

bool TH2Parser::createLinePointBezier()
{
    th2element_up uniqueLinePoint =
        TH2Factory::createLinePointBezier(
            QPointF(linePointCoordinates.value(0), linePointCoordinates.value(1)),
            QPointF(linePointCoordinates.value(2), linePointCoordinates.value(3)),
            QPointF(linePointCoordinates.value(4), linePointCoordinates.value(5)),
            currentParent(),
            m_th2File,
            currentLineNumber);

    return appendLinePoint(std::move(uniqueLinePoint));
}

bool TH2Parser::appendLinePoint(th2element_up uniqueElement)
{
    newLinePoint = dynamic_cast<TH2LinePoint *>(uniqueElement.get());

    if (newLinePoint == nullptr)
    {
        parseError(
            QStringLiteral("dynamic_cast<TH2LinePoint *>(uniqueLinePoint.get()) failed."),
            QStringLiteral("TH2Parser::createLinePointStraight"));
        return false;
    }

    newLine->appendElement(std::move(uniqueElement));

    appendLinePointOptions(newLinePoint);

    if (!parseEndOfLine())
    {
        parseError(
            QStringLiteral("Failure parsing end of line of TH2LinePointStraight."),
            QStringLiteral("TH2Parser::createLinePointStraight"));
        return false;
    }

    return true;
}

/**
 * @brief TH2Parser::joinLinesToAreas
 *
 * This step is done at the end of the file reading process so we are certain that all lines
 * already exist as an area can be defined in a file before it's border line.
 *
 */
void TH2Parser::joinLinesToAreas()
{
    QMultiHash<QString, QString>::const_iterator i = linesXareas.constBegin();
    TH2Line *aLine;
    TH2Area *aArea;

    while (i != linesXareas.constEnd()) {
        if (m_th2File->existsByID(i.key()))
        {
            aLine = m_th2File->lineByID(i.key());
            if (aLine == nullptr) {
                m_th2File->addMissingLineOfArea(i.key(), i.value());
                QString message = QString("Failed getting line with ID '%1' for areas '%2'.")
                        .arg(i.key(), i.value());
                parseError(message, QStringLiteral("TH2Parser::joinLinesToAreas()"));
            }
        }
        else
        {
            m_th2File->addMissingLineOfArea(i.key(), i.value());
            aLine = nullptr;
            QString message = QString("No line with ID '%1' for area '%2'.")
                    .arg(i.key(), i.value());
            parseError(message, QStringLiteral("TH2Parser::joinLinesToAreas()"));
        }

        if (m_th2File->exists(i.value()))
        {
            aArea = m_th2File->area(i.value());
            if (aArea == nullptr)
            {
                m_th2File->addMissingLineOfArea(i.key(), i.value());
                QString message = QString("Can't get area '%1' which supposely uses line ID '%2'.")
                        .arg(i.value(), i.key());
                parseError(message, QStringLiteral("TH2Parser::joinLinesToAreas()"));
            }
        }
        else
        {
            m_th2File->addMissingLineOfArea(i.key(), i.value());
            aArea = nullptr;
            QString message = QString("No area '%1' which supposely uses line ID '%2'.")
                    .arg(i.value(), i.key());
            parseError(message, QStringLiteral("TH2Parser::joinLinesToAreas()"));
        }

        if ((aLine != nullptr) && (aArea != nullptr))
        {
            aLine->setBorderToArea(aArea);
        }

        ++i;
    }
}

// Method responsable for parsing the multiline content of a TH2 line.
bool TH2Parser::parseLineLine()
{
    // Comment
    QString trimmedCurrentLine = currentLine.trimmed();
    if (trimmedCurrentLine.startsWith(COMMENT_PREFIX))
    {
        nextWord();
        return parseComment();
    }
    else if (trimmedCurrentLine == COMMENT_ELEMENT_TYPE)
    {
        return parseMultiLineComment();
    }

    bool doubleRecognized;
    qreal newDouble;

    linePointCoordinates.clear();
    linePointOriginalRepresentations.clear();

    while (true)
    {
        if (!nextWord())
        {
            break;
        }

        newDouble = currentWord.toDouble(&doubleRecognized);
        if (!doubleRecognized)
        {
            break;
        }

        linePointCoordinates.append(newDouble);
        linePointOriginalRepresentations.append(currentWord);
    }

    auto countCoordinates = linePointCoordinates.count();

    if (countCoordinates > 0)
    {
        if (countCoordinates == 2)
        {
            createLinePointStraight();
        }
        else if (countCoordinates == 6)
        {
            createLinePointBezier();
        }
        else
        {
            QString problem = QString("Line point with unknown number of coordinates: $1.")
                .arg(countCoordinates);
//            unrecognizedLine(problem);
            parseError(problem, QStringLiteral("TH2Parser::parseLineLine"));
            return false;
        }
    }
    else
    {
        optionID = currentWord;
        return parseLinePointOption();
    }

    return true;
}

bool TH2Parser::parseAreaLine()
{
    // Comment
    if (currentLine.startsWith(COMMENT_PREFIX))
    {
        return parseComment();
    }
    else if (currentLine.trimmed() == COMMENT_ELEMENT_TYPE)
    {
        return parseMultiLineComment();
    }

    if (!nextWord())
    {
        parseError(
                    QStringLiteral("Can't get line ID."),
                    QStringLiteral("TH2Parser::parseAreaLine"));
        return false;
    }

    th2element_up aBorderLineID =
        TH2Factory::createTH2BorderLineID(
            currentWord,
            currentParent(),
            m_th2File,
            currentLineNumber);
    newArea->appendElement(std::move(aBorderLineID));
    linesXareas.insert(currentWord, newArea->cod());

    return true;
}

bool TH2Parser::parseLineContents()
{
    while (!eof)
    {
        if (eol)
        {
            nextLine();
            if (backslashedLine && eol)
            {
                backslashedLine = false;
            }
            continue;
        }

        if (!justPeek())
        {
            parseError(
                QStringLiteral("'justPeek' failed."),
                QStringLiteral("TH2Parser::parseLineContents"));
            return false;
        }
        if (justPeekWord.toLower() == QStringLiteral("endline"))
        {
            // Actually consume the "endline" word.
            nextWord();

            if (!nextLinePointOptions.empty())
            {
//                QString message = QString("TH2Line ending in line %1 has %2 dangling inside line option(s) that were probably created by a bug in XTherion. TH2Parser is discarding them.")
//                    .arg(currentLineNumber)
//                    .arg(nextLinePointOptions.size());
//                parseError(message, QStringLiteral("TH2Parser::parseLineContents()"));
                nextLinePointOptions.clear();
            }

            return parseEndOfLine();
        }

        if (!parseLineLine())
        {
            parseError(
                QStringLiteral("parseLineLine failed."),
                QStringLiteral("TH2Parser::parseLineContents"));
            return false;
        }
    }

    return true;
}

bool TH2Parser::parseAreaContents()
{
    while(!eof)
    {
        if (eol)
        {
            nextLine();
            if (backslashedLine && eol)
            {
                backslashedLine = false;
            }
            continue;
        }

        if (!justPeek())
        {
            parseError(
                        QStringLiteral("'justPeek' failed."),
                        QStringLiteral("TH2Parser::parseAreaContents"));
            return false;
        }
        if (justPeekWord.toLower() == QStringLiteral("endarea"))
        {
            // Actually consume the "endarea" word.
            nextWord();
            return parseEndOfLine();
        }

        if (!parseAreaLine())
        {
            parseError(
                        QStringLiteral("parseAreaLine failed."),
                        QStringLiteral("TH2Parser::parseAreaContents"));
            return false;
        }
    }

    return true;
}

bool TH2Parser::parseEncoding()
{
    if (!nextWord())
    {
        unrecognizedLine(QStringLiteral("Encoding without encoding ID."));
        return false;
    }

    m_th2File->setEncoding(currentWord);
    parseEndOfLine();

    if (m_th2File->encoding() == DEFAULT_ENCODING)
    {
        return true;
    }

    QTextCodec *newCodec = QTextCodec::codecForName(currentWord.toLocal8Bit());
    if (newCodec == 0)
    {
        parseError(
            QString("Can't create new codec for encoding '%1'. Will continue using UTF-8 encoding.")
                .arg(currentWord),
            QStringLiteral("TH2Parser::parseEncoding"));
        return false;
    }

    codec = newCodec;

    return true;
}

bool TH2Parser::parseCommentWordType()
{
    if (currentWord.toUpper().startsWith(XTHERION_SETTING_ID))
    {
        return parseXTherionSetting();
    }
    else
    {
        return parseComment();
    }
}

bool TH2Parser::parseComment(const bool aIsPreviousLineEndComment)
{
    th2element_up uniqueComment =
        TH2Factory::createComment(
            currentWord,
            aIsPreviousLineEndComment,
            currentParent(),
            m_th2File,
            currentLineNumber);

    appendElement(std::move(uniqueComment));

    return true;
}

bool TH2Parser::parseXTherionSetting()
{
    // Rewinding current position as the line content will be parsed as a
    // XTherion setting and not as a simple comment.
    currentLinePos = XTHERION_SETTING_ID.size();
    eol = false;

    if (!nextWord() || (currentWordType != WordType::Regular))
    {
        parseError(
            QStringLiteral("Unsupported XTherion setting ID."),
            QStringLiteral("TH2Parser::parseXTherionSetting()"));
        return false;
    }

    xtherionSetting = currentWord.toLower();
    xtherionParameters = currentLine.sliced(currentLinePos).trimmed();

    if (xtherionSetting == XTHERION_IMAGE_SETTING_ID)
    {
        return parseXTherionImageSetting();
    }
    else
    {
        // Parsing of this line is finished.
        eol = true;

        th2element_up newXTherionUnknownSetting =
            TH2Factory::createXTherionUnknownSetting(
                xtherionSetting,
                xtherionParameters,
                currentParent(),
                m_th2File,
                currentLineNumber);

        appendElement(std::move(newXTherionUnknownSetting));

        return true;
    }
}

bool TH2Parser::parseXTherionImageSetting()
{
    th2element_up uniqueXTherionImageSetting =
        TH2Factory::createTH2XTherionImageSetting(
            xtherionParameters,
            currentParent(),
            m_th2File,
            currentLineNumber);

    TH2XTherionImageSetting *newXTherionImageSetting =
        (TH2XTherionImageSetting *)(uniqueXTherionImageSetting.get());

    // xx parameters
    if (!nextWord() || (currentWordType != WordType::Curly))
    {
        parseError(
            QStringLiteral("Parse of XX parameters failed."),
            QStringLiteral("TH2Parser::parseXTherionImageSetting"));
        return false;
    }
    QStringList parameters = currentWord.split(spaceSplitterRegex, Qt::SkipEmptyParts);
    qint32 parametersCount = parameters.count();

    if (parametersCount > 0)
    {
        newXTherionImageSetting->setXX(parameters.at(0).toDouble());
    }
    else
    {
        newXTherionImageSetting->setXX(0);
    }

    if (parametersCount > 1)
    {
        newXTherionImageSetting->setVsb(parameters.at(1).toInt());
    }
    else
    {
        newXTherionImageSetting->setVsb(1);
    }

    if (parametersCount > 2)
    {
        newXTherionImageSetting->setIgamma(parameters.at(2).toDouble());
    }
    else
    {
        newXTherionImageSetting->setIgamma(1.0);
    }

    // yy parameters
    if (!nextWord() || (currentWordType != WordType::Curly))
    {
        parseError(
                    QStringLiteral("Parse of YY parameters failed."),
                    QStringLiteral("TH2Parser::parseXTherionImageSetting"));
        return false;
    }
    parameters = currentWord.split(spaceSplitterRegex, Qt::SkipEmptyParts);
    parametersCount = parameters.count();

    if (parametersCount > 0)
    {
        newXTherionImageSetting->setYY(parameters.at(0).toDouble());
    }
    else
    {
        newXTherionImageSetting->setYY(0);
    }

    if (parametersCount > 1)
    {
        newXTherionImageSetting->setXVIroot(parameters.at(1));
    }
    else
    {
        newXTherionImageSetting->setXVIroot(QStringLiteral(""));
    }

    newXTherionImageSetting->setIsXVI(false);

    // fname
    if (!nextWord() || ((currentWordType != WordType::Regular) && (currentWordType != WordType::Quoted)))
    {
        parseError(
            QStringLiteral("Parse of fname parameter failed."),
            QStringLiteral("TH2Parser::parseXTherionImageSetting"));
        return false;
    }

    newXTherionImageSetting->setFname(currentWord);

    // iidx parameters
    if (!nextWord() || (currentWordType != WordType::Regular))
    {
        parseError(
            QStringLiteral("Parse of iidx parameter failed."),
            QStringLiteral("TH2Parser::parseXTherionImageSetting"));
        return false;
    }

    newXTherionImageSetting->setIidx(currentWord.toInt());

    // imgx parameters
    if (!nextWord() || (currentWordType != WordType::Curly))
    {
        parseError(
                    QStringLiteral("Parse of imgx parameter failed."),
                    QStringLiteral("TH2Parser::parseXTherionImageSetting"));
        return false;
    }
    parameters = currentWord.split(spaceSplitterRegex, Qt::SkipEmptyParts);
    parametersCount = parameters.count();

    if (parametersCount > 0)
    {
        newXTherionImageSetting->setImgx(parameters.at(0));
    }
    else
    {
        newXTherionImageSetting->setImgx(QStringLiteral(""));
    }

    if (parametersCount > 1)
    {
        newXTherionImageSetting->setXdata(parameters.at(1));
        newXTherionImageSetting->setXimage(true);
    }
    else
    {
        newXTherionImageSetting->setImgx(QStringLiteral(""));
        newXTherionImageSetting->setXimage(false);
    }

    appendElement(std::move(uniqueXTherionImageSetting));

    return true;
}

bool TH2Parser::parseScrap()
{
    if (!nextWord())
    {
        unrecognizedLine(QStringLiteral("Scrap without ID."));
        return false;
    }

    th2element_up uniqueScrap =
        TH2Factory::createTH2Scrap(
            currentWord,
            currentParent(),
            m_th2File,
            currentLineNumber);

    newScrap = dynamic_cast<TH2Scrap *>(uniqueScrap.get());

    if (newScrap == nullptr)
    {
        parseError(
            QStringLiteral("dynamic_cast<TH2Scrap *>(uniqueScrap.get()) failed."),
            QStringLiteral("TH2Parser::parseScrap"));
        return false;
    }

    appendElement(std::move(uniqueScrap));

    m_th2File->registerID(newScrap->id(), newScrap->cod());

    addNewParent(newScrap);
    if (!optionsInLine(std::bind(&TH2Parser::parseScrapOption, this)))
    {
        parseError(
            QStringLiteral("Failure parsing options inline of scrap."),
            QStringLiteral("TH2Parser::parseScrap"));
        removeCurrentParent();
        return false;
    }

    if (!parseLines(std::bind(&TH2Parser::parseWordScrap, this)))
    {
        parseError(
            QStringLiteral("Failure parsing inner content of scrap."),
            QStringLiteral("TH2Parser::parseScrap"));
        removeCurrentParent();
        return false;
    }

    removeCurrentParent();
    return true;
}

bool TH2Parser::parseInput()
{
    if (!nextWord())
    {
        parseError(
            QStringLiteral("Failure parsing input."),
            QStringLiteral("TH2Parser::parseInput"));
        return false;
    }

    th2element_up uniqueInput =
        TH2Factory::createTHInput(
            currentParent(),
            m_th2File,
            currentLineNumber);


    auto newInput = dynamic_cast<THInput *>(uniqueInput.get());

    if (newInput == nullptr)
    {
        parseError(
            QStringLiteral("dynamic_cast<THInput *> failed."),
            QStringLiteral("TH2Parser::parseInput"));
        return false;
    }

    appendElement(std::move(uniqueInput));

    newInput->setFilenameValue(currentWord);

    return true;
}

bool TH2Parser::parsePoint()
{
    bool result;
    bool conversionSucceded;
    TH2DoubleValueNumberType x;
    TH2DoubleValueNumberType y;
    QString xOriginal;
    QString yOriginal;

    result = nextWord();
    x = currentWord.toDouble(&conversionSucceded);
    xOriginal = currentWord;
    if (!result || !conversionSucceded)
    {
        unrecognizedLine(QStringLiteral("Unrecognized x coordinate of point."));
        return false;
    }

    result = nextWord();
    y = currentWord.toDouble(&conversionSucceded);
    yOriginal = currentWord;
    if (!result || !conversionSucceded)
    {
        unrecognizedLine(QStringLiteral("Unrecognized y coordinate of point."));
        return false;
    }

    if (!nextWord())
    {
        unrecognizedLine(QStringLiteral("Unrecognized point type."));
        return false;
    }

    bool typeContainsSubtype = currentWord.contains(TH2SubtypeOption::SUBTYPE_SEPARATOR);
    QString aType;
    QString aSubtype;

    if (typeContainsSubtype)
    {
        auto subtypeSeparatorPos = currentWord.indexOf(TH2SubtypeOption::SUBTYPE_SEPARATOR);
        aSubtype = currentWord.sliced(subtypeSeparatorPos + 1);
        aType = currentWord.left(subtypeSeparatorPos);
    }
    else
    {
       aType = currentWord;
    }

    if (!TH2Point::isSupportedPointType(aType))
    {
        unrecognizedLine(QStringLiteral("Unsupported point type."));
        return false;
    }

    th2element_up uniquePoint =
        TH2Factory::createPoint(
            currentWord,
            QPointF(x, y),
            currentParent(),
            m_th2File,
            currentLineNumber);

    newPoint = dynamic_cast<TH2Point *>(uniquePoint.get());

    appendElement(std::move(uniquePoint));
    addNewParent(newPoint);

    if (typeContainsSubtype)
    {
        if (aSubtype.isEmpty())
        {
            parseError(
                QStringLiteral("Empty subtype."),
                QStringLiteral("TH2Parser::parsePoint"));
            return false;
        }

        if (!TH2Point::isSupportedOptionType(
            POINT_ELEMENT_TYPE,
            newPoint->type(),
            SUBTYPE_OPTION_NAME))
        {
            parseError(
                QString("Unsupported subtype %1 for point type %2.")
                    .arg(aSubtype, newPoint->type()),
                QStringLiteral("TH2Parser::parsePoint"));
            return false;
        }

        th2element_up uniqueOption = TH2Factory::createTH2SubtypeOption(
            newPoint->type(),
            newPoint,
            m_th2File,
            currentLineNumber);

        TH2SubtypeOption *newSubtypeOption =
            dynamic_cast<TH2SubtypeOption *>(uniqueOption.get());

        if (newSubtypeOption == nullptr)
        {
            parseError(
                QStringLiteral("dynamic_cast<TH2SubtypeOption *>(uniqueOption.get()) failed."),
                QStringLiteral("TH2Parser::parsePoint"));
            return false;
        }

        if (!newSubtypeOption->setSubtypeByText(aSubtype))
        {
            parseError(
                QStringLiteral("Setting subtype option failed."),
                QStringLiteral("TH2Parser::parsePoint"));
            return false;
        }
        newSubtypeOption->setIsSubtypeInline(true);

        newPoint->appendOption(std::move(uniqueOption));
    }

    if (!optionsInLine(std::bind(&TH2Parser::parsePointOption, this)))
    {
        parseError(
            QStringLiteral("Failure parsing options inline of point."),
            QStringLiteral("TH2Parser::parsePoint"));
        removeCurrentParent();
        return false;
    }

    removeCurrentParent();
    return true;
}

bool TH2Parser::parseArea()
{
    if (!nextWord())
    {
        unrecognizedLine(QStringLiteral("Area without type."));
        return false;
    }

    if (!TH2Area::isSupportedType(currentWord))
    {
        unrecognizedLine(QStringLiteral("Unsupported area type."));
        return false;
    }

    th2element_up uniqueArea =
        TH2Factory::createTH2Area(
            currentWord,
            currentParent(),
            m_th2File,
            currentLineNumber);

    newArea = dynamic_cast<TH2Area *>(uniqueArea.get());

    if (newArea == nullptr)
    {
        parseError(
            QStringLiteral("Failure creating new TH2Area."),
            QStringLiteral("TH2Parser::parseArea"));
        return false;
    }
    newArea->setType(currentWord);

    appendElement(std::move(uniqueArea));
    addNewParent(newArea);

    if (!optionsInLine(std::bind(&TH2Parser::parseAreaOption, this)))
    {
        parseError(
            QStringLiteral("Failure parsing area inline options."),
            QStringLiteral("TH2Parser::parseArea"));
        removeCurrentParent();
        return false;
    }

    if (!parseAreaContents())
    {
        parseError(
            QStringLiteral("Failure parsing contents of area."),
            QStringLiteral("TH2Parser::parseArea"));
        removeCurrentParent();
        return false;
    }

    removeCurrentParent();

    return true;
}

bool TH2Parser::parseLine()
{
    if (!nextWord())
    {
        unrecognizedLine(QStringLiteral("Line without type."));
        return false;
    }

    if (!TH2Line::isSupportedType(currentWord))
    {
        unrecognizedLine(QStringLiteral("Unsupported line type."));
        return false;
    }

    th2element_up uniqueLine =
        TH2Factory::createTH2Line(
            currentWord,
            currentParent(),
            m_th2File,
            currentLineNumber);

    newLine = dynamic_cast<TH2Line *>(uniqueLine.get());

    if (newLine == nullptr)
    {
        parseError(
            QStringLiteral("Failure creating new TH2Line."),
            QStringLiteral("TH2Parser::parseLine"));
        return false;
    }

    appendElement(std::move(uniqueLine));
    addNewParent(newLine);
    if (!optionsInLine(std::bind(&TH2Parser::parseLineOption, this)))
    {
        parseError(
            QStringLiteral("Failure parsing options inline of line."),
            QStringLiteral("TH2Parser::parseLine"));
        removeCurrentParent();
        return false;
    }

    if (!parseLineContents())
    {
        parseError(
            QStringLiteral("Failure parsing contents of line."),
            QStringLiteral("TH2Parser::parseLine"));
        removeCurrentParent();
        return false;
    }

    removeCurrentParent();
    return true;
}

TH2Parser::optionsInLineParseFuncReturnType TH2Parser::parseScrapOption()
{
    return parseOption(SCRAP_ELEMENT_TYPE, newScrap->type(), newScrap);
}

TH2Parser::optionsInLineParseFuncReturnType TH2Parser::parseLineOption()
{
    return parseOption(LINE_ELEMENT_TYPE, newLine->type(), newLine);
}

TH2Parser::optionsInLineParseFuncReturnType TH2Parser::parseOption(
    const QString &aElementType,
    const QString &aSymbolType,
    TH2OptionList *aOptionList)
{
//    if (!isSupportedOption(aElementType, aSymbolType, optionID))
//    {
//        return false;
//    }

    th2element_up uniqueOption =
        TH2Factory::createOption(
            aElementType,
            aSymbolType,
            optionID,
            currentParent(),
            m_th2File,
            currentLineNumber);

    currentOption = dynamic_cast<TH2Option *>(uniqueOption.get());
    optionIsID = false;

    if (currentOption == nullptr)
    {
        parseError(
            QStringLiteral("Failure casting uniqueOption.get to <TH2Option *>."),
            QStringLiteral("TH2Parser::parseOption()"));
        return false;
    }

    currentElementType = currentOption->elementType();

    bool setValueResult;

    if (currentElementType == ALTITUDE_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2AltitudeOption *>();
    }
    else if (currentElementType == AUTHOR_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2AuthorOption *>();
    }
    else if (currentElementType == CONTEXT_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2ContextOption *>();
    }
    else if (currentElementType == COPYRIGHT_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2CopyrightOption *>();
    }
    else if (currentElementType == DOUBLE_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2DoubleOption *>();
    }
    else if (currentElementType == EXTEND_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2ExtendOption *>(FetchNextWord::No);
    }
    else if (currentElementType == LENGTH_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2LengthOption *>();
    }
    else if (currentElementType == MARK_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2MarkOption *>();
    }
    else if (currentElementType == ORIENTATION_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2OrientationOption *>();
    }
    else if (currentElementType == POINT_SCALE_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2PointScaleOption *>();
    }
    else if (currentElementType == PROJECTION_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2ProjectionOption *>();
    }
    else if (currentElementType == SCRAP_SCALE_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2ScrapScaleOption *>();
    }
    else if (currentElementType == SINGLE_SET_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2SingleSetOption *>(FetchNextWord::No);
    }
    else if (currentElementType == SKETCH_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2SketchOption *>();
    }
    else if (currentElementType == STATION_NAMES_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2StationNamesOption *>();
    }
    else if (currentElementType == SUBTYPE_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2SubtypeOption *>();
    }
    else if (currentElementType == VALUE_OPTION_ELEMENT_TYPE)
    {
        setValueResult = setOptionValue<TH2ValueOption *>();
    }
    else
    {
        setValueResult = setOptionValue<TH2FreeTextOption *>();
    }

    if (!setValueResult)
    {
        parseError(
            QStringLiteral("Failure setting option %1 value.").arg(currentOption->name()),
            QStringLiteral("TH2Parser::parseOption()"));
        return false;
    }

    if (aElementType == LINEPOINT_ELEMENT_TYPE)
    {
        nextLinePointOptions.push_back(std::move(uniqueOption));
    }
    else
    {
        aOptionList->appendOption(std::move(uniqueOption));
        if (optionIsID)
        {
            m_th2File->registerID(currentID, currentOption->parent()->cod());
        }
    }

    return true;
}

TH2Parser::optionsInLineParseFuncReturnType TH2Parser::parseAreaOption()
{
    return parseOption(AREA_ELEMENT_TYPE, newArea->type(), newArea);
}

bool TH2Parser::parseMultiLineComment()
{
    QString content;

    while (!eof && nextLine(true))
    {
        if (currentLine.trimmed() == QStringLiteral("endcomment"))
        {
            // Consuming the 'endcomment' word.
            nextWord();

            th2element_up uniqueMultiLineComment =
                TH2Factory::createTH2MultiLineComment(
                    content,
                    currentParent(),
                    m_th2File,
                    currentLineNumber);

            TH2MultiLineComment *newComment =
                dynamic_cast<TH2MultiLineComment *>(uniqueMultiLineComment.get());

            if (newComment == nullptr)
            {
                parseError(
                    QStringLiteral("Failure casting uniqueMultiLineComment.get to <TH2MultiLineComment *>."),
                    QStringLiteral("TH2Parser::parseMultiLineComment()"));
                return false;
            }

            appendElement(std::move(uniqueMultiLineComment));

            return true;
        }
        content.append(currentLine + lineEnding);
    }

    return false;
}

bool TH2Parser::optionsInLine(optionsInLineParseFuncType symbolParseOptionFunc)
{
    while (!eol)
    {
        // This must be a -option
        if (!nextWord()
                || (currentWordType != WordType::Regular)
                || (currentWord.length() < 2)
                || !currentWord.startsWith(OPTION_NAME_PREFIX))
        {
            parseError(
                QStringLiteral("Expecting a regular word starting with a '-'."),
                QStringLiteral("TH2Parser::optionsInLine"));
            return false;
        }

        optionID = currentWord.mid(1);

        if(!symbolParseOptionFunc())
        {
            parseError(
                QStringLiteral("Error parsing option inline."),
                QStringLiteral("TH2Parser::optionsInLine"));
            return false;
        }
    }

    if (!parseEndOfLine())
    {
        parseError(
            QStringLiteral("Error parsing end of line."),
            QStringLiteral("TH2Parser::optionsInLine"));
        return false;
    }

    return true;
}

bool TH2Parser::parseLines(parseLinesParseAllowedWordsFuncType parseAllowedWords)
{
    if (!nextLine())
    {
        return false;
    }

    while (!eof)
    {
        while (!eol)
        {
            if (!nextWord())
            {
                return false;
            }
            switch(parseAllowedWords())
            {
            case ParseAllowedWordResult::Failure:
                parseError(
                    QStringLiteral("Failure parsing word."),
                    QStringLiteral("TH2Parser::parseLines"));
                return false;
            case ParseAllowedWordResult::SectionEnd:
                return true;
            case ParseAllowedWordResult::Success:
                break;
            }
        }
#ifdef TH2_DEBUG
//        qInfo() << "Entire line parsed!";
#endif
        nextLine();
    }

#ifdef TH2_DEBUG
        qInfo() << "Parsed words:";
        for(ParsedWord* parsedWord : m_parsedWords) {
            qInfo() << "Word:" << parsedWord->word << " - Type: " << parsedWord->type;
        }
#endif

        return true;
}

bool TH2Parser::setOptionValuePerType(TH2OrientationOption *newOrientationOption)
{
    return setAzimuthValue(&(newOrientationOption->azimuth()));
}

bool TH2Parser::setOptionValuePerType(TH2SingleSetOption *newSingleSetOption)
{
    // Checking for 'empty option' single set option.
    if (!justPeek() || justPeekWord.startsWith(OPTION_NAME_PREFIX))
    {
        if (!newSingleSetOption->setValueByText(EMPTY_STRING))
        {
            parseError(
                QString("Failure setting TH2SingleSetOption empty option value by name for option %1.")
                    .arg(newSingleSetOption->name()),
                QStringLiteral("TH2Parser::setOptionValuePerType(TH2SingleSetOption *newSingleSetOption)"));
            return false;
        }

        return true;
    }

    // Expecting a single set option.
    if (!nextWord())
    {
        return false;
    }

    if (!newSingleSetOption->setValueByText(currentWord))
    {
        parseError(
            QString("Failure setting TH2SingleSetOption option value by name for option %1.")
                .arg(newSingleSetOption->name()),
            QStringLiteral("TH2Parser::setOptionValuePerType(TH2SingleSetOption *newSingleSetOption)"));
        return false;
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2SketchOption *newSketchOption)
{
    if (!newSketchOption->setFilenameValue(currentWord))
    {
        return false;
    }

    if (!nextWord())
    {
        return false;
    }

    if (!newSketchOption->setXNumericValueByText(currentWord))
    {
        return false;
    }

    if (!nextWord())
    {
        return false;
    }

    if (!newSketchOption->setYNumericValueByText(currentWord))
    {
        return false;
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2StationNamesOption *newStationNamesOption)
{
    if (!newStationNamesOption->setPrefixText(currentWord))
    {
        return false;
    }

    if (!nextWord())
    {
        return false;
    }

    if (!newStationNamesOption->setSuffixText(currentWord))
    {
        return false;
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2PointScaleOption *newPointScaleOption)
{
    if (!newPointScaleOption->setCurrentValueByText(currentWord))
    {
        parseError(
            QStringLiteral("Failure setting TH2ScaleOption option value."),
            QStringLiteral("TH2Parser::setOptionValuePerType(TH2ScaleOption *newScaleOption)"));
        return false;
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2ProjectionOption *newProjectionOption)
{
    if (currentWordType == Parser::Bracketed)
    {
        QRegularExpressionMatch match = projectionElevationRegex.match(currentWord);

        if (!match.hasMatch())
        {
            return false;
        }

        if (!newProjectionOption->setProjectionTypeByText(ELEVATION_PROJECTION_TYPE))
        {
            return false;
        }

        if (!match.captured("index").isEmpty()
            && !newProjectionOption->setIndexText(match.captured("index")))
        {
            return false;
        }

        if (!newProjectionOption->setElevationDirectionNumericValueByText(match.captured("number")))
        {
            return false;
        }

        if (!match.captured("unit").isEmpty()
            && !newProjectionOption->setElevationDirectionUnitByText(match.captured("unit")))
        {
            return false;
        }

        if (!newProjectionOption->setHasElevationDirection(true))
        {
            return false;
        }
    }
    else
    {
        QRegularExpressionMatch match = projectionGeneralRegex.match(currentWord);

        if (!match.hasMatch())
        {
            return false;
        }

        if (!newProjectionOption->setProjectionTypeByText(match.captured("type")))
        {
            return false;
        }

        if (!match.captured("index").isEmpty())
        {
            if (match.captured("type") == NONE_PROJECTION_TYPE)
            {
                return false;
            }

            if (!newProjectionOption->setIndexText(match.captured("index")))
            {
                return false;
            }
        }

        if (!newProjectionOption->setHasElevationDirection(false))
        {
            return false;
        }
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2ScrapScaleOption *newScrapScaleOption)
{
    if (currentWordType != WordType::Bracketed)
    {
        if (!newScrapScaleOption->setDrawingUnitSizeInRealityByText(currentWord))
        {
            return false;
        }

        newScrapScaleOption->setForm(TH2ScrapScaleOption::Form::SizeOfDrawingUnitInReality);

        return true;
    }

    QRegularExpressionMatch match = scrapScaleCompleteRegex.match(currentWord);

    if (match.hasMatch())
    {
        if (!newScrapScaleOption->setX1ScrapByText(match.captured("x1scrap")))
        {
            return false;
        }
        if (!newScrapScaleOption->setY1ScrapByText(match.captured("y1scrap")))
        {
            return false;
        }
        if (!newScrapScaleOption->setX2ScrapByText(match.captured("x2scrap")))
        {
            return false;
        }
        if (!newScrapScaleOption->setY2ScrapByText(match.captured("y2scrap")))
        {
            return false;
        }
        if (!newScrapScaleOption->setX1RealityByText(match.captured("x1reality")))
        {
            return false;
        }
        if (!newScrapScaleOption->setY1RealityByText(match.captured("y1reality")))
        {
            return false;
        }
        if (!newScrapScaleOption->setX2RealityByText(match.captured("x2reality")))
        {
            return false;
        }
        if (!newScrapScaleOption->setY2RealityByText(match.captured("y2reality")))
        {
            return false;
        }
        if (!match.captured("unit").isEmpty())
        {
            if (!newScrapScaleOption->setLengthUnitByText(match.captured("unit")))
            {
                return false;
            }
        }

        newScrapScaleOption->setForm(TH2ScrapScaleOption::Form::Complete);

        return true;
    }

    match = scrapScaleAmountDrawingUnitsRegex.match(currentWord);

    if (match.hasMatch())
    {
        if (!newScrapScaleOption->setAmountOfDrawingUnitsByText(match.captured("amountOfDrawingUnits")))
        {
            return false;
        }
        if (!newScrapScaleOption->setDrawingUnitSizeInRealityByText(match.captured("drawingUnitSize")))
        {
            return false;
        }
        if (!match.captured("unit").isEmpty())
        {
            if (!newScrapScaleOption->setLengthUnitByText(match.captured("unit")))
            {
                return false;
            }
        }

        newScrapScaleOption->setForm(TH2ScrapScaleOption::Form::AmountOfDrawingUnitsPerSizeInReality);

        return true;
    }

    match = scrapScaleDrawingUnitSizeRegex.match(currentWord);

    if (match.hasMatch())
    {
        if (!newScrapScaleOption->setDrawingUnitSizeInRealityByText(match.captured("drawingUnitSize")))
        {
            return false;
        }
        if (!match.captured("unit").isEmpty())
        {
            if (!newScrapScaleOption->setLengthUnitByText(match.captured("unit")))
            {
                return false;
            }
        }

        newScrapScaleOption->setForm(TH2ScrapScaleOption::Form::SizeOfDrawingUnitInReality);

        return true;
    }

    return false;
}

bool TH2Parser::setOptionValuePerType(TH2SubtypeOption *newSubtypeOption)
{
    if (!newSubtypeOption->setSubtypeByText(currentWord))
    {
        parseError(
            QStringLiteral("Failure setting TH2SubtypeOption option value by name."),
            QStringLiteral("TH2Parser::setOptionValuePerType(TH2SubtypeOption *newSubtypeOption)"));
        return false;
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2ValueOption *newValueOption)
{
    TH2Point *pointParent = dynamic_cast<TH2Point *>(newValueOption->parent());
    assert(pointParent != nullptr);
    const QString symbolType = pointParent->type();
    if (symbolType == ALTITUDE_POINT_TYPE)
    {
        QRegularExpressionMatch match = altitudeValueRegex.match(currentWord);

        if (!match.hasMatch())
        {
            return false;
        }

        if (!newValueOption->setIsFix(!match.captured("fix").isEmpty()))
        {
            return false;
        }

        if (isNaN(match.captured("number")))
        {
            if (!newValueOption->setNumericValue(0))
            {
                return false;
            }
        }
        else if (!newValueOption->setNumericValueByText(match.captured("number")))
        {
            return false;
        }

        if (!match.captured("unit").isEmpty())
        {
            if (!newValueOption->setUnitByText(match.captured("unit")))
            {
                return false;
            }
        }
    }
    else if (symbolType == DATE_POINT_TYPE)
    {
        return setDatetimeValue(&(newValueOption->date()));
    }
    else if (symbolType == DIMENSIONS_POINT_TYPE)
    {
        QRegularExpressionMatch match = dimensionsValueRegex.match(currentWord);

        if (!match.hasMatch())
        {
            return false;
        }

        if (!newValueOption->setAboveValueByText(match.captured("above")))
        {
            return false;
        }

        if (!newValueOption->setBelowValueByText(match.captured("below")))
        {
            return false;
        }

        if (!match.captured("unit").isEmpty())
        {
            if (!newValueOption->setUnitByText(match.captured("unit")))
            {
                return false;
            }
        }
    }
    else if (symbolType == EXTRA_POINT_TYPE)
    {
        QRegularExpressionMatch match = measurementWithOptionalUnitRegex.match(currentWord);

        if (!match.hasMatch())
        {
            return false;
        }

        if (!newValueOption->setNumericValueByText(match.captured("number")))
        {
            return false;
        }

        if (!match.captured("unit").isEmpty())
        {
            if (!newValueOption->setUnitByText(match.captured("unit")))
            {
                return false;
            }
        }
    }
    else if (symbolType == HEIGHT_POINT_TYPE)
    {
        QRegularExpressionMatch match = heightValueRegex.match(currentWord);

        if (!match.hasMatch())
        {
            return false;
        }

        if (match.captured("sign").isEmpty())
        {
            if (!newValueOption->setSign(TH2ValueOption::Sign::DepthHeight))
            {
                return false;
            }
        }
        else
        {
            if (match.captured("sign") == ABOVE_HEIGHT_SIGN)
            {
                if (!newValueOption->setSign(TH2ValueOption::Sign::Height))
                {
                    return false;
                }
            }
            else if (match.captured("sign") == BELOW_DEPTH_SIGN)
            {
                if (!newValueOption->setSign(TH2ValueOption::Sign::Depth))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        if (!newValueOption->setNumericValueByText(match.captured("number")))
        {
            return false;
        }

        if (!newValueOption->setIsPresumed(!match.captured("presumed").isEmpty()))
        {
            return false;
        }

        if (!match.captured("unit").isEmpty())
        {
            if (!newValueOption->setUnitByText(match.captured("unit")))
            {
                return false;
            }
        }
    }
    else if (symbolType == PASSAGE_HEIGHT_POINT_TYPE)
    {
        QRegularExpressionMatch match = passageHeightSingleValueRegex.match(currentWord);

        if (match.hasMatch())
        {
            if (match.captured("sign").isEmpty())
            {
                if (!newValueOption->setPassageHeight(TH2ValueOption::PassageHeight::Distance))
                {
                    return false;
                }
            }
            else
            {
                if (match.captured("sign") == ABOVE_HEIGHT_SIGN)
                {
                    if (!newValueOption->setPassageHeight(TH2ValueOption::PassageHeight::Height))
                    {
                        return false;
                    }
                }
                else if (match.captured("sign") == BELOW_DEPTH_SIGN)
                {
                    if (!newValueOption->setPassageHeight(TH2ValueOption::PassageHeight::Depth))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            if (!newValueOption->setNumericValueByText(match.captured("number")))
            {
                return false;
            }
        }
        else
        {
            match = passageHeightDoubleValueRegex.match(currentWord);
            if (!match.hasMatch())
            {
                return false;
            }

            if (!newValueOption->setPassageHeight(TH2ValueOption::PassageHeight::DepthHeight))
            {
                return false;
            }

            if (!newValueOption->setAboveValueByText(match.captured("above")))
            {
                return false;
            }

            if (!newValueOption->setBelowValueByText(match.captured("below")))
            {
                return false;
            }
        }

        if (!match.captured("unit").isEmpty())
        {
            if (!newValueOption->setUnitByText(match.captured("unit")))
            {
                return false;
            }
        }
    }

    return true;
}

bool TH2Parser::setAzimuthValue(TH2AzimuthValue *aAzimuthValue)
{
    QString aUnit = TH2_AZIMUTH_VALUE_DEF.defaultValueName;

    QRegularExpressionMatch match = measurementWithOptionalUnitRegex.match(currentWord);

    if (match.hasMatch())
    {
        if (!aAzimuthValue->setNumericValueByText(match.captured(NUMBER_REGEX_NAME)))
        {
            parseError(
                QStringLiteral("Failure setting TH2AzimuthValue number value by text."),
                QStringLiteral("TH2Parser::setAzimuthValue"));
            return false;
        }

        if (match.captured(UNIT_REGEX_NAME) != "")
        {
           aUnit =  match.captured(UNIT_REGEX_NAME);
        }
    }
    else
    {
        match = degreeSpecialNotationRegex.match(currentWord);

        if (!match.hasMatch())
        {
            parseError(
                QStringLiteral("Failure parsing TH2AzimuthValue."),
                QStringLiteral("TH2Parser::setAzimuthValue"));
            return false;
        }

        TH2DoubleValueNumberType aValue = match.captured(DEGREES_REGEX_NAME).toDouble();

        const TH2DoubleValueNumberType minutes =
            match.captured(MINUTES_REGEX_NAME).toDouble() / MINUTES_PER_DEGREE;

        if (aValue < 0)
        {
            aValue -= minutes;
        }
        else
        {
            aValue += minutes;
        }

        if (!match.captured(SECONDS_REGEX_NAME).isEmpty())
        {
            const TH2DoubleValueNumberType seconds =
                match.captured(SECONDS_REGEX_NAME).toDouble() / SECONDS_PER_DEGREE;

            if (aValue < 0)
            {
               aValue -= seconds;
            }
            else
            {
                aValue += seconds;
            }
        }

        if (!aAzimuthValue->setNumericValue(aValue))
        {
            parseError(
                QStringLiteral("Failure setting TH2AzimuthValue number value by text."),
                QStringLiteral("TH2Parser::setAzimuthValue"));
            return false;
        }
    }

    if (!aAzimuthValue->setUnitByText(aUnit))
    {
        parseError(
            QStringLiteral("Failure setting TH2AzimuthValue unit value by text."),
            QStringLiteral("TH2Parser::setAzimuthValue"));
        return false;
    }

    return true;
}

bool TH2Parser::setDatetimeValue(TH2DateValue *aDateValue)
{
    if (currentWord == TH2DateValue::UNSPECIFIED_TEXT)
    {
        if (!aDateValue->setDateTimeFromText(TH2DateValue::UNSPECIFIED_TEXT))
        {
            return false;
        }

        if (!aDateValue->setDateTimeFromText(currentWord))
        {
            return false;
        }

        return true;
    }

    QRegularExpressionMatch match =
        TH2DateValue::rangeDateTimeRegex.match(currentWord);

    if (match.hasMatch() && !match.captured("end_year").isEmpty())
    {
        return aDateValue->setDateTimeFromText(currentWord);
    }

    match = TH2DateValue::singleDateTimeRegex.match(currentWord);

    if (!match.hasMatch())
    {
        return false;
    }

    QString newDateTimeText = currentWord;

    if (justPeek()
            && (justPeekWord == TH2DateValue::DATETIME_RANGE_SEPARATOR))
    {
        // Consuming the datetime range separator
        nextWord();

        if (!nextWord())
        {
            return false;
        }

        match = TH2DateValue::singleDateTimeRegex.match(currentWord);

        if (!match.hasMatch())
        {
            return false;
        }

        newDateTimeText.append(
            QString(" %1 %2").arg(
                TH2DateValue::DATETIME_RANGE_SEPARATOR,
                currentWord));
    }

    if (!aDateValue->setDateTimeFromText(newDateTimeText))
    {
        return false;
    }

    return true;
}

bool TH2Parser::setLengthValue(TH2LengthValue *aLengthValue)
{
    QRegularExpressionMatch match = measurementWithOptionalUnitRegex.match(currentWord);

    if (!match.hasMatch())
    {
        parseError(
            QStringLiteral("Failure parsing TH2LengthValue."),
            QStringLiteral("TH2Parser::TH2LengthValue"));
        return false;
    }

    if (!aLengthValue->setNumericValueByText(match.captured(NUMBER_REGEX_NAME)))
    {
        parseError(
            QStringLiteral("Failure setting TH2LengthValue number value by text."),
            QStringLiteral("TH2Parser::setLengthValue"));
        return false;
    }

    QString aUnit = (match.captured(UNIT_REGEX_NAME) == "")
        ? TH2_LENGTH_VALUE_DEF.defaultValueName
        : match.captured(UNIT_REGEX_NAME);

    if (!aLengthValue->setUnitByText(aUnit))
    {
        parseError(
            QStringLiteral("Failure setting TH2AzimuthValue unit value by text."),
            QStringLiteral("TH2Parser::setAzimuthValue"));
        return false;
    }

    return true;
}

bool TH2Parser::setPersonValue(TH2PersonValue *aPersonValue)
{
    if (!nextWord())
    {
        return false;
    }

    if (currentWordType == Parser::Quoted)
    {
        if (!aPersonValue->setFullName(currentWord))
        {
            return false;
        }
    }
    else
    {
        QString name = currentWord;

        if (!nextWord())
        {
            return false;
        }

        name.append(QString(" %1").arg(currentWord));

        if (!aPersonValue->setFullName(name))
        {
            return false;
        }
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2ContextOption *newContextOption)
{
    if (!newContextOption->setSymbolElementTypeByText(currentWord))
    {
        parseError(
            QString("Failure setting TH2ContextOption option symbol element type value by name for '%1'.")
                .arg(currentWord),
            QStringLiteral("TH2Parser::setOptionValuePerType(TH2ContextOption *newContextOption)"));
        return false;
    }

    if (!nextWord())
    {
        parseError(
            QStringLiteral("Failure reading <symbol-type> part of context option value."),
            QStringLiteral("TH2Parser::setOptionValuePerType(TH2ContextOption *newContextOption)"));
        return false;
    }

    if (!newContextOption->setSymbolTypeText(currentWord))
    {
        parseError(
            QString("Failure setting TH2ContextOption option symbol type value for '%1'.")
                .arg(currentWord),
            QStringLiteral("TH2Parser::setOptionValuePerType(TH2ContextOption *newContextOption)"));
        return false;
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2CopyrightOption *newCopyrightOption)
{
    if (!setDatetimeValue(&(newCopyrightOption->date())))
    {
        return false;
    }

    if (!nextWord())
    {
        return false;
    }

    if (!newCopyrightOption->setNoticeText(currentWord))
    {
        return false;
    }

    return true;
}

bool TH2Parser::setOptionValuePerType(TH2DoubleOption *newDoubleOption)
{
    return (newDoubleOption->setNumericValueByText(currentWord));
}

bool TH2Parser::setOptionValuePerType(TH2LengthOption *newLengthOption)
{
    return setLengthValue(&(newLengthOption->dist()));
}

bool TH2Parser::setOptionValuePerType(TH2MarkOption *newMarkOption)
{
    return newMarkOption->setKeywordText(currentWord);
}

bool TH2Parser::parseLinePointOption()
{
    return parseOption(LINEPOINT_ELEMENT_TYPE, EMPTY_STRING, newLinePoint);
}

TH2Parser::optionsInLineParseFuncReturnType TH2Parser::parsePointOption()
{
    if (!TH2Point::isSupportedOptionType(
        POINT_ELEMENT_TYPE,
        newPoint->type(),
        optionID))
    {
        return false;
    }

    return parseOption(POINT_ELEMENT_TYPE, newPoint->type(), newPoint);
}

bool TH2Parser::parsingSucceeded() const
{
    return m_parsingSucceeded;
}

TH2Element *TH2Parser::currentParent() const
{
    return m_ancestor.last();
}

void TH2Parser::addNewParent(TH2Element *newAncestor)
{
    m_ancestor.append(newAncestor);
}

void TH2Parser::removeCurrentParent()
{
    m_ancestor.removeLast();
}

void TH2Parser::appendElement(th2element_up uniqueElement)
{
    // Only files and scraps have children elements.
    if (m_ancestor.count() == 1)
    {
        // TH2File parent.
        m_th2File->appendElement(std::move(uniqueElement));
    }
    else if (m_ancestor.count() > 1)
    {
        // TH2Parent.
        TH2Parent *thisParent = dynamic_cast<TH2Parent *>(currentParent());
        if (thisParent == nullptr)
        {
            parseError(
                QStringLiteral("Failure casting currentParent to TH2Parent."),
                QStringLiteral("TH2Parser::appendElement"));
            return;
        }

        thisParent->appendElement(std::move(uniqueElement));
        return;
    }
    else
    {
        parseError(
            QStringLiteral("Unexpected amount of parents registered in m_ancestor."),
            QStringLiteral("TH2Parser::appendElement"));
        return;
    }
}

void TH2Parser::initializeTH2Parser()
{
    linesXareas.clear();

    m_ancestor.clear();
    m_ancestor.append(nullptr);

    nextLinePointOptions.clear();
}

bool TH2Parser::internalParse()
{
    initializeTH2Parser();

    bool result = parseLines(std::bind(&TH2Parser::parseWordGeneral, this));

    joinLinesToAreas();

    m_th2File->setLineEndingType(lineEndingType);

    return result;
}

TH2Parser::TH2Parser(const QString &aFilename)
    : Parser()
    , m_parsingSucceeded(false)
{
    initializeTH2Parser();

    setFilename(aFilename);
}

TH2Parser::TH2Parser()
{
    initializeTH2Parser();
}
