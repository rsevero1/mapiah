/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#ifndef XVIPARSER_H
#define XVIPARSER_H

#include "data/xvi/xvi.h"

#include <QFile>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>

/**
 *  We need to tell fusion about our structs to make them first-class fusion
 *  citizens. This has to be in global scope.
 */
BOOST_FUSION_ADAPT_STRUCT(
    XVI::InitialGrid,
    size,
    unit
)

BOOST_FUSION_ADAPT_STRUCT(
    XVI::Point,
    x,
    y
)

BOOST_FUSION_ADAPT_STRUCT(
    XVI::Station,
    coordinates,
    name
)

BOOST_FUSION_ADAPT_STRUCT(
    XVI::Images
)

BOOST_FUSION_ADAPT_STRUCT(
    XVI::Shot,
    from,
    to
)

BOOST_FUSION_ADAPT_STRUCT(
    XVI::SketchLine,
    color,
    points
)

BOOST_FUSION_ADAPT_STRUCT(
    XVI::FinalGrid,
    bottom,
    x_dist_1,
    y_dist_1,
    x_dist_2,
    y_dist_2,
    qt_x,
    qt_y
)

BOOST_FUSION_ADAPT_STRUCT(
    XVI::File,
    initialGrid,
    stations,
    images,
    shots,
    sketchlines,
    finalGrid
)

namespace fusion = boost::fusion;
namespace phoenix = boost::phoenix;
namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;

class XVIParser
{
    template <typename Iterator>
    struct XVIGrammar : qi::grammar<Iterator, XVI::File(), ascii::space_type>
    {
        XVIGrammar() : XVIGrammar::base_type(xvifile, "xvifile")
        {
            using qi::lit;
            using qi::lexeme;
            using qi::double_;
            using qi::int_;
            using qi::no_case;
            using ascii::char_;
            using ascii::alnum;

            using namespace qi::labels;

            unit = char_("mf");

            initialGrid =
                no_case[lit("set")]
                >> no_case[lit("xvigrids")]
                >> '{'
                >> double_
                >> unit
                >> '}'
            ;

            point =
                double_
                >> double_
            ;

            station_name =
                lexeme[+(alnum | char_(".,;-()[]+"))]
            ;

            station =
                '{'
                >> point
                >> station_name
                >> '}'
            ;

            stations =
                no_case[lit("set")]
                >> no_case[lit("xvistations")]
                >> '{'
                >> +(station)
                >> '}'
            ;

            images =
                no_case[lit("set")]
                >> no_case[lit("xviimages")]
                >> '{'
                >> '}'
            ;

            shot =
                '{'
                >> point
                >> point
                >> '}'
            ;

            shots =
                no_case[lit("set")]
                >> no_case[lit("xvishots")]
                >> '{'
                >> +(shot)
                >> '}'
            ;

            color_name =
                lexeme[+(alnum | char_('-'))]
            ;

            sketchline =
                '{'
                >> color_name
                >> +(point)
                >> '}'
            ;

            sketchlines =
                no_case[lit("set")]
                >> no_case[lit("xvisketchlines")]
                >> '{'
                >> +(sketchline)
                >> '}'
            ;

            finalGrid =
                no_case[lit("set")]
                >> no_case[lit("xvigrid")]
                >> '{'
                >> point
                >> double_
                >> double_
                >> double_
                >> double_
                >> double_
                >> double_
                >> '}'
            ;

            xvifile =
                (
                    (initialGrid)
                    ^ (stations)
                    ^ (images)
                    ^ (shots)
                    ^ (sketchlines)
                    ^ (finalGrid)
                )
            ;

            xvifile.name("xvifile");
            unit.name("unit");
            initialGrid.name("initialGrid");
            station_name.name("station_name");
            point.name("point");
            station.name("station");
            stations.name("stations");
            images.name("images");
            shot.name("shot");
            shots.name("shots");
            color_name.name("color_name");
            sketchline.name("sketchline");
            sketchlines.name("sketchlines");
            finalGrid.name("finalGrid");
        }

        qi::rule<Iterator, XVI::File(), ascii::space_type> xvifile;
        qi::rule<Iterator, char(), ascii::space_type> unit;
        qi::rule<Iterator, XVI::InitialGrid(), ascii::space_type> initialGrid;
        qi::rule<Iterator, XVI::Point(), ascii::space_type> point;
        qi::rule<Iterator, std::string(), ascii::space_type> station_name;
        qi::rule<Iterator, XVI::Station(), ascii::space_type> station;
        qi::rule<Iterator, XVI::station_vec_t(), ascii::space_type> stations;
        qi::rule<Iterator, XVI::Images(), ascii::space_type> images;
        qi::rule<Iterator, XVI::Shot(), ascii::space_type> shot;
        qi::rule<Iterator, XVI::shot_vec_t(), ascii::space_type> shots;
        qi::rule<Iterator, std::string(), ascii::space_type> color_name;
        qi::rule<Iterator, XVI::SketchLine(), ascii::space_type> sketchline;
        qi::rule<Iterator, XVI::sketchline_vec_t(), ascii::space_type> sketchlines;
        qi::rule<Iterator, XVI::FinalGrid(), ascii::space_type> finalGrid;
    };

    QString m_filename;
    QFile file;

    XVI::File *m_XVIFile;

public:
    XVIParser(const QString aFilename);

    const QString &filename() const;
    void setFilename(const QString &newFilename);

    bool parse();
    XVI::File *parsedFile() const;
};

#endif // XVIPARSER_H
