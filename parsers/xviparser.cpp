/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "xviparser.h"

#ifdef TH2_DEBUG
    #include <QDebug>
#endif

const QString &XVIParser::filename() const
{
    return m_filename;
}

void XVIParser::setFilename(const QString &newFilename)
{
    m_filename = newFilename;
}

bool XVIParser::parse()
{
    file.setFileName(m_filename);
    if (!file.open(QIODevice::ReadOnly))
    {
#ifdef TH2_DEBUG
        qInfo() << file.errorString();
#endif
        return false;
    }

    std::string fileContents = file.readAll().toStdString();

    XVIGrammar<std::string::const_iterator> xvi; // Our grammar

    using boost::spirit::ascii::space;
    std::string::const_iterator iter = fileContents.begin();
    std::string::const_iterator end = fileContents.end();
    m_XVIFile = new XVI::File;
    bool r = phrase_parse(iter, end, xvi, space, *m_XVIFile);

    if (r && iter == end)
    {
#ifdef TH2_DEBUG
        qInfo() << "-------------------------\n";
        qInfo() << "Parsing succeeded\n";
        qInfo() << "-------------------------\n";
#endif
        return true;
    }
    else
    {
#ifdef TH2_DEBUG
        qInfo() << "-------------------------\n";
        qInfo() << "Parsing failed\n";
        qInfo() << "-------------------------\n";
#endif
        delete m_XVIFile;
        return false;
    }
}

XVI::File *XVIParser::parsedFile() const
{
    return m_XVIFile;
}

XVIParser::XVIParser(const QString aFilename) : m_filename(aFilename)
{

}
