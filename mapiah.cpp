﻿/**
    This file is part of Mapiah.

    Mapiah is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mapiah is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mapiah.  If not, see <https://www.gnu.org/licenses/>.
  */

/**
 *  Copyright (c) 2021 Rodrigo Severo (rsev@pm.me)
 */

#include "mapiah.h"
#include "./ui_mapiah.h"

#include "functional/th2writer.h"

#include "parsers/th2parser.h"

#include <QButtonGroup>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QImageReader>
#include <QRadioButton>

#include <math.h>

#ifdef TH2_DEBUG
#include <QDebug>
#endif

Mapiah::Mapiah(QWidget *parent)
    : m_th2file(nullptr)
    , m_saveAsPath("")
    , m_openPath("")
    , ui(new Ui::Mapiah)
    , QMainWindow(parent)
{
    ui->setupUi(this);

    // Setting view layout
    m_scene = new TH2Scene(this);
    m_view = new TH2View(this);

    m_view->setScene(m_scene);
    m_view->setRenderHint(QPainter::Antialiasing);
    m_view->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_view->setMinimumSize(0, 0);

    ui->horizontalLayout_View->addWidget(m_view);

    m_scaleLabel = new QLabel(this);
    ui->statusbar->addWidget(m_scaleLabel);
    connect(m_view, &TH2View::zoomChanged, this, &Mapiah::on_zoomUpdated);

    // Setting buttons layout
    zoomToFit = new TH2ActionButton(this);
    zoomToFit->setAction(ui->actionZoom_to_fit);
    zoomToFit->setEnabled(false);

    zoomTo100 = new TH2ActionButton(this);
    zoomTo100->setAction(ui->action100_zoom);
    zoomTo100->setEnabled(false);

    openTH2File = new TH2ActionButton(this);
    openTH2File->setAction(ui->actionOpenTH2File);

    saveAsTH2File = new TH2ActionButton(this);
    saveAsTH2File->setAction(ui->actionSave_TH2_File_As);
    saveAsTH2File->setEnabled(false);

    ui->horizontalLayout_Buttons->addWidget(zoomToFit);
    ui->horizontalLayout_Buttons->addWidget(zoomTo100);
    ui->horizontalLayout_Buttons->addStretch();
    ui->horizontalLayout_Buttons->addWidget(openTH2File);
    ui->horizontalLayout_Buttons->addWidget(saveAsTH2File);
}

Mapiah::~Mapiah()
{
    delete ui;
}

QString &Mapiah::saveAsPath()
{
    if (m_saveAsPath == "")
    {
        if (m_th2file == nullptr)
        {
            m_saveAsPath = "./";
        }
        else {
            m_saveAsPath = pathOnly(m_th2file->filename());
        }
    }
    return m_saveAsPath;
}

void Mapiah::setSaveAsPath(const QString &newSaveAsPath)
{
    m_saveAsPath = pathOnly(newSaveAsPath);
}

QString Mapiah::openPath()
{
    if (m_openPath.isEmpty())
    {
        return QString("./");
    }
    {
        return m_openPath;
    }
}

void Mapiah::setOpenPath(const QString &newOpenPath)
{
    m_openPath = pathOnly(newOpenPath);
}

QString Mapiah::pathOnly(const QString &aPath)
{
    QFileInfo aPathFileInfo = QFileInfo(aPath);
    QString baseName = aPathFileInfo.baseName();
    QDir path;

    if (baseName == "")
    {
        path = QDir(aPath);
    }
    else
    {
        path = aPathFileInfo.dir();
    }
    return path.canonicalPath();
}

TH2Scene *Mapiah::getScene()
{
    return m_scene;
}

TH2View *Mapiah::getView()
{
    return m_view;
}

void Mapiah::on_zoomUpdated(qreal newScale)
{
    qint16 scalePercentage = round(newScale * 100);
    QString scalePercentageMessage = "%1%";

    m_scaleLabel->setText(scalePercentageMessage.arg(scalePercentage));
}

void Mapiah::on_actionZoom_to_fit_triggered()
{
    m_view->zoomToFit();
}

void Mapiah::on_action100_zoom_triggered()
{
    m_view->setZoom(1);
}

void Mapiah::on_actionOpenTH2File_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
        this,
        tr("Open TH2 file"),
        openPath(),
        tr("TH2 files (*.th2)"));

    if (fileName.isEmpty())
    {
        qInfo() << "No file selected for openning.";
        return;
    }

    TH2Parser th2Parser(fileName);

    m_th2file = th2Parser.parseTH2();
    qInfo() << "TH2 file read result: " << (m_th2file.get() != nullptr);
    if (m_th2file)
    {
        m_th2file->draw(m_scene);
        m_view->zoomToFit();
        zoomToFit->setEnabled(true);
        zoomTo100->setEnabled(true);
        saveAsTH2File->setEnabled(true);
        setSaveAsPath(fileName);
        setOpenPath(fileName);
    }
}

void Mapiah::on_actionInsert_Image_triggered()
{
    QString imageFormatsString = "";
    QByteArray imageFormat;
    QList<QByteArray> imageFormatsList = QImageReader::supportedImageFormats();
    //        qInfo() << "Image formats: " << imageFormatsList;

    foreach(imageFormat, imageFormatsList)
    {
        imageFormatsString.append(" *." + imageFormat);
    }
    imageFormatsString = "Image Files (" + imageFormatsString.trimmed() + ")";

    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open TH2 file"), "./",
                                                    tr(imageFormatsString.toLocal8Bit()));
    qInfo() << "Image to be inserted to be openned: " << fileName;
}

void Mapiah::on_actionSave_TH2_File_As_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    saveAsPath(),
                                                    tr("TH2 files (*.th2)"));
    if (fileName == "")
    {
        qInfo() << "No file selected for saving.";
        return;
    }

    QFileInfo aFile(fileName);

    ui->statusbar->showMessage(QString("Saving '%1'…").arg(aFile.fileName()));

    TH2Writer aTH2Writer(m_th2file.get(), fileName);

    aTH2Writer.write();
    ui->statusbar->showMessage(QString("'%1' saved at '%2'").arg(aFile.fileName(), aFile.dir().canonicalPath()), 5000);
    setSaveAsPath(fileName);
}
